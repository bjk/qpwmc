/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDCLIENTDIALOG_H
#define PWMDCLIENTDIALOG_H

#include <QDialog>
#include <QTimer>
#include <QColor>
#include "pwmd.h"
#include "pwmdClientInfo.h"
#include "ui_pwmdClientDialog.h"

class PwmdMainWindow;
class PwmdClientDialog : public QDialog
{
  Q_OBJECT
 public:
  PwmdClientDialog (Pwmd *, const QStringList &, const QString &,
                    PwmdMainWindow *);
  ~PwmdClientDialog ();
  void refreshClientList();

  typedef enum {
      Name, Time, File, Lock, State, Host, Uid, MaxFields
  } Field;

 private slots:
  void slotKillClient ();
  void slotRefreshClientList ();
  void slotClientItemChanged (QTreeWidgetItem *, QTreeWidgetItem *);
  void slotCommandResult (PwmdCommandQueueItem *item,
                          QString result, gpg_error_t rc, bool queued);
  void slotStatusMessage (QString line, void *data);
  void slotConnectionStateChanged (Pwmd::ConnectionState s);
  void slotRemoveStaleClients ();
  void slotBusy (int, bool);
  void slotMainClientLockStateChanged (bool);

 private:
  QString clientState (unsigned);
  void clearClientList (bool stale = false);
  QTreeWidgetItem *buildClientStateItem (const QString &,
                                         QTreeWidgetItem * = 0);
  void refreshClientListFinalize (const QString &);
  void disable (bool = true);
  QList <QTreeWidgetItem *> staleList;
  void setClientState (unsigned);
  void updateStateForClient (const QString &);
  void setLocked (bool);

  Ui::PwmdClientDialog ui;
  QStringList invokingUser;
  QString connectedUser;
  Pwmd *pwm;
  QTimer *staleTimer;
  QColor staleColor;
};

Q_DECLARE_METATYPE (ClientInfo *);

#endif
