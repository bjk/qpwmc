This is a full-featured pwmd (Password Manager Daemon) client using the Qt
toolkit to edit a pwmd data file.  You can use the Pwmd class easily in your
own application, too. Read pwmd.h and pwmdRemoteHost.h headers for details.
You can also also use qpwmc as a socket, data file and element selector by
spawning a qpwmc instance then parsing the result of the -d command line
switch.


Requirements:
-------------
libpwmd 8.3.0 or later (android: 8.4.0), pwmd 3.3.0 or later and Qt 6.x are
required.  Be sure that "disable_list_and_dump" is false in your pwmd
configuration file (the default).

Rather than sub-classing all widgets that contain sensitive data to wipe their
memory contents, a library that wipes all allocated memory for a program can
be used instead. This is done with libwipemem available at
https://gitlab.com/bjk/libwipemem and should be preloaded before the running
qpwmc by doing:

$ LD_PRELOAD=/path/to/libwipemem.so qpwmc [args]

Note that not all platforms support using LD_PRELOAD.


Building:
---------
Get the latest code by doing:

git clone https://gitlab.com/bjk/qpwmc.git

After cloning or after extracting an archive, do:

  qmake6 WITH_CLANG=1 && make -j8

or

  qmake6 && make -j8

The WITH_CLANG=1 will use the clang compiler but if clang is not installed on
your machine then you can omit that part.

After the build is complete, do:

  make install

You can also create an installable debian package by instead doing:

  qmake && make deb

Then use 'dpkg -i <package_name>' to install the package that was created in
the parent directory.


Usage:
------
Be sure pwmd is running then run 'qpwmc'. This will have qpwmc sit in the
system tray. Double-click the icon to open the editor or right-click to edit
and select shortcuts. See -h for command line options.

There really isn't a standard for applications to follow when it comes to
element paths and element attributes but there are two element attributes that
qpwmc treats specially: the "password" and "hidden" attributes. The "password"
attribute tells qpwmc that an element content is a password and will be
presented specially by showing password input field along with a password
generator. Note that the content of an element with this attribute is only a
single line and not a full text area. For multi-line sensitive data, you can
enable the "Hidden" checkbox from the Content menu. This will also add the
"hidden" attribute for the element.


Issue tracker:
--------------
Please send any bug reports, feature requests or patches to the issue tracker
at Gitlab: https://gitlab.com/bjk/qpwmc/issues.


Ben Kibbey <bjk AT luxsci DOT net>
https://gitlab.com/bjk/qpwmc/wikis
https://gitlab.com/bjk/libwipemem
