/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QSettings>
#include <QDir>
#include <QStringList>
#include <QCommonStyle>
#include <QScroller>
#include <libpwmd.h>
#include <unistd.h>
#include "pwmdSocketWidget.h"
#include "pwmdRemoteHost.h"
#include "pwmdFileDialog.h"
#ifdef Q_OS_ANDROID
#include "pwmdAndroidJNI.h"

extern PwmdAndroidJNI *androidJni;
#endif

PwmdSocketWidget::PwmdSocketWidget (QWidget *p)
  : QFrame (p)
{
  ui.setupUi (this);
  ipVersion = 0;
  QSettings cfg ("qpwmc");

  QStringList list = cfg.value ("localSockets").toStringList ();
  for (int i = 0; i < list.size(); i++)
    ui.cb_socket->addItem (list.at (i));

  int size = cfg.beginReadArray ("remoteHosts");
  unsigned features = pwmd_features ();

  for (int i = 0; i < size; ++i)
    {
      cfg.setArrayIndex (i);
      if (cfg.value ("name").toString ().isEmpty ())
        continue;

      QListWidgetItem *item = new QListWidgetItem (ui.lb_remoteHosts);
      PwmdRemoteHost data = PwmdRemoteHost (cfg.value ("name").toString ());
      Qt::ItemFlags flags = item->flags ();

      data.setType (cfg.value ("type").toInt ());
      data.setHostname (cfg.value ("hostname").toString ());
      data.setPort (cfg.value ("port").toInt ());
      data.setIpProtocol (cfg.value ("ipProtocol").toInt ());
      data.setSocketArgs (cfg.value ("socketArgs").toStringList ());
      data.setTlsVerify (cfg.value ("tlsVerify").toBool ());
      data.setTlsPriority (cfg.value ("tlsPriority").toString ());
      data.setConnectTimeout (cfg.value ("connectTimeout").toInt ());
      data.setSocketTimeout (cfg.value ("socketTimeout").toInt ());
      data.setSshUsername (cfg.value ("sshUsername").toString ());
      data.setSshAgent (cfg.value ("sshAgent").toBool ());
#ifdef Q_OS_ANDROID
      data.setClientCertificateAlias (cfg.value ("clientCertificateAlias").toString ());
#endif
      item->setData (Qt::UserRole, QVariant::fromValue (data));
      item->setText (data.name ());

      if (data.type () == PWMD_SOCKET_SSH && !(features & PWMD_FEATURE_SSH))
	flags &= ~(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

#ifndef Q_OS_ANDROID
      else if (data.type () == PWMD_SOCKET_TLS
	       && !(features & PWMD_FEATURE_GNUTLS))
	flags &= ~(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
#endif

      item->setFlags (flags);
    }

  cfg.endArray ();

  connect (ui.cb_socket, SIGNAL (activated (int)), this,
           SLOT (slotLocalSocketChanged (int)));
  connect (ui.pb_clearLocalSocket, SIGNAL (clicked()), this,
           SLOT (slotClearLocalSocket ()));
  connect (ui.pb_selectSocket, SIGNAL (clicked()), this,
           SLOT (slotSelectSocket ()));
  connect (ui.rb_ipany, SIGNAL (toggled (bool)),
	   SLOT (slotIpVersionToggled (bool)));
  connect (ui.rb_ipv4, SIGNAL (toggled (bool)),
	   SLOT (slotIpVersionToggled (bool)));
  connect (ui.rb_ipv6, SIGNAL (toggled (bool)),
	   SLOT (slotIpVersionToggled (bool)));
  connect (ui.ck_remote, SIGNAL (toggled (bool)),
	   SLOT (slotRemoteToggled (bool)));
  connect (ui.ck_agent, SIGNAL (toggled (bool)),
	   SLOT (slotUseAgentToggled (bool)));

  connect (ui.cb_connectionType, SIGNAL (activated (int)), this,
	   SLOT (slotConnectionTypeChanged (int)));
  ui.cb_connectionType->setCurrentIndex (cfg.value ("connectionType", 0).
					 toInt ());
  connect (ui.pb_clientCert, SIGNAL (clicked ()), this,
	   SLOT (slotSelectTlsClientCert ()));
  connect (ui.pb_clientKey, SIGNAL (clicked ()), this,
	   SLOT (slotSelectTlsClientKey ()));
  connect (ui.pb_caCert, SIGNAL (clicked ()), this,
	   SLOT (slotSelectTlsCaCert ()));
  connect (ui.pb_newHost, SIGNAL (clicked ()), this,
	   SLOT (slotNewRemoteHost ()));
  ui.pb_newHost->setIcon (QIcon (":icons/matt-icons_list-add.svg"));
  connect (ui.pb_removeHost, SIGNAL (clicked ()), this,
	   SLOT (slotRemoveRemoteHost ()));
  ui.pb_removeHost->setIcon (QIcon (":icons/matt-icons_list-remove.svg"));
  connect (ui.lb_remoteHosts,
	   SIGNAL (currentItemChanged (QListWidgetItem *, QListWidgetItem *)),
	   SLOT (slotRemoteHostItemChanged
		 (QListWidgetItem *, QListWidgetItem *)));
  connect (ui.le_remoteName, SIGNAL (textEdited (const QString &)),
	   SLOT (slotRemoteHostNameChanged (const QString &)));

  connect (ui.pb_identity, SIGNAL (clicked ()),
	   SLOT (slotChooseIdentityFile ()));
  connect (ui.pb_knownhosts, SIGNAL (clicked ()),
	   SLOT (slotChooseKnownhostsFile ()));

  if (!ui.ck_remote->isChecked ())
    ui.gr_hostDetails->setEnabled (false);

  if (features & PWMD_FEATURE_SSH)
    ui.cb_connectionType->addItem (tr ("SSH"));

#ifndef Q_OS_ANDROID
  if (features & PWMD_FEATURE_GNUTLS)
#endif
    ui.cb_connectionType->addItem (tr ("TLS"));

#if !defined (Q_OS_ANDROID) && !defined (__MINGW32__)
  if (!(features & PWMD_FEATURE_GNUTLS) && !(features & PWMD_FEATURE_SSH))
    ui.ck_remote->setEnabled (false);
#else
  ui.f_uds->setHidden (true);
  ui.ck_remote->setEnabled (true);
  ui.ck_remote->setChecked (true);
  toggleRemote (true);
#endif

  QScroller::grabGesture (ui.scrollArea, QScroller::TouchGesture);
  QCommonStyle style;
  ui.cb_socket->lineEdit()->setPlaceholderText (tr ("Default"));
  ui.pb_clearLocalSocket->setIcon (QIcon (":icons/nicubunu-Broom.svg"));
  ui.pb_selectSocket->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  ui.pb_knownhosts->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  ui.pb_identity->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  ui.pb_caCert->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  ui.pb_clientCert->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  ui.pb_clientKey->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
#ifdef Q_OS_ANDROID
  ui.pb_certificateAlias->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  connect (ui.pb_certificateAlias, SIGNAL (clicked ()), this,
           SLOT (slotSelectCertificateAlias ()));
#endif

  ui.fr_certificateAlias->setHidden (true);
  ui.fr_ssh->setHidden (true);
  ui.fr_tls->setHidden (true);
}

PwmdSocketWidget::~PwmdSocketWidget ()
{
  saveHosts ();
}

#ifdef Q_OS_ANDROID
void
PwmdSocketWidget::slotSelectCertificateAlias ()
{
  androidJni->selectCertificateAlias (ui.le_certificateAlias);
}
#endif

void
PwmdSocketWidget::slotSelectSocket ()
{
  PwmdFileDialog d (this, tr ("Local socket"));

  d.setFilter (d.filter () | QDir::System);
  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;

  ui.cb_socket->lineEdit ()->setText (d.selectedFiles ().at (0));
  ui.cb_socket->addItem (d.selectedFiles ().at (0));
}

void
PwmdSocketWidget::slotClearLocalSocket ()
{
  ui.cb_socket->lineEdit ()->setText ("");
}

void
PwmdSocketWidget::slotLocalSocketChanged (int)
{
}

void
PwmdSocketWidget::saveHosts ()
{
  QSettings cfg ("qpwmc");

  QStringList list;
  for (int i = 0; i < ui.cb_socket->count (); i++)
    {
      QString s = ui.cb_socket->itemText (i);

      for (int l = 0; l < list.size (); l++)
        if (list.at (l) == s)
          continue;

      list.append (s);
    }

  cfg.setValue ("localSockets", list);

  cfg.beginWriteArray ("remoteHosts");
  QList < QListWidgetItem * >items = ui.lb_remoteHosts->findItems ("*", Qt::MatchWildcard);

  for (int i = 0; i < items.size (); i++)
    {
      QListWidgetItem *item = items.at (i);
      PwmdRemoteHost data = item->data (Qt::UserRole).value < PwmdRemoteHost > ();

      cfg.setArrayIndex (i);
      cfg.setValue ("name", data.name ());
      cfg.setValue ("type", data.type ());
      cfg.setValue ("hostname", data.hostname ());
      cfg.setValue ("port", data.port ());
      cfg.setValue ("ipProtocol", data.ipProtocol ());
      cfg.setValue ("socketArgs", data.socketArgs ());
      cfg.setValue ("tlsVerify", data.tlsVerify ());
      cfg.setValue ("tlsPriority", data.tlsPriority ());
      cfg.setValue ("connectTimeout", data.connectTimeout ());
      cfg.setValue ("socketTimeout", data.socketTimeout ());
      cfg.setValue ("sshUsername", data.sshUsername ());
      cfg.setValue ("sshAgent", data.sshAgent ());
#ifdef Q_OS_ANDROID
      cfg.setValue ("clientCertificateAlias", data.clientCertificateAlias ());
#endif
    }

  cfg.endArray ();
}

bool
PwmdSocketWidget::currentHostData (PwmdRemoteHost &hostData)
{
  if (ui.ck_remote->isChecked ())
    return PwmdRemoteHost::fillRemoteHost (ui.lb_remoteHosts->currentItem ()->text (), hostData);

  return false;
}

void
PwmdSocketWidget::slotConnectionTypeChanged (int idx)
{
  unsigned features = pwmd_features ();
#ifndef Q_OS_ANDROID
  if (!(features & PWMD_FEATURE_GNUTLS) && !(features & PWMD_FEATURE_SSH))
    return;
#endif

  QSettings cfg ("qpwmc");

  if (idx == 0 && (features & PWMD_FEATURE_SSH))
    {
#ifdef Q_OS_ANDROID
      ui.fr_certificateAlias->setHidden (true);
#else
      ui.fr_tls->setHidden (true);
#endif
      ui.fr_ssh->setHidden (false);
      ui.sp_port->setValue (cfg.value ("sshPort", 22).toInt ());
    }
  else
    {
#ifdef Q_OS_ANDROID
      ui.fr_certificateAlias->setHidden (false);
#else
      ui.fr_tls->setHidden (false);
#endif
      ui.fr_ssh->setHidden (true);
      ui.sp_port->setValue (cfg.value ("tlsPort", 6466).toInt ());
    }
}

void
PwmdSocketWidget::slotIpVersionToggled (bool b)
{
  (void) b;

  if (ui.rb_ipany->isChecked ())
    ipVersion = 0;
  else if (ui.rb_ipv4->isChecked ())
    ipVersion = 1;
  else if (ui.rb_ipv6->isChecked ())
    ipVersion = 2;
}

void
PwmdSocketWidget::slotChooseIdentityFile ()
{
  PwmdFileDialog d (this, tr ("SSH identity file"));

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_identity->setText (d.selectedFiles ().at (0));
}

void
PwmdSocketWidget::slotChooseKnownhostsFile ()
{
  PwmdFileDialog d (this, tr ("SSH knownhosts file"),
#ifdef Q_OS_ANDROID
                 "/sdcard"
#else
                 QDir::home ().path ()
#endif
  );

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_knownhosts->setText (d.selectedFiles ().at (0));
}

void
PwmdSocketWidget::toggleRemote (bool b)
{
  QListWidgetItem *item = ui.lb_remoteHosts->currentItem ();
  Qt::ItemFlags flags;

  if (item)
    flags = item->flags ();

  ui.cb_socket->setEnabled (!b);
  ui.pb_clearLocalSocket->setEnabled (!b);
  ui.pb_selectSocket->setEnabled (!b);
  ui.ck_agent->setEnabled (b && getenv ("SSH_AUTH_SOCK") != nullptr);
  slotConnectionTypeChanged (ui.cb_connectionType->currentIndex ());
  ui.gr_hostDetails->setEnabled (b ? item && (flags & Qt::ItemIsEnabled)
				 ? b : false : b);

  if (b)
    slotUseAgentToggled (ui.ck_agent->isEnabled ()
			 && ui.ck_agent->isChecked ());
}

void
PwmdSocketWidget::slotRemoteToggled (bool b)
{
#if defined(Q_OS_ANDROID) || defined (__MINGW32__)
  if (!b)
    ui.ck_remote->setChecked (true);
  return;
#endif

  toggleRemote (b);
}

void
PwmdSocketWidget::slotUseAgentToggled (bool b)
{
  ui.le_identity->setEnabled (!b);
  ui.pb_identity->setEnabled (!b);
}


void
PwmdSocketWidget::slotSelectTlsClientCert ()
{
  PwmdFileDialog d (this, tr ("TLS client certificate file"),
#ifdef Q_OS_ANDROID
                 "/sdcard"
#else
                 QDir::home ().path ()
#endif
  );

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_clientCert->setText (d.selectedFiles ().at (0));
}

void
PwmdSocketWidget::slotSelectTlsClientKey ()
{
  PwmdFileDialog d (this, tr ("TLS client certificate key file"),
#ifdef Q_OS_ANDROID
                 "/sdcard"
#else
                 QDir::home ().path ()
#endif
  );

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_clientKey->setText (d.selectedFiles ().at (0));
}

void
PwmdSocketWidget::slotSelectTlsCaCert ()
{
  PwmdFileDialog d (this, tr ("TLS CA certificate file"),
#ifdef Q_OS_ANDROID
                 "/sdcard"
#else
                 QDir::home ().path ()
#endif
  );

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_caCert->setText (d.selectedFiles ().at (0));
}

void
PwmdSocketWidget::initNewRemoteHost ()
{
  ui.le_remoteName->setText ("");
  ui.cb_connectionType->setCurrentIndex (0);
  slotConnectionTypeChanged (0);
  ui.le_hostname->setText ("");
  ui.rb_ipany->setChecked (true);
  ui.sp_port->setValue (22);
  ui.le_identity->setText ("");
  ui.ck_agent->setChecked (ui.ck_agent->isEnabled ());
  ui.le_knownhosts->setText ("");
  ui.sp_connectTimeout->setValue (120);
  ui.sp_socketTimeout->setValue (300);
  ui.le_clientCert->setText ("");
  ui.le_clientKey->setText ("");
  ui.le_caCert->setText ("");
  ui.le_tlsServerFingerprint->setText ("");
  ui.ck_tlsVerify->setChecked (true);
  ui.le_tlsPriority->setText ("");
#ifndef __MINGW32__
  ui.le_username->setText (getlogin () ? getlogin () : "");
#endif
}

void
PwmdSocketWidget::updateRemoteHost ()
{
  updateRemoteHost (ui.lb_remoteHosts->currentItem ());
}

void
PwmdSocketWidget::updateRemoteHost (QListWidgetItem * item)
{
  if (!item)
    return;

  PwmdRemoteHost data = item->data (Qt::UserRole).value < PwmdRemoteHost > ();
  data.setName (ui.le_remoteName->text ());

  if (ui.cb_connectionType->currentText () == "SSH")
    data.setType (PWMD_SOCKET_SSH);
  else
#ifdef Q_OS_ANDROID
    data.setType (PWMD_SOCKET_USER);
#else
    data.setType (PWMD_SOCKET_TLS);
#endif

  data.setHostname (ui.le_hostname->text ());
  data.setPort (ui.sp_port->value ());

  if (ui.rb_ipany->isChecked ())
    data.setIpProtocol (0);
  else if (ui.rb_ipv4->isChecked ())
    data.setIpProtocol (1);
  else if (ui.rb_ipv6->isChecked ())
    data.setIpProtocol (2);

  QStringList list;
  if (data.type () == PWMD_SOCKET_SSH)
    {
      list.append (ui.le_identity->text ());
      list.append (ui.le_knownhosts->text ());
    }
  else
    {
      list.append (ui.le_clientCert->text ());
      list.append (ui.le_clientKey->text ());
      list.append (ui.le_caCert->text ());
      list.append (ui.le_tlsServerFingerprint->text ());
    }

  data.setSocketArgs (list);
  data.setTlsVerify (ui.ck_tlsVerify->isChecked ());
  data.setTlsPriority (ui.le_tlsPriority->text ());
  data.setConnectTimeout (ui.sp_connectTimeout->value ());
  data.setSocketTimeout (ui.sp_socketTimeout->value ());
  data.setSshUsername (ui.le_username->text ());
  data.setSshAgent (ui.ck_agent->isChecked ());
#ifdef Q_OS_ANDROID
  data.setClientCertificateAlias (ui.le_certificateAlias->text ());
#endif
  item->setData (Qt::UserRole, QVariant::fromValue (data));
}

void
PwmdSocketWidget::slotNewRemoteHost ()
{
  QListWidgetItem *item = ui.lb_remoteHosts->currentItem ();

  updateRemoteHost (item);
  ui.lb_remoteHosts->addItem (new QListWidgetItem (tr ("New host")));
  ui.lb_remoteHosts->setCurrentItem (ui.lb_remoteHosts->item (ui.lb_remoteHosts->count () - 1));
  ui.le_remoteName->setFocus (Qt::MouseFocusReason);
  initNewRemoteHost ();
  ui.gr_hostDetails->setEnabled (true);
}

void
PwmdSocketWidget::slotRemoveRemoteHost ()
{
  if (!ui.lb_remoteHosts->currentItem ())
    return;

  QListWidgetItem *item =
    ui.lb_remoteHosts->takeItem (ui.lb_remoteHosts->row (ui.lb_remoteHosts->currentItem ()));
  delete item;
}

void
PwmdSocketWidget::slotRemoteHostItemChanged (QListWidgetItem * item,
				       QListWidgetItem * old)
{
#ifndef Q_OS_ANDROID
  if (!(pwmd_features () & PWMD_FEATURE_SSH)
      && !(pwmd_features () & PWMD_FEATURE_GNUTLS))
    {
      ui.ck_remote->setChecked (false);
      return;
    }
#endif

  updateRemoteHost (old);
  if (!item)
    {
      ui.gr_hostDetails->setEnabled (false);
      initNewRemoteHost ();
      return;
    }

  PwmdRemoteHost data = item->data (Qt::UserRole).value < PwmdRemoteHost > ();
  ui.le_remoteName->setText (data.name ());

  if (!data.type () || data.type () == PWMD_SOCKET_SSH)
    {
      ui.cb_connectionType->setCurrentIndex (0);
      ui.fr_tls->setHidden (true);
#ifdef Q_OS_ANDROID
      ui.fr_certificateAlias->setHidden (true);
      ui.fr_socketProtocol->setHidden(false);
#endif
      ui.fr_ssh->setHidden (false);
    }
  else  if (data.type () == PWMD_SOCKET_TLS || data.type () == PWMD_SOCKET_USER)
    {
      if (!(pwmd_features () & PWMD_FEATURE_SSH))
        ui.cb_connectionType->setCurrentIndex (0);
      else
        ui.cb_connectionType->setCurrentIndex (1);

#ifdef Q_OS_ANDROID
      ui.fr_certificateAlias->setHidden (false);
      ui.fr_socketProtocol->setHidden(true);
#else
      ui.fr_tls->setHidden (false);
#endif
      ui.fr_ssh->setHidden (true);
    }

  ui.le_hostname->setText (data.hostname ());
  switch (data.ipProtocol ())
    {
    case 0:
      ui.rb_ipany->setChecked (true);
      break;
    case 1:
      ui.rb_ipv4->setChecked (true);
      break;
    case 2:
      ui.rb_ipv6->setChecked (true);
      break;
    }

  ui.sp_port->setValue (data.port ());

  /* Fill the argument list. 6 is the maximum to pass to pwmd_connect() but
   * any amount more than the known 5 used can be appended. */
  QStringList args = data.socketArgs ();
  for (int n = args.count () - 1; n < 6; n++)
    args.append (QString (""));

  if (data.type () == PWMD_SOCKET_SSH)
    {
      ui.le_identity->setText (args.at (0));
      ui.le_knownhosts->setText (args.at (1));
    }
  else
    {
      ui.le_clientCert->setText (args.at (0));
      ui.le_clientKey->setText (args.at (1));
      ui.le_caCert->setText (args.at (2));
      ui.le_tlsServerFingerprint->setText (args.at (3));
    }

  ui.le_username->setText (data.sshUsername ());
  ui.sp_connectTimeout->setValue (data.connectTimeout ());
  ui.sp_socketTimeout->setValue (data.socketTimeout ());
  ui.ck_tlsVerify->setChecked (data.tlsVerify ());
  ui.le_tlsPriority->setText (data.tlsPriority ());
  ui.ck_agent->setChecked (data.sshAgent ());
#ifdef Q_OS_ANDROID
  ui.le_certificateAlias->setText (data.clientCertificateAlias ());
#endif
  ui.gr_hostDetails->setEnabled (true);
}

void
PwmdSocketWidget::slotRemoteHostNameChanged (const QString & str)
{
  QListWidgetItem *item = ui.lb_remoteHosts->currentItem ();

  if (!item)
    {
      ui.gr_hostDetails->setEnabled (false);
      return;
    }

  PwmdRemoteHost data = item->data (Qt::UserRole).value < PwmdRemoteHost > ();
  data.setName (str);
  item->setData (Qt::UserRole, QVariant::fromValue (data));
  item->setText (str);
}

QString
PwmdSocketWidget::socket ()
{
  if (ui.ck_remote->isChecked ())
      return ui.lb_remoteHosts->currentItem ()->text ();

  return ui.cb_socket->lineEdit()->text ();
}

void
PwmdSocketWidget::setSelected (const QString &sock)
{
  foreach (QListWidgetItem *item, ui.lb_remoteHosts->findItems ("*", Qt::MatchWildcard))
    {
      if (item->text () == sock)
	{
	  ui.ck_remote->setChecked (true);
	  ui.lb_remoteHosts->setCurrentItem (item);
	  if (!(item->flags () & Qt::ItemIsEnabled))
	    ui.gr_hostDetails->setEnabled (false);
	  return;
	}
    }

  ui.cb_socket->lineEdit()->setText (sock);
}
