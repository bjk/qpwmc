/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDSOCKETWIDGET_H
#define PWMDSOCKETWIDGET_H

#include <QFrame>
#include <QListWidgetItem>
#include "ui_pwmdSocketWidget.h"
#include "pwmdRemoteHost.h"

class PwmdSocketWidget : public QFrame
{
  Q_OBJECT
 public:
  PwmdSocketWidget (QWidget * = 0);
  ~PwmdSocketWidget ();
  Ui::PwmdSocketWidget ui;

  // Return the socket string needed by pwmd_connect().
  QString socket ();

  // Set the current QListWidgetItem host to name.
  void setSelected (const QString &name);

  // Used in PwmdSocketDialog to update changes during QDialog::accept().
  void updateRemoteHost ();

  /* Write the remote hosts to disk. May be needed if the widget is not part
   * of a QDialog. */
  void saveHosts ();

  /* Returns true and fills hostData with the currently selected hostname.
   * Returns false if a local socket or no remote host is selected. */
  bool currentHostData (PwmdRemoteHost &hostData);

private slots:
  void slotRemoteToggled (bool);
  void slotUseAgentToggled (bool);
  void slotChooseIdentityFile ();
  void slotChooseKnownhostsFile ();
  void slotIpVersionToggled (bool);
  void slotSelectTlsClientCert ();
  void slotSelectTlsClientKey ();
  void slotSelectTlsCaCert ();
  void slotNewRemoteHost ();
  void slotRemoveRemoteHost ();
  void slotRemoteHostItemChanged (QListWidgetItem *, QListWidgetItem *);
  void slotRemoteHostNameChanged (const QString &);
  void slotConnectionTypeChanged (int);
  void slotLocalSocketChanged (int);
  void slotClearLocalSocket ();
  void slotSelectSocket ();
#ifdef Q_OS_ANDROID
  void slotSelectCertificateAlias ();
#endif

 private:
  void initNewRemoteHost ();
  void updateRemoteHost (QListWidgetItem *);
  void toggleRemote (bool);

  int ipVersion;
};

#endif
