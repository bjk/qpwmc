/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QMenu>
#include <QDrag>
#include <QMimeData>
#include "pwmdMainWindow.h"
#include "pwmdTreeWidget.h"
#include "pwmdTreeWidgetItemData.h"
#include "pwmd.h"

static QMenu *popupMenu;
static QAction *copyAction, *moveAction, *targetAction;
static bool isBusy;

PwmdTreeWidget::PwmdTreeWidget (QWidget * p) : QTreeWidget (p)
{
  _parent = nullptr;
  lastItem = nullptr;
  expandTimer = new QTimer (this);
  expandTimer->setSingleShot (true);
  connect (expandTimer, SIGNAL (timeout ()), this,
	   SLOT (slotExpandTimeout ()));
  dndMove = false;
}

void
PwmdTreeWidget::setPwmdParent (PwmdMainWindow *p)
{
  _parent = p;
  connect (p, SIGNAL (attributesRetrieved (QTreeWidgetItem *)), this,
           SLOT (slotAttributesRetrieved (QTreeWidgetItem *)));
}

void
PwmdTreeWidget::setPwmdHandle (Pwmd *p)
{
  disconnect (p, SIGNAL (busy (int, bool)), this, SLOT (slotBusy (int, bool)));
  connect (p, SIGNAL (busy (int, bool)), this, SLOT (slotBusy (int, bool)));
}

void
PwmdTreeWidget::slotAttributesRetrieved (QTreeWidgetItem *item)
{
  if (popupMenu && popupMenu->isVisible ())
    popupDragContextMenu (item);
}

void
PwmdTreeWidget::resizeEvent (QResizeEvent *ev)
{
  emit treeWidgetResized (ev->size ());
  QTreeWidget::resizeEvent (ev);
}

void
PwmdTreeWidget::keyPressEvent (QKeyEvent * ev)
{
  if (!_parent)
    {
      QTreeWidget::keyPressEvent (ev);
      return;
    }

  if (_parent->isBusy)
    return;

  // Alt-Enter
  if (ev->key () == Qt::Key_Return && ev->nativeModifiers () == 8)
    {
      QCursor::setPos (mapToGlobal
                       (QPoint (visualItemRect (currentItem ()).topRight ())));
      _parent->popupContextMenu ();
      ev->accept ();
      return;
    }
  else if (ev->key () == Qt::Key_Escape && !ev->nativeModifiers ())
    {
      _parent->resetSelectedItems ();
      ev->accept ();
      return;
    }

  QTreeWidget::keyPressEvent (ev);
}

void
PwmdTreeWidget::mouseReleaseEvent (QMouseEvent * ev)
{
  ev->accept ();

  if (!_parent || ev->button () != Qt::RightButton)
      return;

  _parent->popupContextMenu ();
}

void
PwmdTreeWidget::mousePressEvent (QMouseEvent * ev)
{
  if (_parent && _parent->isBusy)
    {
      ev->accept ();
      return;
    }

  dragStartPosition = ev->pos ();
  QTreeWidget::mousePressEvent (ev);
}

/* Test if the source element is being dropped to the resolved path of a
 * child of the source.
 */
bool
PwmdTreeWidget::dropIsChild (QTreeWidgetItem * item, QTreeWidgetItem *drop)
{
  if (!_parent)
    return true;

  for (int i = 0, t = item->childCount (); i < t; i++)
    {
      QTreeWidgetItem *p = _parent->resolveElementPath (item->child (i));

      if (p == drop || dropIsChild (item->child (i), drop))
	return true;
    }

  return false;
}

bool
PwmdTreeWidget::isValidDndMove (QTreeWidgetItem * drop, bool move)
{
  PwmdTreeWidgetItemData *data = drop ? qvariant_cast <PwmdTreeWidgetItemData * >(drop->data (0, Qt::UserRole)) : nullptr;

  if (!_parent)
    return false;

  if (data)
    {
      if (data->badTarget () || data->badPermissions () || data->targetLoop ())
        return false;
    }
  else if (!drop) // New root element
    return true;

  if (_parent->isParentOf (_parent->selectedElement, drop)
      || _parent->selectedElement == drop
      || (!_parent->selectedElement->parent () && !drop))
    return false;

  QTreeWidgetItem *srcElement = _parent->resolveElementPath (_parent->selectedElement);
  QTreeWidgetItem *dropElement = _parent->resolveElementPath (drop);
  QTreeWidgetItem *a = dropElement, *b = srcElement;

  if (_parent->selectedElement)
    {
      data = qvariant_cast <PwmdTreeWidgetItemData * >(_parent->selectedElement->data(0, Qt::UserRole));
      if (!_parent->isElementOwner (_parent->selectedElement)
          && data->badPermissions ())
        return false;
      else if (!move && !_parent->isElementOwner (_parent->selectedElement)
               && data->badTarget ())
        return false;
    }

  for (; move && a; a = a->parent ())
    {
      if (a == b)
	return false;

      if (_parent->isParentOf (_parent->selectedElement, dropElement)
	  || _parent->selectedElement == a
	  || (!_parent->selectedElement->parent () && !a))
	return false;
    }

  a = dropElement;
  b = srcElement;
  if (!b || !a)
    return true;

  for (; move && a; a = a->parent ())
    {
      if (a == b)
	return false;

      if (dropElement == _parent->resolveElementPath (b->parent ()))
	return false;

      if (_parent->isParentOf (_parent->selectedElement, a)
	  || _parent->selectedElement == a
	  || (!_parent->selectedElement->parent () && !a))
	return false;
    }

  if (dropIsChild (srcElement, dropElement))
    return false;

  return true;
}

QPoint
PwmdTreeWidget::mousePosition ()
{
  return _mousePosition;
}

bool
PwmdTreeWidget::validTargetDropElement (QTreeWidgetItem *item)
{
  if (!item)
    return false;

  PwmdTreeWidgetItemData *data = qvariant_cast <PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  if (!_parent->isElementOwner (item) && (data->badTarget ()
                                          || data->targetLoop ()
                                          || data->badPermissions ()))
    return false;
  else if (_parent->isParentOf (_parent->selectedElement, item))
    return false;

  return true;
}

void
PwmdTreeWidget::slotBusy (int, bool b)
{
  static int refCount;
  static bool last;

  refCount += b ? 1 : -1;
  if (refCount < 0)
    refCount = 0;

  isBusy = !!refCount;

  if (isBusy != last)
    setPopupMenuItemsEnabled ();

  last = isBusy;
}

void
PwmdTreeWidget::setPopupMenuItemsEnabled ()
{
  if (!_parent || !copyAction || !moveAction || !targetAction)
    return;

  if (isBusy || !isValidDndMove (_parent->dropElement))
    copyAction->setEnabled (false);
  else
    copyAction->setEnabled (true);

  if (isBusy || !isValidDndMove (_parent->dropElement, true)
      || (!_parent->dropElement && _parent->ui.elementTree->indexOfTopLevelItem (_parent->selectedElement) != -1))
    moveAction->setEnabled (false);
  else
    moveAction->setEnabled (true);

  if (isBusy || !_parent->dropElement
      || _parent->selectedElement == _parent->dropElement
      || !validTargetDropElement (_parent->dropElement)
      || _parent->isParentOf (_parent->dropElement, _parent->selectedElement)
      || !_parent->isElementOwner (_parent->selectedElement))
    targetAction->setEnabled (false);
  else
    targetAction->setEnabled (true);
}

void
PwmdTreeWidget::popupDragContextMenu(QTreeWidgetItem *item)
{
  PwmdTreeWidgetItemData *data = item ? qvariant_cast <PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole)) : nullptr;

  if (item && item != _parent->dropElement)
    return;

  if (!popupMenu)
    {
      popupMenu = new QMenu (this);
      copyAction = popupMenu->addAction (tr ("&Copy here"), _parent,
                                         SLOT (slotDndCopyElement ()));
      moveAction = popupMenu->addAction (tr ("&Move here"), _parent,
                                         SLOT (slotDndMoveElement ()));
      targetAction = popupMenu->addAction (tr ("Set as &target"), _parent,
                                           SLOT (slotDndCreateTarget ()));
    }

  setPopupMenuItemsEnabled ();

  if (!item || (item != _parent->dropElement && item != _parent->selectedElement))
    {
      if (!data && _parent->dropElement)
        data = qvariant_cast <PwmdTreeWidgetItemData * >(_parent->dropElement->data (0, Qt::UserRole));

      if (!data || data->attributes ()->isEmpty ())
        _parent->refreshAttributeList (_parent->dropElement, false, false, true);
    }

  popupMenu->popup (QCursor::pos ());
}

void
PwmdTreeWidget::mouseMoveEvent (QMouseEvent * ev)
{
  dndMove = false;
  _mousePosition = ev->pos ();

  if (!_parent)
    {
      ev->accept ();
      return;
    }

  QTreeWidgetItem *item = itemAt(ev->pos());
  if (!item)
    {
      ev->accept ();
      return;
    }

  _parent->setElementStatusTip (item);

  if (!ev->buttons () && !ev->modifiers ())
    {
      ev->accept ();
      return;
    }

  if ((ev->pos () - dragStartPosition).manhattanLength ()
      < QApplication::startDragDistance ())
    {
      ev->accept ();
      return;
    }

  QDrag *drag = new QDrag (this);
  QMimeData *m = new QMimeData;
  m->setText (_parent->elementPath (_parent->selectedElement));
  drag->setMimeData (m);
  Qt::DropAction action;

  if ((ev->buttons () & Qt::LeftButton) && ev->modifiers ())
    {
      _parent->dndOperation = PwmdMainWindow::DndNone;
      switch (ev->modifiers ())
	{
	case Qt::ControlModifier:
          _parent->dndOperation = PwmdMainWindow::DndAny;
	  action = drag->exec (Qt::CopyAction);
	  _parent->slotElementEntered (nullptr, 0);
          expandTimer->stop ();

	  if (drag->target () && action == Qt::CopyAction
              && isValidDndMove (_parent->dropElement))
	    _parent->slotDndCopyElement ();
          else
            _parent->dndOperation = PwmdMainWindow::DndNone;
	  break;
	case Qt::AltModifier:
          _parent->dndOperation = PwmdMainWindow::DndAny;
	  action = drag->exec (Qt::LinkAction);
          expandTimer->stop ();
	  _parent->slotElementEntered (nullptr, 0);

	  if (drag->target ()
	      && action == Qt::LinkAction && _parent->dropElement
	      && validTargetDropElement (_parent->dropElement))
	    _parent->slotDndCreateTarget ();
          else
            _parent->dndOperation = PwmdMainWindow::DndNone;
	  break;
	default:
	  break;
	}

      ev->accept ();
      return;
    }
#ifdef Q_OS_ANDROID
  else if (!ev->modifiers () && (ev->buttons () & Qt::LeftButton))
#else
  else if (!ev->modifiers () && (ev->buttons () & Qt::RightButton))
#endif
    {
      _parent->dndOperation = PwmdMainWindow::DndAny;
      drag->exec (Qt::CopyAction);
      _parent->slotElementEntered (nullptr, 0);
      expandTimer->stop ();

      if (!drag->target ()
          || drag->target ()->parent () != _parent->ui.elementTree)
        {
          _parent->dndOperation = PwmdMainWindow::DndNone;
          ev->accept ();
          return;
        }

      popupDragContextMenu();
    }
  else if (!ev->modifiers () && (ev->buttons () & Qt::LeftButton))
    {
      // Fake a move action. Would be good if Qt would let us get the cursor
      // displayed while doing a Qt::MoveAction since we're doing a
      // Qt::CopyAction. Oh well.
      //drag->setDragCursor(moveactionpixmap, Qt::CopyAction);
      dndMove = true;
      _parent->dndOperation = PwmdMainWindow::DndAny;
      action = drag->exec (Qt::CopyAction);
      _parent->slotElementEntered (nullptr, 0);
      expandTimer->stop ();

      if (drag->target ()
	  && drag->target ()->parent () == _parent->ui.elementTree
	  && action == Qt::CopyAction)
        {
          if (ev->source () == Qt::MouseEventSynthesizedBySystem)
            popupDragContextMenu ();
          else
            _parent->slotDndMoveElement ();
        }
      else
        _parent->dndOperation = PwmdMainWindow::DndNone;

      ev->accept ();
    }
}

void
PwmdTreeWidget::dragMoveEvent (QDragMoveEvent * ev)
{
  QTreeWidget::dragMoveEvent (ev);

  if (!_parent || !_parent->selectedElement)
    {
      ev->ignore ();
      return;
    }

  QTreeWidgetItem *item = _parent->ui.elementTree->itemAt (ev->position ().toPoint ());

  if (ev->dropAction () == Qt::LinkAction)
    {
      if (!item || item == _parent->selectedElement)
	{
          expandTimer->stop ();
	  ev->ignore ();
	  return;
	}

      if (lastItem == item)
	{
	  if (!expandTimer->isActive ())
	    expandTimer->start (500);

          ev->accept ();
	  return;
	}
    }

  _parent->slotElementEntered (item, 0);
  expandTimer->stop ();

  if (dndMove)
    {
      if (!isValidDndMove (item, true))
	{
          if (item)
            {
              lastItem = item;
              expandTimer->start (500);
            }

          ev->ignore ();
          return;
        }
    }

  if (item)
    expandTimer->start (500);

  lastItem = item;
  ev->accept ();
}

void
PwmdTreeWidget::slotExpandTimeout ()
{
  lastItem->setExpanded (true);
}

void
PwmdTreeWidget::dragEnterEvent (QDragEnterEvent * ev)
{
  if (!_parent || ev->source () != _parent->ui.elementTree)
    return;

  if (ev->mimeData ()->hasFormat ("text/plain"))
    ev->acceptProposedAction ();
}

// Can't do the actual modifiying of elements here in case of pwmd error the
// elementTree would already be modified. This only happens with
// Qt::MoveAction for some reason.
bool
PwmdTreeWidget::dropMimeData (QTreeWidgetItem * dst, int, const QMimeData *,
                              Qt::DropAction)
{
  if (!_parent)
    return false;

  expandTimer->stop ();
  _parent->dropElement = dst;
  return true;
}

QStringList
PwmdTreeWidget::mimeTypes () const
{
  QStringList l;

  l.append ("text/plain");
  return l;
}

Qt::DropActions PwmdTreeWidget::supportedDropActions () const
{
  return Qt::MoveAction | Qt::CopyAction | Qt::LinkAction;
}
