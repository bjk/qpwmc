/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDLISTWIDGET_H
#define PWMDLISTWIDGET_H

#include <QApplication>
#include <QListWidget>
#include <QLineEdit>
#include <QItemDelegate>
#include <QRegularExpression>
#include <QValidator>
#include <QTapAndHoldGesture>

class PwmdMainWindow;
class PwmdListWidget : public QListWidget
{
  Q_OBJECT
 public:
  PwmdListWidget (QWidget *);
  ~PwmdListWidget () {};
  void setPwmdParent (PwmdMainWindow *);

 private:
  void contextMenuEvent (QContextMenuEvent *);
  void keyPressEvent (QKeyEvent *);
  void mousePressEvent (QMouseEvent *);
  bool event (QEvent *);
  bool gestureEvent (QGestureEvent *);
  void tapAndHoldGesture (QTapAndHoldGesture *);

  PwmdMainWindow *_parent;
};

class PwmdListWidgetItemDelegate : public QItemDelegate
{
  Q_OBJECT
 public:
  PwmdListWidgetItemDelegate (QObject * p = 0) : QItemDelegate (p) {};

  QWidget *createEditor (QWidget * p, const QStyleOptionViewItem & o,
			 const QModelIndex & i) const
  {
    QWidget *editor = QItemDelegate::createEditor (p, o, i);
#ifndef Q_OS_ANDROID
    QLineEdit *lineEdit = qobject_cast < QLineEdit * >(editor);

    if (lineEdit)
      {
        QRegularExpression rx ("^[^\\s]+[^\\s]*$");
        QValidator *v = new QRegularExpressionValidator (rx, lineEdit);

        lineEdit->setValidator (v);
        lineEdit->setInputMethodHints (Qt::ImhNoPredictiveText|Qt::ImhNoAutoUppercase);
      }
#else
    qApp->inputMethod ()->show ();
#endif

    return editor;
  };

};

#endif
