/*
    Copyright (C) 2018-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QMenu>
#include <QSettings>
#include <QClipboard>
#include <QTimer>
#include <QMessageBox>
#include "main.h"
#include "trayIcon.h"
#include "pwmdRemoteHost.h"

#define PwmdCmdIdGetContent 0

#define RotateStepDegrees	8

static bool waitingForClose;

TrayIcon::TrayIcon () : QSystemTrayIcon (QIcon (":icons/trayicon.svg"))
{
  qRegisterMetaType <Pwmd::ConnectionState>("Pwmd::ConnectionState");
  qRegisterMetaType <gpg_error_t>("gpg_error_t");

  QCoreApplication::setOrganizationName ("QPwmc");
  QCoreApplication::setOrganizationDomain ("qpwmc.sourceforge.net");
  QCoreApplication::setApplicationName ("QPwmc");
  QCoreApplication::setApplicationVersion (QPWMC_VERSION);
  shortcutMenu = new QMenu ();
  connect (shortcutMenu, SIGNAL (triggered (QAction *)), this,
	   SLOT (slotShortCut (QAction *)));
  refreshShortcuts ();
  connect (this, SIGNAL (activated (QSystemTrayIcon::ActivationReason)),
	   SLOT (slotSystemTrayActivated
		 (QSystemTrayIcon::ActivationReason)));
  setContextMenu (shortcutMenu);
  clipboardTimer = new QTimer (this);
  clipboardTimer->setSingleShot (true);
  connect (clipboardTimer, SIGNAL (timeout ()), SLOT (slotClearClipboard ()));
  rotateTimer = new QTimer (this);
  connect (rotateTimer, SIGNAL (timeout ()), SLOT (slotRotateIcon ()));
  rotateIcon (true);
  lingerTimer = new QTimer (this);
  connect (lingerTimer, SIGNAL (timeout ()), SLOT (slotLinger ()));
  lingerRemaining = 0;
  lingerTime = 0;
  closeFile = false;
  statusData = nullptr;
  pwm = nullptr;
}

TrayIcon::~TrayIcon ()
{
  delete rotateTimer;
  delete lingerTimer;
  delete clipboardTimer;
  if (statusData)
    delete statusData;
}

void
TrayIcon::startStopTimer (bool start)
{
  if (start)
    {
      setIcon (QIcon (":icons/trayicon-linger.svg"));
      lingerTimer->start (1000);
      return;
    }

  lingerTimer->stop ();
  setIcon (QIcon (":icons/trayicon.svg"));
  lingerRemaining = 0;
}

void
TrayIcon::slotLinger ()
{
  lingerRemaining--;
  if (pwm && lingerRemaining > 0)
    return;

  startStopTimer (false);
  delete pwm;
  pwm = nullptr;
}

void
TrayIcon::slotRotateIcon ()
{
  rotateIcon ();
}

void
TrayIcon::rotateIcon (bool reset)
{
  static float rotate = 1;
  static float mod = -0.1;

  if (reset)
    {
      rotate = 1;
      mod = -0.1;
      rotateTimer->stop ();
      setIcon (QIcon (":icons/trayicon.svg"));
      setToolTip (tr ("Double click to spawn the editor, right-click for shortcuts."));
      return;
    }

  QSize size (22, 22);
  QPixmap p = QIcon (":icons/trayicon.svg").pixmap (size);
  QTransform t;

  if (pwm->state () == Pwmd::Init || pwm->state () == Pwmd::Connecting)
    {
      if (rotate + mod < 0.0)
        mod = 0.1;
      else if (rotate + mod > 1)
        mod = -0.1;

      rotate += mod;
      t.scale (rotate, rotate);
    }
  else
    {
      if (rotate + RotateStepDegrees > 360)
        rotate = 0;

      rotate += RotateStepDegrees;
      t.rotate (rotate);
    }

  QPixmap pp = p.transformed (t);
  setIcon (QIcon (pp));
}

void
TrayIcon::showMessage (unsigned rc, bool reset)
{
  if (gpg_err_source (rc) == GPG_ERR_SOURCE_PINENTRY
      && gpg_err_code (rc) == GPG_ERR_CANCELED)
    goto done;

  if (supportsMessages())
    {
      QSystemTrayIcon::showMessage(tr ("There was an error while communicating with pwmd."),
                                   Pwmd::errorString(rc, pwm));
    }
  else
    Pwmd::showError (rc, pwm);

done:
  if (reset || !lingerRemaining)
    {
      startStopTimer (false);
      waitingForClose = true;
    }
}

void
TrayIcon::slotConnectionStateChanged (Pwmd::ConnectionState s)
{
  pwmd_socket_t type;
  static int last;

  pwmd_socket_type (pwm->handle (), &type);

  /* Fixes a race condition when Pwmd::connect() fails. The handle may have
   * been reset (which clears the error code) before the dialog is shown.
   */
  pwm->tlsError = pwmd_gnutls_error(pwm->handle(), nullptr);

  switch (s)
    {
    case Pwmd::Init:
      startStopTimer (false);
      pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
		   currentHostData.connectTimeout ());
      pwmd_setopt (pwm->handle (), PWMD_OPTION_SSH_AGENT,
		   currentHostData.sshAgent ());
      pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_VERIFY,
		   currentHostData.tlsVerify ());
      if (!currentHostData.tlsPriority().isEmpty ())
        pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_PRIORITY,
                     currentHostData.tlsPriority ().toUtf8 ().data ());
      pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
		   currentHostData.connectTimeout ());
      setToolTip (tr ("Connecting..."));
      break;
    case Pwmd::Connected:
      pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
		   currentHostData.socketTimeout ());
      if (last != Pwmd::Opened && last != s)
        setToolTip (tr ("Opening data file..."));
      break;
    case Pwmd::Opened:
      setToolTip (tr ("Retrieving content..."));
      break;
    default:
      break;
    }

  last = s;
}

void
TrayIcon::shortCutFinalize (const QString &result)
{
  QClipboard *c = QApplication::clipboard ();

  c->setText (result, QClipboard::Selection);
  c->setText (result);
  refreshShortcuts ();

  if (clipboardTimeout)
    clipboardTimer->start (clipboardTimeout * 1000);

  lingerRemaining = lingerTime;
  if (lingerTime > 0)
    {
      startStopTimer (true);
      if (closeFile && pwm->state () == Pwmd::Opened)
        pwm->close ();
    }

  if (lingerRemaining)
    return;

  delete pwm;
  pwm = nullptr;
}

void
TrayIcon::slotKnownHostCallback (void *data, const char *host, const char *key,
                                 size_t len)
{
  gpg_error_t rc = Pwmd::knownHostPrompt (data, host, key, len);
  emit knownHostRc (rc);
}

void
TrayIcon::slotStatusMessage (QString line, void *userData)
{
  QString *shortcut = static_cast <QString *> (userData);
  QStringList l = QString (line).split (" ");

  if (l.at (0) == "EXPIRE")
    {
      unsigned t = l.at(1).toUInt ();
      QDateTime date;
      date.setSecsSinceEpoch (t);
      QString title = QString (tr ("The content for the shortcut \"%1\" has expired.")). arg (*shortcut);
      QString desc = QString (tr ("Use the editor to update the content or to change the expiry time. The element\ncontent has been copied to the clipboard.\n\nExpired at: %1.")).arg(date.toString());

      if (supportsMessages())
        QSystemTrayIcon::showMessage(title, desc);
      else
        {
          QMessageBox m;

          m.setText (title);
          m.setInformativeText (desc);
          m.setIcon (QMessageBox::Information);
          m.exec ();
        }
    }
}

void
TrayIcon::slotShortCut (QAction * a)
{
  EditShortcut data = a->data ().value < EditShortcut > ();
  if (data.name ().isEmpty ())
    return;

  bool reset = false;
  startStopTimer (false);
  QSettings cfg ("qpwmc");
  lingerTime = cfg.value ("linger", 20).toInt ();
  closeFile = cfg.value ("closeFile", false).toBool ();
  PwmdRemoteHost lastHostData = currentHostData;
  QString lastSocket = pwm ? pwm->socket () : QString ();
  QString lastFile = pwm ? pwm->filename () : QString ();

  rotateTimer->start (40);

  bool remote = false;
  if (PwmdRemoteHost::fillRemoteHost (data.socket (), currentHostData))
    {
      remote = true;
      if (currentHostData != lastHostData)
        reset = true;
    }
  else
    {
      if (data.socket () != lastSocket)
        reset = true;
    }

  if (reset || !pwm)
    {
      reset = true;
      delete pwm;
      pwm = new Pwmd (data.filename (), "qpwmc", 0, this);
    }

  if (remote)
    {
      pwm->setSocket (PwmdRemoteHost::socketUrl (currentHostData));
      pwm->setConnectParameters (currentHostData.socketArgs ());
    }
  else
    pwm->setSocket (data.socket ());

  pwm->setFilename (data.filename ());
  pwmd_setopt (pwm->handle (), PWMD_OPTION_SSH_AGENT,
	       currentHostData.sshAgent ());
  pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
	       currentHostData.socketTimeout ());
  pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_VERIFY,
	       currentHostData.tlsVerify ());
  if (!currentHostData.tlsPriority().isEmpty ())
    pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_PRIORITY,
                 currentHostData.tlsPriority ().toUtf8 ().data ());

  connect (pwm, SIGNAL (statusMessage (QString, void *)), this,
	   SLOT (slotStatusMessage (QString, void *)));
  connect (pwm, SIGNAL (stateChanged (Pwmd::ConnectionState)), this,
	   SLOT (slotConnectionStateChanged (Pwmd::ConnectionState)));
  connect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, unsigned, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, unsigned, bool)));
  connect (pwm, SIGNAL (busy (int, bool)), this, SLOT (slotBusy (int, bool)));
  connect (pwm, SIGNAL (knownHost (void *, const char *, const char *, size_t)), this, SLOT (slotKnownHostCallback (void *, const char *, const char *, size_t)));

  /* Trigger the stateChanged() signal. */
  pwm->reset (true, reset);

  QString path = data.path ();
  PwmdInquireData *inq = new PwmdInquireData (pwm->handle (), data.filename ());
  if (reset)
    pwm->connect (Pwmd::inquireCallback, inq, true);

  if (reset || lastFile != data.filename () || pwm->state () != Pwmd::Opened)
    pwm->open (Pwmd::inquireCallback, inq, true);

  PwmdInquireData *cinq = new PwmdInquireData (path);
  if (statusData)
    delete statusData;
  statusData = new QString (data.name ());
  pwm->setStatusMessageData (statusData);
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdGetContent, "GET", Pwmd::inquireCallback, cinq));
}

void
TrayIcon::slotBusy (int, bool b)
{
  if (!b && waitingForClose && !pwm->queued ())
    {
      waitingForClose = false;
      delete pwm;
      pwm = nullptr;
    }
}

void
TrayIcon::slotCommandResult (PwmdCommandQueueItem *item,
                             QString result, gpg_error_t rc, bool queued)
{
  if (rc)
    rotateIcon (true);

  switch (item->id ())
    {
    case PwmdCmdIdInternalConnect:
    case PwmdCmdIdInternalOpen:
    case PwmdCmdIdInternalCloseFile:
      break;
    case PwmdCmdIdGetContent:
      rotateIcon (true);
      if (!rc)
        shortCutFinalize (result);
      break;
    default:
      break;
    }

  if (rc && !item->checkError (rc) && !queued)
    showMessage (rc, !(item->id () == PwmdCmdIdGetContent));

  item->setSeen ();
}


void
TrayIcon::refreshShortcuts ()
{
  QMenu *current;
  QList<QMenu *> list;

  shortcutMenu->clear ();
  shortcutMenu->addAction (tr ("Edit XML"), this, SLOT (slotEditPwmd ()));
  shortcutMenu->addAction (tr ("Edit shortcuts"), this,
			   SLOT (slotEditShortcuts ()));
  shortcutMenu->addSeparator ();
  QSettings cfg ("qpwmc");
  clipboardTimeout = cfg.value ("clipboardTimeout", 20).toInt ();
  int size = cfg.beginReadArray ("shortcuts");

  for (int i = 0; i < size; ++i)
    {
      current = shortcutMenu;
      cfg.setArrayIndex (i);
      QString sub = cfg.value("subMenu").toString();
      QMenu *m;

      if (!sub.isEmpty())
        {
          int n;

          for (n = 0; n < list.count(); n++) {
              m = list.at(n);

              if (m->title() == sub) {
                current = m;
                break;
              }
          }

          if (current == shortcutMenu)
            {
              current = m = new QMenu(sub, shortcutMenu);
              list.append(m);
              shortcutMenu->addMenu(m);
            }
        }

      EditShortcut data = EditShortcut (cfg.value ("filename").toString (),
					cfg.value ("name").toString (),
					cfg.value ("path").toString (),
					cfg.value ("socket").toString (),
					cfg.value ("subMenu").toString ());

      QAction *a = current->addAction (data.name ());
      a->setData (QVariant::fromValue (data));
    }

  cfg.endArray ();

  if (size)
    shortcutMenu->addSeparator ();

  shortcutMenu->addAction (tr ("Clear clipboard"), this,
			   SLOT (slotClearClipboardReal ()));
  shortcutMenu->addAction (tr ("About"), this, SLOT (slotAbout ()));
  shortcutMenu->addAction (tr ("Quit"), this, SLOT (slotQuit ()));
  list.clear();
}

void
TrayIcon::slotQuit ()
{
  QApplication::quit ();
}

void
TrayIcon::slotSystemTrayActivated (QSystemTrayIcon::ActivationReason n)
{
  if (n == QSystemTrayIcon::DoubleClick)
    slotEditPwmd ();
}

void
TrayIcon::slotAbout ()
{
  QMessageBox::about (0, "QPwmc",
		      QString (tr ("QPwmc version %1\nlibpwmd version %2\n\n"
				   "Copyright 2014-2025 Ben Kibbey <bjk@luxsci.net>\n"
				   "https://gitlab.com/bjk/qpwmc")).
		      arg (QPWMC_VERSION, pwmd_version ()));
}

// Don't check that QPwmc owns the content. Forces clearing the clipboard.
void
TrayIcon::slotClearClipboardReal ()
{
  QClipboard *c = QApplication::clipboard();

  if (c->supportsSelection ())
    c->setText("", QClipboard::Selection);
  c->setText("", QClipboard::Clipboard);
}

void
TrayIcon::slotClearClipboard ()
{
  QClipboard *c = QApplication::clipboard();

  clipboardTimer->stop();
  if (c->supportsSelection ())
    c->clear (QClipboard::Selection);
  c->clear (QClipboard::Clipboard);
}

void
TrayIcon::slotEditShortcuts ()
{
  EditShortcutDialog *d = new EditShortcutDialog ();
  bool b = d->exec ();

  delete d;

  if (b)
    refreshShortcuts ();
}

void
TrayIcon::slotEditPwmd ()
{
  startStopTimer (false);
  delete pwm;
  pwm = nullptr;

  Pwmd *spwm = new Pwmd (0, 0, 0, this);
  QString result;
  spwm->spawnEditor (result, 0, 0, 0, false);
  delete spwm;
}
