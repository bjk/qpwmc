#!/bin/sh
#
# The known dependencies are found in mxe-win32.deps and can be piped to this
# script.
#
QPWMC_PREFIX="`pwd`/qpwmc-win32"
MXE_ROOT=

usage () {
    echo "Copy known needed dependencies for qpwmc."
    echo "Usage: $0 --mxe-root=<path> [<file> ...]"
    exit $1
}

TEMP=$(getopt -o 'hr:' --long 'help,mxe-root:' -n "$0" -- "$@")

if [ $? -ne 0 ]; then
    usage 1
fi

eval set -- "$TEMP"
unset TEMP

while true; do
    case "$1" in
	'-p'|'--mxe-root')
	    MXE_ROOT="$2"
	    shift 2
	    continue
	    ;;
	'-h'|'--help')
	    usage 0
	    ;;
	'--')
	    shift
	    break
	    ;;
	*)
	    usage 1
    esac
done

if [ -z $MXE_ROOT ]; then
    usage 1
fi

MXE_PREFIX="$MXE_ROOT/usr/i686-w64-mingw32.shared"
echo "Using MXE prefix \"$MXE_PREFIX\""

copy_file () {
    if [ -d "$MXE_PREFIX/bin/$1" ]; then
	mkdir -p $QPWMC_PREFIX/$1 || exit 1
	return
    elif [ -d "$MXE_PREFIX/qt5/bin/$1" ]; then
	mkdir -p $QPWMC_PREFIX/$1 || exit 1
	return
    elif [ -d "$MXE_PREFIX/qt5/plugins/$1" ]; then
	mkdir -p $QPWMC_PREFIX/$1 || exit 1
	return
    fi

    if [ -f "$MXE_PREFIX/bin/$1" ]; then
	echo "Copying file $MXE_PREFIX/bin/$1"
	cp -f "$MXE_PREFIX/bin/$1" "$QPWMC_PREFIX/$1" || exit 1
    elif [ -f "$MXE_PREFIX/qt5/bin/$1" ]; then
	echo "Copying file $MXE_PREFIX/qt5/bin/$1"
	cp -f "$MXE_PREFIX/qt5/bin/$1" "$QPWMC_PREFIX/$1" || exit 1
    elif [ -f "$MXE_PREFIX/qt5/plugins/$1" ]; then
	echo "Copying file $MXE_PREFIX/qt5/plugins/$1"
	cp -f "$MXE_PREFIX/qt5/plugins/$1" "$QPWMC_PREFIX/$1" || exit 1
    fi
}

mkdir -p "$QPWMC_PREFIX" || exit 1

if [ $# -eq 0 ]; then
    while read f; do
	copy_file "$f"
    done
fi

for arg; do
    copy_file $arg
done

for f in *.xml COPYING.rtf mkmsi.bat qpwmc.wxl; do
    cp -f "$f" "$QPWMC_PREFIX"
done
