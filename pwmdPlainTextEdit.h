/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDPLAINTEXTEDIT
#define PWMDPLAINTEXTEDIT

#include <QPlainTextEdit>
#include <QKeyEvent>

class PwmdMainWindow;
class PwmdPlainTextEdit : public QPlainTextEdit
{
 Q_OBJECT
 public:
  PwmdPlainTextEdit (QWidget * parent);
  ~PwmdPlainTextEdit () { };
  void setConfirmOnEnter (bool = true);
  void setPwmdParent (PwmdMainWindow *);

 signals:
  void updateContent ();

 private:
  void insertFromMimeData (const QMimeData *);
  void keyPressEvent (QKeyEvent *);
  void mousePressEvent (QMouseEvent *);

  PwmdMainWindow *_parent;
  bool _confirmOnEnter;
};

#endif
