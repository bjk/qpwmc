/*
    Copyright (C) 2018-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include "applicationFormExpiryButton.h"

ApplicationFormExpiryButton::ApplicationFormExpiryButton (ApplicationFormWidget *w, QWidget *p)
     : QPushButton (p)
{
  _widget = w;
}

ApplicationFormWidget *
ApplicationFormExpiryButton::formWidget ()
{
  return _widget;
}

void
ApplicationFormExpiryButton::setExpiryAge (time_t t)
{
  _widget->setExpiry (_widget->expiryDate (), t);
}

time_t
ApplicationFormExpiryButton::expiryAge ()
{
  return _widget->expiryAge ();
}

void
ApplicationFormExpiryButton::setExpiryDate (time_t t)
{
  _widget->setExpiry (t, _widget->expiryAge ());
}

time_t
ApplicationFormExpiryButton::expiryDate ()
{
  return _widget->expiryDate ();
}
