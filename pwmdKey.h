/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDKEYID
#define PWMDKEYID

#include <QList>
#include <QString>
#include <QTreeWidgetItem>

class PwmdUserId;
class PwmdSubKey;
class PwmdKey
{
public:
  PwmdKey (bool = false, bool = false, bool = false, bool = false,
           bool = false, bool = false, bool = false, bool = false,
           bool = false, bool = false, int = 0, int = 0);
  ~PwmdKey();

  void setUserIds (QList <PwmdUserId *>);
  QList <PwmdUserId *> userIds();
  void setSubkeys (QList <PwmdSubKey *>);
  QList <PwmdSubKey *> subKeys();
  bool isRevoked();
  void setRevoked(bool = true);
  bool isExpired();
  void setExpired(bool = true);
  bool isDisabled();
  void setDisabled(bool = true);
  bool isInvalid();
  void setInvalid(bool = true);
  bool canEncrypt();
  void setCanEncrypt(bool = true);
  bool canSign();
  void setCanSign(bool = true);
  bool canCertify();
  void setCanCertify(bool = true);
  bool isSecret();
  void setSecret(bool = true);
  bool canAuthenticate();
  void setCanAuthenticate(bool = true);
  bool isQualified();
  void setQualified(bool = true);
  int protocol ();
  void setProtocol (int);
  QString issuerSerial ();
  void setIssuerSerial (QString);
  QString issuerName ();
  void setIssuerName (QString);
  QString chainId ();
  void setChainId (QString);
  int ownerTrust();
  void setOwnerTrust (int);

private:
  bool _revoked;
  bool _expired;
  bool _disabled;
  bool _invalid;
  bool _canEncrypt;
  bool _canSign;
  bool _canCertify;
  bool _secret;
  bool _canAuthenticate;
  bool _qualified;
  int _protocol;
  QString _issuerSerial;
  QString _issuerName;
  QString _chainId;
  int _ownerTrust;
  QList <PwmdUserId *> _userIds;
  QList <PwmdSubKey *> _subKeys;
};

class PwmdSubKey : public PwmdKey
{
public:
  PwmdSubKey ();
  ~PwmdSubKey ();

  bool isCardKey();
  void setCardKey(bool = true);
  int pubkeyAlgo();
  void setPubkeyAlgo(int);
  unsigned nBits ();
  void setNBits (unsigned);
  QString keyId();
  void setKeyId(QString);
  QString fingerprint();
  void setFingerprint(QString);
  QString keygrip();
  void setKeygrip(QString);
  long created();
  void setCreated(long);
  long expires();
  void setExpires (long);
  QString cardNumber();
  void setCardNumber (QString);
  QString curve();
  void setCurve(QString);

private:
  bool _isCardKey;
  int _pubkeyAlgo;
  unsigned _nbits;
  QString _keyId;
  QString _fingerprint;
  QString _keygrip;
  long _timestamp;
  long _expires;
  QString _cardNumber;
  QString _eccCurve;
};

class PwmdUserId
{
public:
  PwmdUserId ();
  ~PwmdUserId ();

  bool isRevoked();
  void setRevoked (bool = true);
  bool isInvalid();
  void setInvalid (bool = true);
  int validity();
  void setValidity(int);
  QString userId();
  void setUserId(QString);
  QString name();
  void setName (QString);
  QString email();
  void setEmail (QString);
  QString comment();
  void setComment (QString);

private:
  bool _revoked;
  bool _invalid;
  int _validity;
  QString _userId;
  QString _name;
  QString _email;
  QString _comment;
};

class PwmdKeyItemData
{
public:
  PwmdKeyItemData (PwmdKey *, PwmdSubKey *);
  ~PwmdKeyItemData ();
  PwmdKey *key();
  PwmdSubKey *subKey();
  void setKey (PwmdKey *);
  void setSubKey (PwmdSubKey *);

private:
  PwmdKey *_key;
  PwmdSubKey *_subKey;
};

Q_DECLARE_METATYPE (PwmdKeyItemData *);

#endif
