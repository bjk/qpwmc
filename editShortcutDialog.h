/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef EDITSHORTCUTDIALOG_H
#define EDITSHORTCUTDIALOG_H

#include "ui_editShortcutDialog.h"
#include <libpwmd.h>

class EditShortcutDialog : public QDialog
{
 Q_OBJECT
 public:
  EditShortcutDialog ();
  ~EditShortcutDialog ();

 signals:
  void knownHostRc (gpg_error_t);

 private slots:
  void slotNewShortcut ();
  void slotRemoveShortcut ();
  void slotShortcutItemChanged (QListWidgetItem *, QListWidgetItem *);
  void slotShortcutFilenameChanged (const QString &);
  void slotShortcutNameChanged (const QString &);
  void slotShortcutSubMenuChanged (const QString &);
  void slotShortcutPathChanged (const QString &);
  void slotShortcutSocketChanged (const QString &);
  void slotChooseElement ();
  void slotCancelled ();
  void slotUp ();
  void slotDown ();
  void slotChooseSocket ();
  void slotKnownHostCallback (void *data, const char *host, const char *key,
                              size_t len);

 private:
  Ui::EditShortcutDialog ui;
  bool cancelled;
};

class EditShortcut
{
 public:
  EditShortcut () { };
  EditShortcut (const QString & f, const QString & n, const QString & p,
		const QString & s = 0, const QString &sub = 0)
  {
    _file = f;
    _name = n;
    _path = p;
    _socket = s;
    _subMenu = sub;
  };

  ~EditShortcut () { };

  QString socket ()
  {
    return _socket;
  };

  QString filename ()
  {
    return _file;
  };

  QString name ()
  {
    return _name;
  };

  void setName (const QString & n)
  {
    _name = n;
  };

  QString path ()
  {
    return _path;
  };

  void setPath (const QString & p)
  {
    _path = p;
  };

  void setFilename (const QString & f)
  {
    _file = f;
  };

  void setSocket (const QString & s)
  {
    _socket = s;
  };

  void setSubMenu(const QString &s)
  {
    _subMenu = s;
  };

  QString subMenu()
  {
    return _subMenu;
  };

 private:
  QString _socket;
  QString _file;
  QString _name;
  QString _path;
  QString _subMenu;
};

Q_DECLARE_METATYPE (EditShortcut);

#endif
