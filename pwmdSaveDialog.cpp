/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QSettings>
#include "pwmdSaveDialog.h"

PwmdSaveDialog::PwmdSaveDialog (Pwmd *handle, bool newFile, QWidget *p) :
  QDialog (p)
{
  ui.setupUi (this);
  QSettings cfg ("qpwmc");
  restoreGeometry (cfg.value ("saveDialogGeometry").toByteArray ());
  setWindowFlags (Qt::WindowTitleHint);
  ui.saveWidget->setParent (this);
  ui.saveWidget->setHandle (handle, newFile);

  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Reset);
  pb->disconnect (this);
  pb->setHidden (false);
  connect (pb, SIGNAL (clicked ()), ui.saveWidget, SLOT (slotResetRecipients ()));
  disconnect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(accept()));

  pb = ui.buttonBox->button (QDialogButtonBox::Save);
  connect (pb, SIGNAL (clicked ()), this, SLOT (slotAccepted ()));
  pb->setEnabled (false);
  connect (ui.saveWidget, SIGNAL (saveReady (bool)), this,
           SLOT (slotSaveReady (bool)));
}

PwmdSaveDialog::~PwmdSaveDialog ()
{
  QSettings cfg ("qpwmc");

  cfg.setValue ("saveDialogGeometry", saveGeometry ());
}

void
PwmdSaveDialog::slotSaveReady (bool b)
{
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Save);
  pb->setEnabled (b);
}

void
PwmdSaveDialog::slotAccepted ()
{
  if (ui.saveWidget->isSearching ())
    return;

  gpg_error_t rc = ui.saveWidget->save ();
  
  if (!rc)
    QDialog::accept ();
}

PwmdSaveWidget *
PwmdSaveDialog::widget()
{
  return ui.saveWidget;
}
