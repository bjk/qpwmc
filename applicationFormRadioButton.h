/*
    Copyright (C) 2013 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef APPLICATIONFORMRADIOBUTTON_H
#define APPLICATIONFORMRADIOBUTTON_H

#include <QRadioButton>

class ApplicationFormRadioButton : public QRadioButton
{
  Q_OBJECT
 public:
  ApplicationFormRadioButton (QString);
  void setId (int);
  int id ();

 private:
  int _id;
};

#endif
