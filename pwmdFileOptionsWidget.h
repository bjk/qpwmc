/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDFILEOPTIONSWIDGET_H
#define PWMDFILEOPTIONSWIDGET_H

#include <QFrame>
#include "pwmd.h"
#include "pwmdClientInfo.h"
#include "ui_pwmdFileOptionsWidget.h"

class PwmdFileOptionsDialog;
class PwmdFileOptionsWidget : public QFrame
{
 Q_OBJECT
 public:
  PwmdFileOptionsWidget (QWidget * = 0);
  ~PwmdFileOptionsWidget ();
  void setHandle (Pwmd *);
  void setFilename (QString, bool opened = true);
  bool lock ();
  QString decryptKeyFile ();
  bool updateCacheTimeout ();
  void setLocked (bool);

 signals:
  void clientLockStateChanged (bool);
  void commandsFinished ();

 private slots:
  void slotUpdateCacheTimeout ();
  void slotClearCacheEntry ();
  void slotChangePassword ();
  void slotCacheTimeoutChanged (int);
  void slotStatusMessage (QString, void *);
  void slotCommandResult (PwmdCommandQueueItem *item, QString result,
                          gpg_error_t rc, bool queued);
  void slotLock (int);
  void slotNoTimeoutChanged (int);
  void slotNotCachedChanged (int);

 private:
  void isCachedFinalize (gpg_error_t);
  void cacheTimeoutFinalize (const QString &);
  void updateCacheTimeoutFinalize (gpg_error_t);
  void clearCacheEntryFinalize (gpg_error_t);
  void keyInfoFinalize (gpg_error_t, const QString &);
  void refreshClientInfoFinalize (const QString &);

  Ui::PwmdFileOptionsWidget ui;
  Pwmd *pwm;
  bool _sym;
  bool _sign;
  QString _filename;
  bool _opened;
  QString _decryptKeyFile;
  bool _finishing;
};

#endif
