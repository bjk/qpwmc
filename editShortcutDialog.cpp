/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QSettings>
#include <QCommonStyle>
#include "editShortcutDialog.h"
#include "pwmdSocketDialog.h"
#include "pwmd.h"

EditShortcutDialog::EditShortcutDialog ():QDialog ()
{
  ui.setupUi (this);
  QCommonStyle style;
  setWindowFlags (Qt::WindowTitleHint);
  setParent (0);
  connect (ui.pb_newShortcut, SIGNAL (clicked ()), SLOT (slotNewShortcut ()));
  ui.pb_newShortcut->setIcon (QIcon (":icons/matt-icons_list-add.svg"));
  connect (ui.pb_removeShortcut, SIGNAL (clicked ()),
	   SLOT (slotRemoveShortcut ()));
  ui.pb_removeShortcut->setIcon (QIcon (":icons/matt-icons_list-remove.svg"));
  connect (ui.lb_shortcuts,
	   SIGNAL (currentItemChanged (QListWidgetItem *, QListWidgetItem *)),
	   SLOT (slotShortcutItemChanged
		 (QListWidgetItem *, QListWidgetItem *)));
  connect (ui.le_shortcutName, SIGNAL (textEdited (const QString &)),
	   SLOT (slotShortcutNameChanged (const QString &)));
  connect (ui.le_subMenu, SIGNAL (textEdited (const QString &)),
	   SLOT (slotShortcutSubMenuChanged (const QString &)));
  connect (ui.le_shortcutPath, SIGNAL (textChanged (const QString &)),
	   SLOT (slotShortcutPathChanged (const QString &)));
  connect (ui.le_shortcutFilename, SIGNAL (textChanged (const QString &)),
	   SLOT (slotShortcutFilenameChanged (const QString &)));
  connect (ui.le_socket, SIGNAL (textChanged (const QString &)),
	   SLOT (slotShortcutSocketChanged (const QString &)));
  connect (ui.pb_selectSocket, SIGNAL (clicked ()),
	   SLOT (slotChooseSocket ()));
  connect (ui.pb_selectElement, SIGNAL (clicked ()),
	   SLOT (slotChooseElement ()));
  connect (ui.buttonBox, SIGNAL (rejected ()), this, SLOT (reject ()));
  connect (ui.buttonBox, SIGNAL (accepted ()), this, SLOT (accept ()));

  QSettings
  cfg ("qpwmc");
  int size = cfg.beginReadArray ("shortcuts");

  for (int i = 0; i < size; ++i)
    {
      cfg.setArrayIndex (i);
      QListWidgetItem *item = new QListWidgetItem (ui.lb_shortcuts);
      EditShortcut data = EditShortcut (cfg.value ("filename").toString (),
			     cfg.value ("name").toString (),
			     cfg.value ("path").toString (),
			     cfg.value ("socket").toString (),
			     cfg.value ("subMenu").toString ());

      item->setData (Qt::UserRole, QVariant::fromValue (data));
      item->setText (data.name ());
    }

  cfg.endArray ();
  ui.sp_timeout->setValue (cfg.value ("clipboardTimeout", 20).toInt ());
  ui.sp_linger->setValue (cfg.value ("linger", 0).toInt ());
  ui.ck_closeFile->setChecked (cfg.value ("closeFile", false).toBool ());
  cancelled = false;
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Cancel);
  connect (pb, SIGNAL (clicked ()), this, SLOT (slotCancelled ()));
  ui.tb_up->setIcon (QIcon (":icons/matt-icons_go-up.svg"));
  connect (ui.tb_up, SIGNAL (clicked ()), this, SLOT (slotUp ()));
  ui.tb_down->setIcon (QIcon (":icons/matt-icons_go-down.svg"));
  connect (ui.tb_down, SIGNAL (clicked ()), this, SLOT (slotDown ()));
}

void
EditShortcutDialog::slotUp ()
{
  int row = ui.lb_shortcuts->currentRow ();
  QListWidgetItem *item = ui.lb_shortcuts->takeItem (row);

  ui.lb_shortcuts->insertItem (--row, item);
  ui.lb_shortcuts->setCurrentRow (row);
}

void
EditShortcutDialog::slotDown ()
{
  int row = ui.lb_shortcuts->currentRow ();
  QListWidgetItem *item = ui.lb_shortcuts->takeItem (row);

  ui.lb_shortcuts->insertItem (++row, item);
  ui.lb_shortcuts->setCurrentRow (row);
}

void
EditShortcutDialog::slotCancelled ()
{
  cancelled = true;
}

void
EditShortcutDialog::slotChooseSocket ()
{
  PwmdSocketDialog *d = new PwmdSocketDialog (ui.le_socket->text (), this);
  if (d->exec ())
    ui.le_socket->setText (d->socket ());

  delete d;
}

void
EditShortcutDialog::slotKnownHostCallback (void *data, const char *host,
                                           const char *key, size_t len)
{
  gpg_error_t rc = Pwmd::knownHostPrompt (data, host, key, len);
  emit knownHostRc (rc);
}

void
EditShortcutDialog::slotChooseElement ()
{
  QString result;
  QListWidgetItem *item = ui.lb_shortcuts->currentItem ();
  EditShortcut data = item->data (Qt::UserRole).value < EditShortcut > ();
  Pwmd *pwm = new Pwmd (data.filename (), "qpwmc", data.socket (), this);

  if (pwm->spawnEditor (result, data.socket(), data.filename(),
                        data.path ().replace ("<TAB>", "\t", Qt::CaseInsensitive),
                        false))
    {
      int n;
      QString s = Pwmd::extractToken (result, "<SOCKET>", n);

      data.setSocket (s);
      ui.le_socket->setText (s);

      s = Pwmd::extractToken (result, "<FILE>", n);
      data.setFilename (s);
      ui.le_shortcutFilename->setText (s);

      s = Pwmd::extractToken (result, "<PATH>", n);
      data.setPath (s);
      ui.le_shortcutPath->setText (s.replace ("\t", "<TAB>"));

      item->setData (Qt::UserRole, QVariant::fromValue (data));
    }

  delete pwm;
}

EditShortcutDialog::~EditShortcutDialog ()
{
  if (cancelled)
    return;

  QSettings cfg ("qpwmc");
  QList < QListWidgetItem * >items;

  items = ui.lb_shortcuts->findItems ("*", Qt::MatchWildcard);
  cfg.beginWriteArray ("shortcuts");

  for (int i = 0; i < items.size (); i++)
    {
      QListWidgetItem *item = items.at (i);
      EditShortcut data = item->data (Qt::UserRole).value < EditShortcut > ();

      cfg.setArrayIndex (i);
      cfg.setValue ("name", data.name ());
      cfg.setValue ("subMenu", data.subMenu ());
      cfg.setValue ("socket", data.socket ());
      cfg.setValue ("filename", data.filename ());
      cfg.setValue ("path",
		    data.path ().replace ("<TAB>", "\t", Qt::CaseInsensitive));
    }

  cfg.endArray ();
  cfg.setValue ("clipboardTimeout", ui.sp_timeout->value ());
  cfg.setValue ("linger", ui.sp_linger->value ());
  cfg.setValue ("closeFile", ui.ck_closeFile->isChecked ());
}

void
EditShortcutDialog::slotShortcutNameChanged (const QString & s)
{
  QListWidgetItem *item = ui.lb_shortcuts->currentItem ();
  if (!item)
    return;

  EditShortcut data = item->data (Qt::UserRole).value < EditShortcut > ();
  data.setName (s);
  item->setData (Qt::UserRole, QVariant::fromValue (data));
  item->setText (s);
}

void
EditShortcutDialog::slotShortcutSubMenuChanged (const QString & s)
{
  QListWidgetItem *item = ui.lb_shortcuts->currentItem ();
  if (!item)
    return;

  EditShortcut data = item->data (Qt::UserRole).value < EditShortcut > ();
  data.setSubMenu (s);
  item->setData (Qt::UserRole, QVariant::fromValue (data));
}

void
EditShortcutDialog::slotShortcutPathChanged (const QString & s)
{
  QListWidgetItem *item = ui.lb_shortcuts->currentItem ();
  if (!item)
    return;

  EditShortcut data = item->data (Qt::UserRole).value < EditShortcut > ();
  data.setPath (s);
  item->setData (Qt::UserRole, QVariant::fromValue (data));
}

void
EditShortcutDialog::slotShortcutFilenameChanged (const QString & s)
{
  QListWidgetItem *item = ui.lb_shortcuts->currentItem ();
  if (!item)
    return;

  EditShortcut data = item->data (Qt::UserRole).value < EditShortcut > ();
  data.setFilename (s);
  item->setData (Qt::UserRole, QVariant::fromValue (data));
}

void
EditShortcutDialog::slotShortcutSocketChanged (const QString & s)
{
  QListWidgetItem *item = ui.lb_shortcuts->currentItem ();
  if (!item)
    return;

  EditShortcut data = item->data (Qt::UserRole).value < EditShortcut > ();
  data.setSocket (s);
  item->setData (Qt::UserRole, QVariant::fromValue (data));
}

void
EditShortcutDialog::slotNewShortcut ()
{
  ui.lb_shortcuts->addItem (new QListWidgetItem (tr ("New shortcut")));
  ui.lb_shortcuts->setCurrentItem (ui.lb_shortcuts->
				   item (ui.lb_shortcuts->count () - 1));
  ui.le_shortcutName->setFocus (Qt::MouseFocusReason);
}

void
EditShortcutDialog::slotRemoveShortcut ()
{
  if (!ui.lb_shortcuts->currentItem ())
    return;

  QListWidgetItem *item =
    ui.lb_shortcuts->takeItem (ui.lb_shortcuts->
			       row (ui.lb_shortcuts->currentItem ()));
  delete item;
}

void
EditShortcutDialog::slotShortcutItemChanged (QListWidgetItem * item,
					     QListWidgetItem * old)
{
  (void) old;

  ui.fr_details->setEnabled (item != nullptr);
  ui.tb_up->setEnabled (item != nullptr);
  ui.tb_down->setEnabled (item != nullptr);

  if (!item)
    {
      ui.le_shortcutName->setText ("");
      ui.le_shortcutPath->setText ("");
      ui.le_shortcutFilename->setText ("");
      ui.le_socket->setText ("");
      ui.le_subMenu->setText ("");
      return;
    }

  if (ui.lb_shortcuts->row (item) == ui.lb_shortcuts->count () - 1)
    ui.tb_down->setEnabled (false);
  else
    ui.tb_down->setEnabled (true);

  if (ui.lb_shortcuts->row (item) == 0)
    ui.tb_up->setEnabled (false);
  else
    ui.tb_up->setEnabled (true);

  EditShortcut data = item->data (Qt::UserRole).value < EditShortcut > ();
  ui.le_shortcutFilename->setText (data.filename ());
  ui.le_shortcutName->setText (data.name ());
  ui.le_socket->setText (data.socket ());
  ui.le_subMenu->setText (data.subMenu ());
  ui.le_shortcutPath->setText (data.path ().
			       replace ("\t", "<TAB>", Qt::CaseInsensitive));
}
