/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QApplication>
#include <QDir>
#include <QCommonStyle>
#include <QDateTime>
#include <QSettings>
#include <QTimer>
#include <QMessageBox>
#include <QClipboard>
#include <QScroller>
#include <QShortcut>

#include "pwmdSaveDialog.h"
#include "pwmdSaveWidget.h"
#include "cmdIds.h"
#include "pwmdPasswordDialog.h"
#include "pwmdKeyGenDialog.h"

static bool searching;

PwmdSaveWidget::PwmdSaveWidget (QWidget *p) : QFrame (p)
{
  ui.setupUi (this);
  QCommonStyle style;
  pwm = nullptr;
  newFile = false;
  _recipients = QString ();
  _signers = QString ();
  _needsRefresh = true;
  newSigners = QString ();
  newRecipients = QString ();
  currentKeyListIndex = 0;
  selectSignKeyId = false;
  QSettings cfg ("qpwmc");
  keyFile = QString ();
  encryptKeyFile = QString ();
  signKeyFile = QString ();
  init = true;
  generatedKeyId = QString ();

  ui.keyList->setParent(this);
  ui.recipientList->setParent(this);
  ui.recipientList->setIsRecipientList();
  connect (ui.tb_refreshKeyList, SIGNAL (clicked()), this, SLOT(slotRefreshKeyList()));
  ui.tb_refreshKeyList->setIcon (QIcon (":icons/matt-icons_view-refresh.svg"));
  connect (ui.keyList, SIGNAL (currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)), this, SLOT (slotKeyIdChanged (QTreeWidgetItem *, QTreeWidgetItem *)));
  connect (ui.keyList, SIGNAL (itemSelectionChanged ()), this, SLOT (slotKeySelectionChanged ()));
  connect (ui.recipientList, SIGNAL (currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)), this, SLOT (slotKeyIdChanged (QTreeWidgetItem *, QTreeWidgetItem *)));
  QStringList list;
  list << tr("Key ID") << tr("Validity") << tr("Trust") << tr ("Flags")
    << tr ("User ID");
  ui.keyList->setHeaderLabels(list);
  ui.recipientList->setHeaderLabels(list);
  ui.keyList->header()->restoreState(cfg.value ("keyListHeaderState").toByteArray());
  ui.recipientList->header()->restoreState(cfg.value ("keyListHeaderState").toByteArray());

  connect (ui.keyList->header(), SIGNAL(sectionResized(int, int, int)), this, SLOT (slotHeaderSectionResized(int, int, int)));

  connect (ui.pb_generate, SIGNAL (clicked ()), this, SLOT (slotGenerate ()));
  connect (ui.ck_symmetric, SIGNAL (stateChanged (int)), this, SLOT (slotSymmetricChanged(int)));
  connect (ui.ck_secretOnly, SIGNAL (stateChanged (int)), this, SLOT (slotSecretOnlyChanged (int)));
  ui.tb_addRecipient->setIcon (QIcon (":icons/matt-icons_list-add.svg"));
  connect (ui.tb_addRecipient, SIGNAL (clicked()), this, SLOT (slotAddRecipients()));
  ui.tb_removeRecipient->setIcon (QIcon (":icons/matt-icons_list-remove.svg"));
  connect (ui.tb_removeRecipient, SIGNAL (clicked()), this, SLOT (slotRemoveRecipients()));

  QShortcut *sc = new QShortcut (QKeySequence ("Ctrl+F"), this);
  connect (sc, SIGNAL (activated ()), this, SLOT (slotFindKeyActivated ()));
  connect (ui.le_searchKeyList, SIGNAL (textChanged (const QString &)), this,
           SLOT (slotSearchKeyListChanged (const QString &)));
  connect (ui.le_searchKeyList, SIGNAL (returnPressed()), this,
           SLOT (slotSearchKeyList()));
  connect (ui.tb_searchKeyList, SIGNAL (clicked()), this,
           SLOT (slotSearchKeyList()));

  connect (ui.tb_learn, SIGNAL (clicked()), this, SLOT (slotLearn()));
  ui.tb_learn->setIcon (QIcon (":icons/rodentia-icons_emblem-information.svg"));

  connect (ui.pb_keyFileOptions, SIGNAL (clicked()), this, SLOT (slotSelectKeyFile()));

  connect (ui.tb_delete, SIGNAL (clicked ()), SLOT (slotDeleteKey ()));
  ui.tb_delete->setIcon (QIcon (":icons/rodentia-icons_user-trash-full.svg"));
  ui.tb_delete->setEnabled (false);
  ui.tb_removeRecipient->setEnabled (false);
  ui.tb_addRecipient->setEnabled (false);
  QScroller::grabGesture(ui.scrollArea, QScroller::TouchGesture);
}

PwmdSaveWidget::~PwmdSaveWidget ()
{
  QSettings cfg ("qpwmc");

  cfg.setValue ("keyListHeaderState", ui.keyList->header()->saveState());
  clearKeyList ();
}

bool
PwmdSaveWidget::isSearching ()
{
  return ui.le_searchKeyList->hasFocus ();
}

void
PwmdSaveWidget::slotKeySelectionChanged ()
{
  QList <QTreeWidgetItem *> list = ui.keyList->selectedItems ();

  ui.tb_delete->setEnabled (!(list.isEmpty () || list.size () > 1));
}

void
PwmdSaveWidget::slotStatusMessage (QString line, void *user)
{
  (void)user;
  QStringList l = QString (line).split (" ");

  if (l.at (0) == "GENKEY" && l.size () == 3)
    {
      generatedKeyId = l.at(2);
    }
}

void
PwmdSaveWidget::slotGenerate ()
{
  PwmdKeyGenDialog d (this, pwm);
  d.exec ();
}

void
PwmdSaveWidget::slotLearn ()
{
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdSaveLearn, "KEYINFO", nullptr,
                                          new PwmdInquireData ("--learn")));
}

void
PwmdSaveWidget::slotCopyKeyIdToClipboard ()
{
  QClipboard *c = QApplication::clipboard ();
  QTreeWidgetItem *item = ui.keyList->currentItem ();
  PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));

  if (c->supportsSelection ())
    c->setText (data->subKey()->keyId (), QClipboard::Selection);
  c->setText (data->subKey()->keyId ());
}

void
PwmdSaveWidget::slotCopyRecipientKeyIdToClipboard ()
{
  QClipboard *c = QApplication::clipboard ();
  QTreeWidgetItem *item = ui.recipientList->currentItem ();
  PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));

  if (c->supportsSelection ())
    c->setText (data->subKey()->keyId (), QClipboard::Selection);
  c->setText (data->subKey()->keyId ());
}

void
PwmdSaveWidget::slotDeleteKey ()
{
  QList <QTreeWidgetItem *> list = ui.keyList->selectedItems ();
  PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(list.at (0)->data (0, Qt::UserRole));
  QString key = QString ("User ID: %1\nFingerprint: %2").arg (data->key()->userIds ().at(0)->userId(), data->subKey ()->fingerprint ());
  int b = QMessageBox::question (this, tr ("Delete key pair?"),
                                 QString (tr ("Do you really want to delete the following public and private keys from the keyring?\n\n%1").arg (key)),
                                 QMessageBox::Yes | QMessageBox::No,
                                 QMessageBox::No);

  if (b != QMessageBox::Yes)
    return;

  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdSaveDeleteKey,
                                                         "DELETEKEY", nullptr,
                                                         new PwmdInquireData (data->subKey ()->fingerprint ()));
  pwm->command (item);
}

void
PwmdSaveWidget::slotCommandResult (PwmdCommandQueueItem *item,
                                   QString result, gpg_error_t rc, bool queued)
{
  PwmdInquireData *inq = item->data ();

  if ((item->id () >= 0 && item->id () <= PwmdCmdIdOpenMax)
      || item->id () >= PwmdCmdIdSaveMax)
    {
      item->setSeen ();
      return;
    }

  switch (item->id ())
    {
    case PwmdCmdIdInternalConnect:
    case PwmdCmdIdInternalOpen:
    case PwmdCmdIdInternalPassword:
      item->setSeen ();
      return;
    case PwmdCmdIdInternalSave:
      (void)pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 0);
      (void)pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 0);
      saveFinalize ();
      break;
    case PwmdCmdIdSaveKeyInfo:
      if (!rc)
        updateCurrentKeyInfoFinalize (result);
      break;
    case PwmdCmdIdInternalGenKey:
      (void)pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 0);
      (void)pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 0);
      if (!rc)
        refreshKeyLists (true);
      else
        {
          item->setSeen ();
          return;
        }
      break;
    case PwmdCmdIdSaveLearn:
      if (!rc)
        refreshKeyLists (true);
      break;
    case PwmdCmdIdSaveListKeys:
      if (!rc)
        getKeyListFinalize (result, inq->userBool());
      break;
    case PwmdCmdIdSaveDeleteKey:
      if (!rc)
        refreshKeyLists (true);
      break;
    default:
      break;
    }

  if (rc && !item->checkError (rc) && !queued)
    Pwmd::showError (rc, pwm);

  item->setSeen ();
}

void
PwmdSaveWidget::slotSelectKeyFile ()
{
  PwmdPasswordDialog *d = new PwmdPasswordDialog (this);
 
  slotConfirmKeySelection ();
  d->setSymmetric (ui.ck_symmetric->isChecked ());
  d->setHasSigners (!newSigners.isEmpty ());
  d->setNewFile (newFile);
  d->setDecryptKeyFile (keyFile);
  d->setEncryptKeyFile (encryptKeyFile);
  d->setSignKeyFile (signKeyFile);

  if (d->exec () && d->useKeyFile ())
    {
      keyFile = d->decryptKeyFile ();
      encryptKeyFile = d->encryptKeyFile ();
      signKeyFile = d->signKeyFile ();
    }
  else
    keyFile = encryptKeyFile = signKeyFile = QString ();

  delete d;
}

void
PwmdSaveWidget::slotSecretOnlyChanged (int s)
{
  updateKeyList (s == Qt::Checked);
}

void
PwmdSaveWidget::resetRecipients ()
{
  newRecipients = QString ();
  newSigners = QString ();
  clearRecipientList ();
}

void
PwmdSaveWidget::slotResetRecipients ()
{
  if (keyList.count() && (recipients().isEmpty () || signers().isEmpty()))
    updateCurrentKeyInfo ();

  setKeyListRecipients ();
}

void
PwmdSaveWidget::slotSymmetricChanged (int s)
{
  bool b = s == Qt::Checked ? true : false;

  ui.ck_secretOnly->setChecked (b);
  ui.ck_secretOnly->setEnabled (!b);
  ui.pb_keyFileOptions->setEnabled (ui.recipientList->topLevelItemCount () || b);
  emit saveReady (testRecipients ());
}

void
PwmdSaveWidget::reset ()
{
}

bool
PwmdSaveWidget::needsRefresh ()
{
  return _needsRefresh;
}

void
PwmdSaveWidget::setNeedsRefresh (bool b)
{
  _needsRefresh = b;
}

void
PwmdSaveWidget::refreshKeyLists (bool force)
{
  slotResetRecipients ();

  if (needsRefresh () || force)
    {
      _recipients = QString ();
      _signers = QString ();
      updateCurrentKeyInfo ();
      getKeyList (QString (), force, ui.ck_symmetric->isChecked ());
    }
}

void
PwmdSaveWidget::showEvent (QShowEvent *ev)
{
  if (!ev->spontaneous())
    refreshKeyLists (false);

  QFrame::showEvent (ev);
}

void
PwmdSaveWidget::updateCurrentKeyInfoFinalize (const QString &result)
{
  QStringList list = result.split ("\n");
  bool sign = false;
  QString str;

  for (int n = 0; n < list.count(); n++)
    {
      if (list.at(n).at(0) == 'S' && !sign)
        {
          if (!str.isEmpty())
            str.chop(1);

          _recipients = str;
          sign = true;
          str = QString ();
        }

      if (sign)
        str.append (list.at(n).mid(1)+",");
      else
        str.append (list.at(n)+",");
    }

  if (!str.isEmpty())
    str.chop(1);

  if (sign)
    _signers = str;
  else
    _recipients = str;

  setSymmetric ();
  setNeedsRefresh (false);
}

void
PwmdSaveWidget::updateCurrentKeyInfo ()
{
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdSaveKeyInfo, "KEYINFO",
                                          nullptr, new PwmdInquireData ()));
}

void
PwmdSaveWidget::slotHeaderSectionResized (int idx, int old, int newSize)
{
  (void)idx;
  (void)old;
  (void)newSize;
  QByteArray b = ui.keyList->header()->saveState();

  ui.recipientList->header()->restoreState (b);
}

void
PwmdSaveWidget::keyPressEvent (QKeyEvent *ev)
{
  QFrame::keyPressEvent (ev);
}

void
PwmdSaveWidget::setSymmetric ()
{
  if (recipients().isEmpty ())
    {
      ui.ck_symmetric->setHidden (!newFile);
      ui.ck_symmetric->setChecked (!newFile);
    }
  else
    {
      ui.ck_symmetric->setHidden (!newFile);
      ui.ck_symmetric->setChecked (false);
    }
}

void
PwmdSaveWidget::setIsNew (bool b)
{
  newFile = b;
  setSymmetric ();
}

void
PwmdSaveWidget::setHandle (Pwmd *p, bool isNewFile)
{
  if (pwm)
    disconnect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));

  pwm = p;
  connect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));
  connect (pwm, SIGNAL (statusMessage (QString, void *)), this,
	   SLOT (slotStatusMessage (QString, void *)));
  setIsNew (isNewFile);
}

void
PwmdSaveWidget::saveFinalize ()
{
  newFile = false;
  _signers = newSigners;
  _recipients = newRecipients;
  updateCurrentKeyInfo ();
  setKeyListRecipients ();
}

gpg_error_t
PwmdSaveWidget::save (const QString &args, bool fromWidget)

{
  gpg_error_t rc = 0;
  PwmdInquireData *inq = new PwmdInquireData (pwm->handle (), pwm->filename ());
  QString saveArgs = args;
  bool useKeyFile = false;

  if (fromWidget)
    {
      pwm->save (saveArgs, Pwmd::inquireCallback, inq);
      return 0;
    }

  slotConfirmKeySelection ();

  if (!keyFile.isEmpty () || !encryptKeyFile.isEmpty ()
      || !signKeyFile.isEmpty())
    useKeyFile = true;

  if (ui.ck_symmetric->isChecked () && ui.ck_symmetric->isEnabled ())
    saveArgs.append (" --symmetric");

  if (!newRecipients.isEmpty())
    saveArgs.append(QString(" --keyid=%1").arg (newRecipients));

  if (!newSigners.isEmpty())
    saveArgs.append(QString(" --sign-keyid=%1").arg (newSigners));
  else if (newSigners.isEmpty() && ui.ck_symmetric->isChecked() && !init)
    saveArgs.append(QString(" --sign-keyid="));

  if (useKeyFile)
    {
      pwmd_socket_t type;

      pwmd_socket_type (pwm->handle (), &type);
      if (type != PWMD_SOCKET_LOCAL || useKeyFile)
        {
          if (useKeyFile)
            {
              inq->setKeyFile (keyFile);
              inq->setEncryptKeyFile (encryptKeyFile);
              inq->setSignKeyFile (signKeyFile);
            }

          rc = pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 1);
          if (!rc)
            rc = pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 1);
        }
      if (rc)
        {
          reset ();
          delete inq;
          return rc;
        }

      pwm->save (saveArgs, Pwmd::inquireCallback, inq);
    }
  else
    pwm->save (saveArgs, Pwmd::inquireCallback, inq);

  return 0;
}

static QString
keyFlags (PwmdKey *key)
{
  QString s;

  if (key->canEncrypt())
    s.append ("e");

  if (key->canSign())
    s.append ("s");

  if (key->canCertify())
    s.append ("c");

  if (key->canAuthenticate())
    s.append("a");

  return s;
}

static int
currentKeyListItemIndex (QTreeWidget *w, QTreeWidgetItem *item)
{
  int i;
  QList < QTreeWidgetItem * >list = w->findItems ("*", Qt::MatchWildcard | Qt::MatchRecursive);

  for (i = 0; i < list.count(); i++)
    {
      if (list.at(i) == item)
        return i;
    }

  return -1;
}

static QString
openpgp_algorithm (PwmdSubKey *key)
{
  switch (key->pubkeyAlgo())
    {
    case 1:
    case 2:
    case 3:
      return "RSA";
    case 8:
      return "KYBER";
    case 16:
    case 20:
      return "ELG";
    case 17:
      return "DSA";
    case 301:
    case 302:
    case 18:
      return "ECC";
    default:
      break;
    }

  return QApplication::tr("unknown");
}

void
PwmdSaveWidget::slotKeyIdChanged (QTreeWidgetItem * item,
                                  QTreeWidgetItem * old)
{
  (void) old;

  if (!item)
    {
      if (old && old->treeWidget () == ui.keyList)
        ui.tb_addRecipient->setEnabled (false);
      else if (old && old->treeWidget () == ui.recipientList)
        ui.tb_removeRecipient->setEnabled (false);
      return;
    }

  ui.pb_keyFileOptions->setEnabled (ui.recipientList->topLevelItemCount ()
                                    || ui.ck_symmetric->isChecked ());
  PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
  ui.l_fingerprint->setText(data->subKey()->fingerprint());
  ui.l_keygrip->setText(data->subKey()->keygrip());

  QString cn = data->subKey()->cardNumber();
  ui.l_cardNumber->setText(cn.isEmpty () ? "-" : cn);

  QDateTime t = QDateTime::fromSecsSinceEpoch (data->subKey()->created());
  ui.l_created->setText(t.toString("MM/dd/yyyy hh:mm:ss"));

  if (data->subKey()->expires())
    {
      t = QDateTime::fromSecsSinceEpoch (data->subKey()->expires());
      ui.l_expires->setText(t.toString("MM/dd/yyyy hh:mm:ss"));
    }
  else
      ui.l_expires->setText("-");

  if (data->subKey()->curve().isEmpty ())
    ui.l_algorithm->setText (QString ("%1-%2").arg (openpgp_algorithm(data->subKey())).arg (data->subKey()->nBits()));
  else
    ui.l_algorithm->setText (QString ("%1-%2-%3").arg (openpgp_algorithm(data->subKey())).arg (data->subKey()->nBits()).arg (data->subKey()->curve()));

  if (item->treeWidget () == ui.keyList)
    {
      if (ui.ck_symmetric->isChecked () && !data->subKey()->canSign ())
        ui.tb_addRecipient->setEnabled (false);
      else
        ui.tb_addRecipient->setEnabled (true);
    }
  else if (item->treeWidget () == ui.recipientList)
    ui.tb_removeRecipient->setEnabled (true);

  ui.keyList->scrollToItem (item);
  
  if (!searching)
    currentKeyListIndex = currentKeyListItemIndex (ui.keyList,
                                                   ui.keyList->currentItem ());
}

void
PwmdSaveWidget::slotConfirmKeySelection ()
{
  QList <QTreeWidgetItem *> list;

  list = ui.recipientList->findItems ("*", Qt::MatchWildcard);
  int i;
  QString signer, recipient;

  for (i = 0; i < list.count(); i++)
    {
      QTreeWidgetItem *item = list.at(i);
      PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
      PwmdSubKey *s = data->subKey();

      if (s->canSign())
        signer.append (s->fingerprint()+",");

      if (s->canEncrypt())
        recipient.append (s->keyId()+",");
    }

  if (signer.size())
    signer.chop(1);

  if (recipient.size())
    recipient.chop(1);

  newSigners = signer;
  newRecipients = recipient;
}

void
PwmdSaveWidget::slotRefreshKeyList ()
{
  ui.l_created->setText (tr("-"));
  ui.l_expires->setText (tr("-"));
  ui.l_fingerprint->setText (tr("-"));
  ui.l_keygrip->setText (tr("-"));
  ui.l_cardNumber->setText (tr("-"));
  getKeyList (QString (), true, selectSignKeyId);
  updateCurrentKeyInfo ();
}

QTreeWidgetItem *
PwmdSaveWidget::searchKeyList (const QString &str, int &index, bool next,
                               bool wrapAround)
{
  QList < QTreeWidgetItem * >items = ui.keyList->findItems ("*", Qt::MatchWildcard | Qt::MatchRecursive);
  int i, x;
  QTreeWidgetItem *item = nullptr;

  if (index >= items.count())
    x = 0;
  else
    x = index < 0 ? 0 : index;

  for (i = x; i < items.count(); i++)
    {
      item = items.at(i);
      if (item->isHidden())
        continue;

      PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
      int n;

      for (n = 0; n < data->key()->userIds().count(); n++)
        {
          if (data->key()->userIds().at(n)->userId().contains (str, Qt::CaseInsensitive)
              && ui.keyList->indexOfTopLevelItem (item) != -1)
            goto done;
          else if (data->key()->userIds().at(n)->comment().contains (str, Qt::CaseInsensitive)
                   && ui.keyList->indexOfTopLevelItem (item) != -1)
            goto done;
        }

      if (data->subKey()->fingerprint().contains (str, Qt::CaseInsensitive))
        goto done;
      if (data->subKey()->keygrip().contains (str, Qt::CaseInsensitive))
        goto done;
      else if (data->subKey()->keyId().contains (str, Qt::CaseInsensitive))
        goto done;
    }

done:
  if (i < items.count())
    index = i+1;
  else if (next && !wrapAround)
    {
      x = 0;
      item = searchKeyList (str, x, next, true);
      if (item)
        index = x;
      return item;
    }

  return i < items.count() ? item : nullptr;
}

void
PwmdSaveWidget::slotSearchKeyListChanged (const QString &str)
{
  ui.tb_searchKeyList->setEnabled (!str.isEmpty ());
  if (str.isEmpty())
    return;

  int n = currentKeyListIndex;
  QTreeWidgetItem *item = searchKeyList (str, n);
  if (!item)
    return;

  ui.keyList->setCurrentItem (item);
  ui.keyList->scrollToItem (item);
}

void
PwmdSaveWidget::slotFindKeyActivated ()
{
  ui.le_searchKeyList->setFocus ();
  if (ui.le_searchKeyList->text ().isEmpty ())
    return;

  ui.le_searchKeyList->setSelection (0, ui.le_searchKeyList->text ().length ());
}

void
PwmdSaveWidget::slotSearchKeyList ()
{
  if (ui.le_searchKeyList->text ().isEmpty ())
    return;

  QTreeWidgetItem *item = searchKeyList (ui.le_searchKeyList->text(),
                                         currentKeyListIndex, true);
  if (!item)
    return;

  // Fixes slotKeyIdChanged changing currentKeyListIndex.
  searching = true;
  ui.keyList->setCurrentItem (item);
  ui.keyList->scrollToItem (item);
  searching = false;
}

static QString
ownerTrust (PwmdKey *key)
{
  if (key->protocol() != 0) // GPGME_PROTOCOL_OPENPGP
    return QString ();

  switch (key->ownerTrust())
    {
    case 0:
      return QApplication::tr("Unknown");
    case 1:
      return QApplication::tr("Undefined");
    case 2:
      return QApplication::tr("Never");
    case 3:
      return QApplication::tr("Marginal");
    case 4:
      return QApplication::tr("Full");
    case 5:
      return QApplication::tr("Ultimate");
    }

  return QString ();
}

static QString
validity (PwmdKey *key)
{
  QString s = QString ();

  if (key->isRevoked())
    s = QApplication::tr("Revoked");
  else if (key->isExpired())
    s = QApplication::tr("Expired");
  else if (key->isInvalid())
    s = QApplication::tr("Invalid");
  else if (key->isDisabled())
    s = QApplication::tr("Disabled");
  else
    s = QApplication::tr ("Valid");

  return s;
}

void
PwmdSaveWidget::createSubKeyItemOnce (QTreeWidgetItem *item, QString keyId,
                                      PwmdKey *key, QString uids)
{
  item->setText (0, QString ("0x")+keyId.right (8));
  item->setText (1, validity (key));
  item->setText (2, ownerTrust (key));
  item->setText (3, keyFlags (key));
  item->setText (4, uids);
}

static QString
buildKeyUserIds (PwmdKey *key)
{
  QString uids;
  int i;

  for (i = 0; i < key->userIds().count(); i++)
    uids += key->userIds().at(i)->userId() + "\n";

  uids.chop(1);
  return uids;
}

void
PwmdSaveWidget::createSubKeyItem (PwmdKey *key)
{
  int i;
  QString uids;
  QTreeWidgetItem *parent = nullptr;
  PwmdSubKey *subKey = key->subKeys().at(0);
  QTreeWidgetItem *item = new QTreeWidgetItem (parent);
  PwmdKeyItemData *data = new PwmdKeyItemData (key, subKey);

  uids = buildKeyUserIds (key);
  item->setTextAlignment (0, Qt::AlignTop);
  item->setTextAlignment (1, Qt::AlignTop);
  item->setTextAlignment (2, Qt::AlignTop);
  item->setTextAlignment (3, Qt::AlignTop);
  item->setTextAlignment (4, Qt::AlignTop);
  createSubKeyItemOnce (item, subKey->keyId(), key, uids);
  item->setData (0, Qt::UserRole, QVariant::fromValue (data));
  ui.keyList->addTopLevelItem (item);
  parent = item;

  for (i = 0; i < key->subKeys().count(); i++)
    {
      subKey = key->subKeys().at(i);
      item = new QTreeWidgetItem (parent);
      data = new PwmdKeyItemData (key, subKey);
      item->setTextAlignment (0, Qt::AlignTop);
      item->setTextAlignment (1, Qt::AlignTop);
      item->setTextAlignment (2, Qt::AlignTop);
      item->setTextAlignment (3, Qt::AlignTop);
      createSubKeyItemOnce (item, subKey->keyId(), subKey);
      item->setData (0, Qt::UserRole, QVariant::fromValue (data));
    }
}

void
PwmdSaveWidget::setKeyListRecipients ()
{
  clearRecipientList ();
  doSetKeyListRecipients (recipients());
  doSetKeyListRecipients (signers());
  emit saveReady (testRecipients ());
}

void
PwmdSaveWidget::doSetKeyListRecipients (QString keys)
{
  QStringList r;
  QList <QTreeWidgetItem *> list = ui.keyList->findItems ("*",
                                                          Qt::MatchWildcard);
  int i;
 
  r = keys.split (",");
  r.removeDuplicates();

  for (i = 0; i < r.count(); i++)
    {
      int n;

      if (r.at(i).isEmpty())
        continue;

      for (n = 0; n < list.count(); n++)
        {
          QTreeWidgetItem *item = list.at(n);
          PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
          QString keyId = data->subKey()->keyId ();
          QString fpr = data->subKey()->fingerprint ();
          int c;

          if (keyId.right (8).toLower () == r.at(i).right (8).toLower ()
              || fpr.right (8).toLower () == r.at(i).right (8).toLower ())
            {
              addRecipient (data);
              break;
            }

          for (c = 0; c < item->childCount(); c++)
            {
              QTreeWidgetItem *child = item->child(c);

              data = qvariant_cast <PwmdKeyItemData * >(child->data (0, Qt::UserRole));
              keyId = data->subKey()->keyId ();
              fpr = data->subKey()->fingerprint ();

              if (keyId.right (8).toLower () == r.at(i).right (8).toLower ()
                  || fpr.right (8).toLower () == r.at(i).right (8).toLower ())
                {
                  addRecipient (data);
                  break;
                }
            }
        }
    }
}

void
PwmdSaveWidget::updateKeyList (bool secret)
{
  QList <QTreeWidgetItem *> list = ui.keyList->findItems ("*",
                                                          Qt::MatchWildcard|Qt::MatchRecursive);
  int n;
  
  selectSignKeyId = secret;

  for (n = 0; n < list.count(); n++)
    {
      QTreeWidgetItem *item = list.at(n);
      PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
      Qt::ItemFlags flags = item->flags();
      bool hasSecret = data->key()->isSecret();
      bool revoked = data->key()->isRevoked() || data->subKey()->isRevoked();

      if (!data->key()->isSecret() && secret)
        item->setHidden (true);
      else if (!secret)
        item->setHidden (false);

      if (data->key()->canEncrypt() || data->subKey()->canEncrypt()
          || ((data->key()->canSign() || data->subKey()->canSign()) && hasSecret))
        flags |= Qt::ItemIsSelectable;
      else
        flags &= ~Qt::ItemIsSelectable;

      if (revoked)
        flags &= ~Qt::ItemIsSelectable;

      item->setFlags (flags);
    }

  setKeyListRecipients ();
  if (ui.recipientList->topLevelItemCount ())
    {
      QTreeWidgetItem *item = ui.recipientList->topLevelItem (0);
      PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
      slotJumpToKey (data->subKey()->fingerprint ());
      ui.recipientList->setCurrentItem (item);
      ui.recipientList->scrollToItem (item);
    }
}

static QString
openpgp_unescape (QString str)
{
  str.replace (QString("\\x3a"), QString(":"));
  str.replace ("\\x5c", "\\");
  return str;
}

void
PwmdSaveWidget::clearRecipientList ()
{
  ui.recipientList->clear ();
}

void
PwmdSaveWidget::clearKeyList ()
{
  while (!keyList.isEmpty ())
    {
      PwmdKey *key = keyList.takeFirst ();
      delete key;
    }

  foreach (QTreeWidgetItem *item,
           ui.keyList->findItems ("*", Qt::MatchWildcard | Qt::MatchRecursive))
    {
      PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
      data->setKey (nullptr);
      data->setSubKey (nullptr);
      delete data;
    }

  ui.keyList->clear ();
  clearRecipientList ();
}

void
PwmdSaveWidget::getKeyListFinalize (const QString &result, bool secret)
{
  QStringList lines;
  int l;
  QList <PwmdKey *> newKeys;

  if (result.isEmpty())
    return;

  lines = result.split ("\n");
  for (l = 0; l < lines.count(); l++)
    {
      PwmdKey *key = new PwmdKey();
      QStringList fields = lines.at(l).split (":");
      int f, i;
      QList <PwmdSubKey *> subKeys;
      QList <PwmdUserId *> userIds;
      unsigned n_subKeys = 0, s;

      for (f = i = 0; i < 17; i++, f++)
        {
          bool b = false;

          if (i < 10)
            b = fields.at(f).toInt() == 0 ? false : true;

          switch (i)
            {
            case 0: key->setRevoked(b); break;
            case 1: key->setExpired(b); break;
            case 2: key->setDisabled(b); break;
            case 3: key->setInvalid(b); break;
            case 4: key->setCanEncrypt(b); break;
            case 5: key->setCanSign(b); break;
            case 6: key->setCanCertify(b); break;
            case 7: key->setSecret(b); break;
            case 8: key->setCanAuthenticate(b); break;
            case 9: key->setQualified(b); break;
            case 10: key->setProtocol(fields.at(f).toInt()); break;
            case 11: key->setIssuerSerial(fields.at(f)); break;
            case 12: key->setIssuerName(fields.at(f)); break;
            case 13: key->setChainId(fields.at(f)); break;
            case 14: key->setOwnerTrust(fields.at(f).toInt()); break;
            case 15: break; // n_userIds = fields.at(f).toUInt(); break;
            case 16: n_subKeys = fields.at(f).toUInt(); break;
            }
        }

      for (s = 0; s < n_subKeys; s++)
        {
          PwmdSubKey *subKey = new PwmdSubKey();
          bool b = false;

          for (i = 0; i < 20; i++, f++)
            {
              if (i < 11)
                b = fields.at(f).toInt() == 0 ? false : true;

              switch (i)
                {
                case 0: subKey->setRevoked(b); break;
                case 1: subKey->setExpired(b); break;
                case 2: subKey->setDisabled(b); break;
                case 3: subKey->setInvalid(b); break;
                case 4: subKey->setCanEncrypt(b); break;
                case 5: subKey->setCanSign(b); break;
                case 6: subKey->setCanCertify(b); break;
                case 7: subKey->setSecret(b); break;
                case 8: subKey->setCanAuthenticate(b); break;
                case 9: subKey->setQualified(b); break;
                case 10: subKey->setCardKey(b); break;
                case 11: subKey->setPubkeyAlgo(fields.at(f).toInt()); break;
                case 12: subKey->setNBits(fields.at(f).toUInt()); break;
                case 13: subKey->setKeyId (fields.at(f)); break;
                case 14: subKey->setFingerprint (fields.at(f)); break;
                case 15: subKey->setKeygrip (fields.at(f)); break;
                case 16: subKey->setCreated (fields.at(f).toLong()); break;
                case 17: subKey->setExpires (fields.at(f).toLong()); break;
                case 18: subKey->setCardNumber (fields.at(f).length() > 1
                                                ? fields.at(f) : QString ());
                         break;
                case 19: subKey->setCurve (openpgp_unescape (fields.at (f)));
                         break;
                }
            }

          subKeys.append (subKey);
        }

      // Re-create a line containing the userIds.
      for (; f < fields.count (); f++)
        {
          PwmdUserId *userId = new PwmdUserId();

          userId->setRevoked (fields.at(f++).toUInt());
          userId->setInvalid (fields.at(f++).toUInt());
          userId->setValidity (fields.at(f++).toUInt());
          userId->setUserId (openpgp_unescape (fields.at(f++)));
          userId->setName (openpgp_unescape (fields.at(f++)));
          userId->setEmail (openpgp_unescape (fields.at(f++)));
          userId->setComment (openpgp_unescape (fields.at(f)));
          userIds.append(userId);
        }

      key->setSubkeys (subKeys);
      key->setUserIds (userIds);
      newKeys.append (key);
    }

  clearKeyList ();
  keyList = newKeys;

  for (l = 0; l < keyList.count(); l++)
    createSubKeyItem (keyList.at(l));

  init = false;
  updateKeyList (secret);

  if (!generatedKeyId.isEmpty ())
    slotJumpToKey (generatedKeyId);

  generatedKeyId = QString ();
}

void
PwmdSaveWidget::getKeyList (const QString &s, bool force, bool secret)
{
  if (keyList.count() && !force)
    {
      updateKeyList (secret);
      return;
    }

  PwmdInquireData *inq = new PwmdInquireData (s);
  inq->setUserBool (secret);
  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdSaveListKeys,
                                                         "LISTKEYS",
                                                         Pwmd::inquireCallback,
                                                         inq);
  pwm->command (item);
}

void
PwmdSaveWidget::slotRemoveRecipients ()
{
  QList <QTreeWidgetItem *> list = ui.recipientList->selectedItems ();

  while (!list.isEmpty ())
    {
      QTreeWidgetItem *item = list.takeFirst ();
      int n = ui.recipientList->indexOfTopLevelItem (item);

      ui.recipientList->takeTopLevelItem (n);
      delete item;
    }

  ui.pb_keyFileOptions->setEnabled (ui.recipientList->topLevelItemCount ()
                                    || ui.ck_symmetric->isChecked ());
  emit saveReady (testRecipients ());
}

void
PwmdSaveWidget::addRecipient (PwmdKeyItemData *data)
{
  QTreeWidgetItem *newItem = new QTreeWidgetItem ();
  PwmdKey *key = data->key ();
  PwmdSubKey *subKey = data->subKey ();

  newItem->setTextAlignment (0, Qt::AlignTop);
  newItem->setTextAlignment (1, Qt::AlignTop);
  newItem->setTextAlignment (2, Qt::AlignTop);
  newItem->setTextAlignment (3, Qt::AlignTop);
  newItem->setTextAlignment (4, Qt::AlignTop);

  if (subKey->canSign ())
    {
      QList < QTreeWidgetItem * >list = ui.recipientList->findItems ("*", Qt::MatchWildcard | Qt::MatchRecursive);
      foreach (QTreeWidgetItem *item, list)
        {
          PwmdKeyItemData *d = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
          if (d->subKey ()->canSign ())
            {
              ui.recipientList->takeTopLevelItem (ui.recipientList->indexOfTopLevelItem (item));
              delete item;
              break;
            }
        }
    }

  createSubKeyItemOnce (newItem, subKey->keyId(), subKey, buildKeyUserIds(key));
  newItem->setData (0, Qt::UserRole, QVariant::fromValue (data));
  ui.recipientList->addTopLevelItem (newItem);
  ui.pb_keyFileOptions->setEnabled (true);
}

void
PwmdSaveWidget::slotAddSubKey ()
{
  QList <QTreeWidgetItem *> keys = ui.keyList->selectedItems();
  QTreeWidgetItem *item = keys.at (0);

  PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
  PwmdKeyGenDialog d (this, pwm);
  d.setSubKey (data->key ());
  d.exec ();
}

bool
PwmdSaveWidget::testRecipients ()
{
  bool sign = false;
  bool encrypt = false;

  if (ui.ck_symmetric->isChecked ())
    return true;

  QList < QTreeWidgetItem * >list = ui.recipientList->findItems ("*", Qt::MatchWildcard | Qt::MatchRecursive);

  foreach (QTreeWidgetItem *item, list)
    {
      PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
      PwmdSubKey *s = data->subKey();

      if (s->canSign ())
        sign = true;

      if (s->canEncrypt ())
        encrypt = true;
    }

  return sign && encrypt ? true : false;
}

void
PwmdSaveWidget::slotAddRecipients ()
{
  QList <QTreeWidgetItem *> list = ui.keyList->selectedItems();

  while (!list.isEmpty ())
    {
      QTreeWidgetItem *item = list.takeFirst ();
      PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
      PwmdSubKey *s = data->subKey ();

      if (ui.ck_symmetric->isChecked () && !s->canSign ())
        continue;

      addRecipient (data);
    }

  emit saveReady (testRecipients ());
}

void
PwmdSaveWidget::changeKeyListFocus (PwmdKeyTreeWidget *w, bool out)
{
  if (!out)
    slotKeyIdChanged (w->currentItem(), nullptr);
}

QString
PwmdSaveWidget::recipients ()
{
  return _recipients;
}

QString
PwmdSaveWidget::signers ()
{
  return _signers;
}

void
PwmdSaveWidget::slotJumpToKey (const QString &fpr)
{
  QString s = fpr;

  if (fpr.isEmpty ())
    {
      QList <QTreeWidgetItem *> list = ui.recipientList->selectedItems();

      if (list.isEmpty ())
        return;

      QTreeWidgetItem *item = list.at(0);
      PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
      s = data->subKey()->fingerprint ();
    }

  int n = 0;
  QTreeWidgetItem *match = searchKeyList (s, n);
  if (match)
    {
      ui.keyList->setCurrentItem (match);
      ui.keyList->scrollToItem (match);
    }
}
