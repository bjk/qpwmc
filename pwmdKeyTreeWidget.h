/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDKEYTREEWIDGET
#define PWMDKEYTREEWIDGET

#include <QTreeWidget>
#include <QKeyEvent>
#include <QTapAndHoldGesture>

class PwmdSaveWidget;
class PwmdKeyTreeWidget : public QTreeWidget
{
  Q_OBJECT
  public:
    PwmdKeyTreeWidget (QWidget * = 0);
    ~PwmdKeyTreeWidget ();
    void setParent (PwmdSaveWidget *);
    QPoint mousePosition();
    void setIsRecipientList (bool = true);
    bool isRecipientList();

  private:
    void keyPressEvent (QKeyEvent *);
    void mouseDoubleClickEvent (QMouseEvent *);
    void mousePressEvent (QMouseEvent *);
    void mouseReleaseEvent (QMouseEvent *);
    void mouseMoveEvent (QMouseEvent *);
    void popupContextMenu ();
    void popupRecipientContextMenu();
    void focusInEvent (QFocusEvent *);
    void focusOutEvent (QFocusEvent *);
    bool event (QEvent *);
    bool gestureEvent (QGestureEvent *);
    void tapAndHoldGesture (QTapAndHoldGesture *);

    PwmdSaveWidget *_parent;
    QPoint _mousePosition;
    bool _isRecipientList;
};

#endif
