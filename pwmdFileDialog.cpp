/*
    Copyright (C) 2018-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QSettings>
#include "pwmdFileDialog.h"
#ifdef Q_OS_ANDROID
#include "pwmdAndroidJNI.h"

extern PwmdAndroidJNI *androidJni;
#endif

static QString
lastFileLocation ()
{
  QSettings cfg ("qpwmc");
#ifdef Q_OS_ANDROID
  return cfg.value ("lastFileDialogLocation", "/sdcard").toString ();
#else
  return cfg.value ("lastFileDialogLocation", QDir::home ().path ()).toString ();
#endif
}

PwmdFileDialog::PwmdFileDialog (QWidget *p, Qt::WindowFlags f) :
  QFileDialog (p, f)
{
  setDirectory (lastFileLocation ());
  _updateLastLocation = true;
}

PwmdFileDialog::PwmdFileDialog (QWidget *p, const QString &a, const QString &b,
                                const QString &c) :
  QFileDialog (p, a, b, c)
{
  setDirectory (b.isEmpty () ? lastFileLocation () : b);
  _updateLastLocation = true;
}

PwmdFileDialog::~PwmdFileDialog ()
{
}

void
PwmdFileDialog::setUpdateLastLocation (bool b)
{
  _updateLastLocation = b;
}

bool
PwmdFileDialog::updateLastLocation ()
{
  return _updateLastLocation;
}

int
PwmdFileDialog::exec ()
{
#ifdef Q_OS_ANDROID
  if (!PwmdAndroidJNI::checkPermissions (acceptMode ()))
    return QDialog::Rejected;

  setWindowState (Qt::WindowFullScreen);
#endif
  int n = QFileDialog::exec ();

  if (n && updateLastLocation ())
    {
      QSettings cfg ("qpwmc");

      cfg.setValue ("lastFileDialogLocation", directory ().absolutePath ());
      cfg.sync ();
    }

  return n;
}
