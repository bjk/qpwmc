/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QApplication>
#include <QMessageBox>
#include <QFile>
#include "pwmd.h"

#ifdef Q_OS_ANDROID
#include "pwmdPasswordPromptDialog.h"
#include "pwmdAndroidJNI.h"
#include <unistd.h>
#endif

#ifndef ASSUAN_LINELENGTH
#define ASSUAN_LINELENGTH 1002
#endif

/* A garbage collector for PwmdCommandQueueItem's. The item is not deleted
 * until all receivers of the Pwmd::commandResult() signal have done
 * item->setSeen ().
 */
PwmdCommandCollectorThread::PwmdCommandCollectorThread(Pwmd *p)
{
  pwm = p;
  listMutex = new QMutex ();
}

void
PwmdCommandCollectorThread::run ()
{
  while (!pwm->_quit)
    {
      listMutex->lock ();
      int r = pwm->receivers (SIGNAL (commandResult (PwmdCommandQueueItem *,
                                                     QString, gpg_error_t, bool)));

      for (int i = 0; r && i < list.count();)
        {
          PwmdCommandQueueItem *item = list.at (i);

          if (item->seen () >= r)
            {
              item = list.takeAt (i);
              delete item;
              i = 0;
              continue;
            }
          i++;
        }

      listMutex->unlock ();
      QThread::msleep (100);
    }
}

PwmdCommandCollectorThread::~PwmdCommandCollectorThread ()
{
  listMutex->lock ();
  while (!list.isEmpty ())
    {
      PwmdCommandQueueItem *item = list.takeFirst ();
      delete item;
    }
  listMutex->unlock ();

  delete listMutex;
}

void
PwmdCommandCollectorThread::addItem (PwmdCommandQueueItem *item)
{
  listMutex->lock ();
  list.append (item);
  listMutex->unlock ();
}

bool
PwmdCommandCollectorThread::isQueued (int id)
{
  bool b = false;

  listMutex->lock ();
  foreach (PwmdCommandQueueItem *item, list)
    {
      if (item->id () == id)
        {
          b = true;
          break;
        }
    }

  listMutex->unlock ();
  return b;
}

bool
PwmdCommandCollectorThread::isQueued (PwmdCommandQueueItem *cmd)
{
  listMutex->lock ();
  bool b = list.contains (cmd);
  listMutex->unlock ();
  return b;
}

PwmdCommandQueueItem::PwmdCommandQueueItem (int id, QString cmd,
                                            pwmd_inquire_cb_t cb,
                                            PwmdInquireData *data)
{
  _error = 0;
  _errorString = QString ();
  _id = id;
  _cmd = cmd;
  _cb = cb;
  _data = data;
  _seen = 0;
  _errorList.append (gpg_err_make (GPG_ERR_SOURCE_USER_1, GPG_ERR_NO_DATA));
  _errorList.append (0);
  _queuedOnly = false;
  _needsInquire = false;
  _isFinal = false;
}

void
PwmdCommandQueueItem::setNeedsInquire (bool b)
{
  _needsInquire = b;
}

bool
PwmdCommandQueueItem::needsInquire ()
{
  return _needsInquire;
}

void
PwmdCommandQueueItem::setHostData (PwmdRemoteHost &data)
{
  _hostData = data;
}

PwmdRemoteHost &
PwmdCommandQueueItem::hostData ()
{
  return _hostData;
}

void
PwmdCommandQueueItem::setQueuedOnly (bool b)
{
  _queuedOnly = b;
}

bool
PwmdCommandQueueItem::queuedOnly ()
{
  return _queuedOnly;
}

void
PwmdCommandQueueItem::setErrorString (QString s)
{
  _errorString = s;
}

QString
PwmdCommandQueueItem::errorString ()
{
  return _errorString;
}

void
PwmdCommandQueueItem::setError (gpg_error_t rc)
{
  _error = rc;
}

gpg_error_t
PwmdCommandQueueItem::error ()
{
  return _error;
}

void
PwmdCommandQueueItem::setSeen ()
{
  _seen++;
}

int
PwmdCommandQueueItem::seen ()
{
  return _seen;
}

QList <gpg_error_t>
PwmdCommandQueueItem::errorList ()
{
  return _errorList;
}

/* Test that 'rc' is a known or valid error for command 'item'. */
bool
PwmdCommandQueueItem::checkError (gpg_error_t rc)
{
  for (int i = 0; i < _errorList.count(); i++)
    {
      gpg_error_t err = _errorList.at (i);

      if (err == rc)
        return true;
    }

  return false;
}

void
PwmdCommandQueueItem::addError (gpg_error_t e)
{
  _errorList.append (e);
}

void
PwmdCommandQueueItem::addError (gpg_err_source_t s, gpg_err_code_t e)
{
  _errorList.append (gpg_err_make (s, e));
}

void
PwmdCommandQueueItem::clearErrorList ()
{
  _errorList.clear ();
}

PwmdCommandQueueItem::~PwmdCommandQueueItem ()
{
  if (_data)
    delete _data;
}

int
PwmdCommandQueueItem::id ()
{
  return _id;
}

pwmd_inquire_cb_t
PwmdCommandQueueItem::callback ()
{
  return _cb;
}

PwmdInquireData *
PwmdCommandQueueItem::data ()
{
  return _data;
}

void
PwmdCommandQueueItem::setData (PwmdInquireData *p)
{
  _data = p;
}

QString
PwmdCommandQueueItem::command ()
{
  return _cmd;
}

bool
PwmdCommandQueueItem::isFinalItem ()
{
  return _isFinal;
}

void
PwmdCommandQueueItem::setFinalItem (bool b)
{
  _isFinal = b;
}

void
PwmdCommandQueueItem::setCommand (QString s)
{
  _cmd = s;
}

PwmdCommandThread::PwmdCommandThread (Pwmd *p)
{
  pwm = p;
  queueMutex = new QMutex ();
}

PwmdCommandThread::~PwmdCommandThread ()
{
  queueMutex->lock ();
  while (!queue.isEmpty ())
    {
      PwmdCommandQueueItem *item = queue.takeFirst ();
      delete item;
    }
  queueMutex->unlock ();

  delete queueMutex;
}

gpg_error_t
PwmdCommandThread::knownHostCallback (void *data, const char *host,
                                      const char *key, size_t len)
{
  Pwmd *pwm = static_cast <Pwmd *> (data);
  emit pwm->knownHost (pwm->_knownHostData, host, key, len);

  /* Wait for the knownHostRc signal emitted from the Pwmd parent object to
   * return an error code that we can return here. */
  while (pwm->needKnownHostRc)
    {
      msleep (100);
    };

  return pwm->knownHostRc;
}

gpg_error_t
PwmdCommandThread::connectSocket (PwmdCommandQueueItem *item)
{
  gpg_error_t rc = 0;

  pwm->needKnownHostRc = true;

  if (item->data ()->_internalInt != -1)
#ifdef Q_OS_ANDROID
    rc = pwmd_connect_fd (pwm->handle (), dup(item->data ()->_internalInt));
#else
    rc = pwmd_connect_fd (pwm->handle (), item->data ()->_internalInt);
#endif
  else
    {
      char *sock = pwm->socket ().isEmpty () ? nullptr : pwmd_strdup (pwm->socket ().toUtf8 ().data ());
      char *arg1 = pwm->connectParameterValue (0);
      char *arg2 = pwm->connectParameterValue (1);
      char *arg3 = pwm->connectParameterValue (2);
      char *arg4 = pwm->connectParameterValue (3);
      char *arg5 = pwm->connectParameterValue (4);
      char *arg6 = pwm->connectParameterValue (5);

      rc = pwmd_connect (pwm->handle(), sock, arg1, arg2, arg3, arg4, arg5, arg6);
      pwmd_free (sock);
      pwmd_free (arg1);
      pwmd_free (arg2);
      pwmd_free (arg3);
      pwmd_free (arg4);
      pwmd_free (arg5);
      pwmd_free (arg6);
    }

  if (!rc)
    {
      pwm->_state = Pwmd::Connected;
      pwm->statusMsgThread.start ();
    }
  else
    pwm->_state = Pwmd::Init;

  emit pwm->stateChanged (pwm->state ());
  return rc;
}

gpg_error_t
PwmdCommandThread::saveFile (PwmdCommandQueueItem *item)
{
  PwmdInquireData *inq = item->data ();
  pwmd_socket_t type;
  gpg_error_t rc = pwmd_socket_type (pwm->pwm, &type);

  if (!rc && type != PWMD_SOCKET_LOCAL)
    rc = pwmd_setopt (pwm->pwm, PWMD_OPTION_LOCAL_PINENTRY, 1);

  if (rc)
    return rc;

#ifdef Q_OS_ANDROID
  pwmd_setopt (pwm->pwm, PWMD_OPTION_NO_PINENTRY, 1);
  pwmd_setopt (pwm->pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 1);
#endif

  return pwmd_save (pwm->pwm, inq->userInternal ().isEmpty () ? nullptr : inq->userInternal ().toUtf8 ().data (), item->callback(), inq);
}

gpg_error_t
PwmdCommandThread::genKey (PwmdCommandQueueItem *item)
{
  PwmdInquireData *inq = item->data ();
  pwmd_socket_t type;
  gpg_error_t rc = pwmd_socket_type (pwm->pwm, &type);

  if (!rc && type != PWMD_SOCKET_LOCAL)
    rc = pwmd_setopt (pwm->pwm, PWMD_OPTION_LOCAL_PINENTRY, 1);

  if (rc)
    return rc;

#ifdef Q_OS_ANDROID
  pwmd_setopt (pwm->pwm, PWMD_OPTION_NO_PINENTRY, 1);
  pwmd_setopt (pwm->pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 1);
#endif

  return pwmd_genkey (pwm->pwm, inq->userInternal ().isEmpty () ? nullptr : inq->userInternal ().toUtf8 ().data (), item->callback(), inq);
}

gpg_error_t
PwmdCommandThread::changePassword (PwmdCommandQueueItem *item)
{
  PwmdInquireData *inq = item->data ();
  pwmd_socket_t type;
  gpg_error_t rc = pwmd_socket_type (pwm->pwm, &type);

  if (!rc && type != PWMD_SOCKET_LOCAL)
    rc = pwmd_setopt (pwm->pwm, PWMD_OPTION_LOCAL_PINENTRY, 1);

  if (rc)
    return rc;

#ifdef Q_OS_ANDROID
  pwmd_setopt (pwm->pwm, PWMD_OPTION_NO_PINENTRY, 1);
  pwmd_setopt (pwm->pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 1);
#endif

  return pwmd_passwd (pwm->pwm, inq->userInternal ().isEmpty () ? nullptr : inq->userInternal ().toUtf8 ().data (), item->callback(), inq);
}

gpg_error_t
PwmdCommandThread::openFile (PwmdCommandQueueItem *item)
{
  pwmd_socket_t type;
  PwmdInquireData *inq = item->data ();
  gpg_error_t rc = pwmd_socket_type (pwm->pwm, &type);

  if (!rc)
    {
      rc = pwmd_setopt (pwm->pwm, PWMD_OPTION_LOCK_ON_OPEN, pwm->lockOnOpen ());
      if (!rc)
	{
	  bool doFinal = false;

	  if (type != PWMD_SOCKET_LOCAL)
	    (void)pwmd_setopt (pwm->pwm, PWMD_OPTION_LOCAL_PINENTRY, 1);

#ifdef Q_OS_ANDROID
          pwmd_setopt (pwm->pwm, PWMD_OPTION_NO_PINENTRY, 1);
          pwmd_setopt (pwm->pwm, PWMD_OPTION_OVERRIDE_INQUIRE, 1);
#endif

	  do
	    {
	      rc = pwmd_open (pwm->pwm, pwm->filename ().toUtf8 (),
                              item->callback (), inq);
	      if (pwm->badPassphraseCallback &&
		  gpg_err_code (rc) == GPG_ERR_BAD_PASSPHRASE)
		{
		  gpg_error_t tempRc;

		  tempRc = pwm->badPassphraseCallback (pwm->badPassphraseData, false);
		  // Keep the error source.
		  if (tempRc && gpg_err_code (tempRc) != gpg_err_code (rc))
		    {
		      rc = tempRc;
		      break;
		    }
		  else if (gpg_err_code (tempRc) == gpg_err_code (rc))
		    break;

		  doFinal = true;
		}
	    }
	  while (gpg_err_code (rc) == GPG_ERR_BAD_PASSPHRASE
		 && pwm->badPassphraseCallback);

	  if (doFinal)
	    (void)pwm->badPassphraseCallback (pwm->badPassphraseData, true);
	}

      if (!rc)
        {
          pwm->_state = Pwmd::Opened;
          emit pwm->stateChanged (pwm->state ());
        }
    }

  return rc;
}

bool
PwmdCommandThread::isQueued (int id)
{
  bool b = false;

  queueMutex->lock ();
  foreach (PwmdCommandQueueItem *item, queue)
    {
      if (item->id () == id)
        {
          b = true;
          break;
        }
    }

  // The command was in the queue but not yet garbage collected by calling
  // setSeen(). It may be better to pass a PwmdCommandQueueItem to compare when
  // the item was queued: return indexOf(cmd) < lastIndexOf (cmdId).
  if (!b)
    b = pwm->collectorThread.isQueued (id);

  queueMutex->unlock ();
  return b;
}

bool
PwmdCommandThread::isQueued (PwmdCommandQueueItem *cmd)
{
  queueMutex->lock ();
  bool b = queue.contains (cmd);

  if (!b)
    b = pwm->collectorThread.isQueued (cmd);

  queueMutex->unlock ();
  return b;
}

/* Returns true if there was an existing cur->cmdId() in the command queue and
 * resets the isFinalItem of existing commands of the same cmdId to false.
 */
bool
PwmdCommandThread::hasFinalItem (PwmdCommandQueueItem *cur,
                                 QList <PwmdCommandQueueItem *> list)
{
  bool b = false;

  foreach (PwmdCommandQueueItem *t, list)
    {
      if (t->id () == cur->id () && t != cur)
        {
          b = true;
          t->setFinalItem (false);
        }
    }

  cur->setFinalItem (true);
  return b;
}

void
PwmdCommandThread::run ()
{
  pwm->collectorThread.start ();
  pwm->commandMutex->lock ();
  PwmdCommandQueueItem *item;
  QString result;
  gpg_error_t rc = 0;
  char *tmp = nullptr;
  size_t len = 0;
  bool internal = true;
  size_t offset = 0;
  char *bulk = nullptr;
  QList <PwmdCommandQueueItem *> bulkList;
  bool bulkInquire = false;

  queueMutex->lock ();
  if (queue.isEmpty ())
    {
      queueMutex->unlock ();
      pwm->commandMutex->unlock ();
      return;
    }

  item = queue.first ();

  if (pwm->state () == Pwmd::Init
      && item->id () != PwmdCmdIdInternalConnect
      && item->id () != PwmdCmdIdInternalConnectHost)
    {
      queueMutex->unlock ();
      pwm->commandMutex->unlock ();
      return;
    }

  if (item->id () < 0) // Internal command
    {
      item = queue.takeAt (queue.indexOf (item));
      pwm->collectorThread.addItem (item);
      emit pwm->busy (item->id (), true);
      queueMutex->unlock ();
    }

  if (item->id () == PwmdCmdIdInternalConnect)
    {
      pwm->_state = Pwmd::Connecting;
      emit pwm->stateChanged (pwm->state ());
      rc = connectSocket (item);
      goto done;
    }
  else if (item->id () == PwmdCmdIdInternalConnectHost)
    {
      pwm->_state = Pwmd::Connecting;
      emit pwm->stateChanged (pwm->state ());
      emit pwm->connectReady (item->hostData ());
      goto done;
    }
  else if (item->id () == PwmdCmdIdInternalCloseFile)
    {
      rc = pwmd_command (pwm->pwm, nullptr, nullptr, nullptr, nullptr, "%s", "RESET");
      if (!rc)
        {
          pwm->_state = Pwmd::Connected;
          emit pwm->stateChanged (pwm->state ());
        }
      goto done;
    }
  else if (item->id () == PwmdCmdIdInternalOpen)
    {
      rc = openFile (item);
      goto done;
    }
  else if (item->id () == PwmdCmdIdInternalSave)
    {
      rc = saveFile (item);
      goto done;
    }
  else if (item->id () == PwmdCmdIdInternalGenKey)
    {
      rc = genKey (item);
      goto done;
    }
  else if (item->id () == PwmdCmdIdInternalPassword)
    {
      rc = changePassword (item);
      goto done;
    }

  foreach (PwmdCommandQueueItem *cur, queue)
    {
      if (cur->needsInquire ())
        {
          bulkInquire = true;
          break;
        }
    }

  // Create a BULK command containing all queued commands.
  foreach (PwmdCommandQueueItem *cur, queue)
    {
      QString id = QString::number (cur->id ());
      int o;

      /* The next command is an internal one which uses its own bulk command
       * list in libpwmd. Maybe sometime in the future we will be able to
       * prepend or append to the internal bulk command list of libpwmd. */
      if (cur->id () < 0)
        break;

      internal = false;

      /* A single queued command. Create a single command from the inquire
       * arguments to avoid an extra server inquire of the BULK command. */
      if (queue.length () == 1 && bulkList.isEmpty ())
        {
          QString cmd = QString ();

          /* A known protocol command that uses an inquire. */
          int n = cur->command ().indexOf (' ');
          if (n != -1)
            cmd = cur->command ().mid (0, n);
          else
            cmd = cur->command ();

          if (cmd.toUpper() == "STORE" || cmd.toUpper() == "IMPORT")
            {
              item = cur;
              break;
            }

          if (cur->data () && cur->data ()->data ())
            {
              if (cur->command ().length ()
                  + strlen (cur->data ()->data ()) + 1 < ASSUAN_LINELENGTH)
                {
                  cmd = cur->command () +  " " + cur->data ()->data ();
                  cur->data ()->setData (nullptr, 0); // Free the existing.
                }
              else
                cmd = cur->command () + " " + "--inqiure";
            }
          else
            cmd = cur->command ();

          cur->setCommand (cmd);
          item = cur;
          break;
        }

      // Initial BULK command. Avoid pwmd_bulk_append_rc() hell.
      if (!bulk)
        {
          rc = pwmd_bulk_append (&bulk, "NOP", 3, "NOP", nullptr, 0, &offset);
          if (rc)
            {
              emit pwm->commandResult (nullptr, QString (), rc, true);
              flushQueue ();
              queueMutex->unlock ();
              pwm->commandMutex->unlock ();
              return;
            }

          o = 0;
        }
      else
        o = 1;

      gpg_error_t *rcs = new gpg_error_t[cur->errorList ().length ()+1];
      int n = 0;
      foreach (gpg_error_t trc, cur->errorList ())
        rcs[n++] = trc;
      rcs[n] = GPG_ERR_MISSING_ERRNO; // List terminator

      offset -= o;
      rc = pwmd_bulk_append_rc (&bulk, rcs, id.toUtf8 ().data (), id.length (),
                                cur->command ().toUtf8 ().data (),
                                cur->data () ? cur->data ()->data () : nullptr,
                                cur->data () ? cur->data ()->size () : 0,
                                &offset);
      delete []rcs;
      if (rc)
        {
          pwmd_free (bulk);
          emit pwm->commandResult (nullptr, QString (), rc, true);
          flushQueue ();
          queueMutex->unlock ();
          pwm->commandMutex->unlock ();
          return;
        }

      bulkList.append (cur);
      if (!hasFinalItem (cur, bulkList))
        emit pwm->busy (cur->id (), true);

      queue.removeAt (queue.indexOf (cur));
      pwm->collectorThread.addItem (cur);
    }

  /* A single command only. Avoids a BULK inquire from the server. */
  if (bulkList.isEmpty ())
    {
      item = queue.takeFirst ();
      pwm->collectorThread.addItem (item);
      queueMutex->unlock ();
      emit pwm->busy (item->id (), true);
      rc = pwmd_command (pwm->handle (), &tmp, &len, item->callback (),
                         item->data (), "%s",
                         item->command ().toUtf8 ().data ());
      result = tmp ? QString::fromUtf8(tmp, -1) : QString ();
      pwmd_free (tmp);
      item->setFinalItem (true);
      emit pwm->busy (item->id (), false);
      emit pwm->commandResult (item, result, rc, false);
    }
  else
    {
      size_t offset = 0;

      queueMutex->unlock ();
      rc = pwmd_bulk_finalize (&bulk);
      if (!rc && (strlen (bulk) - 5 >= ASSUAN_LINELENGTH || bulkInquire))
        {
          rc = pwmd_bulk (pwm->handle (), &tmp, &len, item->callback (),
                          item->data (), bulk, strlen (bulk));
        }
      else if (!rc)
        {
          rc = pwmd_command (pwm->handle (), &tmp, &len, item->callback (),
                             item->data (), "BULK %s", bulk);
        }
      pwmd_free (bulk);
      if (!rc)
        {
          foreach (PwmdCommandQueueItem *cur, bulkList)
            {
              const char *r;
              size_t rlen;
              gpg_error_t rrc;
              char *id = strdup (QString::number (cur->id ()).toUtf8 ().data());

              rc = pwmd_bulk_result (tmp, len, id, strlen (id), &offset, &r,
                                     &rlen, &rrc);
              free (id);
              bulkList.removeAt (bulkList.indexOf (cur));

              if (cur->isFinalItem ())
                emit pwm->busy (cur->id (), false);

              emit pwm->commandResult (cur, QString::fromUtf8 (r, rlen),
                                       rc ? rc : rrc, rc ? true : false);

              /* The return code for this command may have an error. If it is a
               * known error then continue processing commands in the bulk
               * result. Otherwise, notify each remaining command receiver of
               * the error. */
              if (!rc && rrc && !cur->checkError (rrc))
                {
                  pwmd_free (tmp);
                  rc = gpg_error (GPG_ERR_MISSING_ERRNO);
                  goto flush;
                }
            }

          pwmd_free (tmp);
        }
      else
        {
flush:
          foreach (PwmdCommandQueueItem *cur, bulkList)
            {
              bulkList.removeAt (bulkList.indexOf (cur));

              if (cur->isFinalItem ())
                emit pwm->busy (cur->id (), false);

              emit pwm->commandResult (cur, QString (), rc, true);
              rc = gpg_error (GPG_ERR_MISSING_ERRNO);
            }
        }
    }

done:
  if (internal)
    {
      emit pwm->busy (item->id (), false);
      emit pwm->commandResult (item, result, rc, false);

      if (rc && !item->checkError (rc))
        {
          /* Assume the remaining commands depend on the success of previous
           * ones.  Flush the command queue upon command failure. */
          flushQueue (true);
          pwm->commandMutex->unlock ();
          return;
        }
    }

  pwm->commandMutex->unlock ();
  run ();
}

void
PwmdCommandThread::flushQueue (bool collect, gpg_error_t rc)
{
  queueMutex->lock ();
  if (!rc)
    rc = gpg_error (GPG_ERR_MISSING_ERRNO);

  while (!queue.isEmpty ())
    {
      PwmdCommandQueueItem *item = queue.takeFirst ();
      if (collect)
        {
          pwm->collectorThread.addItem (item);
          emit pwm->commandResult (item, QString (), rc, true);
        }
      else
        delete item;
    }

  queueMutex->unlock ();
}

unsigned
PwmdCommandThread::queued ()
{
  queueMutex->lock ();
  unsigned n = queue.count ();
  queueMutex->unlock ();
  return n;
}

bool
PwmdCommandThread::setCommand (PwmdCommandQueueItem *item, bool queueOnly)
{
  queueMutex->lock ();
  item->setQueuedOnly (queueOnly);
  queue.append (item);
  queueMutex->unlock ();
  return true;
}

PwmdStatusMessageThread::PwmdStatusMessageThread (Pwmd *p)
{
  pwm = p;
}

void
PwmdStatusMessageThread::run ()
{
  for (;;)
    {
      pwm->commandMutex->lock ();
      if (pwm->state () == Pwmd::Init)
	{
	  pwm->commandMutex->unlock ();
	  break;
	}

      gpg_error_t rc = pwmd_process (pwm->pwm);
      pwm->commandMutex->unlock ();
      if (rc)
	{
          PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalStatusError);
          pwm->collectorThread.addItem (item);
          emit pwm->commandResult (item, 0, rc, false);
	  break;
	}

      msleep (100);
    }
}

gpg_error_t
Pwmd::knownHostPrompt (void *data, const char *host, const char *key,
                       size_t len)
{
  (void) key;
  (void) len;
  (void) data;
  QMessageBox msgBox;
  msgBox.setText (QString (tr
	      ("While attempting an SSH connection to %1, there was a problem verifying it's host key against the known and trusted hosts file because it's host key was not found in the database.\n\nWould you like to treat this connection as trusted for this and future connections by adding %2's host key to the known hosts file?")).
	     arg (host, host));

  msgBox.setIcon (QMessageBox::Question);
  msgBox.setInformativeText (tr ("Accept this host key as trusted?"));
  msgBox.setStandardButtons (QMessageBox::Yes | QMessageBox::No);
  msgBox.setDefaultButton (QMessageBox::No);
  return msgBox.exec () == QMessageBox::No ? GPG_ERR_CANCELED : 0;
}

void
Pwmd::slotKnownHostRc (gpg_error_t rc)
{
  knownHostRc = rc;
  needKnownHostRc = false;
}

void
Pwmd::setStatusMessageData (void *p)
{
  _statusMessageData = p;
}

void *
Pwmd::statusMessageData ()
{
  return _statusMessageData;
}

gpg_error_t
Pwmd::statusCallback (void *data, const char *line)
{
  Pwmd *pwm = static_cast < Pwmd * >(data);

  QStringList l = QString (line).split (' ');
  if (l.at (0) == "INQUIRE_MAXLEN")
    pwm->inquireMaxlen = l.at (1).toUInt (nullptr);
  else if (l.size() > 2 && (l.at (0) == "ERROR" || l.at (0) == "FAILURE"))
    pwm->lastError = l.at (2).toUInt();
#ifdef Q_OS_ANDROID
  else if (l.at (0) == "PASSPHRASE_HINT")
    pwm->pinentryHint = l.mid (1).join (' ');
  else if (l.at (0) == "PASSPHRASE_INFO")
    pwm->pinentryInfo = l.mid (1).join (' ');
#endif

  emit pwm->statusMessage (line, pwm->statusMessageData ());
  return 0;
}

Pwmd::Pwmd (const QString & file, const QString & name, const QString & sock,
            QObject *p)
  : QObject (p), statusMsgThread (this), commandThread (this), collectorThread (this)
{
  inquireMaxlen = 0;
  _socket = sock;
  _filename = file;
  clientName = name;
  badPassphraseCallback = nullptr;
  badPassphraseData = nullptr;
  tlsError = 0;
  lastError = 0;
  _quit = false;
  spawnError = 0;
  _knownHostData = nullptr;
  _statusMessageData = nullptr;
#ifdef Q_OS_ANDROID
  pinentryHint = QString ();
  pinentryInfo = QString ();
#endif

  pwmd_init ();
  pwm = nullptr;
  _lockOnOpen = false;
  commandMutex = new QRecursiveMutex ();
  reset ();
}

Pwmd::~Pwmd ()
{
  resetHandle ();
  if (pwm)
    pwmd_close (pwm);

  _quit = true;
  collectorThread.wait ();
  commandThread.wait ();
  delete commandMutex;
}

unsigned
Pwmd::queued ()
{
  return commandThread.queued ();
}

void
Pwmd::setKnownHostData (void *p)
{
  _knownHostData = p;
}

void *
Pwmd::knownHostData ()
{
  return _knownHostData;
}

int
Pwmd::receivers (const char *s)
{
  return QObject::receivers (s);
}

void
Pwmd::setBadPassphraseCallback (gpg_error_t (*fn)(void *, bool))
{
  badPassphraseCallback = fn;
}

void
Pwmd::setBadPassphraseData (void *data)
{
  badPassphraseData = data;
}

static gpg_error_t
convertErrorCode (QFile::FileError n)
{
  gpg_err_code_t rc;

  switch (n)
    {
    case QFile::OpenError:
      rc = GPG_ERR_ENOENT;
      break;
    case QFile::PermissionsError:
      rc = GPG_ERR_EPERM;
      break;
    case QFile::ResourceError:
      rc = GPG_ERR_ENOMEM;
      break;
    default:
      rc = GPG_ERR_UNKNOWN_ERRNO;
      break;
    }

  return gpg_error (rc);
}

static gpg_error_t
checkKeyFile (const QString & filename, char **data, size_t * size,
              PwmdInquireData *inq)
{
  PwmdCommandQueueItem *item = inq ? inq->commandItem () : nullptr;
  QFile file (filename);
  qint64 containsNull = -1;
  gpg_error_t rc = 0;

  *data = nullptr;
  *size = 0;

  if (!file.open (QIODevice::ReadOnly))
    {
      rc = convertErrorCode (file.error ());
      if (item)
        {
          item->setErrorString (QString ("%1: %2").arg (file.fileName(),
                                                        file.errorString ()));
          item->setError (rc);
        }
      return rc;
    }

  if (!file.size ())
    return GPG_ERR_INV_PASSPHRASE;

  size_t len = file.size ();
  char *buf = (char *) pwmd_malloc (file.size ());
  if (!buf)
    {
      file.close ();
      return GPG_ERR_ENOMEM;
    }

  if (file.read (buf, file.size ()) != file.size ())
    {
      pwmd_free (buf);
      rc = convertErrorCode (file.error ());
      if (item)
        {
          item->setErrorString (QString ("%1: %2").arg (file.fileName(),
                                                        file.errorString ()));
          item->setError (rc);
        }
      return rc;
    }

  for (qint64 c = 0; c < file.size (); c++)
    {
      if (buf[c] == 0)
	{
	  containsNull = c;
	  break;
	}
    }

  file.close ();
  if (containsNull != -1)
    {
      pwmd_free (buf);
      rc = GPG_ERR_INV_VALUE;
      if (item)
        {
          item->setErrorString (QString ("%1: null at position %2").arg (file.fileName()).arg (containsNull));
          item->setError (rc);
        }
      return rc;
    }

  *data = buf;
  *size = len;
  return 0;
}

#ifdef Q_OS_ANDROID
gpg_error_t
Pwmd::sshPassphraseCallback (void *data, const char *host, const char *filename,
                             char **rPassphrase)
{
  Pwmd *pwm = static_cast <Pwmd *>(data);
  QString desc = QString (tr ("Enter the passphrase for the SSH identity file."
                              "\n\nHostname: %1\nIdentity: %2"))
    .arg (host, filename);
  QString prompt = tr ("Passphrase:");

  pwm->needPassphraseRc = true;
  emit pwm->passphrase (pwm, "PASSPHRASE", desc, prompt, QPrivateSignal ());
  while (pwm->needPassphraseRc)
    {
      QThread::msleep (100);
    }

  gpg_error_t rc = pwm->passphraseRc;
  if (!rc)
    {
      *rPassphrase = pwmd_strdup (pwm->thePassphraseResult.toUtf8().data());
      pwm->thePassphraseResult = QString ();
    }

  return rc;
}
#endif

gpg_error_t
Pwmd::passphraseCommon (Pwmd *pwm, const QString &keyfile,
                        char **r_passphrase, size_t *r_size,
                        QString keyword, PwmdInquireData *inq)
{
  gpg_error_t rc = 0;

  if (keyfile.isEmpty ())
#ifdef Q_OS_ANDROID
    {
      QString desc = QString ();
      QString prompt = tr ("Passphrase:");
      QString uid = QString ();

      if (!pwm->pinentryHint.isEmpty ())
        {
          QStringList l = pwm->pinentryHint.split (' ');

          uid = QString (tr ("\nUser: %1\nKey ID: %2").arg (l.mid (1).join (' ')).arg (l.at (0)));
        }

      pwm->pinentryHint = QString ();
      pwm->pinentryInfo = QString ();

      if (keyword == "PASSPHRASE")
        desc = QString (tr ("Please enter the passphrase for decryption.\n\n")) + QString (tr ("Filename: %1")).arg (pwm->filename ()) + uid;
      else if (keyword == "NEW_PASSPHRASE")
        desc = QString (tr ("Please enter the new passphrase.\n\n")) + QString (tr ("Filename: %1")).arg (pwm->filename ()) + uid;
      else if (keyword == "SIGN_PASSPHRASE")
        desc = QString (tr ("Please enter the passphrase for signing.\n\n")) + QString (tr ("Filename: %1")).arg (pwm->filename ()) + uid;

      pwm->needPassphraseRc = true;
      emit pwm->passphrase (pwm, keyword, desc, prompt, QPrivateSignal ());
      while (pwm->needPassphraseRc)
        {
          QThread::msleep (100);
        }

      rc = pwm->passphraseRc;
      if (!rc)
        {
          *r_passphrase = pwmd_strdup (pwm->thePassphraseResult.toUtf8().data());
          *r_size = pwm->thePassphraseResult.length ();
          if (*r_size == 0)
            *r_size = 1;

          pwm->thePassphraseResult = QString ();
        }
    }
#else
    rc = pwmd_password (pwm->handle (), keyword.toUtf8().data(),
                        r_passphrase, r_size);
#endif
  else
    rc = checkKeyFile (keyfile, r_passphrase, r_size, inq);

  return rc;
}

#ifdef Q_OS_ANDROID
void
Pwmd::slotPassphraseResult (QString s)
{
  thePassphraseResult = s;
  needPassphraseRc = false;
}

void
Pwmd::slotPassphrase (Pwmd *pwm, QString keyword, QString desc, QString prompt)
{
  PwmdPasswordPromptDialog d;
  unsigned features = pwmd_features ();

  d.setDescription (desc);
  d.setPrompt (prompt);
  d.setRepeat (keyword == "NEW_PASSPHRASE");
  d.setQuality ((features & PWMD_FEATURE_QUALITY)
                && keyword == "NEW_PASSPHRASE");

  if (d.exec () == QDialog::Rejected)
    pwm->passphraseRc = gpg_error (GPG_ERR_CANCELED);
  else
    pwm->passphraseRc = 0;

  emit pwm->passphraseResult (d.passphrase (), QPrivateSignal ());
}
#endif

gpg_error_t
Pwmd::inquireCallback (void *user, const char *keyword,
		       gpg_error_t rc, char **data, size_t * len)
{
  if (rc)
    return rc;

  QString what = keyword;
  PwmdInquireData *inq = static_cast < PwmdInquireData * >(user);
  size_t n;

  if (!inq)
    return gpg_error (GPG_ERR_INV_ARG);

  // Continue sending data from the previous inquire. We do it in chunks to
  // save some memory and to let progress messages get parsed.
  if (inq->sent ())
    {
      n = inq->sent () + ASSUAN_LINELENGTH > inq->size ()?
	inq->size () - inq->sent () : ASSUAN_LINELENGTH;
      *data = inq->data () + inq->sent ();
      *len = n;
      inq->setSent (inq->sent () + n);

      // prepare for the next inquire in the same command.
      if (inq->sent () == inq->size ())
	{
	  inq->setSent (0);
	  return GPG_ERR_EOF;
	}

      return 0;
    }

  if (what == "PASSPHRASE" || what == "NEW_PASSPHRASE"
      || what == "SIGN_PASSPHRASE")
    {
      char *tmp = nullptr;
      size_t size = 0;

      if (what == "PASSPHRASE")
        rc = passphraseCommon (inq->pwm, inq->keyFile ().toUtf8().data(),
                               &tmp, &size, keyword, inq);
      else if (what == "NEW_PASSPHRASE")
        rc = passphraseCommon (inq->pwm,
                               inq->encryptKeyFile ().toUtf8().data(),
                               &tmp, &size, keyword, inq);
      else if (what == "SIGN_PASSPHRASE")
        rc = passphraseCommon (inq->pwm,
                               inq->signKeyFile().toUtf8().data(),
                               &tmp, &size, keyword, inq);

      if (rc)
        return rc;

      pwmd_free (inq->_internalData);
      inq->_internalData = tmp;
      *data = tmp;
      *len = size;
      return GPG_ERR_EOF;
    }

  // Normal inquire DATA. Sent in chunks. See above.
  *data = inq->data ();
  n = inq->size () > ASSUAN_LINELENGTH ? ASSUAN_LINELENGTH : inq->size ();
  *len = n;
  // Prepare for the next inquire in the same command.
  inq->setSent (n != inq->size ()? inq->sent () + n : 0);
  pwmd_setopt (inq->handle (), PWMD_OPTION_INQUIRE_TOTAL, inq->size ());
  return n != inq->size () ? 0 : GPG_ERR_EOF;
}

pwm_t *
Pwmd::handle ()
{
  return pwm;
}

void
Pwmd::resetHandle ()
{
  commandMutex->lock ();
  _state = Init;

  if (pwm)
    pwmd_disconnect (pwm);

  commandMutex->unlock ();
  statusMsgThread.wait ();
}

gpg_error_t
Pwmd::close (bool queueOnly, PwmdCommandQueueItem **rItem)
{
  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalCloseFile);
  if (!command (item, queueOnly))
    {
      delete item;
      return false;
    }
  else if (rItem)
    *rItem = item;

  return true;
}

gpg_error_t
Pwmd::reset (bool doEmit, bool disco)
{
  qRegisterMetaType <size_t> ("size_t");
  gpg_error_t rc = 0;

  if (disco || !pwm)
    resetHandle ();

  if (!pwm)
    {
      rc = pwmd_new (clientName.isEmpty ()? nullptr : clientName.toUtf8 ().data (),
                     &pwm);
      QObject::connect (parent(), SIGNAL (knownHostRc (gpg_error_t)), this, SLOT (slotKnownHostRc (gpg_error_t)));
      pwmd_setopt (pwm, PWMD_OPTION_KNOWNHOST_CB,
                   PwmdCommandThread::knownHostCallback);
      pwmd_setopt (pwm, PWMD_OPTION_KNOWNHOST_DATA, this);
#ifdef Q_OS_ANDROID
      QObject::connect (this, SIGNAL (passphrase (Pwmd *, QString, QString, QString)), this, SLOT (slotPassphrase (Pwmd *, QString, QString, QString)));
      QObject::connect (this, SIGNAL (passphraseResult (QString)), this, SLOT (slotPassphraseResult (QString)));
#endif
    }
  else
    {
      pwmd_setopt (pwm, PWMD_OPTION_READ_CB, nullptr);
      pwmd_setopt (pwm, PWMD_OPTION_WRITE_CB, nullptr);
    }

  lastError = 0;

  if (doEmit)
    emit stateChanged (_state);

  if (!rc)
    {
      pwmd_setopt (pwm, PWMD_OPTION_STATUS_CB, statusCallback);
      pwmd_setopt (pwm, PWMD_OPTION_STATUS_DATA, this);
#ifdef Q_OS_ANDROID
      pwmd_setopt (pwm, PWMD_OPTION_PASSPHRASE_CB, sshPassphraseCallback);
      pwmd_setopt (pwm, PWMD_OPTION_PASSPHRASE_DATA, this);
#endif
    }

  return rc;
}

void
Pwmd::runQueue ()
{
  commandThread.start ();
}

void
Pwmd::flushQueue ()
{
  commandThread.flushQueue ();
}

bool
Pwmd::command (PwmdCommandQueueItem *item, bool queueOnly)
{
  if (!commandThread.setCommand (item, queueOnly))
    return false;

  if (queueOnly)
    return true;

  runQueue ();
  return true;
}

void
Pwmd::setConnectParameters (QStringList params)
{
  _connectParameters = params;
}

char *
Pwmd::connectParameterValue (int idx)
{
  if (_connectParameters.count () - 1 < idx
      || _connectParameters.at (idx).isEmpty ())
    return nullptr;

  return pwmd_strdup (_connectParameters.at (idx).toUtf8 ().data ());
}

bool
Pwmd::connect (pwmd_inquire_cb_t cb, void *data, bool queueOnly,
               PwmdCommandQueueItem **rItem)
{
  PwmdInquireData *inq = new PwmdInquireData();
  inq->setUserPtr (data);
  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalConnect, 0, cb, inq);

  if (!command (item, queueOnly))
    {
      delete inq;
      delete item;
      return false;
    }
  else if (rItem)
    *rItem = item;

  return true;
}

bool
Pwmd::connectFd (int fd, pwmd_inquire_cb_t cb, void *data, bool queueOnly,
               PwmdCommandQueueItem **rItem)
{
  PwmdInquireData *inq = new PwmdInquireData ();
  inq->setUserPtr (data);
  inq->_internalInt = fd;
  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalConnect, 0, cb, inq);

  if (!command (item, queueOnly))
    {
      delete inq;
      delete item;
      return false;
    }
  else if (rItem)
    *rItem = item;

  return true;
}

bool
Pwmd::connectHost (PwmdRemoteHost hostData, bool queueOnly,
                   PwmdCommandQueueItem **rItem)
{
  PwmdInquireData *inq = new PwmdInquireData ();
  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalConnectHost);
  item->setHostData (hostData);

  if (!command (item, queueOnly))
    {
      delete inq;
      delete item;
      return false;
    }
  else if (rItem)
    *rItem = item;

  return true;
}

void
Pwmd::setLockOnOpen (bool b)
{
  _lockOnOpen = b;
}

bool
Pwmd::lockOnOpen ()
{
  return _lockOnOpen;
}

bool
Pwmd::open (pwmd_inquire_cb_t cb, PwmdInquireData *inq, bool queueOnly,
            PwmdCommandQueueItem **rItem)
{
  bool owner = !inq;

  if (!inq)
    inq = new PwmdInquireData ();
  inq->pwm = this;

  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalOpen, 0, cb, inq);
  inq->setCommandItem (item);

  if (!command (item, queueOnly))
    {
      if (owner)
        delete inq;

      delete item;
      return false;
    }
  else if (rItem)
    *rItem = item;

  return true;
}

void
Pwmd::setFilename (QString f)
{
  _filename = f;
}

QString
Pwmd::filename ()
{
  return _filename;
}

QString
Pwmd::socket ()
{
  return _socket;
}

void
Pwmd::setSocket (const QString & s)
{
  _socket = s;
}

Pwmd::ConnectionState Pwmd::state ()
{
  return _state;
}

bool
Pwmd::isQueued (int id)
{
  return commandThread.isQueued (id);
}

bool
Pwmd::isQueued (PwmdCommandQueueItem *cmd)
{
  return commandThread.isQueued (cmd);
}

bool
Pwmd::genkey (const QString & args, pwmd_inquire_cb_t cb, PwmdInquireData *inq,
              bool queueOnly, PwmdCommandQueueItem **rItem)
{
  bool owner = !inq;

  if (!inq)
    inq = new PwmdInquireData ();
  inq->setUserInternal (args);
  inq->pwm = this;

  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalGenKey, 0, cb, inq);
  inq->setCommandItem (item);

  if (!command (item, queueOnly))
    {
      if (owner)
        delete inq;

      delete item;
      return false;
    }
  else if (rItem)
    *rItem = item;

  return true;
}

bool
Pwmd::save (const QString & args, pwmd_inquire_cb_t cb, PwmdInquireData *inq,
            bool queueOnly, PwmdCommandQueueItem **rItem)
{
  bool owner = !inq;

  if (!inq)
    inq = new PwmdInquireData ();
  inq->setUserInternal (args);
  inq->pwm = this;

  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalSave, 0, cb, inq);
  inq->setCommandItem (item);

  if (!command (item, queueOnly))
    {
      if (owner)
        delete inq;

      delete item;
      return false;
    }
  else if (rItem)
    *rItem = item;

  return true;
}

bool
Pwmd::passwd (const QString & args, pwmd_inquire_cb_t cb, PwmdInquireData *inq,
              bool queueOnly, PwmdCommandQueueItem **rItem)
{
  bool owner = !inq;

  if (!inq)
    inq = new PwmdInquireData ();
  inq->setUserInternal (args);
  inq->pwm = this;

  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdInternalPassword, 0, cb, inq);

  inq->setCommandItem (item);

  if (!command (item, queueOnly))
    {
      if (owner)
        delete inq;

      delete item;
      return false;
    }
  else if (rItem)
    *rItem = item;

  return true;
}

void
Pwmd::lockMutex ()
{
  commandMutex->lock ();
}

void
Pwmd::unlockMutex ()
{
  commandMutex->unlock ();
}

QString
Pwmd::errorString(gpg_error_t rc, Pwmd *pwm)
{
  switch (gpg_err_code (rc))
    {
    case GPG_ERR_CHECKSUM:
      return tr ("The currently open data file has been externally modified; either by another client or by some other means. Reopen the file to fix this error.");
      break;
    case GPG_ERR_LOCKED:
      return tr ("Another client has the same data file opened and has requested that other clients must wait until it has finished.");
      break;
    case GPG_ERR_ELOOP:
      return tr ("A recursion loop was detected while processing a command. This happens when an element with a \"target\" attribute references itself. To fix the error when the attribute already exists and a command failed, open the attribute editor for the offending element then remove or modify the \"target\" attribute. If the previous command failed do to this error then it means pwmd returned the error any there is nothing more to do.");
      break;
    case GPG_ERR_ASS_CONNECT_FAILED:
      return tr ("There was a problem connecting to the pwmd server. Be sure that it is running and that the correct socket location was specified in the Options. This error may also be returned if there was a problem connecting to the pinentry program or gpg-agent. Be sure that gpg-agent is running and that the correct path to the pinentry binary has been configured.");
      break;
    case GPG_ERR_TOO_LARGE:
      return tr ("This error usually means there was too much data sent during an INQUIRE from the pwmd server. Some INQUIRE's have limits on the amount of data that can be sent. The last known maximum size for an INQUIRE was %1 bytes. Try using a smaller key file, for example.").arg (pwm ? pwm->inquireMaxlen : 0);
      break;
    default:
      break;
    }

  if (pwm && (pwm->tlsError || pwmd_gnutls_error (pwm->handle(), nullptr)))
    {
      const char *str;
      (void) pwmd_gnutls_error (pwm->handle(), &str);

      return QString("TLS: %1\n").arg(str);
    }

  return QString ("Error %1: %2").arg (rc).arg (gpg_strerror (rc));
}

void
Pwmd::showError (gpg_error_t rc, Pwmd *pwm, bool bulk)
{
  if (rc && (gpg_err_code (rc) == GPG_ERR_CANCELED
             || (pwm && gpg_err_code (pwm->lastError) == GPG_ERR_CANCELED)))
    return;
  else if (!rc && !pwm->tlsError)
    return;

  if (pwm && pwm->lastError)
    {
      rc = pwm->lastError;
      pwm->lastError = 0;
    }

  QMessageBox m;
  
  m.setText (QApplication::
	     tr ("There was an error while communicating with pwmd."));
  if (bulk)
    m.setText (m.text () + tr (" One or more commands failed."));

  m.setInformativeText (QString ("ERR %1: %2: %3").arg (rc).arg (gpg_strsource (rc), gpg_strerror (rc)));
  m.setDetailedText(errorString(rc, pwm));
  m.setIcon (QMessageBox::Critical);
  m.exec ();
}

void
Pwmd::setData (QByteArray d)
{
  _data = d;
}

QByteArray
Pwmd::data ()
{
  return _data;
}

PwmdInquireData::PwmdInquireData ()
{
  _filename = QString ();
  _handle = nullptr;
  _user = QString ();
  _user2 = QString ();
  _data = nullptr;
  _size = 0;
  _sent = 0;
  _userInt = 0;
  _userPtr = nullptr;
  _userInternal = QString ();
  _keyFile = _encryptKeyFile = _signKeyFile = QString ();
  _item = nullptr;
  _internalData = nullptr;
  _internalInt = -1;
  _userBool = false;
  _extendedSave = _isSaving = false;
}

PwmdInquireData::PwmdInquireData (const QString & s, pwm_t * h)
{
  _filename = QString ();
  _handle = h;
  _user = QString ();
  _user2 = QString ();
  _data = nullptr;
  _size = 0;
  _sent = 0;
  _userBool = false;
  _userPtr = nullptr;
  _userInternal = QString ();
  _keyFile = _encryptKeyFile = _signKeyFile = QString ();
  _item = nullptr;
  _internalData = nullptr;
  _internalInt = -1;
  _userBool = false;
  _extendedSave = _isSaving = false;

  if (s.isEmpty ())
    return;

  _data = (char *) pwmd_calloc (1, s.toUtf8 ().length () + 1);
  if (_data)
    {
      _size = s.toUtf8 ().length ();
      memcpy (_data, s.toUtf8 ().data (), _size);
    }
}

PwmdInquireData::PwmdInquireData (pwm_t * h, QString f, char *data, size_t len)
{
  _data = data;
  _size = len;
  _filename = f;
  _handle = h;
  _user = QString ();
  _user2 = QString ();
  _sent = 0;
  _userBool = false;
  _userInternal = QString ();
  _keyFile = _encryptKeyFile = _signKeyFile = QString ();
  _item = nullptr;
  _internalData = nullptr;
  _internalInt = -1;
  _userBool = false;
  _extendedSave = _isSaving = false;
}

PwmdInquireData::~PwmdInquireData ()
{
  pwmd_free (_data);
  pwmd_free (_internalData);
}

void
PwmdInquireData::setSaving (bool extended)
{
  _isSaving = true;
  _extendedSave = extended;
}

bool
PwmdInquireData::isSaving (bool &extended)
{
  extended = _extendedSave;
  return _isSaving;
}

void
PwmdInquireData::setKeyFile (QString f)
{
  _keyFile = f;
}

const QString &
PwmdInquireData::keyFile ()
{
  return _keyFile;
}

void
PwmdInquireData::setEncryptKeyFile (QString f)
{
  _encryptKeyFile = f;
}

const QString &
PwmdInquireData::encryptKeyFile ()
{
  return _encryptKeyFile;
}

void
PwmdInquireData::setSignKeyFile (QString f)
{
  _signKeyFile = f;
}

const QString &
PwmdInquireData::signKeyFile ()
{
  return _signKeyFile;
}

void
PwmdInquireData::setData (const QString & s)
{
  pwmd_free (_data);
  _data = nullptr;
  _size = 0;
  _sent = 0;

  if (s.isEmpty ())
    return;

  _data = (char *) pwmd_calloc (1, s.toUtf8 ().length () + 1);
  if (_data)
    {
      _size = s.toUtf8 ().length ();
      memcpy (_data, s.toUtf8 ().data (), _size);
    }
}

void
PwmdInquireData::setData (char *d, size_t s)
{
  pwmd_free (_data);
  _data = d;
  _size = s;
  _sent = 0;
}

char *
PwmdInquireData::data ()
{
  return _data;
}

size_t
PwmdInquireData::size ()
{
  return _size;
}

void
PwmdInquireData::setFilename (QString f)
{
  _filename = f;
}

const QString &
PwmdInquireData::filename ()
{
  return _filename;
}

void
PwmdInquireData::setHandle (pwm_t * h)
{
  _handle = h;
}

pwm_t *
PwmdInquireData::handle ()
{
  return _handle;
}

void
PwmdInquireData::setUser (QString u)
{
  _user = u;
}

const QString &
PwmdInquireData::user ()
{
  return _user;
}

void
PwmdInquireData::setUser2 (QString u)
{
  _user2 = u;
}

const QString &
PwmdInquireData::user2 ()
{
  return _user2;
}

void
PwmdInquireData::setUserInternal (QString u)
{
  _userInternal = u;
}

const QString &
PwmdInquireData::userInternal ()
{
  return _userInternal;
}

void
PwmdInquireData::setSent (size_t n)
{
  _sent = n;
}

size_t
PwmdInquireData::sent ()
{
  return _sent;
}

void
PwmdInquireData::setUserBool (bool b)
{
  _userBool = b;
}

bool
PwmdInquireData::userBool ()
{
  return _userBool;
}

void
PwmdInquireData::setUserPtr (void *p)
{
  _userPtr = p;
}

void *
PwmdInquireData::userPtr ()
{
  return _userPtr;
}

void
PwmdInquireData::setUserInt (int n)
{
  _userInt = n;
}

int
PwmdInquireData::userInt ()
{
  return _userInt;
}

void
PwmdInquireData::setCommandItem (PwmdCommandQueueItem *item)
{
  _item = item;
}

PwmdCommandQueueItem *
PwmdInquireData::commandItem ()
{
  return _item;
}

QString
Pwmd::extractToken (const QString str, const QString token, int &offset)
{
  QString pos;
  int n = str.indexOf (token);

  offset = n;

  if (n != -1)
    {
      int i = 0;

      n += token.size();
      QString t = str.mid(n);

      while (t.at (i).isDigit ())
        pos.append (t.at (i++));

      i = pos.toInt();
      return str.mid (n+pos.size()+1, i);
    }

  return QString ();
}

void
Pwmd::slotProcessError (QProcess::ProcessError e)
{
  QString str;

  switch (e)
    {
    case QProcess::FailedToStart:
      str = tr ("Failed to spawn the qpwmc editor process. Do you have qpwmc in your PATH?");
      break;
    case QProcess::Crashed:
      str = tr ("The qpwmc editor process crashed.");
      break;
    case QProcess::Timedout:
      str = tr ("The qpwmc editor process timed out.");
      break;
    case QProcess::WriteError:
      str = tr ("The qpwmc editor suffered a write error.");
      break;
    case QProcess::ReadError:
      str = tr ("The qpwmc editor suffered a read error.");
      break;
    case QProcess::UnknownError:
      str = tr ("An unknown error occured in the qpwmc editor process.");
      break;
    default:
      break;
    }

  QMessageBox::critical (0, tr ("QPwmc Process Error"), str);
  spawnError = e;
}

bool
Pwmd::spawnEditor (QString &result, QString sock, QString file, QString path,
                   bool external)
{
  QStringList args;
  QProcess p;
  spawnError = 0;

  QObject::connect (&p, SIGNAL (errorOccurred (QProcess::ProcessError)), this, SLOT (slotProcessError (QProcess::ProcessError)));

  if (!sock.isEmpty ())
    {
      args.append ("-s");
      args.append (sock);
    }

  args.append ("-e");
  if (!file.isEmpty ())
    args.append (file);

  if (!path.isEmpty ())
    {
      args.append ("-p");
      args.append (path.replace ("<TAB>", "\t"));
    }

  args.append ("-d");
  args.append ("1");
  p.start (external ? "qpwmc" : QCoreApplication::applicationFilePath (),
           args, QIODevice::ReadOnly);
  p.waitForFinished (-1);

  if (spawnError)
    return false;

  result = p.readAllStandardOutput();
  QObject::disconnect (&p, SIGNAL (errorOccurred (QProcess::ProcessError)), this, SLOT (slotProcessError (QProcess::ProcessError)));
  return true;
}

gpg_error_t
Pwmd::cancel ()
{
  gpg_error_t rc = pwmd_cancel (pwm);

  // Possibly connecting (Android)
  if (gpg_err_code (rc) == GPG_ERR_INV_STATE)
    reset ();

  return rc;
}
