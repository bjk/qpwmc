/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QPushButton>
#include <QCommonStyle>
#include "cmdIds.h"
#include "pwmdKeyGenDialog.h"
#include "pwmdFileDialog.h"

static time_t now;

PwmdKeyGenDialog::PwmdKeyGenDialog (QWidget *p, Pwmd *h) : QDialog (p)
{
  pwm = h;
  genKeyError = 0;
  //setWindowFlags (Qt::WindowTitleHint);
  ui.setupUi (this);
  ui.f_generate->setHidden (true);
  QSize s = size ();
  resize (s.rwidth(), 0);

  disconnect(ui.buttonBox, SIGNAL(accepted ()), this, SLOT(accept ()));
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Ok);
  pb->disconnect (this);
  connect (pb, SIGNAL (clicked ()), this, SLOT (slotGenerate ()));
  connect (ui.ck_neverExpire, SIGNAL (stateChanged (int)), this,
           SLOT (slotNeverExpire (int)));
  connect (ui.ck_protected, SIGNAL (clicked ()), this, SLOT (slotProtected ()));
  connect (ui.le_name, SIGNAL (textChanged (const QString &)), this,
           SLOT (slotNameChanged (const QString &)));
  connect (ui.le_email, SIGNAL (textChanged (const QString &)), this,
           SLOT (slotEmailChanged (const QString &)));
  connect (ui.le_subKey, SIGNAL (textChanged (const QString &)), this,
           SLOT (slotSubKeyChanged (const QString &)));
  connect (ui.tb_keyFile, SIGNAL (clicked ()), this, SLOT (slotSelectKeyFile ()));
  QCommonStyle style;
  ui.tb_keyFile->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  connect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));

  QDateTime dt;
  now = time (nullptr);
  dt.setSecsSinceEpoch (now);
  ui.de_expire->setDateTime (dt.addYears (3));
  ui.de_expire->setMinimumDateTime (dt);

  QString user = qgetenv ("USER");
  ui.le_name->setText (qgetenv ("REALNAME").size () ? qgetenv ("REALNAME") : user);
  ui.le_email->setText (qgetenv ("EMAIL").size () ? qgetenv ("EMAIL") : user);
}

PwmdKeyGenDialog::~PwmdKeyGenDialog ()
{
}

void
PwmdKeyGenDialog::slotCommandResult (PwmdCommandQueueItem *item,
                                     QString result, gpg_error_t rc,
                                     bool queued)
{
  (void)result;

  if ((item->id () >= 0 && item->id () <= PwmdCmdIdOpenMax)
      || item->id () >= PwmdCmdIdSaveMax)
    {
      item->setSeen ();
      return;
    }

  if (rc && !item->checkError (rc) && !queued)
    {
      genKeyError = rc;
      Pwmd::showError (rc, pwm);
    }

  item->setSeen ();
}

void
PwmdKeyGenDialog::slotNameChanged (const QString &s)
{
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Ok);
  pb->setEnabled (!(s.isEmpty () && ui.le_email->text ().isEmpty ()));
}

void
PwmdKeyGenDialog::slotEmailChanged (const QString &s)
{
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Ok);
  pb->setEnabled (!(s.isEmpty () && ui.le_name->text ().isEmpty ()));
}

void
PwmdKeyGenDialog::slotSubKeyChanged (const QString &s)
{
  bool b = s.isEmpty ();

  ui.le_name->setEnabled (b);
  ui.le_email->setEnabled (b);
}

void
PwmdKeyGenDialog::slotProtected ()
{
  ui.le_keyFile->setEnabled (ui.ck_protected->isChecked ());
  ui.tb_keyFile->setEnabled (ui.ck_protected->isChecked ());
}

void
PwmdKeyGenDialog::slotNeverExpire (int s)
{
  ui.de_expire->setEnabled (!(s == Qt::Checked));
}

QString
PwmdKeyGenDialog::name ()
{
  return ui.le_name->text ();
}

void
PwmdKeyGenDialog::setName (const QString &s)
{
  ui.le_name->setText (s);
}


QString
PwmdKeyGenDialog::email ()
{
  return ui.le_email->text ();
}

void
PwmdKeyGenDialog::setEmail (const QString &s)
{
  ui.le_email->setText (s);
}

QString
PwmdKeyGenDialog::algorithm ()
{
  if (ui.le_algorithm->text ().isEmpty ())
    return "default";

  return ui.le_algorithm->text ();
}

bool
PwmdKeyGenDialog::protection ()
{
  return ui.ck_protected->isChecked ();
}

unsigned long
PwmdKeyGenDialog::expires ()
{
  unsigned long t;

  if (ui.ck_neverExpire->isChecked ())
    return 0;

  t = ui.de_expire->dateTime ().toSecsSinceEpoch () - now;
  if (!t)
    return (unsigned long)time (nullptr) - ui.de_expire->dateTime ().toSecsSinceEpoch ();

  return t;
}

QString
PwmdKeyGenDialog::usage ()
{
  if (ui.rb_sign->isChecked ())
    return "sign";
  else if (ui.rb_encrypt->isChecked ())
    return "encrypt";

  return "default";
}

void
PwmdKeyGenDialog::setSubKey (PwmdKey *key)
{
  setName (key->userIds ().at (0)->name ());
  setEmail (key->userIds ().at (0)->email ());
  ui.le_subKey->setText (key->subKeys ().at (0)->fingerprint ());
}

QString
PwmdKeyGenDialog::subKey ()
{
  return ui.le_subKey->text ();
}

void
PwmdKeyGenDialog::slotGenerate ()
{
  QString args;
  PwmdInquireData *inq = nullptr;

  if (!name ().isEmpty ())
    {
      args += "--userid=\"" + name ();
      if (!email ().isEmpty ())
        args += " <" + email () + ">";
      args += "\"";
    }
  else if (!email ().isEmpty ())
    args += "--userid=\"" + email () + "\"";

  if (!algorithm ().isEmpty ())
    args += " --algo=" + algorithm ();

  args += " --usage=" + usage ();

  if (ui.ck_protected->isEnabled () && !protection ())
    args += " --no-passphrase";

  if (ui.ck_neverExpire->isChecked ())
    args += QString (" --no-expire");
  else
    args += QString (" --expire=%1").arg (expires ());

  if (!subKey().isEmpty ())
    args += QString (" --subkey-of=%1").arg (subKey ());

  pwmd_socket_t type;
  pwmd_socket_type (pwm->handle (), &type);
  if (type != PWMD_SOCKET_LOCAL || !ui.le_keyFile->text ().isEmpty ())
    {
      gpg_error_t rc;

      inq = new PwmdInquireData (pwm->handle (), pwm->filename ());
      if (!ui.le_keyFile->text ().isEmpty ())
        {
          inq->setKeyFile (ui.le_keyFile->text ());
          inq->setEncryptKeyFile (ui.le_keyFile->text ());
          inq->setSignKeyFile (ui.le_keyFile->text ());
        }

      rc = pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 1);
      if (!rc)
        rc = pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 1);

      if (rc)
        {
          delete inq;
          pwm->showError (rc, pwm);
          return;
        }
    }

  QSize origSize = size ();
  ui.f_params->setEnabled (false);
  ui.f_generate->setHidden (false);
  ui.buttonBox->setEnabled (false);

  genKeyError = 0;
  PwmdCommandQueueItem *item = nullptr;
  if (pwm->genkey (args, inq ? Pwmd::inquireCallback : 0, inq, false, &item))
    {
      while (pwm->isQueued (item))
        {
          QApplication::processEvents ();
          QThread::msleep (50);
        }
    }

  if (genKeyError)
    {
      ui.f_params->setEnabled (true);
      ui.f_generate->setHidden (true);
      ui.buttonBox->setEnabled (true);
      QApplication::processEvents ();
      resize (origSize);
      return;
    }

  QDialog::accept ();
}

void
PwmdKeyGenDialog::slotSelectKeyFile ()
{
  PwmdFileDialog d (this, tr ("Passphrase file"));

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_keyFile->setText (d.selectedFiles ().at (0));
}
