/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QMenu>
#include <QGesture>
#include <QInputDevice>
#include "pwmdSaveWidget.h"
#include "pwmdKeyTreeWidget.h"

PwmdKeyTreeWidget::PwmdKeyTreeWidget (QWidget *p) : QTreeWidget (p)
{
  _isRecipientList = false;
  _parent = nullptr;

  foreach (const QInputDevice *i, QInputDevice::devices ())
    {
      if (i->type () == QInputDevice::DeviceType::TouchScreen)
        {
          grabGesture(Qt::TapAndHoldGesture);
          break;
        }
    }
}

PwmdKeyTreeWidget::~PwmdKeyTreeWidget ()
{
}

bool
PwmdKeyTreeWidget::event (QEvent *ev)
{
  if (ev->type () == QEvent::Gesture)
    return gestureEvent (static_cast<QGestureEvent *> (ev));

  return QTreeWidget::event (ev);
}

bool
PwmdKeyTreeWidget::gestureEvent (QGestureEvent *ev)
{
  if (QGesture *tap = ev->gesture (Qt::TapAndHoldGesture))
    tapAndHoldGesture (static_cast<QTapAndHoldGesture *>(tap));

  ev->accept ();
  return true;
}

void
PwmdKeyTreeWidget::tapAndHoldGesture (QTapAndHoldGesture *ev)
{
  if (ev->state () == Qt::GestureStarted)
    {
      if (isRecipientList())
        popupRecipientContextMenu ();
      else
        popupContextMenu ();
    }
}

void
PwmdKeyTreeWidget::setParent (PwmdSaveWidget *w)
{
  _parent = w;
}

void
PwmdKeyTreeWidget::keyPressEvent (QKeyEvent * ev)
{
  if (!_parent)
    {
      QTreeWidget::keyPressEvent (ev);
      return;
    }

  // Alt-Enter
  if (ev->key () == Qt::Key_Return && ev->nativeModifiers () == 8)
    {
      QCursor::setPos (mapToGlobal
                       (QPoint (visualItemRect (currentItem ()).topRight ())));
      if (isRecipientList())
        popupRecipientContextMenu ();
      else
        popupContextMenu ();
      return;
    }

  QTreeWidget::keyPressEvent (ev);
}

void
PwmdKeyTreeWidget::mouseReleaseEvent (QMouseEvent * ev)
{
  if (!_parent)
    return;

  if (ev->button () != Qt::RightButton)
    return;

  if (isRecipientList())
    popupRecipientContextMenu ();
  else
    popupContextMenu ();
  QTreeWidget::mouseReleaseEvent (ev);
}

void
PwmdKeyTreeWidget::mousePressEvent (QMouseEvent * ev)
{
  //dragStartPosition = ev->pos ();
  QTreeWidget::mousePressEvent (ev);
}

void
PwmdKeyTreeWidget::mouseDoubleClickEvent (QMouseEvent * ev)
{
  if (!_parent)
    {
      QTreeWidget::mouseDoubleClickEvent (ev);
      return;
    }

  QTreeWidget::mouseDoubleClickEvent (ev);
}

void
PwmdKeyTreeWidget::mouseMoveEvent (QMouseEvent * ev)
{
  _mousePosition = ev->pos ();
}

QPoint
PwmdKeyTreeWidget::mousePosition()
{
  return _mousePosition;
}

void
PwmdKeyTreeWidget::setIsRecipientList (bool b)
{
  _isRecipientList = b;
}

bool
PwmdKeyTreeWidget::isRecipientList ()
{
  return _isRecipientList;
}

void
PwmdKeyTreeWidget::popupContextMenu ()
{
  QTreeWidgetItem *item = _parent->ui.keyList->currentItem ();

  if (!item)
    return;

  QMenu *m = new QMenu (this);
  QAction *a = m->addAction (tr ("&Add recipient(s)"), _parent,
                             SLOT (slotAddRecipients ()));
  if (_parent->ui.keyList->selectedItems().count() == 0)
    a->setEnabled (false);
  else
    {
      QList <QTreeWidgetItem *> list = _parent->ui.keyList->selectedItems ();
      foreach (QTreeWidgetItem *item, list)
        {
          PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));
          if (_parent->ui.ck_symmetric->isChecked ()
              && !data->subKey()->canSign())
            {
              a->setEnabled (false);
              break;
            }
        }
    }

  a = m->addAction (tr ("Add &subkey..."), _parent,
                    SLOT (slotAddSubKey ()));
  if (_parent->ui.keyList->selectedItems().count() > 1)
    a->setEnabled (false);

  a = m->addAction (tr ("&Delete key..."), _parent,
                    SLOT (slotDeleteKey ()));
  if (_parent->ui.keyList->selectedItems().count() > 1)
    a->setEnabled (false);
  else
    {
      QList <QTreeWidgetItem *> list = _parent->ui.keyList->selectedItems ();

      a->setEnabled (false);
      foreach (QTreeWidgetItem *item, list)
        {
          QStringList l = _parent->recipients ().split (",");
          PwmdKeyItemData *data = qvariant_cast <PwmdKeyItemData * >(item->data (0, Qt::UserRole));

          if (l.contains (data->subKey ()->fingerprint ().right (16)))
            {
              a->setEnabled (true);
              break;
            }

          l = _parent->signers ().split (",");
          if (l.contains (data->subKey ()->fingerprint ().right (16)))
            {
              a->setEnabled (true);
              break;
            }
        }
    }

  m->addSeparator ();

  a = m->addAction (tr ("&Copy key ID"), _parent,
                    SLOT (slotCopyKeyIdToClipboard ()));
  if (_parent->ui.keyList->selectedItems().count() > 1)
    a->setEnabled (false);

  m->exec (QCursor::pos ());
  delete m;
}

void
PwmdKeyTreeWidget::popupRecipientContextMenu ()
{
  QPoint pos = mousePosition ();
  QTreeWidgetItem *item = itemAt (pos.x (), pos.y ());

  if (!item)
    return;

  QMenu *m = new QMenu (this);

  QAction *a = m->addAction (tr ("&Remove recipient(s)"), _parent,
                             SLOT (slotRemoveRecipients ()));
  a->setStatusTip (tr ("Remove the selected key(s) from the recipient list."));
  if (_parent->ui.recipientList->selectedItems().count() == 0)
    a->setEnabled (false);

  a = m->addAction (tr ("&Jump to key"), _parent,
                    SLOT (slotJumpToKey ()));
  a->setStatusTip (tr ("Jump to the selected key in the key list."));
  if (_parent->ui.recipientList->selectedItems().count() > 1
      ||_parent->ui.recipientList->selectedItems().count() == 0)
    a->setEnabled (false);

  a = m->addAction (tr ("&Copy key ID"), _parent,
                    SLOT (slotCopyRecipientKeyIdToClipboard ()));
  a->setStatusTip (tr ("Copy the key ID to the clipboard."));
  if (!_parent->ui.recipientList->selectedItems().count()
      || _parent->ui.recipientList->selectedItems().count() > 1)
    a->setEnabled (false);

  m->exec (QCursor::pos ());
  delete m;
}

void
PwmdKeyTreeWidget::focusInEvent (QFocusEvent *ev)
{
  (void)ev;
  _parent->changeKeyListFocus (this);
}

void
PwmdKeyTreeWidget::focusOutEvent (QFocusEvent *ev)
{
  (void)ev;
  _parent->changeKeyListFocus (this, false);
}
