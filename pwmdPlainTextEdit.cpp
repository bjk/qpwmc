/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QMimeData>
#include "pwmdPlainTextEdit.h"
#include "pwmdMainWindow.h"

PwmdPlainTextEdit::PwmdPlainTextEdit (QWidget * parent):QPlainTextEdit (parent)
{
  _confirmOnEnter = false;
  _parent = nullptr;
  setInputMethodHints (Qt::ImhNoPredictiveText|Qt::ImhNoAutoUppercase|Qt::ImhSensitiveData);
}

void
PwmdPlainTextEdit::setPwmdParent (PwmdMainWindow *p)
{
  _parent = p;
}

void
PwmdPlainTextEdit::setConfirmOnEnter (bool b)
{
  _confirmOnEnter = b;
}

void
PwmdPlainTextEdit::insertFromMimeData (const QMimeData * data)
{
  if (!data->hasText ())
    return;

  QMimeData *d = new QMimeData ();
  QString s = data->text ();

  s.replace ("\t", "<TAB>", Qt::CaseInsensitive);
  d->setText (s);
  QPlainTextEdit::insertFromMimeData (d);
  delete d;
}

void
PwmdPlainTextEdit::keyPressEvent (QKeyEvent *ev)
{
  if (_parent->isBusy)
    return;

  if (ev->key() == Qt::Key_Return && _confirmOnEnter)
    {
      emit updateContent ();
      return;
    }

  QPlainTextEdit::keyPressEvent (ev);
}

void
PwmdPlainTextEdit::mousePressEvent (QMouseEvent * ev)
{
  if (_parent->isBusy)
    return;

  QPlainTextEdit::mousePressEvent (ev);
}
