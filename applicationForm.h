/*
    Copyright (C) 2013-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef APPLICATIONFORM_H
#define APPLICATIONFORM_H

#include <QWizard>
#include "applicationFormFinalizePage.h"
#include "pwmdTreeWidget.h"
#include "applicationFormWidget.h"

#include "ui_applicationForm.h"

class PwmdMainWindow;
class ApplicationFormXmlStream;
class ApplicationFormWidget;
class ApplicationForm : public QWizard
{
  Q_OBJECT
 public:
  ApplicationForm (const QString &filename);
  ApplicationForm (Pwmd * = 0, const QString &filename = 0,
		   PwmdTreeWidget * = 0, PwmdMainWindow * = 0);
  ApplicationForm (const QString &filename, QString, QString, QString);
  ~ApplicationForm ();
  bool hasError ();
  void setHasError (bool = true);
  bool modified ();
  QString socket ();
  QString filename ();
  QString elementPath ();

 public slots:
  void slotRadioButtonClicked (bool);
  void slotPageChanged (int);

 signals:
  void knownHostRc (gpg_error_t);

 private slots:
  void slotCancel (bool);
  void slotChangeExpiry ();
  void slotAccepted ();
  void slotElementSelector ();
  void slotDateSelector (const QDate &);
  void slotSocketSelector ();
  void slotFormElementSelected (QTreeWidgetItem *, QTreeWidgetItem *);
  void slotKnownHostCallback (void *data, const char *host, const char *key,
                              size_t len);

 private:
  QString buildElementPath (QString path, QString element, bool &);
  void updateForm (QString, QString, QString);
  void doForm (const QString &);
  void addFormRow (QFormLayout *fl, QHBoxLayout *hb, ApplicationFormWidget *w,
		   QWizardPage *wp, bool withLabel);
  void addExpiry (ApplicationFormWidget *w, QHBoxLayout *hb);
  void keyPressEvent (QKeyEvent *);
  void setModified ();

  friend class ApplicationFormXmlStream;
  friend class ApplicationFormFinalizePage;
  ApplicationFormFinalizePage *finalPage;
  Ui::ApplicationForm ui;
  ApplicationFormXmlStream *stream;
  PwmdMainWindow *parent;
  PwmdTreeWidget *elementTree;
  bool _error;
  Pwmd *pwm;
  QList<QObject *> widgetsToDelete;
  QRegularExpressionValidator *lineEditValidator;
  QString _socket;
  QString _dataFilename;
  QString _elementPath;
  bool _modified;
};

class ApplicationElement
{
 public:
  ApplicationElement (QString path, bool hidden)
    {
      _isHidden = hidden;
      _path = path;
    };

  bool isHidden ()
  {
    return _isHidden;
  };

  QString path ()
  {
    return _path;
  };

  QString pathNoContent ()
  {
    QString s = _path;

    if (s.endsWith ("\t"))
      {
	s.chop (1);
	return s;
      }

    int n = _path.lastIndexOf ("\t");
    if (n != -1)
      return _path.left (n);

    return _path;
  };

  void setAttrs (QList<ApplicationFormAttr *> a)
  {
    _attrs = a;
  };

  QList<ApplicationFormAttr *> attrs ()
    {
      return _attrs;
    };

 private:
  bool _isHidden;
  QString _path;
  QList<ApplicationFormAttr *> _attrs;
};

#endif
