/*
    Copyright (C) 2013-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef APPLICATIONFORMFINALIZEPAGE_H
#define APPLICATIONFORMFINALIZEPAGE_H

#include <QWizardPage>
#include "ui_applicationFormFinalizePage.h"
#include "pwmdRemoteHost.h"
#include "pwmd.h"
#include "pwmdSaveDialog.h"

class ApplicationElement;
class ApplicationFormFinalizePage : public QWizardPage
{
  Q_OBJECT
 public:
  ApplicationFormFinalizePage (QWidget * = 0);
  ~ApplicationFormFinalizePage ();
  void setHandle (Pwmd *);
  void commit ();

 signals:
  void knownHostRc (gpg_error_t);

 private slots:
  void slotConnectionStateChanged (Pwmd::ConnectionState);
  void slotStatusMessage (QString, void *);
  void slotCommandResult (PwmdCommandQueueItem *item, QString result,
                          gpg_error_t rc, bool queued);
  void slotBusy (int, bool);
  void slotKnownHostCallback (void *data, const char *host, const char *key,
                              size_t len);

 private:
  void openFileFinalize ();
  bool getContentFinalize (ApplicationElement *, gpg_error_t &, bool, bool &);
  void attrStoreFinalize (ApplicationElement *, gpg_error_t &, bool);
  void save ();
  void message (const QString &);
  void connectionProgress (int);
  void cleanup ();

  Ui::ApplicationFormFinalizePage ui;
  Pwmd *pwm;
  PwmdRemoteHost hostData;
  QList<ApplicationElement *> elements;
  bool newFile;
  QString filename;
  bool ownHandle;
  bool saving;
};

#endif

