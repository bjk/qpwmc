/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDCLIENTINFO_H
#define PWMDCLIENTINFO_H

class ClientInfo
{
 public:
  ClientInfo (QString id, QString name, QString filename, QString host,
              bool lock, bool self, unsigned state, QString userId,
              QString username, time_t connectedAt);
  ~ClientInfo ();

  typedef enum {
      Init = 1,
      Idle,
      Command,
      Disconnect
  } State;

  QString id ();
  QString name ();
  QString filename ();
  QString host ();
  bool locked ();
  void setLocked (bool = true);
  bool self ();
  unsigned state ();
  void setState (unsigned);
  QString userId ();
  QString username ();
  time_t connectedAt ();
  time_t stale ();
  void setStale (time_t);
  static ClientInfo *parseLine (const QString &);

 private:
  QString _id;
  QString _name;
  QString _filename;
  QString _host;
  bool _lock;
  bool _self;
  unsigned _state;
  QString _userId;
  QString _username;
  time_t _connectedAt;
  time_t _stale;
};

#endif
