/*
    Copyright (C) 2018-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDFILEDIALOG_H
#define PWMDFILEDIALOG_H

#include <QFileDialog>

class PwmdFileDialog : public QFileDialog
{
 public:
   PwmdFileDialog (QWidget *, Qt::WindowFlags);
   PwmdFileDialog (QWidget * = 0, const QString & = QString (),
                   const QString & = QString (), const QString & = QString ());
   ~PwmdFileDialog ();
   int exec ();
   void setUpdateLastLocation (bool = true);
   bool updateLastLocation ();

 private:
   bool checkPermissions ();
   bool _updateLastLocation;
};

#endif
