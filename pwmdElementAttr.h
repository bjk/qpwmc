/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDELEMENTATTR_H
#define PWMDELEMENTATTR_H

#include <QString>
#include <QList>

class PwmdElementAttr
{
 public:
  PwmdElementAttr (const QString & n, const QString & v = 0);
  PwmdElementAttr (const PwmdElementAttr & other);
  QString name ();
  QString value ();
  void setName (const QString & n);
  void setValue (const QString & v = 0);
  static bool isReservedAttribute (const QString &attr);
  static bool isProtectedAttribute (const QString &attr);

 private:
  QString _name;
  QString _value;
};

class PwmdAttributeList:public QList < PwmdElementAttr * >
{
 public:
  PwmdAttributeList ();
  void incrRefCount ();
  void decrRefCount ();
  unsigned long refCount ();

 private:
  unsigned long _refCount;
};

#endif
