/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDGENPASSWIDGET_H
#define PWMDGENPASSWIDGET_H

#include <QFrame>
#include "ui_pwmdGenPassWidget.h"

class PwmdGenPassWidget : public QFrame
{
 Q_OBJECT
 public:
  typedef enum
    {
      Any, AlphaNumeric, Alpha, Numeric
    } Type;

  enum
    {
      MaxEntropyBits = 80
    };

  PwmdGenPassWidget (QWidget * = 0);
  ~PwmdGenPassWidget () { };
  void setPassword (const QString &, bool = false);
  QString password ();
  void setType (const QString &);
  QString type ();
  void setModified (bool = true);
  bool isModified ();
  void clipboardPassword ();
  void setEditable (bool);
  static QString generate (const QString &, int, QString = 0, bool = false);
  static unsigned quality (const QString &, unsigned max, double &bits);
  static QString generate (Type, int, const QString & = 0, bool = false);

 signals:
  void modified ();
  void clipboardTimer ();

 private slots:
   void slotShowPassword ();
   void slotGeneratePassword ();
   void slotGenerateTypeChanged (int);
   void slotConstraintsTypeChanged (int);
   void slotClipboardPassword ();
   void slotPasswordTextChanged (const QString &);
   void slotConstraintsTextChanged (const QString &);

 private:
  QString constraints (const QString &, bool &);
  Ui::PwmdGenPassWidget ui;
  bool _modified;
};

#endif
