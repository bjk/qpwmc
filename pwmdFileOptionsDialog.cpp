/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QMessageBox>
#include "pwmdFileOptionsDialog.h"
#include "pwmdMainWindow.h"

PwmdFileOptionsDialog::PwmdFileOptionsDialog (PwmdMainWindow *p) : QDialog (p)
{
  ui.setupUi (this);
  ui.fileOptions->setHandle (p->pwm);
  ui.fileOptions->setFilename (p->pwm->filename ());
  connect (ui.fileOptions, SIGNAL (clientLockStateChanged (bool)), this,
           SLOT (slotClientLockStateChanged (bool)));
  connect (ui.fileOptions, SIGNAL (commandsFinished ()), this,
           SLOT (slotDialogFinished ()));
  connect (p, SIGNAL(mainClientLockStateChanged(bool)), this, SLOT(slotParentClientLockStateChanged (bool)));
}

PwmdFileOptionsDialog::~PwmdFileOptionsDialog ()
{
}

void
PwmdFileOptionsDialog::slotDialogFinished ()
{
  QDialog::accept ();
}

void
PwmdFileOptionsDialog::slotParentClientLockStateChanged (bool b)
{
  ui.fileOptions->setLocked(b);
}

void
PwmdFileOptionsDialog::slotClientLockStateChanged (bool b)
{
  emit clientLockStateChanged (b);
}

void
PwmdFileOptionsDialog::accept ()
{
  if (ui.fileOptions->updateCacheTimeout ())
    QDialog::accept ();
}
