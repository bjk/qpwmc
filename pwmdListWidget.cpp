/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QMenu>
#include "pwmdMainWindow.h"
#include "pwmdListWidget.h"

PwmdListWidget::PwmdListWidget (QWidget * p):QListWidget (p)
{
  _parent = nullptr;
  grabGesture(Qt::TapAndHoldGesture);
}

void
PwmdListWidget::setPwmdParent (PwmdMainWindow *p)
{
  _parent = p;
}

bool
PwmdListWidget::event (QEvent *ev)
{
  if (ev->type() == QEvent::Gesture)
    return gestureEvent (static_cast<QGestureEvent *>(ev));

  return QListWidget::event (ev);
}

bool
PwmdListWidget::gestureEvent (QGestureEvent *ev)
{
  if (QGesture *tap = ev->gesture (Qt::TapAndHoldGesture))
    {
      tapAndHoldGesture (static_cast<QTapAndHoldGesture *>(tap));
      return true;
    }

  return false;
}

void
PwmdListWidget::tapAndHoldGesture (QTapAndHoldGesture *ev)
{
  if (ev->state () == Qt::GestureFinished)
    {
      if (_parent && _parent->isBusy)
        return;
      contextMenuEvent(nullptr);
    }
}

void
PwmdListWidget::contextMenuEvent (QContextMenuEvent * ev)
{
  if ((ev && ev->reason () == QContextMenuEvent::Other) || !_parent)
    return;

  PwmdTreeWidgetItemData *data = nullptr;
  if (_parent->attributeElement)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(_parent->attributeElement->data (0, Qt::UserRole));

  _parent->selectedAttribute = _parent->ui.attributeList->currentItem ();
  QMenu *m = new QMenu (this);
  connect (m, SIGNAL (hovered (QAction *)), _parent,
	   SLOT (slotMenuItemHovered (QAction *)));

  QAction *a = m->addAction (tr ("&New"), _parent, SLOT (slotNewAttribute ()));
  a->setEnabled (data && !data->badPermissions ());
  a->setEnabled (_parent->fixupToolBar (_parent->attributeToolBar, _parent->AttributeToolBarNew, false, true));

  a = m->addAction (tr ("&Delete"), _parent, SLOT (slotDeleteAttribute ()));
  a->setEnabled (_parent->fixupToolBar (_parent->attributeToolBar, _parent->AttributeToolBarDelete, false, true));

  if (!_parent->selectedAttribute)
    a->setEnabled (false);

  a = m->addAction (tr ("&Copy name"), _parent,
		  SLOT (slotClipboardAttribute ()));

  if (!_parent->selectedAttribute)
    a->setEnabled (false);

  m->exec (QCursor::pos ());
  delete m;
}

void
PwmdListWidget::keyPressEvent (QKeyEvent * ev)
{
  if (!_parent)
    {
      QListWidget::keyPressEvent (ev);
      return;
    }

  if (_parent->isBusy)
    {
      ev->ignore ();
      return;
    }

  if (ev->key () == Qt::Key_Return && ev->nativeModifiers () == 8)
    {
      QCursor::
	setPos (mapToGlobal
		(QPoint (visualItemRect (currentItem ()).topRight ())));
      contextMenuEvent (nullptr);
      return;
    }

  QListWidget::keyPressEvent (ev);
}

void
PwmdListWidget::mousePressEvent (QMouseEvent * ev)
{
  if (_parent && _parent->isBusy)
    return;

  QListWidget::mousePressEvent (ev);
}
