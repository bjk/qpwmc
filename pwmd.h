/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
/*
 * This is a generic class that wraps around the most used libpwmd functions.
 * It should serve its purpose for most applications and when it doesn't you
 * can reimplement it or use the handle() to do what's needed.
 */
#ifndef PWMD_H
#define PWMD_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QProcess>
#include <QStringList>
#include <QMetaType>
#include <libpwmd.h>
#include "pwmdRemoteHost.h"

/* Internal command ID's are < 0 although they may still be passed to
 * the commandResult() signal. */
#define PwmdCmdIdInvalid		-255
#define PwmdCmdIdRunQueue		-128 // Deprecated
#define PwmdCmdIdInternalCloseFile	-8
#define PwmdCmdIdInternalConnectHost	-7
#define PwmdCmdIdInternalGenKey		-6
#define PwmdCmdIdInternalStatusError	-5
#define PwmdCmdIdInternalConnect	-4
#define PwmdCmdIdInternalOpen		-3
#define PwmdCmdIdInternalSave		-2
#define PwmdCmdIdInternalPassword	-1

class Pwmd;
class PwmdInquireData;

/* Creates a new command to be passed to the command thread. */
class PwmdCommandQueueItem
{
 public:
  PwmdCommandQueueItem (int id, QString cmd = 0, pwmd_inquire_cb_t = 0,
                        PwmdInquireData * = 0);
  ~PwmdCommandQueueItem ();

  /* The command id which should be >= 0 since internal id's are < 0. */
  int id();

  /* Whether this command is flagged as queued. When this command is the first
   * in the command queue no action is done. You must call Pwmd::runQueue(). */
  void setQueuedOnly (bool);
  bool queuedOnly ();

  /* The command and callback function along with callback data. */
  QString command();
  pwmd_inquire_cb_t callback();
  PwmdInquireData *data();
  void setData (PwmdInquireData *);

  /* A known error code that should not flush the command queue for this
   * command. */
  void addError (gpg_error_t);
  void addError (gpg_err_source_t, gpg_err_code_t);

  /* Check that the passed error code is in the known error code list. */
  bool checkError (gpg_error_t);

  // Return the error list.
  QList <gpg_error_t> errorList ();

  /* Clear all known error codes. */
  void clearErrorList ();

  /*
   * Call this after you are done with this command in each receiver of the
   * Pwmd::commandResult() signal to let the garbage collector delete it.
   */
  void setSeen ();

  /*
   * Set an error string and code from a callback function of a command to be
   * shown in the UI thread.
   */
  void setErrorString (QString);
  QString errorString ();
  void setError (gpg_error_t);
  gpg_error_t error ();

  // Whether this command requires a server inquire.
  void setNeedsInquire (bool = true);
  bool needsInquire ();

  // Whether this is the final item in the queue with a unique command ID.
  bool isFinalItem ();

 private:
  friend class PwmdCommandThread;
  friend class PwmdCommandCollectorThread;
  friend class Pwmd;
  int seen ();
  /* Set host data for use with Pwmd::connectHost() and related signals. */
  void setHostData (PwmdRemoteHost &);
  PwmdRemoteHost &hostData ();
  void setCommand (QString);
  void setFinalItem (bool = true);

  int _id;
  QString _cmd;
  pwmd_inquire_cb_t _cb;
  PwmdInquireData *_data;
  QList <gpg_error_t> _errorList;
  int _seen;
  gpg_error_t _error;
  QString _errorString;
  bool _queuedOnly;
  PwmdRemoteHost _hostData;
  bool _needsInquire;
  bool _isFinal;
};

/* Runs a command queue in a separate thread to prevent blocking the UI. */
class PwmdCommandThread : public QThread
{
 Q_OBJECT
 public:
  PwmdCommandThread (Pwmd *p);
  ~PwmdCommandThread ();
  void run();
  void flushQueue (bool = false, gpg_error_t = 0);
  unsigned queued ();
  bool isQueued (int);
  bool isQueued (PwmdCommandQueueItem *);
  bool setCommand (PwmdCommandQueueItem *, bool queueOnly = false);

 private:
  friend class Pwmd;

  bool hasFinalItem (PwmdCommandQueueItem *, QList <PwmdCommandQueueItem *>);
  gpg_error_t connectSocket (PwmdCommandQueueItem *);
  gpg_error_t openFile (PwmdCommandQueueItem *);
  gpg_error_t saveFile (PwmdCommandQueueItem *);
  gpg_error_t genKey (PwmdCommandQueueItem *);
  gpg_error_t changePassword (PwmdCommandQueueItem *);
  static gpg_error_t knownHostCallback (void *data, const char *host,
                                        const char *key, size_t len);

  Pwmd *pwm;
  QList <PwmdCommandQueueItem *>queue;
  QMutex *queueMutex;
};

class PwmdCommandCollectorThread : public QThread
{
 Q_OBJECT
 public:
  PwmdCommandCollectorThread (Pwmd *p);
  ~PwmdCommandCollectorThread ();
  void run ();
  void addItem (PwmdCommandQueueItem *);
  bool isQueued (int);
  bool isQueued (PwmdCommandQueueItem *);

 private:
  Pwmd *pwm;
  QList <PwmdCommandQueueItem *> list;
  QMutex *listMutex;
};


/* Processes status messages when not in a command. It is started once the
 * connection is established. */
class PwmdStatusMessageThread : public QThread
{
 Q_OBJECT
 public:
  PwmdStatusMessageThread (Pwmd *p);
  void run ();

 private:
  Pwmd *pwm;
};

class Pwmd : public QObject
{
 Q_OBJECT
 public:
  /*
   * 'sock' is the Url to connect to. 'filename' is the default filename
   * to open which can be reset with setFilename(). 'clientName' is used
   * with the pwmd log file.
   */
  Pwmd (const QString & filename = 0, const QString & clientName = 0,
	const QString & sock = 0, QObject *parent = 0);
   ~Pwmd ();

  /* Sent as an argument to the stateChanged() signal. */
  enum ConnectionState
  {
    Init, Connecting, Connected, Opened
  };

  /* The ERROR status message obtained via gpgme/gnupg/gpg-agent. */
  gpg_error_t lastError;

  /* The current pwmd connection state. */
  ConnectionState state ();

  /* The data pointer passed to the statusMessage signal. */
  void setStatusMessageData (void *);
  void *statusMessageData ();

  /* Returns the currently set socket path. */
  QString socket ();

  /* Will not be considered until the next connect(). */
  void setSocket (const QString & sock);

  /* Returns the currently set data filename. */
  QString filename ();

  /*
   * To be called before open() and when a new data file is to be
   * opened.  The default the value passed to the constructor.
   */
  void setFilename (QString filename);

  /* Set user data. May be useful when this class is passed to other
   * classes.
   */
  void setData (QByteArray);
  QByteArray data ();

  /* If you use pwmd_command() from libpwmd on a handle obtained from
   * Pwmd:handle(), these may be needed since the status message thread
   * does socket IO but only after obtaining a mutex.
   */
  void lockMutex ();
  void unlockMutex ();

  /*
   * Sends a command to the pwmd server. When queueOnly is true, the command is
   * only added to the command queue. When false and there are other commands
   * in the queue, the queue is run as if runQueue() had been called. Commands
   * are run in first-in first-out order (FIFO).
   */
  virtual bool command (PwmdCommandQueueItem *cmd, bool queueOnly = false);

  /* Runs all queued commands. */
  virtual void runQueue ();

  /* Flush all queued commands. */
  virtual void flushQueue ();

  /* Returns the number of queued commands. */
  virtual unsigned queued ();

  /* Returns true when command 'id' or 'cmd' is in the command queue or waiting
   * to be garbage collected. */
  virtual bool isQueued (int id);
  virtual bool isQueued (PwmdCommandQueueItem *cmd);

  /* Cancel a connection in progress or running command. */
  virtual gpg_error_t cancel ();

  /* Set positional parameters to pwmd_connect(). The arguments are
   * added to pwmd_connect() in the order the were added to the list. See the
   * libpwmd(3) manual page for details about the parameters.
   */
  virtual void setConnectParameters (QStringList params);

  /* Set the data parameter for the knownHost signal. */
  virtual void setKnownHostData (void *data);
  virtual void *knownHostData ();

  /*
   * Connect the socket set with setSocket(). This function uses the
   * PwmdCmdIdInternalConnect command ID.
   */
  virtual bool connect (pwmd_inquire_cb_t = 0, void *data = 0,
                         bool queueOnly = false, PwmdCommandQueueItem ** = 0);

  /* Associates a handle with an existing connected socket. */
  virtual bool connectFd (int fd, pwmd_inquire_cb_t = 0, void *data = 0,
                          bool queueOnly = false, PwmdCommandQueueItem ** = 0);

  /* For use with connectFd() to keep track of the Connecting/Connected states.
   * You must wait for the connectReady() signal before calling connectFd().
   */
  virtual bool connectHost (PwmdRemoteHost hostData, bool queueOnly = false,
                            PwmdCommandQueueItem ** = 0);

  /* When set, a function to call when GPG_ERR_BAD_PASSPHRASE is
     returned during pwmd_open(). The function should return 0 to try
     again or an error code to cancel (normally
     GPG_ERR_BAD_PASSPHRASE). The 'data' parameter is set with
     setBadPassphraseData(). The 'final' parameter is set to true when
     the callback has been called at least once and before
     Pwmd::open() returns and in this case the return value of the
     callback is ignored. This lets the app for example reset any pinentry
     strings and only when needed.
  */
  virtual void setBadPassphraseCallback (gpg_error_t (*)(void *data, bool final));
  void setBadPassphraseData (void *);

  /*
   * Opens the filename specified with setFilename(). This function uses the
   * PwmdCmdIdInternalOpen command ID.
   */
  virtual bool open (pwmd_inquire_cb_t cb = 0, PwmdInquireData * = 0,
                      bool queueOnly = false, PwmdCommandQueueItem ** = 0);

  /*
   * Generates a new key. The parameter 'args' are any extra arguments to
   * pass to the pwmd GENKEY protocol command. This function uses the
   * PwmdCmdIdInternalGenKey command ID.
   */
  virtual bool genkey (const QString & args = QString (),
                       pwmd_inquire_cb_t cb = 0, PwmdInquireData * = 0,
                       bool queueOnly = false, PwmdCommandQueueItem ** = 0);

  /*
   * Saves any changes do disk. The parameter 'args' are any extra arguments to
   * pass to the pwmd SAVE protocol command. This function uses the
   * PwmdCmdIdInternalSave command ID.
   */
  virtual bool save (const QString & args = QString (),
                      pwmd_inquire_cb_t cb = 0, PwmdInquireData * = 0,
                      bool queueOnly = false, PwmdCommandQueueItem ** = 0);

  /*
   * Changes the passphrase for a data file or secret key. The parameter 'args'
   * are any extra arguments to pass to the pwmd PASSWD protocol command. This
   * function uses the PwmdCmdIdInternalPassword command ID.
   */
  virtual bool passwd (const QString & args = QString (),
                        pwmd_inquire_cb_t cb = 0, PwmdInquireData * = 0,
                        bool queueOnly = false, PwmdCommandQueueItem ** = 0);

  /*
   * Closes any active handle and resets the connection state to Init.
   */
  virtual gpg_error_t reset (bool doEmit = true, bool disco = true);

  /*
   * Keeps the connection open but closes the opened file. */
  virtual gpg_error_t close (bool queueOnly = false,
                             PwmdCommandQueueItem ** = 0);

  /* These should be set before opening a file. */
  void setLockOnOpen (bool = true);	// pwmd option LOCK_ON_OPEN
  bool lockOnOpen ();

  /* If you need call other libpwmd functions then you may need this. */
  pwm_t *handle ();

  /*
   * For convienence. Spawn a qpwmc process to select the socket, file and
   * element path parameters. The 'external' parameter is whether this instance
   * is from another process other than "qpwmc".
   *
   * Returns false on failure or true on success and sets result to the line
   * containing the parameter values. Use extractToken() to parse the result.
   */
  bool spawnEditor (QString &result, QString sock = 0, QString file = 0,
                    QString path = 0, bool external = true);

  /*
   * For convienence. Parse the string returned by spawnEditor() and return the
   * token value. Token may be one of: <SOCKET>, <SOCKET-ARG>, <SSH-AGENT>,
   * <TLS-VERIFY>, <CONNECT-TIMEOUT>, <SOCKET-TIMEOUT>, <FILE> or <PATH>. The
   * offset parameter is set to the position of the found token in str.
   */
  static QString extractToken (const QString str, const QString token,
                               int &offset);


  /* Shows a descriptive error string of the specified error code. */
  static void showError (gpg_error_t, Pwmd * = 0, bool bulk = false);
  static QString errorString(gpg_error_t, Pwmd * = 0);

  /* Returns the number of receivers for a 'signal'. */
  int receivers (const char *signal);

  /* For convienence. Handles passphrases and key files. */
  static gpg_error_t inquireCallback (void *user, const char *keyword,
				      gpg_error_t rc, char **data,
				      size_t * len);

  /*
   * For convienence. Shows a message box prompting to accept or reject a new
   * SSH hostname.
   */
  static gpg_error_t knownHostPrompt(void *data, const char *host,
                                     const char *key, size_t len);

  /*
   * Can be set before showError() to show an error string for a TLS error.
   */
  int tlsError;

 signals:
  /* Required for use with connectFd(). After calling connectHost(), wait for
   * this signal to continue obtaining the file descriptor to pass to
   * connectFd().
   */
  void connectReady (PwmdRemoteHost);

  /* Emitted when the connection state changes. */
  void stateChanged (Pwmd::ConnectionState);

  /*
   * Emitted when a command completes from the command queue. The 'item' is the
   * item passed to command (), or an internal item using an internal command
   * ID. In either case the item must not be deleted by the user. Instead, call
   * PwmdCommandQueueItem::setSeen() in each receiver of this signal when done
   * with the item to let it be garbage collected.
   *
   * The 'queued' parameter is whether the item was queued but removed from the
   * command queue do to a previous command failure.  When 'queued' is true the
   * same 'rc' as the command that failed is passed.
   */
  void commandResult (PwmdCommandQueueItem *item, QString result,
                      gpg_error_t rc, bool queued);

  /* Emitted when a status message is received from the server. */
  void statusMessage (QString msg, void *userData);

  /* Emitted at the start and end of a command or connection. The cmdId is the
   * command that trigged the signal. */
  void busy (int cmdId, bool);

  /*
   * Emitted when verification for a hostname is needed when connecting to a
   * remote SSH pwmd server. The parent object should send the "knownHostRc
   * (gpg_error_t)" signal and set the gpg_error_t to 0 for a successful
   * verification of the remote host, or an error code to terminate the
   * connection. This Pwmd object is already setup to receive the signal.
   *
   * The 'data' parameter should be set with Pwmd::setKnownHostData().
   */
  void knownHost (void *data, const char *host, const char *hash, size_t len);

#ifdef Q_OS_ANDROID
  void passphrase (Pwmd *, QString keyword, QString desc, QString prompt,
                   QPrivateSignal);
  void passphraseResult (QString passphrase, QPrivateSignal);
#endif

 private slots:
  void slotProcessError (QProcess::ProcessError e);
  void slotKnownHostRc (gpg_error_t);
#ifdef Q_OS_ANDROID
  void slotPassphrase (Pwmd *, QString, QString, QString);
  void slotPassphraseResult (QString);
#endif

 private:
  void resetHandle ();
  gpg_error_t doConnect (bool, pwmd_inquire_cb_t, void *);
  static gpg_error_t statusCallback (void *, const char *);
  static gpg_error_t sshPassphraseCallback (void *, const char *, const char *,
                                            char **);
  char *connectParameterValue (int);
  static gpg_error_t passphraseCommon (Pwmd *, const QString &keyfile,
                                       char **r_passphrase, size_t *r_size,
                                       QString keyword, PwmdInquireData *inq);

  friend class PwmdStatusMessageThread;
  friend class PwmdCommandThread;
  friend class PwmdCommandCollectorThread;
  PwmdStatusMessageThread statusMsgThread;
  PwmdCommandThread commandThread;
  PwmdCommandCollectorThread collectorThread;
  QRecursiveMutex *commandMutex;
  pwm_t *pwm;
  ConnectionState _state;
  QString _socket;
  QString _filename;
  QString clientName;
  bool _lockOnOpen;
  QString _knownhosts;
  QString _identity;
  unsigned inquireMaxlen;
  QStringList _connectParameters;
  gpg_error_t (*badPassphraseCallback) (void *, bool);
  void *badPassphraseData;
  QByteArray _data;
  bool _quit;
  bool spawnError;
  bool needKnownHostRc;
  gpg_error_t knownHostRc;
#ifdef Q_OS_ANDROID
  bool needPassphraseRc;
  gpg_error_t passphraseRc;
  QString thePassphraseResult;
  QString pinentryHint;
  QString pinentryInfo;
#endif
  void *_knownHostData;
  void *_statusMessageData;
};

/* This is used in to handle pinentry loopback mode over an SSH channel, for
 * key files and for other inquire data when the inquire type is
 * NoInquirePassphrase. It may also be used generically in your app by passing
 * this class as the data parameter of an inquire callback such as the static
 * Pwmd::inquireCallback().
 *
 * Note that the data of this class must be allocated with the
 * libpwmd(3) allocators when set upon the ctor or setData(). When this
 * class is deleted the data will be freed in the dtor.
 */
class PwmdInquireData
{
 public:
  PwmdInquireData ();
  PwmdInquireData (const QString &, pwm_t * = 0);
  PwmdInquireData (pwm_t * handle, QString filename = 0, char *data = 0,
                   size_t size = 0);
  ~PwmdInquireData ();
  void setFilename (QString);
  const QString & filename ();
  void setKeyFile (QString);
  const QString & keyFile ();
  void setEncryptKeyFile (QString);
  const QString & encryptKeyFile ();
  void setSignKeyFile (QString);
  const QString & signKeyFile ();
  void setData (char *, size_t);
  void setData (const QString &);
  char *data ();
  size_t size ();
  void setHandle (pwm_t *);
  pwm_t *handle ();
  void setUser (QString);
  const QString & user ();
  void setUser2 (QString);
  const QString & user2 ();
  void setSent (size_t);
  size_t sent ();
  void setUserBool (bool);
  bool userBool ();
  void setUserPtr (void *);
  void *userPtr ();
  void setUserInt (int);
  int userInt ();
  void setSaving (bool extended = false);
  bool isSaving (bool &extended);

  /* For internal commands which may be used in a callback function. */
  void setCommandItem (PwmdCommandQueueItem *);
  PwmdCommandQueueItem *commandItem ();

 private:
  friend class Pwmd;
  friend class PwmdCommandThread;
  void setUserInternal (QString);
  const QString & userInternal ();

  QString _keyFile;
  QString _encryptKeyFile;
  QString _signKeyFile;
  char *_data;
  size_t _size;
  pwm_t *_handle;
  QString _filename;
  QString _user;
  QString _user2;
  size_t _sent;
  bool _userBool;
  void *_userPtr;
  int _userInt;
  QString _userInternal;
  PwmdCommandQueueItem *_item;
  void *_internalData; // for keyfiles.
  int _internalInt; // File descriptor for pwmd_connect_fd().
  Pwmd *pwm;
  bool _isBulk;
  bool _extendedSave;
  bool _isSaving;
};

Q_DECLARE_METATYPE (Pwmd::ConnectionState);

#endif
