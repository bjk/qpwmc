/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QStatusBar>
#include <QStatusTipEvent>
#include <QColorDialog>
#include <QClipboard>
#include <QTimer>
#include <QCursor>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QMessageBox>
#include <QMenu>
#include <QContextMenuEvent>
#include <QSettings>
#include <QDateTime>
#include <QCommonStyle>
#include <QScroller>
#include <time.h>

#include "pwmdMainWindow.h"
#include "pwmdTreeWidget.h"
#include "pwmdGenPassDialog.h"
#include "applicationForm.h"
#include "pwmdSocketDialog.h"
#include "pwmdOptionsDialog.h"
#include "pwmdClientDialog.h"
#include "pwmdOpenDialog.h"
#include "pwmdSaveDialog.h"
#include "pwmdFileOptionsDialog.h"
#include "pwmdSaveDialog.h"
#include "main.h"
#include "cmdIds.h"
#include "pwmdExpireDialog.h"
#include "pwmdFileDialog.h"
#include "pwmdLineEdit.h"
#include "pwmdSearchDialog.h"
#ifdef Q_OS_ANDROID
#include "pwmdAndroidJNI.h"
#endif

/* Custom QEvents. */
#define PwmdEventTargetAttr		QEvent::User+1
#define PwmdEventNewElementCancel	QEvent::User+2
#define PwmdEventNewAttributeCancel	QEvent::User+3
#define PwmdEventDnDTargetAttr		QEvent::User+4
#define PwmdEventRefreshElementTree	QEvent::User+5
#define PwmdEventReloadConfig		QEvent::User+6
#define PwmdEventOpenFileClose		QEvent::User+7
#define PwmdEventOpenFileConfirm	QEvent::User+8
#define PwmdEventClientListClose	QEvent::User+9
#define PwmdEventFileOptionsClose	QEvent::User+10
#define PwmdEventOptionsClose		QEvent::User+11
#define PwmdEventAttrsRetrieved		QEvent::User+12
#define PwmdEventBusy			QEvent::User+13

/* The current tab in the content splitter. */
#define TabContent 0
#define TabAttributes 1
#define TabCommand 2

/* Menu items and need to be in positional order. */
#define FileMenuOpen 0
#define FileMenuOpenForm 1
#define FileMenuClose 2
#define FileMenuSave 4
#define FileMenuSaveExt 5
#define ElementMenuNewRoot 0
#define ElementMenuNew 1
#define ElementMenuNewSibling 2
#define ElementMenuDelete 3
#define ElementMenuRename 4
#define ElementMenuFind 6
#define ElementMenuAdvancedFind 7
#define ElementMenuResolveTargets 8
#define ElementMenuEditContent 10
#define ElementMenuRefreshTree 11
#define ElementMenuMulti 12
#define ContentMenuInsert 0
#define ContentMenuSave 1
#define ContentMenuClipboard 2
#define ContentMenuPassword 4
#define ContentMenuExpiry 5
#define ContentMenuHidden 6
#define ContentMenuBase64 7
#define ContentMenuUpdate 9
#define ContentMenuRefresh 10
#define AttributeMenuNew 0
#define AttributeMenuDelete 1
#define AttributeMenuTarget 2
#define AttributeMenuUpdate 4
#define AttributeMenuRefresh 5
#define ViewMenuButtonLabels 0
#define ViewMenuExpandAll 2
#define ViewMenuClientList 4

/* A bitmask for enabled menu/toolbar items. Needed for a multi-selection to
 * determine which items out of the selected items are able to perform which
 * actions. */
#define ElementNewMask 0x01
#define ElementDeleteMask 0x02
#define ContentPasswordMask 0x04
#define ContentExpiryMask 0x08
#define AttributeNewMask 0x10
#define AttributeDeleteMask 0x20

static PwmdInquireData *openInquireData;
static bool reconnectSocket;
static bool disconnectSocket;
static bool updateOnEnter;
static QLabel *statusBarLabel;
static bool incrementalSearch;
static QStringList expandedItemsList;
static QTreeWidgetItem *nextElement;
static QWidget *nextTabWidget;
static int hiddenContent;
#ifdef Q_OS_ANDROID
static bool findingElement;
PwmdAndroidJNI *androidJni;
#endif
static QList <SearchResult *>searchResults;
static SearchResult *currentSearchResult;
static bool prevSearchResult;
static int releaseTimeout;
static int origReleaseTimeout;

static void freeSearchResults ();

enum { Content, Password, Attributes };

PwmdMainWindow::PwmdMainWindow (const QString & f, const QString & name,
                                bool lock, const QString & path,
                                const QString &sock, QWidget * p) :
  QMainWindow (p)
{
  qRegisterMetaType <Pwmd::ConnectionState> ("ConnectionState");
  qRegisterMetaType <gpg_error_t>("gpg_error_t");
  ui.setupUi (this);

  searchResultStatusBarLabel = nullptr;
  advancedSearchDialog = nullptr;
  connectedUser = QString ();
  dndOperation = DndNone;
  newElement = nullptr;
  nextElement = nullptr;
  newAttribute = nullptr;
  previousElement = nullptr;
  oldSelectedItem = nullptr;
  previousRightPage = nullptr;
  selectedElement = attributeElement = nullptr;
  targetAttributeElement = nullptr;
  selectedElementPath = path;
  selectedAttribute = nullptr;
  selectedFilename = QString ();
  nSelectedElements = 0;
  previousAttributeText = QString ();
  copyElement = nullptr;
  newFilename = nullptr;
  iterationsOrig = 0;
  clientDialog = nullptr;
  optionsDialog = nullptr;
  fileOptionsDialog = nullptr;
  openDialog = nullptr;
  _filename = QString ();
  cancelButton = nullptr;
  isBusy = false;
  features = pwmd_features ();
  state = Pwmd::Init;
  commandInquireFilename = QString ();
  lockTimeout = 0;
  hiddenContent = -1;
  searchAndReplace = false;

  setupToolbars ();
  readConfigSettings (_filename, _socket);
  if (incrementalSearch)
    connect (elementToolBar->widgetForAction (findElementAction),
             SIGNAL (editTextChanged (const QString &)), this,
             SLOT (slotFindTextChanged (const QString &)));

  statusBarLabel = new QLabel ();
  statusBar()->addWidget (statusBarLabel);

  searchResultStatusBarLabel = new QLabel;
  searchResultStatusBarLabel->setAlignment (Qt::AlignRight|Qt::AlignVCenter);
  searchResultStatusBarLabel->setIndent (15);
  statusBar()->addPermanentWidget (searchResultStatusBarLabel);
  searchResultStatusBarLabel->hide ();

  if (!f.isEmpty ())
    _filename = f;

  if (!sock.isEmpty ())
    _socket = sock;

  connect (ui.tabWidget, SIGNAL (currentChanged (int)), this, SLOT (slotTabChanged (int)));
  QScroller::grabGesture(ui.contentScrollArea, QScroller::TouchGesture);
  QScroller::grabGesture(ui.attributeScrollArea, QScroller::TouchGesture);
  QScroller::grabGesture(ui.commandScrollArea, QScroller::TouchGesture);

  // File signals
  connect (ui.actionOpen, SIGNAL (triggered ()), this, SLOT (slotOpenFile ()));
  connect (ui.actionOpenForm, SIGNAL (triggered ()), this, SLOT (slotOpenForm ()));
  connect (ui.actionClose, SIGNAL (triggered ()), this, SLOT (slotCloseFile()));
  connect (ui.actionSave, SIGNAL (triggered ()), this, SLOT (slotSaveDocument()));
  connect (ui.actionExtendedSave, SIGNAL (triggered ()), this, SLOT (slotSaveExtended ()));
  connect (ui.actionFileOptions, SIGNAL (triggered ()), this, SLOT (slotFileOptions()));
  connect (ui.actionGeneralOptions, SIGNAL (triggered()), this, SLOT (slotGeneralOptions ()));
  connect (ui.actionReload, SIGNAL (triggered ()), this, SLOT (slotReload()));
  connect (ui.actionQuit, SIGNAL (triggered()), this, SLOT (close()));

  // View signals
  connect (ui.actionButtonLabels, SIGNAL (triggered(bool)), this, SLOT (slotButtonLabels(bool)));
  connect (ui.actionClientList, SIGNAL (triggered()), this, SLOT (slotClientList()));
  connect (ui.actionExpandAll, SIGNAL (triggered(bool)), this, SLOT (slotExpandAll(bool)));

  // Socket signals
  connect (ui.actionSocketOptions, SIGNAL (triggered()), this, SLOT (slotSocketOptions ()));

  // Element signals
  connect (ui.actionNewRootElement, SIGNAL (triggered()), this, SLOT (slotNewRootElement()));
  connect (ui.actionNewElement, SIGNAL (triggered()), this, SLOT (slotNewElement()));
  connect (ui.actionNewSiblingElement, SIGNAL (triggered()), this, SLOT (slotNewSiblingElement ()));
  connect (ui.actionDeleteElement, SIGNAL (triggered()), this, SLOT (slotDeleteElement()));
  connect (ui.actionRenameElement, SIGNAL (triggered()), this, SLOT (slotRenameElement()));
  connect (ui.actionFindElement, SIGNAL (triggered()), this, SLOT (slotFindElement()));
  connect (ui.actionEditContentOrAttributes, SIGNAL (triggered()), this, SLOT (slotEditContent ()));
  fixupEditContentOrAttributes (true);
  connect (ui.actionResolveTargets, SIGNAL (triggered ()), this, SLOT (slotResolveTargets ()));
  connect (ui.actionRefreshTree, SIGNAL (triggered ()), this, SLOT (slotRefreshElementTree ()));
#ifdef Q_OS_ANDROID
  connect (ui.actionToggleMulti, SIGNAL (toggled(bool)), this, SLOT (slotToggleMultiMode(bool)));
#endif

  // Content signals
  ui.textEdit->setPwmdParent (this);
  connect (ui.actionInsertFile, SIGNAL (triggered()), this, SLOT (slotInsertContent()));
  connect (ui.actionSaveToFile, SIGNAL (triggered()), this, SLOT (slotSaveToFile()));
  connect (ui.actionCopyToClipboard, SIGNAL (triggered ()), this, SLOT (slotCopyContentToClipboard ()));
  connect (ui.actionCreatePasswordAttribute, SIGNAL (triggered()), this, SLOT (slotCreatePasswordAttribute()));
  connect (ui.actionChangeExpire, SIGNAL (triggered()), this, SLOT (slotChangeExpire()));
  connect (ui.actionHidden, SIGNAL (toggled(bool)), this, SLOT (slotToggleHiddenContent(bool)));
  connect (ui.actionBase64, SIGNAL (triggered(bool)), this, SLOT (slotToggleBase64Content(bool)));
  connect (ui.actionUpdateContent, SIGNAL (triggered()), this, SLOT (slotSaveContent ()));
  connect (ui.actionRefreshContent, SIGNAL (triggered()), this, SLOT (slotRefreshContent ()));
  connect (ui.passwordWidget, SIGNAL (modified ()), this, SLOT (slotPasswordContentChanged ()));

  // Attribute signals
  ui.attributeList->setPwmdParent (this);
  ui.attributeContent->setPwmdParent (this);
  ui.attributeList->setItemDelegate (new PwmdListWidgetItemDelegate (ui.attributeList));
  connect (ui.attributeList, SIGNAL (currentItemChanged (QListWidgetItem *, QListWidgetItem *)), this, SLOT (slotAttributeSelected (QListWidgetItem *, QListWidgetItem *)));
  connect (ui.attributeList->itemDelegate (), SIGNAL (commitData (QWidget *)), this, SLOT (slotConfirmNewAttribute (QWidget *)));
  connect (ui.attributeList->itemDelegate (), SIGNAL (closeEditor (QWidget *, QAbstractItemDelegate::EndEditHint)), this, SLOT (slotAttributeEditorClosed (QWidget *, QAbstractItemDelegate::EndEditHint)));
  connect (ui.actionNewAttribute, SIGNAL (triggered ()), this, SLOT (slotNewAttribute()));
  connect (ui.actionDeleteAttribute, SIGNAL (triggered ()), this, SLOT (slotDeleteAttribute ()));
  connect (ui.actionToggleTargetAttribute, SIGNAL (triggered (bool)), this, SLOT (slotEditTargetAttributes(bool)));
  connect (ui.actionUpdateAttribute, SIGNAL (triggered ()), this, SLOT (slotSaveContent()));
  connect (ui.actionRefreshAttributes, SIGNAL (triggered ()), this, SLOT (slotRefreshAttributes ()));
  QRegularExpression rx ("^/[^/]+/(.*)$");
  ui.cb_searchAndReplace->setValidator (new QRegularExpressionValidator (rx,
                                                                         ui.cb_searchAndReplace));

  ui.cb_searchAndReplace->lineEdit ()->setClearButtonEnabled (true);
  ui.cb_searchAndReplace->lineEdit ()->setPlaceholderText (tr ("/pattern/replace"));
  connect (ui.cb_searchAndReplace->lineEdit (), SIGNAL (textChanged (const QString &)), this,
           SLOT (slotAttrSearchAndReplaceChanged (const QString &)));
  connect (ui.cb_searchAndReplace->lineEdit (), SIGNAL (returnPressed ()), this,
           SLOT (slotAttrSearchAndReplace ()));
  ui.pb_searchAndReplace->setIcon (QIcon (":icons/system-search-replace.svg"));
  connect (ui.pb_searchAndReplace, SIGNAL (clicked ()), this,
           SLOT (slotAttrSearchAndReplace ()));

  // Command signals
  connect (ui.commandTextEdit, SIGNAL (textChanged ()),
           SLOT (slotCommandContentChanged ()));
  connect (ui.commandResultTextEdit, SIGNAL (anchorClicked (const QUrl &)),
           this, SLOT (slotCommandAnchorClicked (const QUrl &)));
  connect (ui.commandResultTextEdit, SIGNAL (textChanged ()),
	   SLOT (slotCommandResultContentChanged ()));
  connect (ui.pb_commandSearch, SIGNAL (clicked()), this, SLOT (slotSearchCommandResults ()));
  connect (ui.ck_commandInquire, SIGNAL (stateChanged (int)), this,
	   SLOT (slotCommandInquireStateChanged (int)));
  connect (ui.cb_commandHistory, SIGNAL (activated (int)), this,
	   SLOT (slotCommandHistoryChanged (int)));
  connect (ui.cb_commandHistory, SIGNAL (currentIndexChanged (int)), this,
	   SLOT (slotCommandHistoryChanged (int)));
  connect (ui.tb_commandInquireFilename, SIGNAL (clicked ()), this,
           SLOT (slotCommandInquireFilename ()));
  ui.commandTextEdit->setPwmdParent (this);
  slotClearCommand();

  // About signals
  connect (ui.actionVersion, SIGNAL (triggered()), this, SLOT (slotVersion ()));
  connect (ui.actionAboutQt, SIGNAL (triggered()), qApp, SLOT (aboutQt ()));

  progressBar = new QProgressBar (ui.statusbar);
  ui.statusbar->addPermanentWidget (progressBar);
  progressBar->setHidden (true);
  progressBar->setAlignment (Qt::AlignHCenter);

  ui.elementTree->setPwmdParent (this);
  ui.elementTree->header ()->setSectionResizeMode (0, QHeaderView::ResizeToContents);
  ui.elementTree->header ()->setStretchLastSection (false);
  ui.elementTree->setItemDelegate (new PwmdTreeWidgetItemDelegate (ui.elementTree));

  QAction *act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this, SLOT (slotDeleteElement ()));
  ui.elementTree->addAction (act);
  act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this, SLOT (slotNewElement ()));
  ui.elementTree->addAction (act);
  act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this, SLOT (slotRenameElement ()));
  ui.elementTree->addAction (act);
  act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this, SLOT (slotEditAttributes ()));
  ui.elementTree->addAction (act);
  act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this, SLOT (slotResolveTargets ()));
  ui.elementTree->addAction (act);
  act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this, SLOT (slotExecuteCommand ()));
  ui.elementTree->addAction (act);
  act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this, SLOT (slotExpandElement ()));
  ui.elementTree->addAction (act);
  act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this, SLOT (slotCollapseElement ()));
  ui.elementTree->addAction (act);
  act = new QAction (ui.elementTree);
  connect (act, SIGNAL (triggered ()), this,
	   SLOT (slotClipboardElementPath ()));
  ui.elementTree->addAction (act);

  connect (ui.elementTree->itemDelegate (), SIGNAL (commitData (QWidget *)),
	   this, SLOT (slotRenameElementUpdate (QWidget *)));
  connect (ui.elementTree->itemDelegate (),
	   SIGNAL (closeEditor
		   (QWidget *, QAbstractItemDelegate::EndEditHint)), this,
	   SLOT (slotElementEditorClosed
		 (QWidget *, QAbstractItemDelegate::EndEditHint)));
  connect (ui.elementTree,
	   SIGNAL (currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)),
	   this,
	   SLOT (slotElementSelected (QTreeWidgetItem *, QTreeWidgetItem *)));
  connect (ui.elementTree,
	   SIGNAL (itemEntered (QTreeWidgetItem *, int)),
	   this,
	   SLOT (slotElementEntered(QTreeWidgetItem *, int)));
  connect (ui.elementTree,
	   SIGNAL (itemSelectionChanged ()),
	   this,
	   SLOT (slotItemSelectionChanged ()));

  connect (ui.textEdit, SIGNAL (textChanged ()),
	   SLOT (slotContentChanged ()));
  connect (ui.textEdit, SIGNAL (modificationChanged (bool)),
	   SLOT (slotContentModified (bool)));

  setContentItemsEnabled (true);
  clipboardTimer = new QTimer (this);
  clipboardTimer->setSingleShot (true);
  connect (clipboardTimer, SIGNAL (timeout ()), SLOT (slotClearClipboard ()));
  connect (ui.passwordWidget, SIGNAL (clipboardTimer ()), this, SLOT (slotClipboardTimer ()));

  releaseTimer = new QTimer(this);
  connect(releaseTimer, SIGNAL(timeout()), this, SLOT(slotReleaseTimer()));
  newFile = false;
  editTargetAttributes = false;
  ui.hiddenContentFrame->setHidden (true);

  pwm = new Pwmd (filename (), name, 0, this);
  ui.elementTree->setPwmdHandle (pwm);
  pwm->setBadPassphraseCallback (PwmdMainWindow::badPassphraseCallback);
  pwm->setBadPassphraseData (this);
  connect (pwm, SIGNAL (busy (int, bool)), this, SLOT (slotBusy (int, bool)));
  if (PwmdRemoteHost::fillRemoteHost (_socket, currentHostData))
    {
      pwm->setSocket (PwmdRemoteHost::socketUrl (currentHostData));
      pwm->setConnectParameters (currentHostData.socketArgs ());
    }
  else
    pwm->setSocket (_socket);

  pwm->setLockOnOpen (lock);
  connect (pwm, SIGNAL (stateChanged (Pwmd::ConnectionState)), this,
	   SLOT (slotConnectionStateChanged (Pwmd::ConnectionState)));
#ifdef Q_OS_ANDROID
  ui.menubar->setNativeMenuBar (false);
  connect (qApp, SIGNAL (applicationStateChanged (Qt::ApplicationState)),
           this, SLOT (slotApplicationStateChanged (Qt::ApplicationState)));
  androidAuthenticating = false;
  androidJni = new PwmdAndroidJNI ();
  androidJni->init ();
  qRegisterMetaType <PwmdRemoteHost>("PwmdRemoteHost");
  connect (androidJni, SIGNAL (jniAuthenticated ()), this, SLOT (slotAndroidAuthenticated ()));
  connect (androidJni, SIGNAL (jniConnectionResult (PwmdRemoteHost, int)), this, SLOT (slotAndroidConnectResult (PwmdRemoteHost, int)));
  connect (androidJni, SIGNAL (jniConnectionError (gpg_error_t)), this,
	   SLOT (slotAndroidConnectionError (gpg_error_t)));
  connect (pwm, SIGNAL (connectReady (PwmdRemoteHost)), this, SLOT (slotAndroidConnectReady (PwmdRemoteHost)));
#endif
  connect (pwm, SIGNAL (statusMessage (QString, void *)), this,
	   SLOT (slotStatusMessage (QString, void *)));
  connect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));
  connect (pwm, SIGNAL (knownHost (void *, const char *, const char *, size_t)), this, SLOT (slotKnownHostCallback (void *, const char *, const char *, size_t)));

  setConnected (false);

  if (!filename().isEmpty ())
    QTimer::singleShot (1, this, SLOT (slotConnectSocket()));
}

PwmdMainWindow::~PwmdMainWindow ()
{
  disconnect (ui.elementTree,
              SIGNAL (currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)),
              this,
              SLOT (slotElementSelected (QTreeWidgetItem *, QTreeWidgetItem *)));
  disconnect (ui.elementTree, SIGNAL (itemEntered (QTreeWidgetItem *, int)),
              this, SLOT (slotElementEntered(QTreeWidgetItem *, int)));
  disconnect (ui.elementTree->itemDelegate (), SIGNAL (commitData (QWidget *)),
              this, SLOT (slotRenameElementUpdate (QWidget *)));
  disconnect (ui.elementTree->itemDelegate (),
              SIGNAL (closeEditor
                      (QWidget *, QAbstractItemDelegate::EndEditHint)), this,
              SLOT (slotElementEditorClosed
                    (QWidget *, QAbstractItemDelegate::EndEditHint)));
  disconnect (ui.elementTree,
              SIGNAL (itemSelectionChanged ()),
              this,
              SLOT (slotItemSelectionChanged ()));
  clearElementTree (ui.elementTree);
  delete advancedSearchDialog;
  delete pwm;
  delete clientDialog;
  delete openDialog;
  delete fileOptionsDialog;
  delete optionsDialog;
  delete statusBarLabel;
#ifdef Q_OS_ANDROID
  delete androidJni;
#endif
  QSettings cfg ("qpwmc");

  if (state == Pwmd::Opened)
    cfg.setValue ("lastOpenedFile", filename ());

  cfg.setValue ("lastSocket", _socket);
  cfg.setValue ("geometry", saveGeometry ());
  cfg.setValue ("windowState", saveState ());
  cfg.setValue ("tabGeometry", ui.tabSplitter->saveState ());
  cfg.setValue ("attributeGeometry", ui.attributeSplitter->saveState ());
  cfg.setValue ("commandGeometry", ui.commandSplitter->saveState ());

  Qt::ToolButtonStyle s = fileToolBar->toolButtonStyle ();
  bool b = s != Qt::ToolButtonIconOnly;
  cfg.setValue ("buttonLabels", b);
  freeSearchResults ();
}

void
PwmdMainWindow::ignoreErrors (PwmdCommandQueueItem *cmd, bool dnd)
{
  if (!hasMultiSelection ())
    return;

  cmd->addError (536903681); // Permission denied
  cmd->addError (536871048); // Element not found

  if (dnd)
    {
      cmd->addError (536903743); // ELOOP
      return;
    }

  cmd->addError (536870939); // Not found
}

void
PwmdMainWindow::slotFindFinished ()
{
#ifdef Q_OS_ANDROID
  findingElement = false;
  QAction *a = findElementWidget->actions().at(0);
  a->disconnect();
  connect (a, SIGNAL(triggered(bool)),this, SLOT (slotFindElement ()));
#endif
  searchResultStatusBarLabel->hide ();

  fixupToolBar (elementToolBar, ElementToolBarFind, true);
  findElementAction->setVisible (false);
  ui.tabWidget->setEnabled (true);
  disconnect (ui.elementTree,
	   SIGNAL (itemClicked (QTreeWidgetItem *, int)),
	   this,
	   SLOT (slotElementClicked (QTreeWidgetItem *, int)));
  connect (ui.elementTree,
	   SIGNAL (itemSelectionChanged ()),
	   this,
	   SLOT (slotItemSelectionChanged ()));

  if (doSaveContent (true))
    {
      nextElement = ui.elementTree->currentItem ();
      pwm->runQueue ();
      return;
    }

  ui.elementTree->setFocus ();
  elementSelected (ui.elementTree->currentItem ());
}

static void
freeSearchResults ()
{
  while (!searchResults.isEmpty ())
    {
      SearchResult *s = searchResults.takeFirst ();
      delete s;
    }

  currentSearchResult = nullptr;
}

static int
searchResultTotal (bool index)
{
  int n = 0;

  foreach (SearchResult *r, searchResults)
    {
      if (r->resultType () & SearchResult::Element)
        n++;

      if (index && r == currentSearchResult)
        {
          if (currentSearchResult->curAttr () != -1)
            n += currentSearchResult->attrs ().indexOf (currentSearchResult->curAttr ())+1;
          break;
        }

      n += r->attrs ().count ();
    }

  return n;
}

QTreeWidgetItem *
PwmdMainWindow::findElementText (const QString &str, bool elements,
                                 bool attrs, bool attrValues)
{
  bool cs = false;
  QTreeWidgetItem *currentItem = firstSelectedItem ();
  SearchResult *result = nullptr;

  foreach (QChar c, str)
    {
      if (c.isUpper ())
        {
          cs = true;
          break;
        }
    }

  QRegularExpression rx (str, cs ? QRegularExpression::NoPatternOption :
                         QRegularExpression::CaseInsensitiveOption);
  QList < QTreeWidgetItem * >items = ui.elementTree->findItems ("*", Qt::MatchWildcard|Qt::MatchRecursive);
  if (items.isEmpty () || !rx.isValid ())
    return nullptr;

  foreach (QTreeWidgetItem *item, items)
    {
      bool match = false;
      QList <int>attrItems;
      int type = 0;

      if (elements)
        {
          if (rx.match (item->text (0)).hasMatch ())
            {
              match = true;
              type |= SearchResult::Element;
            }
        }

      if (attrs || attrValues)
        {
          PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
          PwmdAttributeList *alist = data->attributes ();

          for (int i = 0, t = alist->count (); i < t; i++)
            {
              PwmdElementAttr *attr = alist->at (i);
              if (attrs && rx.match (attr->name ()).hasMatch ())
                {
                  match = true;
                  type |= SearchResult::Attr;
                  if (!attrItems.contains (i))
                    attrItems.append (i);
                }

              if (attrValues && attr->name() != "_name"
                  && rx.match (attr->value ()).hasMatch ())
                {
                  type |= SearchResult::AttrValue;
                  match = true;
                  if (!attrItems.contains (i))
                    attrItems.append (i);
                }
            }
        }

      if (match)
        {
          SearchResult *r = new SearchResult (item, type, attrItems);

          searchResults.append (r);
          if (item == currentItem)
            currentSearchResult = result = r;
        }

      item->setSelected (false);
    }

  searchResultStatusBarLabel->setText (QString (tr ("Result %1 of %2")).arg (searchResultTotal (true)).arg (searchResultTotal (false)));
  int n = searchResults.indexOf (result);
  n = n == -1 ? 0 : n;
  return !searchResults.isEmpty () ? searchResults.at (n)->element () : nullptr;
}

void
PwmdMainWindow::slotFindTextChanged (const QString &str)
{
  if (str.isEmpty())
    return;

  freeSearchResults ();

  QTreeWidgetItem *item = findElementText (str);
  if (!item)
    {
      QEvent *ev = new QStatusTipEvent (tr ("Element text not found."));
      QCoreApplication::postEvent (statusBar (), ev);
      searchResultStatusBarLabel->setText (QString (tr ("Result %1 of %2")).arg ("0", "0"));
      return;
    }

  if (searchResults.contains (currentSearchResult))
    setCurrentElement (item);
  else
    findElementCommon (false);
}

void
PwmdMainWindow::findElementCommon (bool previous)
{
  searchResultStatusBarLabel->show ();

  if (searchResults.isEmpty ())
    {
      searchResultStatusBarLabel->setText (QString (tr ("Result %1 of %2")).arg ("0").arg (0));
      QEvent *ev = new QStatusTipEvent (tr ("Element text not found."));
      QCoreApplication::postEvent (statusBar (), ev);
      return;
    }

  int n = searchResults.indexOf (currentSearchResult);
  if (n == -1)
    currentSearchResult = nullptr;

  if (currentSearchResult && !currentSearchResult->attrs ().isEmpty ())
    {
      int a = currentSearchResult->curAttr ();

      if (a != -1)
        {
          if (!previous)
            currentSearchResult->nextAttr ();
          else
            currentSearchResult->prevAttr ();

          a = currentSearchResult->curAttr ();
          if (a == -1)
            currentSearchResult->reset ();
          else
            goto done;
        }
    }

  if (!previous && n+1 >= searchResults.count ())
    currentSearchResult = searchResults.first ();
  else if (previous && n-1 < 0)
    currentSearchResult = searchResults.last ();
  else if (previous)
    currentSearchResult = searchResults.at (n-1);
  else
    currentSearchResult = searchResults.at (n+1);

  if (!currentSearchResult)
    currentSearchResult = !searchResults.isEmpty () ? searchResults.at (0) : nullptr;

  if (!currentSearchResult)
    {
      setCurrentElement (nullptr);
      return;
    }

  if (previous && !currentSearchResult->attrs ().isEmpty ())
    currentSearchResult->lastAttr ();

done:
  searchResultStatusBarLabel->setText (QString (tr ("Result %1 of %2")).arg (searchResultTotal (true)).arg (searchResultTotal (false)));
  setCurrentElement (currentSearchResult->element ());
  QEvent *ev = new QStatusTipEvent (tr (""));
  QCoreApplication::postEvent (statusBar (), ev);
}

void
PwmdMainWindow::slotAdvancedSearchNext (const QString &text, bool elements,
                                        bool attrs, bool attrValues,
                                        bool select, bool previousResult)
{
  static QString previous;
  // Toggle of search types
  static bool prevElements, prevAttrs, prevAttrValues, prevSelect;
  bool b = false;

  b |= prevElements != elements, prevElements = elements;
  b |= prevAttrs != attrs, prevAttrs = attrs;
  b |= prevAttrValues != attrValues, prevAttrValues = attrValues;
  b |= prevSelect != select, prevSelect = select;
  b |= previous != text, previous = text;

  if (b)
    freeSearchResults ();

  if (text.isEmpty ())
    return;

  if (searchResults.isEmpty () || b)
    {
      findElementText (text, elements, attrs, attrValues);
      foreach (SearchResult *s, searchResults)
        s->element ()->setSelected (select);
    }

  if (select)
    return;

  findElementCommon (previousResult);

  if ((attrs || attrValues) && currentSearchResult &&
      ((currentSearchResult->resultType () & SearchResult::Attr)
       || currentSearchResult->resultType () & SearchResult::AttrValue))
    editAttributes (false, currentSearchResult->element ());
  else
    slotEditContent ();
}

void
PwmdMainWindow::slotAdvancedSearchFinished (int)
{
  if (searchResultStatusBarLabel)
    searchResultStatusBarLabel->hide ();

  QComboBox *w = qobject_cast<QComboBox*> (elementToolBar->widgetForAction (findElementAction));
  findElementWidget->setEnabled (true);

  if (incrementalSearch)
    disconnect (elementToolBar->widgetForAction (findElementAction),
             SIGNAL (editTextChanged (const QString &)), this,
             SLOT (slotFindTextChanged (const QString &)));

  advancedSearchDialog->setSearchText (w, true);

  if (incrementalSearch)
    connect (elementToolBar->widgetForAction (findElementAction),
             SIGNAL (editTextChanged (const QString &)), this,
             SLOT (slotFindTextChanged (const QString &)));

  advancedSearchDialog->hide ();
  ui.tabWidget->setEnabled (true);
  ui.elementTree->setFocus ();
  QTreeWidgetItem *item = firstSelectedItem ();
  elementSelected (item);

  if (ui.elementTree->selectedItems ().count () > 1)
    slotItemSelectionChanged ();

  connect (ui.elementTree,
	   SIGNAL (itemSelectionChanged ()),
	   this,
	   SLOT (slotItemSelectionChanged ()));
  fixupMultiToolbar ();
#ifdef Q_OS_ANDROID
  ui.actionToggleMulti->setChecked (hasMultiSelection ());
#endif
}

void
PwmdMainWindow::slotAdvancedSearchAction ()
{
  slotAdvancedSearch (QString ());
}

void
PwmdMainWindow::slotSearchShiftEnterPressed ()
{
  prevSearchResult = true;
  slotFindElement ();
}

void
PwmdMainWindow::slotAdvancedSearch (const QString &)
{
  ui.tabWidget->setEnabled (false);
  if (!advancedSearchDialog)
    {
      advancedSearchDialog = new PwmdSearchDialog (this);
      connect (advancedSearchDialog,
               SIGNAL (findNext (const QString &, bool, bool, bool, bool, bool)),
               this,
               SLOT (slotAdvancedSearchNext (const QString &, bool, bool, bool, bool, bool)));
      connect (advancedSearchDialog, SIGNAL (finished (int)), this,
               SLOT (slotAdvancedSearchFinished (int)));
    }

  QComboBox *w = qobject_cast<QComboBox*> (elementToolBar->widgetForAction (findElementAction));
  advancedSearchDialog->setSearchText (w);
  advancedSearchDialog->setIncremental (incrementalSearch);

  disconnect (ui.elementTree,
	   SIGNAL (itemClicked (QTreeWidgetItem *, int)),
	   this,
	   SLOT (slotElementClicked (QTreeWidgetItem *, int)));
  connect (ui.elementTree,
	   SIGNAL (itemClicked (QTreeWidgetItem *, int)),
	   this,
	   SLOT (slotElementClicked (QTreeWidgetItem *, int)));
  disconnect (ui.elementTree,
	   SIGNAL (itemSelectionChanged ()),
	   this,
	   SLOT (slotItemSelectionChanged ()));
  resetSelectedItems ();
  findElementAction->setVisible (false);
  findElementWidget->setEnabled (false);
  advancedSearchDialog->reset ();
  advancedSearchDialog->show ();
}

void
PwmdMainWindow::slotFindElement ()
{
  disconnect (ui.elementTree,
	   SIGNAL (itemClicked (QTreeWidgetItem *, int)),
	   this,
	   SLOT (slotElementClicked (QTreeWidgetItem *, int)));
  connect (ui.elementTree,
	   SIGNAL (itemClicked (QTreeWidgetItem *, int)),
	   this,
	   SLOT (slotElementClicked (QTreeWidgetItem *, int)));
  disconnect (ui.elementTree,
	   SIGNAL (itemSelectionChanged ()),
	   this,
	   SLOT (slotItemSelectionChanged ()));
  ui.tabWidget->setEnabled (false);
  QComboBox *w = qobject_cast<QComboBox*> (elementToolBar->widgetForAction (findElementAction));

#ifdef Q_OS_ANDROID
  if (!findingElement)
    {
      w->setFocus ();
      qApp->inputMethod ()->show ();
      findingElement = true;

      QAction *a = findElementWidget->actions().at(0);
      a->disconnect();
      connect (a, SIGNAL (triggered (bool)), this, SLOT (slotFindFinished ()));
    }
  else
    qApp->inputMethod ()->hide ();
#else
  w->setFocus ();
#endif

  if (!findElementAction->isVisible ())
    {
      doSaveContent ();
      w->setFocus ();
      w->lineEdit()->selectAll ();
      fixupToolBar (elementToolBar, ElementToolBarFind, true);
      findElementAction->setVisible (true);
      prevSearchResult = false;
      return;
    }

  if (w->lineEdit()->text ().isEmpty ())
    {
      prevSearchResult = false;
      return;
    }

  findElementCommon (prevSearchResult);
  prevSearchResult = false;
}

void
PwmdMainWindow::updateElementExpiry (long t)
{
  QTreeWidgetItem *item = previousElement ? previousElement : selectedElement;

  if (!item || !t)
    {
      if (ui.tabWidget->currentWidget() == ui.contentTab)
        setContentItemsEnabled ();

      return;
    }

  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  QDateTime date;
  date.setSecsSinceEpoch (t);
  data->addAttribute ("_expire", QString ("%1").arg (t));
  updateTargetAttributes (item, "_expire", QString ("%1").arg (t));

  if (ui.tabWidget->currentWidget() == ui.contentTab)
    setContentItemsEnabled ();
}

void
PwmdMainWindow::slotChangeExpire ()
{
  doSaveContent (true);
  if (!selectedElement)
    return;

  PwmdTreeWidgetItemData *data = qvariant_cast<PwmdTreeWidgetItemData * >(selectedElement->data (0, Qt::UserRole));
  QString expire, incr;
  long e, i;

  expire = data->attribute ("_expire");
  incr = data->attribute ("_age");
  e = expire.toLong ();
  i = incr.toLong ();

  PwmdExpireDialog d (e >= time (nullptr) ? e : time (nullptr), i, this);
  if (d.exec () == QDialog::Rejected)
    return;

  newAttribute = nullptr;
  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      if (!isElementOwner (resolveTargets (item)))
        continue;

      data = qvariant_cast<PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
      if (!d.expire ())
        {
          if (data->hasAttribute ("_expire"))
            deleteAttribute (item, "_expire", true);

          if (data->hasAttribute ("_age"))
            deleteAttribute (item, "_age", true);

          continue;
        }

      setNewAttribute (item, "_expire", QString ("%1").arg (d.expire ()),
                       false, true);
      if (d.increment ())
        setNewAttribute (item, "_age", QString ("%1").arg (d.increment ()),
                         false, true);
      else if (data->hasAttribute ("_age"))
        deleteAttribute (item, "_age", true);
    }

  pwm->runQueue ();
}

static void
showErrorMsg (gpg_error_t rc, const QString &str)
{
  QMessageBox m;

  m.setText (QApplication::
	     tr ("There was an error while communicating with pwmd."));
  m.setInformativeText (QString ("ERR %1: %2: %3").arg (rc).arg (gpg_strsource(rc), str));
  m.setIcon (QMessageBox::Critical);
  m.exec ();
}

void
PwmdMainWindow::slotCommandResult (PwmdCommandQueueItem *item,
                                   QString result, gpg_error_t rc, bool queued)
{
  PwmdInquireData *inq = item ? item->data () : nullptr;
  QEvent *ev = nullptr;

  releaseTimer->stop ();
  if (releaseTimeout > 0)
    releaseTimer->start (releaseTimeout*1000);

  // BULK command helper result built from existing queued commands.
  if (!item)
    {
      showError (rc);
      return;
    }

  if (item->id () >= PwmdCmdIdMainMax)
    {
      item->setSeen ();
      return;
    }

  switch (item->id ())
    {
    case PwmdCmdIdLock:
    case PwmdCmdIdUnLock:
      releaseTimer->stop();
      releaseTimeout = 0;
      emit mainClientLockStateChanged (item->id () == PwmdCmdIdLock ? true : false);
      break;
    case PwmdCmdIdInternalStatusError:
      /* Don't do anything here. Pretend the connection is still valid so the
       * user has a chance to recover any cached elements. */
      if (state != Pwmd::Opened)
        pwm->reset ();
      break;
    case PwmdCmdIdInternalConnect:
    case PwmdCmdIdInternalConnectHost:
      if (rc)
        pwm->reset ();
      break;
    case PwmdCmdIdInternalOpen:
      openFileFinalize (rc);
      break;
    case PwmdCmdIdInternalGenKey:
    case PwmdCmdIdInternalPassword:
      item->setSeen ();
      return;
      break;
    case PwmdCmdIdInternalSave:
      pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 0);
      pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 0);

      if (!rc)
        {
          setModified (false);
          setWindowModified (false);
          newFile = false;
          if (reconnectSocket)
            {
              pwm->reset ();
              QTimer::singleShot (1, this, SLOT (slotConnectSocket()));
            }
          else if (disconnectSocket)
            pwm->reset ();
        }
      break;
    case PwmdCmdIdInternalCloseFile:
      break;
    case PwmdCmdIdDndMoveElement:
      dndOperation = DndNone;
      if (!rc && item->isFinalItem ())
        dndCopyMoveElementFinalize (inq->user ());
      else if (item->isFinalItem ())
        {
          QEvent *ev = new QEvent (QEvent::Type (PwmdEventRefreshElementTree));
          QCoreApplication::postEvent (this, ev);
        }
      break;
    case PwmdCmdIdDndCopyElement:
      if (rc && item->isFinalItem ())
        {
          if (copyElement)
            selectedElement = attributeElement = copyElement;

          ev = new QEvent (QEvent::Type (PwmdEventNewElementCancel));
          QCoreApplication::postEvent (this, ev);

          if (hasMultiSelection ())
            {
              QEvent *ev = new QEvent (QEvent::Type (PwmdEventRefreshElementTree));
              QCoreApplication::postEvent (this, ev);
            }
          break;
        }
      else if (!rc)
        {
          newElement = nullptr;
          if (item->isFinalItem ())
            dndCopyMoveElementFinalize (inq->user ());
        }
      break;
    case PwmdCmdIdDndCreateTarget:
      if (rc)
        {
          if (item->isFinalItem ())
            {
              dndOperation = DndNone;
              QEvent *ev = new QEvent (QEvent::Type (PwmdEventDnDTargetAttr));
              QCoreApplication::postEvent (this, ev);
            }
          break;
        }

      setAttribute (static_cast <QTreeWidgetItem *>(inq->userPtr ()),
                    "_target", inq->user ());
      if (item->isFinalItem ())
        dndCreateTargetFinalize ();
      break;
    case PwmdCmdIdReload:
      if (!rc)
        {
          reloadFinalize ();
        }
      break;
    case PwmdCmdIdInvokingUser:
      if (!rc)
        {
          invokingUserFinalize (result);
        }
      break;
    case PwmdCmdIdCurrentUser:
        {
          if (!rc)
            currentUserFinalize (result);
        }
      break;
    case PwmdCmdIdLockTimeout:
      break;
    case PwmdCmdIdCommand:
      ui.commandResultTextEdit->clear ();

      if (rc)
        {
          ui.commandResultTextEdit->setPlainText (QString ("ERR %1: %2").arg(rc).arg(gpg_strerror(rc)));
          break;
        }

      if (inq && inq->userBool ())
        {
          ui.commandResultTextEdit->setOpenLinks (true);
          ui.commandResultTextEdit->setHtml (result);
        }
      else
        ui.commandResultTextEdit->setPlainText (result);

      setModified ();
      if (pwm->state () == Pwmd::Opened && (!inq || !inq->userBool ()))
        refreshElementTree ();
      break;
    case PwmdCmdIdGetContent:
      if (rc && item->checkError (rc))
        rc = 0;

      if (rc && previousElement && previousElement != selectedElement)
        {
          if (ui.tabWidget->currentWidget () != ui.commandTab)
            {
              setContentItemsEnabled (false);
              setContent ("");
            }
        }
      else if (!rc)
        elementSelectedFinalize (static_cast <QTreeWidgetItem *> (inq->userPtr ()), result);
      else
        setCurrentElement (selectedElement);
      break;
    case PwmdCmdIdNewElement:
      if (!rc)
        {
          newElementFinalize (inq->user());
          break;
        }
      selectedElement = newElement->parent ();
      ev = new QEvent (QEvent::Type (PwmdEventNewElementCancel));
      QCoreApplication::postEvent (this, ev);
      break;
    case PwmdCmdIdDeleteElement:
      deleteElementFinalize (item);
      if (!rc && item->isFinalItem ())
        setCurrentElement (selectedElement);
      break;
    case PwmdCmdIdRenameElement:
      if (!rc)
        {
          renameElementFinalize ();
          break;
        }
      selectedElement->setText (0, inq->user());
      ev = new QEvent (QEvent::Type (PwmdEventNewElementCancel));
      QCoreApplication::postEvent (this, ev);
      break;
    case PwmdCmdIdElementTreeForm:
      if (rc && item->checkError (rc))
        rc = 0;

      if (!rc)
        {
          QByteArray b = result.toUtf8 ();
          openFormFinalize (b, inq->user (), inq->user2 ());
        }
      break;
    case PwmdCmdIdElementTree:
      if (rc && item->checkError (rc))
        rc = 0;

      if (rc)
        {
          if (gpg_err_code (rc) == GPG_ERR_ELOOP && inq->userBool ()) // target
            {
              QTreeWidgetItem *user = static_cast<QTreeWidgetItem *> (inq->userPtr ());
              rc = deleteAttribute (user ? user : selectedElement, "_target");
            }

          break;
        }

        {
          QByteArray b = result.toUtf8 ();
          refreshElementTreeFinalize (b);
        }
      break;
    case PwmdCmdIdStoreContent:
      if (!rc)
        {
          bool e;

          saveContentFinalize (static_cast <QTreeWidgetItem *> (inq->userPtr()), inq->user(), inq->userBool());
          if (inq->isSaving (e))
            saveDocument (e);
        }
      else
        setCurrentElement (selectedElement);
      break;
    case PwmdCmdIdPasswordContent:
      if (!rc)
        {
          savePasswordContentFinalize (static_cast <QTreeWidgetItem *> (inq->userPtr()), inq->user());
        }
      else
        setCurrentElement (selectedElement);
      break;
    case PwmdCmdIdAttrContent:
      if (!rc)
        {
          AttributeFinalizeT *attr = static_cast <AttributeFinalizeT *> (inq->userPtr ());
          saveAttrContentFinalize (inq->user(), attr);
          fixupToolBar (attributeToolBar, AttributeToolBarOk, false);
        }

        {
          AttributeFinalizeT *attr = static_cast <AttributeFinalizeT *> (inq->userPtr ());
          delete attr;
          if (item->isFinalItem ())
            {
              bool e;
              if (inq->isSaving (e))
                saveDocument (e);
            }
        }
      break;
    case PwmdCmdIdAttrNew:
        {
          AttributeFinalizeT *attr;
          if (!rc)
            {
              attr = static_cast <AttributeFinalizeT *> (inq->userPtr ());
              setNewAttributeFinalize (attr, inq->userBool());
            }
          else
            {
              attr = static_cast <AttributeFinalizeT *> (inq->userPtr ());
              if (attr && item->isFinalItem ())
                ui.attributeList->takeItem (ui.attributeList->row (attr->listItem));
            }
          delete attr->listItem;
          delete attr;
        }
        break;
    case PwmdCmdIdAttrDelete:
        if (!rc)
          {
            deleteAttributeFinalize (static_cast <QTreeWidgetItem *>(inq->userPtr ()), inq->user());
          }
      break;
    case PwmdCmdIdAttrList:
      if (!rc)
        {
          refreshAttributeListFinalize (static_cast <QTreeWidgetItem *>(inq->userPtr ()), result, true, inq->userBool ());
        }
      else
        setCurrentElement (selectedElement);
      break;
    default:
      break;
    }

  if (rc && item->id () != PwmdCmdIdCommand)
    {
      if (!queued)
        {
          if ((item->checkError (rc)
              && hasMultiSelection ()
              && item->isFinalItem ())
              || !item->checkError (rc))
            {
              if (item->error () && !item->errorString().isEmpty ())
                showErrorMsg (item->error (), item->errorString ());
              else
                showError (rc);
            }
        }

      /* The error may have been related to the previously selected element in
       * a bulk command. Restore the view to the previous element. */
      if (previousElement && previousElement != selectedElement)
        {
          ui.elementTree->setCurrentItem (previousElement);
          selectedElement = previousElement;
        }
    }

  item->setSeen ();

  if (hasQueuedRefreshTree ())
    nextElement = nullptr;

  if (!pwm->queued () && nextElement)
    elementSelected (nextElement);
}

void
PwmdMainWindow::slotVersion ()
{
  QMessageBox::about (this, tr ("About QPwmc"),
                      tr ("QPwmc version %1\nlibpwmd %2\n\n"
                          "Copyright 2015-2025 Ben Kibbey <bjk@luxsci.net>\n"
                          "https://gitlab.com/bjk/qpwmc").arg (QPWMC_VERSION,
                                                               pwmd_version ()));
}

void
PwmdMainWindow::openFormFinalize (QByteArray &result, const QString &file,
                                  const QString &dir)
{
  QSettings cfg ("qpwmc");
  cfg.setValue ("lastFormLocation", dir);
  cfg.sync ();

  QList < QTreeWidgetItem * >items = buildElementTree (result, false);
  PwmdTreeWidget *tree = new PwmdTreeWidget ();

  tree->header ()->setSectionResizeMode (0, QHeaderView::ResizeToContents);
  tree->header ()->setStretchLastSection (false);
  tree->insertTopLevelItems (0, items);
  tree->sortByColumn (0, Qt::AscendingOrder);
  tree->setHeaderHidden (true);
  tree->setEditTriggers (QAbstractItemView::NoEditTriggers);
  tree->ungrabGesture(Qt::TapAndHoldGesture);
  tree->ungrabGesture(Qt::TapGesture);
  tree->setCurrentItem (findElement (elementPath (selectedElement), 0, true,
                                     tree));

  ApplicationForm *f = new ApplicationForm (pwm, file, tree, this);
  if (!f->hasError ())
    {
      f->setParent (this, Qt::Dialog);
      if (f->exec () || f->modified ())
        {
          setModified ();
          refreshElementTree ();
        }
    }

  clearElementTree (tree);
  delete tree;
  delete f;
}

void
PwmdMainWindow::slotOpenForm ()
{
  doSaveContent (true);
  PwmdFileDialog d (this, tr ("Open application form"), 0, "*.xml");
  QSettings cfg ("qpwmc");
#ifndef Q_OS_ANDROID
  d.setDirectory (cfg.value ("lastFormLocation", "/usr/local/share/qpwmc").toString ());
#else
  d.setMimeTypeFilters (QStringList ({"text/xml"}));
  d.setDirectory (cfg.value ("lastFormLocation", "/sdcard").toString ());
#endif

  d.setUpdateLastLocation (false);
  d.setFileMode (QFileDialog::ExistingFile);
  d.setOption (QFileDialog::ReadOnly);

  if (!d.exec () || !d.selectedFiles ().count ())
    return;

  PwmdInquireData *inq = new PwmdInquireData ();
  inq->setUser (d.selectedFiles ().at (0));
  inq->setUser2 (d.directory ().absolutePath ());
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdElementTreeForm,
                                           "LIST --recurse --sexp",
                                           Pwmd::inquireCallback, inq));
}

void
PwmdMainWindow::slotFileOptionsDone (int)
{
  QEvent *ev = new QEvent (QEvent::Type (PwmdEventFileOptionsClose));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdMainWindow::slotClientLockStateChanged (bool b)
{
  if (!b)
    {
      releaseTimer->stop();
      releaseTimeout = 0;
    }

  if (clientDialog)
    emit mainClientLockStateChanged (b);
}

void
PwmdMainWindow::slotFileOptions ()
{
  fixupToolBar (fileToolBar, FileToolBarOpen, false);

  if (!fileOptionsDialog)
    {
      fileOptionsDialog = new PwmdFileOptionsDialog (this);
      connect (fileOptionsDialog, SIGNAL (finished (int)), this, SLOT (slotFileOptionsDone (int)));
      connect (fileOptionsDialog, SIGNAL (clientLockStateChanged (bool)), this, SLOT (slotClientLockStateChanged (bool)));
    }

  fileOptionsDialog->show ();
}

void
PwmdMainWindow::slotButtonLabels (bool b)
{
  setupToolBarLabels (b);
}

void
PwmdMainWindow::slotTabChanged (int n)
{
  static int last;
  int which = 0;
  bool update;

  previousElement = nextElement = nullptr;
  disconnect (ui.tabWidget, SIGNAL (currentChanged (int)), this, SLOT (slotTabChanged (int)));
  ui.tabWidget->setCurrentWidget (ui.tabWidget->widget (last));
  update = doSaveContent (true, false, false, &which);

  if (update)
    {
      nextTabWidget = ui.tabWidget->widget (n);
      switch (which)
        {
        case Attributes:
          ui.tabWidget->setCurrentWidget (ui.attributeTab);
          break;
        case Content:
        case Password:
          ui.tabWidget->setCurrentWidget (ui.contentTab);
          break;
        default:
          break;
        }

      connect (ui.tabWidget, SIGNAL (currentChanged (int)), this, SLOT (slotTabChanged (int)));
      pwm->runQueue ();
      return;
    }

  last = n;
  ui.tabWidget->setCurrentWidget (ui.tabWidget->widget (n));
  connect (ui.tabWidget, SIGNAL (currentChanged (int)), this, SLOT (slotTabChanged (int)));
  nextTabWidget = nullptr;
  fixupEditContentOrAttributes (ui.tabWidget->currentWidget () != ui.attributeTab);

  switch (n)
    {
    case TabContent:
      slotEditContent ();
      break;
    case TabAttributes:
      editTargetAttributes = false;
      ui.actionToggleTargetAttribute->setChecked (false);
      setAttributeItemsEnabled ();
      editAttributes (false, selectedElement);
      break;
    case TabCommand:
      doSaveContent (false);
      setCommandItemsEnabled ();
      break;
    default:
      break;
    }

  fixupMultiToolbar ();
}

void
PwmdMainWindow::slotEditContent ()
{
  fixupEditContentOrAttributes (true);
  doSaveContent (false);
  setContentItemsEnabled ();
}

void
PwmdMainWindow::setupToolBarLabels (bool b)
{
  fileToolBar->setToolButtonStyle (b ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly);
  elementToolBar->setToolButtonStyle (b ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly);
  contentToolBar->setToolButtonStyle (b ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly);
  attributeToolBar->setToolButtonStyle (b ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly);
  commandToolBar->setToolButtonStyle (b ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly);
  passwordToolBar->setToolButtonStyle (b ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly);
}

/* En/Dis/ables menu items. Returns whether the button is enabled when 'test'
 * is true or not. */
bool
PwmdMainWindow::fixupMenu (QMenu *m, int which, bool enable, int checked,
                           bool test)
{
  QList <QAction *>actions = m->actions();

  if (test)
    return actions.at (which)->isEnabled ();

  if (m == ui.menuFile)
    {
      switch (which)
        {
        case FileMenuOpen:
        case FileMenuOpenForm:
        case FileMenuClose:
        case FileMenuSave:
        case FileMenuSaveExt:
          actions.at (which)->setEnabled (enable);
          break;
        default:
          break;
        }
    }
  else if (m == ui.menuElement)
    {
      switch (which)
        {
        case ElementMenuNew:
        case ElementMenuDelete:
        case ElementMenuRename:
        case ElementMenuFind:
        case ElementMenuAdvancedFind:
          actions.at (which)->setEnabled (enable);
          break;
        default:
          break;
        }
    }
  else if (m == ui.menuContent)
    {
      switch (which)
        {
        case ContentMenuInsert:
        case ContentMenuSave:
        case ContentMenuClipboard:
        case ContentMenuHidden:
        case ContentMenuBase64:
        case ContentMenuUpdate:
        case ContentMenuPassword:
        case ContentMenuRefresh:
        case ContentMenuExpiry:
          actions.at (which)->setEnabled (enable);
          break;
        default:
          break;
        }
    }
  else if (m == ui.menuAttributes)
    {
      switch (which)
        {
        case AttributeMenuTarget:
          actions.at (which)->setEnabled (enable);

          if (checked != -1)
            actions.at (which)->setChecked (checked);
          break;
        case AttributeMenuNew:
        case AttributeMenuDelete:
        case AttributeMenuUpdate:
        case AttributeMenuRefresh:
          actions.at (which)->setEnabled (enable);
          break;
        default:
          break;
        }
    }
  else if (m == ui.menuView)
    {
      switch (which)
        {
        case ViewMenuButtonLabels:
        case ViewMenuExpandAll:
        case ViewMenuClientList:
          actions.at (which)->setEnabled (enable);
          break;
        default:
          break;
        }
    }

  return true;
}

/* En/Dis/ables toolbar buttons. Returns whether the button is enabled when
 * 'test' is true or not. */
bool
PwmdMainWindow::fixupToolBar (QToolBar *t, int which, bool enable, bool test,
                              QWidget **rWidget)
{
  QList <QAction *> list = t->actions ();
  QWidget *w = nullptr;

  for (int i = 0; i < list.size(); i++)
    {
      if (t == fileToolBar && which == i)
        {
          w = fileToolBar->widgetForAction (list.at (i));
          if (rWidget)
            *rWidget = w;

          if (test)
            return w->isEnabled ();

          switch (which)
            {
            case FileToolBarOpen:
              fixupMenu (ui.menuFile, FileMenuOpen, enable);
              w->setEnabled (enable);
              break;
            case FileToolBarSave:
              fixupMenu (ui.menuFile, FileMenuSave, enable);
              w->setEnabled (enable);
              break;
            default:
              break;
            }
        }
      else if (t == elementToolBar && which == i)
        {
          w = elementToolBar->widgetForAction (list.at (i));
          if (rWidget)
            *rWidget = w;

          if (test)
            return w->isEnabled ();

          switch (which)
            {
            case ElementToolBarCreate:
              fixupMenu (ui.menuElement, ElementMenuNew, enable);
              w->setEnabled (enable);
              break;
            case ElementToolBarDelete:
              fixupMenu (ui.menuElement, ElementMenuDelete, enable);
              w->setEnabled (enable);
              break;
            case ElementToolBarRename:
              fixupMenu (ui.menuElement, ElementMenuRename, enable);
              w->setEnabled (enable);
              break;
            case ElementToolBarFind:
              fixupMenu (ui.menuElement, ElementMenuFind, enable);
              fixupMenu (ui.menuElement, ElementMenuAdvancedFind, enable);
              w->setEnabled (enable);
              findElementWidget->setEnabled (enable);
            default:
              break;
            }
        }
      else if (t == contentToolBar && which == i)
        {
          w = contentToolBar->widgetForAction (list.at (i));
          if (rWidget)
            *rWidget = w;

          if (test)
            return w->isEnabled ();

          switch (which)
            {
            case ContentToolBarOk:
              fixupMenu (ui.menuContent, ContentMenuUpdate, enable);
              w->setEnabled (enable);
              break;
            case ContentToolBarPassword:
              fixupMenu (ui.menuContent, ContentMenuPassword, enable);
              w->setEnabled (enable);
              break;
            case ContentToolBarExpiry:
              fixupMenu (ui.menuContent, ContentMenuExpiry, enable);
              w->setEnabled (enable);
              break;
            case ContentToolBarSave:
              fixupMenu (ui.menuContent, ContentMenuSave, enable);
              w->setEnabled (enable);
              break;
            case ContentToolBarInsert:
              fixupMenu (ui.menuContent, ContentMenuInsert, enable);
              w->setEnabled (enable);
              break;
            case ContentToolBarRefresh:
              fixupMenu (ui.menuContent, ContentMenuRefresh, enable);
              w->setEnabled (enable);
              break;
            default:
              break;
            }
        }
      else if (t == passwordToolBar && which == i)
        {
          w = passwordToolBar->widgetForAction (list.at (i));
          if (rWidget)
            *rWidget = w;

          if (test)
            return w->isEnabled ();

          switch (which)
            {
            case PasswordToolBarOk:
              if (!ui.passwordWidget->isHidden ())
                {
                  fixupMenu (ui.menuContent, ContentMenuUpdate, enable);
                  w->setEnabled (enable);
                }
              break;
            case PasswordToolBarInsert:
            case PasswordToolBarSave:
            case PasswordToolBarExpiry:
            case PasswordToolBarRefresh:
              w->setEnabled (enable);
              break;
            default:
              break;
            }
        }
      else if (t == attributeToolBar && which == i)
        {
          w = attributeToolBar->widgetForAction (list.at (i));
          if (rWidget)
            *rWidget = w;

          if (test)
            return w->isEnabled ();

          switch (which)
            {
            case AttributeToolBarNew:
              fixupMenu (ui.menuAttributes, AttributeMenuNew, enable);
              w->setEnabled (enable);
              break;
            case AttributeToolBarDelete:
              fixupMenu (ui.menuAttributes, AttributeMenuDelete, enable);
              w->setEnabled (enable);
              break;
            case AttributeToolBarOk:
              fixupMenu (ui.menuAttributes, AttributeMenuUpdate, enable);
              w->setEnabled (enable);
              break;
            case AttributeToolBarRefresh:
              fixupMenu (ui.menuAttributes, AttributeMenuRefresh, enable);
              w->setEnabled (enable);
              break;
            default:
              break;
            }
        }
      else if (t == commandToolBar && which == i)
        {
          w = commandToolBar->widgetForAction (list.at (i));
          if (rWidget)
            *rWidget = w;

          if (test)
            return w->isEnabled ();

          switch (which)
            {
            case CommandToolBarSave:
            case CommandToolBarClear:
            case CommandToolBarOk:
            case CommandToolBarRefresh:
            case CommandToolBarHelp:
              w->setEnabled (enable);
              break;
            default:
              break;
            }
        }
    }

  return true;
}
 
void PwmdMainWindow::slotCommandInquireFilename ()
{
  PwmdFileDialog d (this, tr ("Inquire data from file"));

  d.setFileMode (QFileDialog::ExistingFile);
  d.setOption (QFileDialog::ReadOnly);

  if (!d.exec () || !d.selectedFiles ().count ())
    return;

  setCommandInquireFilename (d.selectedFiles ().at (0));
}

void PwmdMainWindow::slotCommandInquireStateChanged (int s)
{
  bool b = s == Qt::Unchecked ? false : true;

  ui.le_commandInquire->setEnabled (b);
  ui.tb_commandInquireFilename->setEnabled (b);
  setCommandInquireFilename (QString ());
}

void
PwmdMainWindow::slotSearchCommandResults ()
{
  if (ui.le_commandSearch->text ().isEmpty ())
    return;

  ui.commandResultTextEdit->find (ui.le_commandSearch->text ());
}

void
PwmdMainWindow::slotClearCommand ()
{
  ui.commandTextEdit->clear ();
  setCommandInquireFilename (QString ());
  ui.commandResultTextEdit->clear ();
  ui.ck_commandInquire->setChecked (false);
  ui.le_commandInquire->setText ("");
  ui.le_commandInquire->setEnabled (false);
  ui.le_commandSearch->setText ("");
  ui.le_commandSearch->setEnabled (false);
  ui.pb_commandSearch->setEnabled (false);
}

void
PwmdMainWindow::slotSaveCommandResult ()
{
  PwmdFileDialog d (this, tr ("Save command result"));

  d.setAcceptMode (QFileDialog::AcceptSave);
  d.setFileMode (QFileDialog::AnyFile);

  if (!d.exec () || d.selectedFiles ().isEmpty ())
    return;

  QStringList f = d.selectedFiles ();
  QFile file (f.at (0));

  if (!file.open (QFile::WriteOnly))
    {
      QMessageBox m;

      m.setText (file.fileName ());
      m.setInformativeText (file.errorString ());
      m.setIcon (QMessageBox::Warning);
      m.exec ();
      return;
    }

  if (file.write (ui.commandResultTextEdit->document ()->toPlainText ().toUtf8 ()) ==
      -1)
    {
      QMessageBox m;

      m.setText (file.fileName ());
      m.setInformativeText (file.errorString ());
      m.setIcon (QMessageBox::Warning);
      m.exec ();
    }
}

void
PwmdMainWindow::slotCommandHelp ()
{
  PwmdInquireData *inq = new PwmdInquireData ();
  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdCommand,
                                                         "HELP --html", 0, inq);

  inq->setUserBool (true);
  pwm->command (item);
}

void
PwmdMainWindow::slotCommandAnchorClicked (const QUrl &url)
{
  QString cmd = QString ("HELP --html %1").arg (url.path ());
  PwmdInquireData *inq = new PwmdInquireData ();
  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdCommand, cmd,
                                                         0, inq);

  inq->setUserBool (true);
  pwm->command (item);
}

void
PwmdMainWindow::slotCommandResultContentChanged ()
{
  bool b = ui.commandResultTextEdit->document ()->characterCount () > 1;

  fixupToolBar (commandToolBar, CommandToolBarSave, b);
  ui.commandResultTextEdit->setEnabled (b);
  ui.le_commandSearch->setEnabled (b);
  ui.pb_commandSearch->setEnabled (b);
}

void
PwmdMainWindow::slotCommandContentChanged ()
{
  tabify (ui.commandTextEdit);
  bool b = ui.commandTextEdit->toPlainText ().length();
  fixupToolBar (commandToolBar, CommandToolBarOk, b); 
  fixupToolBar (commandToolBar, CommandToolBarClear, b);
}

void
PwmdMainWindow::setCommandInquireFilename (const QString &s)
{
  if (s.isEmpty ())
    ui.commandTextEdit->setPlaceholderText ("");
  else
    ui.commandTextEdit->setPlaceholderText(QString (tr ("Inquire data obtained from file %1")).arg (s));

  ui.commandTextEdit->setEnabled (s.isEmpty ());
  commandInquireFilename = s;
  if (!s.isEmpty ())
    ui.commandTextEdit->document ()->setPlainText ("");

  ui.tb_commandInquireFilename->setEnabled (ui.ck_commandInquire->isChecked ());
  fixupToolBar (commandToolBar, CommandToolBarOk, !s.isEmpty ()
                || !ui.commandTextEdit->document ()->toPlainText ().isEmpty ());
}

void
PwmdMainWindow::slotCommandHistoryChanged (int idx)
{
  if (idx == -1)
    return;

  PwmdCommandHistoryData *data = qvariant_cast<PwmdCommandHistoryData *> (ui.cb_commandHistory->currentData ());
  if (!data)
    return;

  ui.commandTextEdit->setPlainText (data->args ());
  ui.ck_commandInquire->setChecked (data->isInquire ());
  ui.le_commandInquire->setText (data->inquireCmd ());
  setCommandInquireFilename (data->inquireFilename ());
}

void
PwmdMainWindow::slotExecuteCommand ()
{
  QString str = ui.commandTextEdit->toPlainText ().replace ("<TAB>", "\t", Qt::CaseInsensitive);

  ui.commandResultTextEdit->setPlainText ("");

  if (str.isEmpty () &&
      (!ui.ck_commandInquire->isChecked () ||
       (ui.ck_commandInquire->isChecked () && ui.le_commandInquire->text ().isEmpty ())))
    return;

  bool match = false;
  for (int i = 0; i < ui.cb_commandHistory->count (); i++)
    {
      PwmdCommandHistoryData *data = qvariant_cast<PwmdCommandHistoryData *> (ui.cb_commandHistory->itemData (i));
      if (data && ui.ck_commandInquire->isChecked () == data->isInquire ()
          && ui.le_commandInquire->text () == data->inquireCmd ()
          && ui.commandTextEdit->toPlainText () == data->args ())
        {
          match = true;
          break;
        }
      else if (data && !commandInquireFilename.isEmpty ()
               && ui.ck_commandInquire->isChecked () == data->isInquire ()
               && ui.le_commandInquire->text () == data->inquireCmd ()
               && commandInquireFilename == data->inquireFilename ())
        {
          match = true;
          break;
        }
    }

  if (!match)
    {
      PwmdCommandHistoryData *data = new PwmdCommandHistoryData (ui.commandTextEdit->toPlainText (), ui.ck_commandInquire->isChecked (), ui.le_commandInquire->text (), commandInquireFilename);
      if (!commandInquireFilename.isEmpty ())
        ui.cb_commandHistory->addItem (ui.le_commandInquire->text ()+": "+commandInquireFilename, QVariant::fromValue (data));
      else
        ui.cb_commandHistory->addItem (ui.commandTextEdit->toPlainText (), QVariant::fromValue (data));
    }

  ui.cb_commandHistory->setEnabled (true);
  saveExpandedItemList ();

  if (ui.ck_commandInquire->isChecked ())
    {
      PwmdInquireData *inq = new PwmdInquireData ();

      if (!commandInquireFilename.isEmpty ())
        {
          QFile file (commandInquireFilename);

          if (!file.open (QIODevice::ReadOnly))
            {
              gpg_err_code_t rc;

              switch (file.error ())
                {
                case QFile::OpenError:
                  rc = GPG_ERR_ENOENT;
                  break;
                case QFile::PermissionsError:
                  rc = GPG_ERR_EPERM;
                  break;
                case QFile::ResourceError:
                  rc = GPG_ERR_ENOMEM;
                  break;
                default:
                  rc = GPG_ERR_UNKNOWN_ERRNO;
                  break;
                }

              delete inq;
              showError (gpg_error (rc));
              return;
            }

          inq->setData (file.readAll ());
          file.close ();
        }
      else
        inq->setData (str);

      pwm->command (new PwmdCommandQueueItem (PwmdCmdIdCommand, ui.le_commandInquire->text ().replace ("<TAB>", "\t").toUtf8 (), Pwmd::inquireCallback, inq));
    }
  else
    {
      pwm->command (new PwmdCommandQueueItem (PwmdCmdIdCommand, str));
    }
}

void
PwmdMainWindow::setupToolbars ()
{
  QCommonStyle style;
  QWidget *w;
  QAction *a;

  fileToolBar = new QToolBar (tr ("File Toolbar"), this);
  fileToolBar->addAction (QIcon (":icons/rodentia-icons_folder-open-blue.svg"), tr ("Open"), this, SLOT (slotOpenFile()));
  fixupToolBar (fileToolBar, FileToolBarOpen, false, false, &w);
  w->setStatusTip (ui.actionOpen->statusTip ());
  fileToolBar->addAction (QIcon (":icons/rodentia-icons_media-floppy.svg"), tr ("Save"), this, SLOT (slotSaveDocument()));
  fixupToolBar (fileToolBar, FileToolBarSave, false, false, &w);
  w->setStatusTip (ui.actionSave->statusTip ());
  fileToolBar->setObjectName ("fileToolBar");
  addToolBar (fileToolBar);

  elementToolBar = new QToolBar (tr ("Element Toolbar"), this);
  elementToolBar->setEnabled (false);
  elementToolBar->addAction (QIcon (":icons/rodentia-icons_folder-add.svg"), tr ("Create Element"), this, SLOT (slotNewElement()));
  fixupToolBar (elementToolBar, ElementToolBarCreate, false, false, &w);
  w->setStatusTip (ui.actionNewElement->statusTip ());
  elementToolBar->addAction (QIcon (":icons/rodentia-icons_user-trash-full.svg"), tr ("Delete Element"), this, SLOT (slotDeleteElement()));
  fixupToolBar (elementToolBar, ElementToolBarDelete, false, false, &w);
  w->setStatusTip (ui.actionDeleteElement->statusTip ());
  elementToolBar->addAction (QIcon (":icons/matt-icons_font-x-generic.svg"), tr ("Rename Element"), this, SLOT (slotRenameElement()));
  fixupToolBar (elementToolBar, ElementToolBarRename, false, false, &w);
  w->setStatusTip (ui.actionRenameElement->statusTip ());

  findElementAction = elementToolBar->addWidget (new QComboBox);
  findElementAction->setVisible (false);
  QComboBox *cb = qobject_cast<QComboBox*> (elementToolBar->widgetForAction (findElementAction));
  cb->setInputMethodHints (Qt::ImhNoAutoUppercase|Qt::ImhNoPredictiveText);
  PwmdLineEdit *le = new PwmdLineEdit;
  cb->setLineEdit (le);
  cb->setInsertPolicy (QComboBox::InsertPolicy::InsertAtTop);
  cb->setSizePolicy (QSizePolicy::Expanding, QSizePolicy::Fixed);
  cb->lineEdit()->setPlaceholderText (tr ("Find element..."));
  cb->lineEdit()->setClearButtonEnabled (true);
  QIcon icon = QCommonStyle().standardIcon (QStyle::SP_ArrowUp);
  a = cb->lineEdit()->addAction (icon, QLineEdit::TrailingPosition);
  connect (a, SIGNAL (triggered ()), this, SLOT (slotSearchShiftEnterPressed ()));
  icon = QCommonStyle().standardIcon (QStyle::SP_ArrowDown);
  a = cb->lineEdit()->addAction (icon, QLineEdit::TrailingPosition);
  connect (a, SIGNAL (triggered ()), this, SLOT (slotFindElement ()));
  connect (cb->lineEdit(), SIGNAL (returnPressed ()), this, SLOT (slotFindElement ()));
  connect (cb->lineEdit(), SIGNAL (shiftEnterPressed ()), this, SLOT (slotSearchShiftEnterPressed ()));
  connect (cb->lineEdit(), SIGNAL (finished ()), this, SLOT (slotFindFinished ()));

  a = elementToolBar->addAction (QIcon (":icons/system-search.svg"), tr ("Find Element"), this, SLOT (slotFindElement ()));
  findElementWidget = elementToolBar->widgetForAction (a);
  fixupToolBar (elementToolBar, ElementToolBarFind, false, false, &w);
  w->setStatusTip (ui.actionFindElement->statusTip ());

  QLabel *l = new QLabel (tr ("<a href=\"advanced\">Advanced</a>"));
  connect (l, SIGNAL (linkActivated (const QString &)), this,
           SLOT (slotAdvancedSearch (const QString &)));
  l->setTextInteractionFlags (Qt::LinksAccessibleByMouse|Qt::LinksAccessibleByKeyboard);
  l->setStatusTip (tr ("Advanced element tree search..."));
  elementToolBar->addWidget (l);
  connect (ui.actionAdvancedFindElement, SIGNAL (triggered ()), this,
           SLOT (slotAdvancedSearchAction ()));
  elementToolBar->setObjectName ("elementToolBar");
  addToolBar (Qt::TopToolBarArea, elementToolBar);

  contentToolBar = new QToolBar (tr ("Content Toolbar"), this);
  contentToolBar->addAction (QIcon (":icons/matt-icons_document-add.svg"), tr ("Insert Content"), this, SLOT (slotInsertContent()));
  fixupToolBar (contentToolBar, ContentToolBarInsert, false, false, &w);
  w->setStatusTip (ui.actionInsertFile->statusTip ());
  contentToolBar->addAction (QIcon (":icons/rodentia-icons_media-floppy.svg"), tr ("Save Content"), this, SLOT (slotSaveToFile()));
  fixupToolBar (contentToolBar, ContentToolBarSave, false, false, &w);
  w->setStatusTip (ui.actionSaveToFile->statusTip ());
  contentToolBar->addAction (QIcon (":icons/trayicon.svg"), tr ("Set Password"), this, SLOT (slotCreatePasswordAttribute()));
  fixupToolBar (contentToolBar, ContentToolBarPassword, true, false, &w);
  w->setStatusTip (ui.actionCreatePasswordAttribute->statusTip ());
  contentToolBar->addAction (QIcon (":icons/expiry.svg"), tr ("Change expiry"), this, SLOT (slotChangeExpire()));
  fixupToolBar (contentToolBar, ContentToolBarExpiry, true, false, &w);
  w->setStatusTip (ui.actionChangeExpire->statusTip ());
  contentToolBar->addSeparator ();
  contentToolBar->addAction (QIcon (":icons/rodentia-icons_ok.svg"), tr ("Update"), this, SLOT (slotSaveContent()));
  fixupToolBar (contentToolBar, ContentToolBarOk, false, false, &w);
  w->setStatusTip (ui.actionUpdateContent->statusTip ());
  contentToolBar->addAction (QIcon (":icons/matt-icons_view-refresh.svg"), tr ("Refresh Content"), this, SLOT (slotRefreshContent()));
  fixupToolBar (contentToolBar, ContentToolBarRefresh, false, false, &w);
  w->setStatusTip (ui.actionRefreshContent->statusTip ());
  contentToolBar->setObjectName ("contentToolBar");
  addToolBar (Qt::RightToolBarArea, contentToolBar);

  passwordToolBar = new QToolBar (tr ("Password Toolbar"), this);
  passwordToolBar->addAction (QIcon (":icons/matt-icons_document-add.svg"), tr ("Insert Content"), this, SLOT (slotInsertContent()));
  fixupToolBar (passwordToolBar, PasswordToolBarInsert, false, false, &w);
  w->setStatusTip (ui.actionInsertFile->statusTip ());
  passwordToolBar->addAction (QIcon (":icons/rodentia-icons_media-floppy.svg"), tr ("Save Content"), this, SLOT (slotSaveToFile()));
  fixupToolBar (passwordToolBar, PasswordToolBarSave, false, false, &w);
  w->setStatusTip (ui.actionSaveToFile->statusTip ());
  passwordToolBar->addAction (QIcon (":icons/expiry.svg"), tr ("Change expiry"), this, SLOT (slotChangeExpire()));
  fixupToolBar (passwordToolBar, PasswordToolBarExpiry, false, false, &w);
  w->setStatusTip (ui.actionChangeExpire->statusTip ());
  passwordToolBar->addSeparator ();
  passwordToolBar->addAction (QIcon (":icons/rodentia-icons_ok.svg"), tr ("Update"), this, SLOT (slotSaveContent()));
  fixupToolBar (passwordToolBar, PasswordToolBarOk, false, false, &w);
  w->setStatusTip (ui.actionUpdateContent->statusTip ());
  passwordToolBar->addAction (QIcon (":icons/matt-icons_view-refresh.svg"), tr ("Refresh Content"), this, SLOT (slotRefreshContent()));
  fixupToolBar (passwordToolBar, PasswordToolBarRefresh, false, false, &w);
  w->setStatusTip (ui.actionRefreshContent->statusTip ());
  passwordToolBar->setObjectName ("passwordToolBar");
  addToolBar (Qt::RightToolBarArea, passwordToolBar);

  attributeToolBar = new QToolBar (tr ("Attribute Toolbar"), this);
  attributeToolBar->addAction (QIcon (":icons/matt-icons_document-add.svg"), tr ("New Attribute"), this, SLOT (slotNewAttribute()));
  fixupToolBar (attributeToolBar, AttributeToolBarNew, false, false, &w);
  w->setStatusTip (ui.actionNewAttribute->statusTip ());
  attributeToolBar->addAction (QIcon (":icons/rodentia-icons_user-trash-full.svg"), tr ("Delete Attribute"), this, SLOT (slotDeleteAttribute()));
  fixupToolBar (attributeToolBar, AttributeToolBarDelete, false, false, &w);
  w->setStatusTip (ui.actionDeleteAttribute->statusTip ());
  attributeToolBar->addSeparator ();
  attributeToolBar->addAction (QIcon (":icons/rodentia-icons_ok.svg"), tr ("Update"), this, SLOT (slotSaveContent()));
  fixupToolBar (attributeToolBar, AttributeToolBarOk, false, false, &w);
  w->setStatusTip (ui.actionUpdateAttribute->statusTip ());
  attributeToolBar->addAction (QIcon (":icons/matt-icons_view-refresh.svg"), tr ("Refresh List"), this, SLOT (slotRefreshAttributes()));
  fixupToolBar (attributeToolBar, AttributeToolBarRefresh, false, false, &w);
  w->setStatusTip (ui.actionRefreshAttributes->statusTip ());
  attributeToolBar->setObjectName ("attributeToolBar");
  addToolBar (Qt::RightToolBarArea, attributeToolBar);

  commandToolBar = new QToolBar (tr ("Command Toolbar"), this);
  commandToolBar->addAction (QIcon (":icons/rodentia-icons_media-floppy.svg"), tr ("Save Result"), this, SLOT (slotSaveCommandResult ()));
  fixupToolBar (commandToolBar, CommandToolBarSave, false, false, &w);
  w->setStatusTip (tr ("Save command result to file."));
  commandToolBar->addAction (QIcon (":icons/nicubunu-Broom.svg"), tr ("Clear Results"), this, SLOT (slotClearCommand ()));
  fixupToolBar (commandToolBar, CommandToolBarClear, false, false, &w);
  w->setStatusTip (tr ("Reset command fields."));
  commandToolBar->addSeparator ();
  commandToolBar->addAction (QIcon (":icons/rodentia-icons_ok.svg"), tr ("Execute"), this, SLOT (slotExecuteCommand ()));
  fixupToolBar (commandToolBar, CommandToolBarOk, false, false, &w);
  w->setStatusTip (tr ("Send command to the server."));
  commandToolBar->addAction (QIcon (":icons/matt-icons_view-refresh.svg"), tr ("Refresh Elements"), this, SLOT (slotRefreshElementTree ()));
  fixupToolBar (commandToolBar, CommandToolBarRefresh, false, false, &w);
  w->setStatusTip (tr ("Refresh the element tree."));
  commandToolBar->setObjectName ("commandToolBar");
  addToolBar (Qt::RightToolBarArea, commandToolBar);
  commandToolBar->addSeparator ();
  commandToolBar->addAction (QIcon (":icons/rodentia-icons_dialog-question.svg"), tr ("Command Help"), this, SLOT (slotCommandHelp ()));
  fixupToolBar (commandToolBar, CommandToolBarHelp, false, false, &w);
  w->setStatusTip (tr ("List available commands and their syntax."));
  ui.tb_commandInquireFilename->setIcon (QIcon (":icons/matt-icons_document-add.svg"));
}

bool
PwmdMainWindow::confirmQuit ()
{
  QSettings cfg ("qpwmc");

  if (cfg.value ("confirmQuit", "true").toBool())
    {
      QMessageBox msgBox;

      msgBox.setText (tr ("Do you really want to quit?"));
      msgBox.setStandardButtons (QMessageBox::Ok | QMessageBox::Cancel);
      msgBox.setDefaultButton (QMessageBox::Cancel);
      int ret = msgBox.exec ();
      if (ret == QMessageBox::Cancel)
        return false;
    }

  return true;
}

void
PwmdMainWindow::closeEvent (QCloseEvent *ev)
{
  bool b;

  if (!doFinished (b, true))
    ev->ignore ();
  else if (!confirmQuit ())
    ev->ignore ();
  else
    {
      if (clipboardTimer->isActive ())
        {
          clipboardTimer->stop ();
          slotClearClipboard ();
        }

      ev->accept ();
    }
}

void
PwmdMainWindow::tabify (QPlainTextEdit * w)
{
  QString s = w->toPlainText ();
  int pos = w->textCursor ().position ();
  int n = s.count ('\t');

  if (!n)
    return;

  s.replace ("\t", "<TAB>", Qt::CaseInsensitive);
  w->document ()->setPlainText (s);
  QTextCursor t = w->textCursor ();
  pos += n * 4;
  t.setPosition (pos, QTextCursor::MoveAnchor);
  w->setTextCursor (t);
}

void
PwmdMainWindow::slotGeneralOptionsDone (int n)
{
  QEvent *ev = new QEvent (QEvent::Type (PwmdEventOptionsClose));
  QCoreApplication::postEvent (this, ev);

  if (!n)
    return;

  ev = new QEvent (QEvent::Type (PwmdEventReloadConfig));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdMainWindow::slotGeneralOptions ()
{
  if (!optionsDialog)
    {
      optionsDialog = new PwmdOptionsDialog (this);
      connect (optionsDialog, SIGNAL (finished (int)), this, SLOT (slotGeneralOptionsDone (int)));
    }

  optionsDialog->show ();
}

void
PwmdMainWindow::slotClientListDone (int)
{
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdClientState, "OPTION",
                                          Pwmd::inquireCallback,
                                          new PwmdInquireData ("CLIENT-STATE=0")));
  QEvent *ev = new QEvent (QEvent::Type (PwmdEventClientListClose));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdMainWindow::slotClientList ()
{
  if (!clientDialog)
    {
      clientDialog = new PwmdClientDialog (pwm, invokingUser, connectedUser,
                                           this);
      connect (clientDialog, SIGNAL (finished (int)), this, SLOT (slotClientListDone (int)));
    }

  clientDialog->show ();
}

void
PwmdMainWindow::readConfigSettings (QString &lastFile, QString &sock, bool init)
{
  QSettings cfg ("qpwmc");

  sortElementTree = cfg.value ("sortElementTree", true).toBool ();
  cacheContent = cfg.value ("cacheContent", true).toBool ();
  cacheContentToggled ();
  clipboardTimeout = cfg.value ("clipboardTimeout", 20).toUInt();

  int n = cfg.value ("lockTimeout", 5).toInt ();
  if (!init && lockTimeout != n && pwm && pwm->state () >= Pwmd::Connected)
    {
      lockTimeout = n;
      pwm->command (new PwmdCommandQueueItem (PwmdCmdIdLockTimeout, "OPTION",
                                              Pwmd::inquireCallback,
                                              new PwmdInquireData (QString ("LOCK-TIMEOUT=%1").arg (lockTimeout * 10), pwm->handle ())));
    }

  lockTimeout = n;
  n = cfg.value ("releaseTimeout", 0).toInt ();
  origReleaseTimeout = releaseTimeout = n;
  if (!init && releaseTimeout != n && pwm && pwm->state () >= Pwmd::Connected)
    releaseTimer->start(releaseTimeout*1000);

  updateOnEnter = cfg.value ("updateOnEnter", false).toBool ();
  incrementalSearch = cfg.value ("incrementalSearch", true).toBool ();

  targetColor = cfg.value ("targetColor", "DarkOrange").value < QColor > ();
  permissionsColor = cfg.value ("permissionsColor", "DarkRed").value < QColor > ();
  invalidTargetColor = cfg.value ("invalidTargetColor", "Red").value < QColor > ();
  hilightColor = cfg.value ("hilightColor", "Green").value < QColor > ();
  targetLoopColor = cfg.value ("targetLoopColor", "DarkCyan").value < QColor > ();
  targetColorBg = cfg.value ("targetColorBg", "DarkGray").value < QColor > ();
  permissionsColorBg = cfg.value ("permissionsColorBg", "DarkGray").value < QColor > ();
  invalidTargetColorBg = cfg.value ("invalidTargetColorBg", "DarkGray").value < QColor > ();
  hilightColorBg = cfg.value ("hilightColorBg", "DarkGray").value < QColor > ();
  targetLoopColorBg = cfg.value ("targetLoopColorBg", "DarkGray").value < QColor > ();

  if (init && cfg.value ("rememberGeometry", true).toBool ())
    {
      restoreGeometry (cfg.value ("geometry").toByteArray ());
      ui.tabSplitter->restoreState (cfg.value ("tabGeometry").toByteArray ());
      ui.attributeSplitter->restoreState (cfg.value ("attributeGeometry").toByteArray ());
      ui.commandSplitter->restoreState (cfg.value ("commandGeometry").toByteArray());
    }

  if (cfg.value ("reopenLastFile").toBool())
    {
      lastFile = cfg.value ("lastOpenedFile", "").toString ();
      sock = cfg.value ("lastSocket", "").toString ();
    }
  else
    {
      lastFile = QString ();
      sock = QString ();
    }

  bool b = cfg.value ("executeOnEnter").toBool ();
  disconnect (ui.commandTextEdit, SIGNAL (updateContent ()), this,
           SLOT (slotExecuteCommand ()));
  ui.commandTextEdit->setConfirmOnEnter (b);
  if (b)
    connect (ui.commandTextEdit, SIGNAL (updateContent ()), this,
             SLOT (slotExecuteCommand ()));

  b = cfg.value ("updateOnEnter").toBool ();
  disconnect (ui.attributeContent, SIGNAL (updateContent ()), this,
              SLOT (slotSaveContent ()));
  /* Always enabled for attributes since the list is newline separated. But not
   * actually updated unless the content is. */
  ui.attributeContent->setConfirmOnEnter (true);
  disconnect (ui.textEdit, SIGNAL (updateContent ()), this,
              SLOT (slotSaveContent ()));
  ui.textEdit->setConfirmOnEnter (b);
  if (b)
    {
      connect (ui.attributeContent, SIGNAL (updateContent ()), this,
               SLOT (slotSaveContent ()));
      connect (ui.textEdit, SIGNAL (updateContent ()), this,
               SLOT (slotSaveContent ()));
    }

  if (init)
    {
      ui.actionButtonLabels->setChecked (cfg.value ("buttonLabels", false).toBool ());
      setupToolBarLabels (ui.actionButtonLabels->isChecked ());
      restoreState (cfg.value ("windowState").toByteArray ());
    }
}

void
PwmdMainWindow::slotSocketOptions ()
{
  PwmdSocketDialog *w = new PwmdSocketDialog (_socket, this);
  bool needToUpdate = needsUpdate ();
  bool reconn = false;
  bool no = false;
  w->setParent (this, Qt::Dialog);
  int ret = w->exec ();
  QString s = ret ? w->socket () : QString ();

  /* Updates the settings database file. */
  delete w;

  if (ret)
    {
      PwmdRemoteHost old, newHost;
      bool a = PwmdRemoteHost::fillRemoteHost (_socket, old);
      bool b = PwmdRemoteHost::fillRemoteHost (s, newHost);
      bool remote = false;

      if (a)
        old = currentHostData;

      if ((a != b && b) || (a && b && old != newHost))
        reconn = remote = true;
      else if (_socket != s)
        reconn = true;

      if (reconn)
        {
         if (!doFinished (no, true))
           return;

         if (remote)
           {
             pwm->setSocket (PwmdRemoteHost::socketUrl (newHost));
             pwm->setConnectParameters (newHost.socketArgs ());
             _socket = newHost.name ();
             currentHostData = newHost;
           }
         else
           {
             pwm->setSocket (s);
             _socket = s;
           }
        }
    }
  else
    return;

  /* If the socket changed in the dialog and the document has changes then
   * slotCommandResult will connect to the new socket after the changes have
   * been committed. Otherwise, there are no document changes and we can
   * connect here. */
  if ((reconn || pwm->state () == Pwmd::Init) && (!needToUpdate || no))
    {
      pwm->reset ();
      reconnectSocket = true;
      QTimer::singleShot (1, this, SLOT (slotConnectSocket()));
    }
  else if (reconn && needToUpdate)
    reconnectSocket = true;
}

QString
PwmdMainWindow::socket ()
{
  return _socket;
}

QString
PwmdMainWindow::filename ()
{
  return _filename;
}

gpg_error_t
PwmdMainWindow::badPassphraseCallback (void *data, bool isFinal)
{
  PwmdMainWindow *d = static_cast <PwmdMainWindow *>(data);
  pwmd_socket_t type;
  gpg_error_t rc = GPG_ERR_BAD_PASSPHRASE;

  if (isFinal)
    return rc; // ignored

  rc = pwmd_socket_type (d->pwm->handle (), &type);
  if (rc)
    return rc;

  /* Already handled by libpwmd. */
  if (type != PWMD_SOCKET_LOCAL)
    return GPG_ERR_BAD_PASSPHRASE;

  if (++d->pinentry_try == 3)
    return GPG_ERR_BAD_PASSPHRASE;

  return 0;
}

void
PwmdMainWindow::clearElementTree (QTreeWidget *tree)
{
  foreach (QTreeWidgetItem * item, tree->findItems ("*",
						    Qt::MatchRecursive |
						    Qt::MatchWildcard))
  {
    PwmdTreeWidgetItemData *data = qvariant_cast<PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
    delete data;
  }

  tree->clear ();
}

void
PwmdMainWindow::slotToggleHiddenContent (bool b)
{
  if (doSaveContent (true))
    {
      disconnect (ui.actionHidden, SIGNAL (toggled(bool)), this, SLOT (slotToggleHiddenContent(bool)));
      ui.actionHidden->setChecked (!b);
      connect (ui.actionHidden, SIGNAL (toggled(bool)), this, SLOT (slotToggleHiddenContent(bool)));
      hiddenContent = b;
      pwm->runQueue ();
      return;
    }

  hiddenContent = -1;

  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      PwmdTreeWidgetItemData *data = qvariant_cast<PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

      if (b && !data->hasAttribute ("hidden"))
        setNewAttribute (item, "hidden", QString (), false, true);
      else if (!b && data->hasAttribute ("hidden"))
        deleteAttribute (item, "hidden", true);
    }

  pwm->runQueue ();
}

void
PwmdMainWindow::slotSaveDocument ()
{
  doSaveContent (false, true, newFile);
}

bool
PwmdMainWindow::needsUpdate ()
{
  return fixupToolBar (passwordToolBar, PasswordToolBarOk, false, true)
    || fixupToolBar (contentToolBar, ContentToolBarOk, false, true)
    || fixupToolBar (attributeToolBar, AttributeToolBarOk, false, true);
}

void
PwmdMainWindow::saveDocument (bool extended)
{
  PwmdSaveDialog *d = new PwmdSaveDialog (pwm, newFile);

  d->setParent (this, Qt::Dialog);
  
  if (extended)
    {
#ifdef Q_OS_ANDROID
      d->showMaximized();
#endif
      d->exec ();
    }
  else
    {
      PwmdSaveWidget *w = d->widget ();
      w->save (0, true);
    }

  delete d;
}

void
PwmdMainWindow::slotSaveExtended ()
{
  doSaveContent (true, true, true);
}

void
PwmdMainWindow::savePasswordContentFinalize (QTreeWidgetItem *item,
                                             const QString &type)
{
  fixupToolBar (passwordToolBar, PasswordToolBarOk, false);
  setAttribute (item, "password", type, true);
  setModifiedContent (false);

  if (ui.tabWidget->currentWidget () == ui.contentTab)
    setContentItemsEnabled ();
}

void
PwmdMainWindow::updatePasswordAttribute (QTreeWidgetItem *item, bool queue)
{
  QString type = ui.passwordWidget->type ();

  PwmdInquireData *inq = new PwmdInquireData (QString ("SET password %1 %2").arg (elementPath (item), type));
  inq->setUser (type);
  inq->setUserPtr (item);
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdPasswordContent,
                                          "ATTR",
                                          Pwmd::inquireCallback, inq), queue);
}

void
PwmdMainWindow::fixupNextTabOrElement ()
{
  if (findElementAction->isVisible ())
    {
      QEvent *ev = new QKeyEvent (QEvent::KeyPress, 'F', Qt::ControlModifier);
      QCoreApplication::postEvent (this, ev);
      return;
    }

  if (nextElement)
    ui.elementTree->setCurrentItem (nextElement);
  else if (nextTabWidget)
    ui.tabWidget->setCurrentWidget (nextTabWidget);

  QTreeWidgetItem *cur = firstSelectedItem ();
  if ((hasTarget (oldSelectedItem, true) || hasTarget (cur, true))
      && resolveTargets (oldSelectedItem) == resolveTargets (cur))
    elementSelected (cur, false, oldSelectedItem);

  nextTabWidget = nullptr;
  nextElement = nullptr;

  if (editTargetAttributes)
    {
      disconnect (ui.actionToggleTargetAttribute, SIGNAL (triggered (bool)), this, SLOT (slotEditTargetAttributes(bool)));
      ui.actionToggleTargetAttribute->setChecked (true);
      connect (ui.actionToggleTargetAttribute, SIGNAL (triggered (bool)), this, SLOT (slotEditTargetAttributes(bool)));
      editAttributes (editTargetAttributes, selectedElement);
      return;
    }
  else if (targetAttributeElement)
    {
      elementSelected (selectedElement);
      targetAttributeElement = nullptr;
      return;
    }
  else if (newAttribute)
    {
      delete newAttribute;
      newAttribute = nullptr;
      slotNewAttribute ();
      return;
    }
  else if (hiddenContent != -1)
    {
      slotToggleHiddenContent (hiddenContent);
      return;
    }

  switch (dndOperation)
    {
    case DndCopy:
      slotDndCopyElement ();
      break;
    case DndMove:
      slotDndMoveElement ();
      break;
    case DndTarget:
      slotDndCreateTarget ();
      break;
    default:
      break;
    }
}

void
PwmdMainWindow::saveContentFinalize (QTreeWidgetItem *item,
                                     const QString &content, bool password)
{
  if (cacheContent)
    updateContentPointer (item, new PwmdElementContent (content.toUtf8 ()));

  if (password)
    return;

  QTreeWidgetItem *r = resolveTargets (item);
  if (!r || r == item)
    setTimeLabels (item, true);
  else if (r)
    setTimeLabels (r, true);

  setModifiedContent (false);
  fixupToolBar (contentToolBar, ContentToolBarOk, false);
  setContent (content, item);
  fixupNextTabOrElement ();
}

void
PwmdMainWindow::saveAttrContentFinalize (const QString &path,
                                         AttributeFinalizeT *attr)
{
  QString value = attr->content.replace ("<TAB>", "\t", Qt::CaseInsensitive);

  if (attr->name == "password")
    setContent (attr->password, attr->item);

  QTreeWidgetItem *item = !editTargetAttributes ? findElement (path) : attr->item;
  setAttribute (item, attr->name, value, true);
  setModifiedContent (false);

  if (attr->name == "_target" || (attr->name == "_acl" && !isElementOwner (item)))
    {
      targetAttributeElement = nullptr;
      QEvent *ev = new QEvent (QEvent::Type (PwmdEventRefreshElementTree));
      QCoreApplication::postEvent (this, ev);
      return;
    }

  fixupNextTabOrElement ();
}

void
PwmdMainWindow::setAttributeContentOnce (QTreeWidgetItem *item,
                                         const QString &path,
                                         const QString &value,
                                         bool save, bool extended, bool queue)
{
  PwmdInquireData *inq = new PwmdInquireData (QString ("SET %1 %2 %3").arg (selectedAttribute->text (), path, value));
  AttributeFinalizeT *attr = new AttributeFinalizeT;
  attr->item = item;
  attr->content = value;
  attr->name = selectedAttribute->text ();
  attr->password = ui.passwordWidget->password ();
  inq->setUser (path);
  inq->setUserPtr (attr);
  if (save)
    inq->setSaving (extended);

  PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdAttrContent, "ATTR", Pwmd::inquireCallback, inq);
  ignoreErrors (cmd);
  pwm->command (cmd, queue
                /* Fixes editing attribute content then selecting
                 * another attribute before committing. */
                && selectedAttribute == ui.attributeList->currentItem ());
}

bool
PwmdMainWindow::doSaveContent (bool queue, bool save, bool extended,
                               int *which, bool force)
{
  if (!needsUpdate ())
    {
      if (save)
        {
          saveDocument (extended);
          return false;
        }

      if (!force)
        return false;
    }

  QString path;
  QTreeWidgetItem *item = nullptr;
  QString content = QString ();
  bool passwordMod = fixupToolBar (passwordToolBar, PasswordToolBarOk, false, true);
  bool contentMod = ui.passwordFrame->isHidden() && fixupToolBar (contentToolBar, ContentToolBarOk, false, true);
  int base64 = 0;

  if (passwordMod || contentMod)
    item = selectedElement;
  else if (ui.tabWidget->currentWidget () == ui.attributeTab)
    item = attributeElement;
  else if (force)
    {
      contentMod = ui.passwordFrame->isHidden ();
      passwordMod = !contentMod;
      item = selectedElement;
    }

  if (!item)
    return false;

  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  path = elementPath (item);

  if (contentMod || passwordMod)
    {
      QByteArray b;

      path.append ("\t");

      if (passwordMod)
        {
          b = QByteArray (ui.passwordWidget->password ().toUtf8 ());
          updatePasswordAttribute (item, queue);
        }
      else
        b = QByteArray (ui.textEdit->toPlainText ().toUtf8 ());

      if (ui.actionBase64->isChecked ())
        {
          content = toBase64 (item, b);
          base64 = 1;
        }
      else if (!ui.actionBase64->isChecked ())
        {
          if (data->attribute ("encoding") == "base64")
            {
              content = QByteArray::fromBase64 (b);
              base64 = 2;
            }
          else
            content = b;
        }
      else
        content = b;

      if (base64 == 1)
        setNewAttribute (item, "encoding", "base64", false, true);
      else if (base64 == 2)
        deleteAttribute (item, "encoding", true);

      if (which)
        *which = passwordMod ? Password : Content;

      path.append (content);
      PwmdInquireData *inq = new PwmdInquireData (path);
      inq->setHandle (pwm->handle ());
      inq->setUserBool (passwordMod && !queue);
      inq->setUser (content);
      inq->setUserPtr (item);

      if (save)
        inq->setSaving (extended);

      PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdStoreContent, "STORE",
                                                            Pwmd::inquireCallback, inq);

      /* Normally a STORE may be included in a BULK command but we can't do
       * that here because of newline characters being parsed by libassuan. */
      if (base64 == 1)
        cmd->setNeedsInquire ();

      pwm->command (cmd, queue);
    }
  else if (fixupToolBar (attributeToolBar, AttributeToolBarOk, false, true))
    {
      if (!selectedAttribute
	  || !(selectedAttribute->flags () & Qt::ItemIsEnabled))
	{
	  setModifiedContent (false);
	  return false;
	}

      if (which)
        *which = Attributes;

      QString value = ui.attributeContent->toPlainText ().replace ("<TAB>", "\t", Qt::CaseInsensitive);

      foreach (QTreeWidgetItem *cur, ui.elementTree->selectedItems ())
        {
          if (selectedAttribute->text () == "_target" || !editTargetAttributes)
            path = elementPath (cur);

          setAttributeContentOnce (cur, path, value, save, extended, queue);
          if (searchAndReplace)
            {
              searchAndReplace = false;
              break;
            }
        }
    }

  return true;
}

void
PwmdMainWindow::slotSaveContent ()
{
  doSaveContent (true);
  pwm->runQueue ();
}

void
PwmdMainWindow::slotCopyContentToClipboard ()
{
  PwmdTreeWidgetItemData *data = qvariant_cast <PwmdTreeWidgetItemData *>(selectedElement->data (0, Qt::UserRole));

  if (data->hasAttribute ("password"))
    {
      ui.passwordWidget->clipboardPassword ();
      return;
    }

  setClipboard (ui.textEdit->toPlainText ().toUtf8 ());
  slotClipboardTimer ();
}

void
PwmdMainWindow::slotSaveToFile ()
{
  PwmdFileDialog d (this, tr ("Save to file"));

  d.setAcceptMode (QFileDialog::AcceptSave);
  d.setFileMode (QFileDialog::AnyFile);

  if (!d.exec () || d.selectedFiles ().isEmpty ())
    return;

  QStringList f = d.selectedFiles ();
  QFile file (f.at (0));

  if (!file.open (QFile::WriteOnly))
    {
      QMessageBox m;

      m.setText (file.fileName ());
      m.setInformativeText (file.errorString ());
      m.setIcon (QMessageBox::Warning);
      m.exec ();
      return;
    }

  QByteArray data;
  PwmdTreeWidgetItemData *idata = qvariant_cast <PwmdTreeWidgetItemData *>(selectedElement->data (0, Qt::UserRole));

  if (idata->hasAttribute ("password"))
    {
      data.append (ui.passwordWidget->password ().toUtf8 ());
    }
  else
    {
      if (ui.actionBase64->isChecked () && idata->attribute ("encoding") == "base64")
        data = QByteArray::fromBase64 (ui.textEdit->toPlainText ().toUtf8 ());
      else
        data.append (ui.textEdit->toPlainText ().toUtf8 ());
    }

  if (file.write (data) == -1)
    {
      QMessageBox m;

      m.setText (file.fileName ());
      m.setInformativeText (file.errorString ());
      m.setIcon (QMessageBox::Warning);
      m.exec ();
    }
}

void
PwmdMainWindow::slotRefreshContent ()
{
  elementSelected (selectedElement, true);
}

void
PwmdMainWindow::cacheContentToggled ()
{
  if (cacheContent)
    return;

  foreach (QTreeWidgetItem * item, ui.elementTree->findItems ("*",
							      Qt::
							      MatchRecursive |
							      Qt::
							      MatchWildcard))
  {
    PwmdTreeWidgetItemData *data = qvariant_cast <PwmdTreeWidgetItemData *>(item->data (0, Qt::UserRole));
    data->setContent (nullptr);
  }
}

QString
PwmdMainWindow::toBase64 (QTreeWidgetItem *item, QByteArray &b)
{
  QString content;
  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  // Don't re-encode the content.
  if (data->attribute ("encoding") == "base64")
    {
      return b;
    }

  content = b.toBase64 ();

  // Do line breaks at 64 characters (OpenSSL style). It is also much
  // faster when setting the content for ui.textEdit.
  for (int c = 0, n = 0, t = content.length (); n < t; c++, n++)
    {
      if (c == 64)
	{
	  content.insert (n, "\n");
	  c = -1;
	  t = content.length ();
	}
    }

  if (content.at (content.size ()) != '\n')
    content.append ("\n");

  return content;
}

void
PwmdMainWindow::slotCreatePasswordAttribute ()
{
  doSaveContent (true);
  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
      if (!data->hasAttribute ("password")
          && isElementOwner (resolveTargets (item)))
        {
          setNewAttribute (item, "password", 0, true, true);
        }
    }

  pwm->runQueue ();
}

void
PwmdMainWindow::slotInsertContent ()
{
  bool base64 = false;
  PwmdFileDialog d (this, tr ("Insert file"));

  d.setFileMode (QFileDialog::ExistingFile);
  d.setOption (QFileDialog::ReadOnly);

  if (!d.exec () || !d.selectedFiles ().count ())
    return;

  QFile file (d.selectedFiles ().at (0));
  if (!file.open (QIODevice::ReadOnly))
    {
      QMessageBox m;

      m.setText (file.fileName ());
      m.setInformativeText (file.errorString ());
      m.setIcon (QMessageBox::Warning);
      m.exec ();
      return;
    }

  QByteArray data = file.readAll ();
  file.close ();
  for (int i = 0, t = data.size (); i < t; i++)
    {
      QChar c = data.at (i);

      if ((c.category () == QChar::Other_Control && c.toLatin1 () != '\n')
	  || c.isNull ())
	{
	  base64 = true;
	  break;
	}
    }

  PwmdTreeWidgetItemData *idata = qvariant_cast < PwmdTreeWidgetItemData * >(selectedElement->data (0, Qt::UserRole));

  if (idata->hasAttribute ("password"))
    {
      if (base64)
        {
          QMessageBox m;

          m.setText (QApplication::
                     tr ("Binary data in file."));
          m.setInformativeText (tr ("Cannot use binary data as a passphrase. Please use only plain text when setting the passphrase from a file."));
          m.setIcon (QMessageBox::Critical);
          m.exec ();
          return;
        }

      ui.passwordWidget->setPassword (data);
      return;
    }

  QMessageBox m (this);
  m.setIcon (QMessageBox::Question);
  m.setStandardButtons (QMessageBox::Apply|QMessageBox::Discard|QMessageBox::Cancel);
  QAbstractButton *pb = m.button (QMessageBox::Apply);
  pb->setText (tr ("Append"));
  pb = m.button (QMessageBox::Discard);
  pb->setText (tr ("Overwrite"));
  m.setDefaultButton (QMessageBox::Cancel);
  m.setEscapeButton (QMessageBox::Cancel);
  m.setText (tr ("Would you like to append the read file to the existing content, or overwrite existing content?"));

  int r = m.exec ();
  if (r == QMessageBox::Cancel)
    return;

  QString content = QString ();
  if (r == QMessageBox::Apply)
    content = ui.textEdit->document ()->toPlainText ();

  if (base64)
    {
      removeAttribute (selectedElement, "encoding");
      content += toBase64 (selectedElement, data);
      setAttribute (selectedElement, "encoding", "base64");
    }
  else
    content += data;

  ui.textEdit->document ()->setPlainText (content);
  ui.actionBase64->setChecked (base64);
  if (base64)
    slotToggleBase64Content (true);
}

void
PwmdMainWindow::slotConnectionStateChanged (Pwmd::ConnectionState s)
{
  Pwmd::ConnectionState old = state;
  state = s;
  pinentry_try = 0;
  reconnectSocket = false;
  disconnectSocket = false;

  /* Fixes a race condition when Pwmd::connect() fails. The handle may have
   * been reset (which clears the error code) before the dialog is shown.
   */
  pwm->tlsError = pwmd_gnutls_error(pwm->handle(), nullptr);

  switch (s)
    {
    case Pwmd::Init:
      setConnected (false);
      break;
    case Pwmd::Connecting:
      updateConnectTimer ();
      break;
    case Pwmd::Connected:
      updateConnectTimer (true);
      setConnected (true, old);
      break;
    case Pwmd::Opened:
      setOpen (true);
      break;
    }
}

void
PwmdMainWindow::slotDisconnectSocket ()
{
  bool b = false;

  if (!doFinished (b, true) && !b)
    return;

  if (b || (!needsUpdate () && !isWindowModified ()))
    pwm->reset ();
  else
    disconnectSocket = true;
}

void
PwmdMainWindow::slotKnownHostCallback (void *data, const char *host,
                                       const char *key, size_t len)
{
  gpg_error_t rc = Pwmd::knownHostPrompt (data, host, key, len);
  emit knownHostRc (rc);
}

void
PwmdMainWindow::updateSocketOptions ()
{
  pwmd_setopt (pwm->handle (), PWMD_OPTION_SSH_AGENT,
	       currentHostData.sshAgent ());
  pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
               pwm->state() == Pwmd::Init ? currentHostData.connectTimeout ()
               : currentHostData.socketTimeout ());
  pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_VERIFY,
	       currentHostData.tlsVerify ());
  if (!currentHostData.tlsPriority ().isEmpty ())
    pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_PRIORITY,
                 currentHostData.tlsPriority ().toUtf8 ().data ());
}

void
PwmdMainWindow::slotCancel ()
{
#ifdef Q_OS_ANDROID
  androidJni->cancel ();
#endif
  pwm->cancel ();
}

void
PwmdMainWindow::slotUpdateConnectTimer ()
{
  updateConnectTimer (false, pwm->state () != Pwmd::Connecting);
}

void
PwmdMainWindow::updateConnectTimer (bool reset, bool cancel)
{
  static int remaining = currentHostData.connectTimeout ();

  if (reset || cancel)
    {
      if (!cancelButton && pwm->state () == Pwmd::Init && !cancel)
        {
          cancelButton = new QPushButton (tr ("Cancel"));
          connect (cancelButton, SIGNAL (clicked ()), this,
                   SLOT (slotCancel ()));
          statusBar ()->addPermanentWidget (cancelButton);
        }
      else if (cancelButton && pwm->state () == Pwmd::Connected)
        {
          delete cancelButton;
          cancelButton = nullptr;
        }

      remaining = currentHostData.connectTimeout ();
      progressBar->setHidden (true);
      progressBar->reset ();
      progressBar->setRange (0, currentHostData.connectTimeout ());
      progressBar->setValue (0);
      return;
    }

  progressBar->setHidden (false);
  progressBar->setTextVisible (true);
  progressBar->setMaximum (currentHostData.connectTimeout ());
  progressBar->setFormat (QString (tr ("Connecting: timeout in %1s...")).arg (remaining));
  progressBar->setValue (remaining--);
  QTimer::singleShot (1000, this, SLOT (slotUpdateConnectTimer ()));
}

#ifdef Q_OS_ANDROID
void
PwmdMainWindow::slotAndroidAuthenticated ()
{
  androidAuthenticating = false;
  centralWidget ()->setHidden (false);
}

/* The state gets messed up when verifying the key guard so we need to keep the
 * state of the previous call. */
void
PwmdMainWindow::slotApplicationStateChanged (Qt::ApplicationState s)
{
  static bool prev;
  QSettings cfg ("qpwmc");

  if (!cfg.value ("requireUnlock", true).toBool())
    return;

  if (androidAuthenticating)
    return;

  if (!prev && (s & Qt::ApplicationActive))
    {
      androidAuthenticating = true;
      if (androidJni->authenticate ())
          centralWidget ()->setHidden (true);
      else
        androidAuthenticating = false;
    }

  prev = !!s;
}

void
PwmdMainWindow::slotAndroidConnectReady (PwmdRemoteHost hostData)
{
  pwmd_setopt(pwm->handle(), PWMD_OPTION_READ_CB_DATA, androidJni);
  pwmd_setopt(pwm->handle(), PWMD_OPTION_WRITE_CB_DATA, androidJni);
  androidJni->setTimeout (hostData.connectTimeout ());
  androidJni->connectSocket (hostData);
}

void
PwmdMainWindow::slotAndroidConnectResult (PwmdRemoteHost hostData, int fd)
{
  if (hostData.type () != PWMD_SOCKET_SSH)
    {
      pwmd_setopt (pwm->handle (), PWMD_OPTION_READ_CB, PwmdAndroidJNI::read);
      pwmd_setopt (pwm->handle (), PWMD_OPTION_WRITE_CB, PwmdAndroidJNI::write);
    }

  androidJni->setTimeout (hostData.socketTimeout ());
  pwm->connectFd (fd);
}

void
PwmdMainWindow::slotAndroidConnectionError (gpg_error_t rc)
{
  if (!rc)
    return;

  pwm->cancel ();
  showError (rc);
  return;
}
#endif

void
PwmdMainWindow::slotConnectSocket ()
{
  if (reconnectSocket)
    _filename = "";

  ui.actionDisconnect->disconnect (this);
  connect (ui.actionDisconnect, SIGNAL (triggered()), this,
           SLOT (slotCancel ()));
  ui.actionDisconnect->setStatusTip (tr ("Disconnect from the pwmd server."));
  ui.actionDisconnect->setText (tr ("&Disconnect"));
  updateConnectTimer (true);
  pwm->flushQueue ();
  updateSocketOptions ();
  pwm->setFilename (filename ());

#ifdef Q_OS_ANDROID
  if (currentHostData.type () == PWMD_SOCKET_USER)
    pwm->connectHost (currentHostData);
  else
    pwm->connect ();
#else
  pwm->connect ();
#endif
}

void
PwmdMainWindow::openFileFinalize (gpg_error_t rc)
{
  /* Will be deleted in Pwmd::PwmdCommandCollectorThread. */
  openInquireData = nullptr;
  pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 0);
  pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 0);
  setWindowModified (false);

  if (rc)
    {
      _filename = tr ("No file");
      state = Pwmd::Connected;
      setOpen (false);
      return;
    }

  releaseTimeout = origReleaseTimeout;
  if (releaseTimeout)
    releaseTimer->start(releaseTimeout*1000);

  QTimer::singleShot (1, this, SLOT (slotEditElements ()));
}

void
PwmdMainWindow::slotDoOpenFile ()
{
  pinentry_try = 0;
  newFile = false;

  if (openInquireData && !openInquireData->keyFile().isEmpty ())
    {
      pwmd_socket_t type;

      pwmd_socket_type (pwm->handle (), &type);
      pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 1);
      pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 1);
      pwm->open (Pwmd::inquireCallback, openInquireData);
    }
  else
    {
      if (openInquireData)
        delete openInquireData;

      openInquireData = new PwmdInquireData (pwm->handle(), pwm->filename());
      pwm->open (Pwmd::inquireCallback, openInquireData);
    }
}

void
PwmdMainWindow::slotOpenFileDone (int r)
{
  if (r)
    {
      _filename = openDialog->filename ();
      openInquireData = new PwmdInquireData (pwm->handle (),
                                             openDialog->filename ());
      pwm->setFilename (openDialog->filename ());
      pwm->setLockOnOpen (openDialog->lock ());
      openInquireData->setKeyFile (openDialog->keyFile ());
    }

  /* Depending on how the file list was scrolled (scrollbar or click-and-drag),
   * determines if there are a bunch of commands in the queue to fetch cache
   * status for each item. Since the dialog is now closed, we can remove these
   * queued commands entirely. */
  pwm->flushQueue ();

  QEvent *ev = new QEvent (QEvent::Type (PwmdEventOpenFileClose));
  QCoreApplication::postEvent (this, ev);

  if (r)
    {
      ev = new QEvent (QEvent::Type (PwmdEventOpenFileConfirm));
      QCoreApplication::postEvent (this, ev);
    }
}

void
PwmdMainWindow::slotCloseFile ()
{
  bool b;

  if (!doFinished (b, true))
    return;

  pwm->close ();
}

void
PwmdMainWindow::slotOpenFile ()
{
  bool b;

  if (!doFinished (b, true))
    return;

  if (!openDialog)
    {
      openDialog = new PwmdOpenDialog (pwm, this);
      connect (openDialog, SIGNAL (finished (int)), this, SLOT (slotOpenFileDone (int)));
    }

  openDialog->show ();
}

void
PwmdMainWindow::setWindowTitle (bool b)
{
  pwmd_socket_t type;
  pwmd_socket_type (pwm->handle (), &type);
  QString s;

  if (type != PWMD_SOCKET_LOCAL)
    s = QString ("QPwmc (%1%2)").arg (type == PWMD_SOCKET_SSH ? currentHostData.sshUsername ()+"@" : "", currentHostData.hostname ());
  else
    s = "QPwmc";

  s.append (QString (": %1[*]").arg (!b ? tr ("No file") : filename ()));
  QMainWindow::setWindowTitle (s);
}

void
PwmdMainWindow::setOpen (bool b)
{
  _filename = pwm->filename ();
  setWindowTitle (b);
  selectedElement = nullptr;
  attributeElement = nullptr;
  dropElement = nullptr;
  targetAttributeElement = nullptr;
  selectedAttribute = nullptr;
  ui.elementTree->setEnabled(b);
  setContent ("");
  clearElementTree (ui.elementTree);
  clearAttributeTab ();
  ui.actionReload->setEnabled(b);
  ui.actionCut->setEnabled (b);
  ui.actionCopy->setEnabled (b);
  ui.actionPaste->setEnabled (b);
  fixupMenu (ui.menuFile, FileMenuSave, b);
  fixupMenu (ui.menuFile, FileMenuSaveExt, b);
  fixupMenu (ui.menuFile, FileMenuOpenForm, b);
  fixupMenu (ui.menuFile, FileMenuClose, b);
  ui.actionFileOptions->setEnabled (b);
  elementToolBar->setEnabled (b);
  setElementItemsEnabled (nullptr, false);
  fixupMenu (ui.menuView, ViewMenuExpandAll, b);
  fixupMenu (ui.menuView, ViewMenuButtonLabels, b);
}

void
PwmdMainWindow::invokingUserFinalize (const QString &result)
{
  invokingUser = result.split (",");
}

void
PwmdMainWindow::currentUserFinalize (const QString &result)
{
  connectedUser = result;
}

void
PwmdMainWindow::setConnected (bool b, Pwmd::ConnectionState old)
{
  QCommonStyle style;
  pwmd_socket_t type;

  setWindowTitle (false);
  pwmd_socket_type (pwm->handle (), &type);
  setWindowModified (false);
  setModified (false);
  setOpen (false);
  selectedElement = nullptr;
  attributeElement = nullptr;
  targetAttributeElement = nullptr;
  ui.actionNew->setEnabled(b);
  ui.actionOpen->setEnabled(b);
  fileToolBar->setEnabled (b);
  ui.actionReload->setEnabled(false);
  ui.actionOpenForm->setEnabled(false);
  ui.tabWidget->setEnabled(false);
  ui.tabWidget->setCurrentIndex (TabContent);
  setContentItemsEnabled (true);
  clearAttributeTab ();
  setContent ("");
  pinentry_try = 0;
  fixupMenu (ui.menuFile, FileMenuOpen, b);

  if (b)
    {
      pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY,
                   type != PWMD_SOCKET_LOCAL);
      ui.actionDisconnect->disconnect (this);
      connect (ui.actionDisconnect, SIGNAL (triggered()), this,
               SLOT (slotDisconnectSocket ()));
      ui.actionDisconnect->setStatusTip (tr ("Disconnect from the pwmd server."));
      ui.actionDisconnect->setText (tr ("&Disconnect"));
      if (type != PWMD_SOCKET_LOCAL)
        pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
                     currentHostData.socketTimeout ());

      fixupMenu (ui.menuView, ViewMenuClientList, b);
      ui.tabWidget->setEnabled (true);
      ui.commandTab->setEnabled (true);

      if (old == Pwmd::Connecting)
        {
          pwm->command (new PwmdCommandQueueItem (PwmdCmdIdInvokingUser,
                                                  "GETCONFIG",
                                                  Pwmd::inquireCallback,
                                                  new PwmdInquireData ("invoking_user", pwm->handle ())),
                        true);

          pwm->command (new PwmdCommandQueueItem (PwmdCmdIdCurrentUser,
                                                  "GETINFO",
                                                  Pwmd::inquireCallback,
                                                  new PwmdInquireData ("user", pwm->handle ())),
                        true);

          pwm->command (new PwmdCommandQueueItem (PwmdCmdIdLockTimeout,
                                                  "OPTION",
                                                  Pwmd::inquireCallback,
                                                  new PwmdInquireData (QString ("LOCK-TIMEOUT=%1").arg (lockTimeout*10), pwm->handle ())),
                        true);
        }

      /* Prevent looping over this when the previous open failed by testing
       * 'old'. */
      if (!filename().isEmpty () && old == Pwmd::Connecting)
        {
          PwmdInquireData *inq = new PwmdInquireData (pwm->handle(), pwm->filename());
          newFile = false;
          pwm->open (Pwmd::inquireCallback, inq, true);
        }

      pwm->runQueue ();
      return;
    }
  else
    {
      updateConnectTimer (true, true);
      delete cancelButton;
      cancelButton = nullptr;
    }

  ui.actionDisconnect->disconnect (this);
  connect (ui.actionDisconnect, SIGNAL (triggered ()), this,
           SLOT (slotConnectSocket ()));
  ui.actionDisconnect->setStatusTip (tr ("Connect to the configured pwmd server."));
  ui.actionDisconnect->setText (tr ("&Connect"));
#ifdef Q_OS_ANDROID
  ui.actionDisconnect->setEnabled (!currentHostData.name ().isEmpty ());
#endif
  pwm->reset (false);
  if (type != PWMD_SOCKET_LOCAL)
    pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
                 currentHostData.connectTimeout ());
}

void
PwmdMainWindow::setElementStatusTip (QTreeWidgetItem *item)
{
  QStatusBar *sb = statusBar();

  if (!item)
    {
      sb->showMessage (QString ());
      return;
    }

  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  if (!data)
    return;

  if (data->badTarget() && !data->targetLoop())
    sb->showMessage (tr("This element contains a target attribute that does not resolve."));
  else if (data->targetLoop())
    sb->showMessage (tr("This element contains a recursion loop. Use the attribute editor to edit the \"target\"."));
  else if (data->badPermissions())
    sb->showMessage (tr("This element has permissions that deny access to the current user ID."));
  else if (!isElementOwner (resolveTargets (item)))
    sb->showMessage (tr ("You cannot modify this element but may add child elements."));
  else
    sb->showMessage (QString ());
}

void
PwmdMainWindow::slotElementEntered (QTreeWidgetItem * item, int)
{
  QBrush br;

  if (item && item == previousElement)
    return;

  if (previousElement)
    {
      setElementColors (previousElement);
      br = previousElement->foreground (0);
      br.setColor (previousElementColor);
      previousElement->setForeground (0, br);
      br.setColor(previousElementColorBg);
      previousElement->setBackground(0, br);
      setElementColors (previousElement);
    }

  if (!item || dndOperation == DndNone)
    {
      previousElement = nullptr;
      return;
    }

  br = item->foreground (0);
  previousElementColor = br.color ();
  br.setColor (hilightColor);
  item->setForeground (0, br);

  br = item->background (0);
  previousElementColorBg = br.color ();
  QBrush bg(QPalette::Window);
  bg.setColor(hilightColorBg);
  item->setBackground(0, bg);

  previousElement = item;
  setElementStatusTip (item);
}

void
PwmdMainWindow::slotElementEditorClosed (QWidget * w,
				     QAbstractItemDelegate::EndEditHint h)
{
  QLineEdit *le = qobject_cast <QLineEdit *> (w);

  if (le->text () == tr ("New element") || le->text ().isEmpty ()
      || h == QAbstractItemDelegate::RevertModelCache)
    {
      QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewElementCancel));
      QCoreApplication::postEvent (this, ev);
      return;
    }

  setElementItemsEnabled (selectedElement, selectedElement);
}

bool
PwmdMainWindow::event (QEvent * ev)
{
  int type = ev->type ();
  AttributesRetrievedEvent *attrsEvent;
  PwmdTreeWidgetItemData *data = nullptr;

  if (type == QEvent::StatusTip)
    {
      QStatusTipEvent *se = static_cast < QStatusTipEvent * >(ev);
      QStatusBar *sb = statusBar ();

      sb->showMessage (se->tip ());
      ev->accept ();
      return true;
    }

  if (type >= QEvent::User)
    dndOperation = DndNone;

  switch (type)
    {
    default:
      break;
    case PwmdEventBusy:
      if (ui.tabWidget->currentWidget () == ui.attributeTab)
        setAttributeItemsEnabled ();
      else if (ui.tabWidget->currentWidget () == ui.contentTab)
        setContentItemsEnabled ();
      else if (ui.tabWidget->currentWidget () == ui.commandTab)
        setCommandItemsEnabled ();

      fixupMultiToolbar ();
      ev->accept ();
      return true;
    case PwmdEventOptionsClose:
      delete optionsDialog;
      optionsDialog = nullptr;
      ev->accept ();
      return true;
    case PwmdEventFileOptionsClose:
      delete fileOptionsDialog;
      fileOptionsDialog = nullptr;
      fixupToolBar (fileToolBar, FileToolBarOpen, true);
      ev->accept ();
      return true;
    case PwmdEventClientListClose:
      delete clientDialog;
      clientDialog = nullptr;
      ev->accept ();
      return true;
    case PwmdEventOpenFileClose:
      delete openDialog;
      openDialog = nullptr;
      ev->accept ();
      return true;
    case PwmdEventOpenFileConfirm:
      slotDoOpenFile ();
      ev->accept ();
      return true;
    case PwmdEventReloadConfig:
        {
          QString tmp, tmp2;
          readConfigSettings (tmp, tmp2, false);
        }
      ev->accept ();
      return true;
    case PwmdEventRefreshElementTree:
    case PwmdEventDnDTargetAttr:
      refreshElementTree (ev->type () == PwmdEventDnDTargetAttr);
      setModified ();
      ev->accept ();
      return true;
    case PwmdEventTargetAttr:
      refreshElementTree ();
      ev->accept ();
      return true;
    case PwmdEventNewElementCancel:
      if (newElement)
        data = qvariant_cast<PwmdTreeWidgetItemData *> (newElement->data (0, Qt::UserRole));
      delete data;
      delete newElement;
      newElement = nullptr;
      attributeElement = selectedElement = previousElement;
      dndOperation = DndNone;
      setElementItemsEnabled (selectedElement, selectedElement);
      ev->accept ();
      return true;
    case PwmdEventNewAttributeCancel:
      delete newAttribute;
      newAttribute = nullptr;
      selectedAttribute = nullptr;
      slotAttributeSelected (ui.attributeList->item (previousAttributeIndex ()),
                             nullptr);
      ev->accept ();
      return true;
    case PwmdEventAttrsRetrieved:
      attrsEvent = static_cast<AttributesRetrievedEvent *> (ev);
      emit attributesRetrieved (attrsEvent->item ());
      ev->accept ();
      return true;
    }

  return QMainWindow::event (ev);
}

void
PwmdMainWindow::newElementFinalize (QString path)
{
  setModified ();
  path.chop (1);
  selectedElementPath = path;
  selectedElement = nullptr;
  newElement = nullptr;
  // Need to create an event to avoid a segfault in the mouse event
  // handler since this function is in an event itself.
  QEvent *ev = new QEvent (QEvent::Type (PwmdEventTargetAttr));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdMainWindow::renameElementFinalize ()
{
  setModified ();
  refreshElementTree ();
}

void
PwmdMainWindow::slotRenameElementUpdate (QWidget * w)
{
  QLineEdit *le = qobject_cast<QLineEdit *> (w);

  if (newElement)
    {
      QString path = QString ();

      if (le->text () == tr ("New element") || le->text ().isEmpty ())
	{
	  QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewElementCancel));
	  QCoreApplication::postEvent (this, ev);
	  return;
	}

      for (QTreeWidgetItem * item = newElement; item; item = item->parent ())
	{
	  PwmdTreeWidgetItemData *data = qvariant_cast <PwmdTreeWidgetItemData *>(item->data (0, Qt::UserRole));

	  path.prepend (QString ("%1%2").arg (data->hasTarget () ? "" : "",
                        item->text (0)));
	  if (item->parent ())
	    path.prepend ('\t');
	}

      if (findElement (path, newElement))
	{
          selectedElement = attributeElement = previousElement;
	  QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewElementCancel));
	  QCoreApplication::postEvent (this, ev);
	  return;
	}

      if (dndOperation != DndNone)
	{
	  (void)doDndCopyElement (elementPath (copyElement),
                                  elementPath (newElement));
	}
      else
	{
	  path.append ('\t');
	  PwmdInquireData *inq = new PwmdInquireData (path);
          inq->setUser (path);
	  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdNewElement, "STORE", Pwmd::inquireCallback, inq));
	}
      return;
    }
  else if (!selectedElement)
    return;

  int n = selectedElementPath.lastIndexOf ("\t") + 1;
  QString origName = selectedElementPath.mid (n);

  if (le->text ().isEmpty ())
    {
      selectedElement->setText (0, origName);
      return;
    }

  if (origName == le->text ())
    return;

  PwmdInquireData *inq = new PwmdInquireData (QString ("%1 %2").arg (selectedElementPath, le->text ()));
  inq->setUser (origName);
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdRenameElement, "RENAME",
                                          Pwmd::inquireCallback, inq));
}

void
PwmdMainWindow::slotContentModified (bool b)
{
  if (ui.tabWidget->currentWidget() == ui.contentTab)
    {
     if (ui.passwordFrame->isHidden ())
       fixupToolBar (contentToolBar, ContentToolBarOk, b);
    }
  else if (ui.tabWidget->currentWidget() == ui.attributeTab)
    fixupToolBar (attributeToolBar, AttributeToolBarOk, b);

  setFileModified (b);
}

void
PwmdMainWindow::setFileModified (bool b)
{
  if (!b)
    {
      ui.textEdit->document ()->setModified (b);
      ui.attributeContent->document ()->setModified (b);
      ui.passwordWidget->setModified (b);
    }

  if (b || (!isWindowModified () && !b))
    setModified (b);
}

void
PwmdMainWindow::setModified (bool b)
{
  if (b && state != Pwmd::Opened)
    return;

  fixupToolBar (fileToolBar, FileToolBarSave, b);

  if (b)
    setWindowModified (true);

  if (!b && isWindowModified ())
    {
      newFile = false;
      setFileModified (b);
    }
}

void
PwmdMainWindow::slotExpandAll (bool b)
{
  QTreeWidgetItem *item;
  unsigned total = 0, n = 0;

  ui.elementTree->setHorizontalScrollMode (QAbstractItemView::ScrollPerItem);
  progressBar->setHidden (false);
  setBusy ();

  foreach (item, ui.elementTree->findItems ("*",
					    Qt::MatchRecursive | Qt::
					    MatchWildcard)) total++;

  if (total > 100)
    {
      progressBar->setMaximum (total);
      progressBar->setFormat (tr ("Building view %p%"));
    }

  foreach (item, ui.elementTree->findItems ("*",
					    Qt::MatchRecursive | Qt::
					    MatchWildcard))
  {
    item->setExpanded (b);

    if (total > 100 && !(++n % 50))
      progressBar->setValue (n);
  }

  item = selectedElement;

  while (item)
    {
      item->setExpanded (b);
      item = item->parent ();
    }

  setBusy (false);
  ui.elementTree->scrollToItem (selectedElement,
				QAbstractItemView::PositionAtCenter);
  ui.elementTree->setHorizontalScrollMode (QAbstractItemView::ScrollPerPixel);
}

void
PwmdMainWindow::slotClipboardAttribute ()
{
  setClipboard (selectedAttribute->text ());
}

void
PwmdMainWindow::setClipboard (const QString &s)
{
  QClipboard *c = QApplication::clipboard ();

  if (c->supportsSelection ())
    c->setText (s, QClipboard::Selection);
  c->setText (s);
}

void
PwmdMainWindow::slotClipboardElementPath ()
{
  setClipboard (elementPath (selectedElement));
}

bool
PwmdMainWindow::doFinished (bool &no, bool ignore)
{
  QMessageBox msgBox;

  no = false;
  msgBox.setIcon (QMessageBox::Question);

  if (isWindowModified () || needsUpdate ())
    {
      QMessageBox m (QMessageBox::Question, tr ("Save to disk?"),
                     tr ("The current data file has changes that have yet to be written to disk. Would you like to save the changes?"));
      m.addButton (QMessageBox::Yes);
      m.addButton (QMessageBox::No);
      m.addButton (QMessageBox::Cancel);
      switch (m.exec ())
        {
        case QMessageBox::Yes:
          doSaveContent (true, true, newFile);
          pwm->runQueue ();
          // Prevent closing the window when there are changes since there may
          // be an error from somewhere like pinentry, gpg-agent, etc.
          return false;
          break;
        case QMessageBox::Cancel:
          doSaveContent (true);
          return false;
        case QMessageBox::No:
          no = ignore == true;
          return ignore ? true : false;
        default:
          break;
        }
    }

  return true;
}

void
PwmdMainWindow::slotClipboardTimer ()
{
  if (clipboardTimeout)
    clipboardTimer->start (clipboardTimeout * 1000);
}

void
PwmdMainWindow::slotClearClipboard ()
{
  QClipboard *c = QApplication::clipboard();

  clipboardTimer->stop();
  if (c->supportsSelection ())
    c->clear (QClipboard::Selection);

  c->clear (QClipboard::Clipboard);
#ifdef Q_OS_ANDROID
  PwmdAndroidJNI::clearClipboard ();
#endif
}

void
PwmdMainWindow::slotReleaseTimer ()
{
  PwmdInquireData *inq = new PwmdInquireData();
  PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdUnLock, "UNLOCK", 0, inq);
  cmd->addError (GPG_ERR_SOURCE_USER_1, GPG_ERR_NOT_LOCKED);
  if (!pwm->command (cmd))
    delete inq;
}

void
PwmdMainWindow::slotPasswordContentChanged ()
{
  fixupToolBar (passwordToolBar, PasswordToolBarOk, true);
  setModifiedContent ();
}

void
PwmdMainWindow::slotContentChanged ()
{
  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(selectedElement->data (0, Qt::UserRole));
  QPlainTextEdit *w = ui.tabWidget->currentWidget () == ui.attributeTab
    ? ui.attributeContent : ui.textEdit;
  bool base64 = false;

  // Undo
  if (!w->document ()->isModified ())
    {
      setFileModified (false);
      return;
    }

  if (ui.tabWidget->currentWidget () == ui.attributeTab)
    {
      if (selectedAttribute && selectedAttribute->text () == "_target")
	tabify (ui.attributeContent);
    }
  else
    {
      QString s = w->toPlainText ();

      for (int i = 0, t = s.length (); i < t; i++)
	{
	  QChar c = s.at (i);

	  if ((c.category () == QChar::Other_Control && c.toLatin1 () != '\n')
	      || c.isNull ())
	    {
	      base64 = true;
	      break;
	    }
	}
    }

  ui.actionBase64->setChecked (base64
                               || data->attribute ("encoding") == "base64");
  setModifiedContent ();
}

void
PwmdMainWindow::updateBusyStatusBar (Pwmd::ConnectionState s)
{
  if (s == Pwmd::Init)
    statusBarLabel->setText (tr ("Not connected."));
  else if (s == Pwmd::Connecting)
    statusBarLabel->setText (tr ("Connecting ..."));
  else
    statusBarLabel->setText (isBusy ? tr ("Busy ...") :
#ifdef Q_OS_ANDROID
                             tr ("Ready."));
#else
                             tr ("Ready. Use Shift-click and Ctrl-click for multi-selection."));
#endif
}

void
PwmdMainWindow::slotBusy (int cmdId, bool b)
{
  setBusy (b, cmdId);
  updateBusyStatusBar (pwm->state ());
}

void
PwmdMainWindow::setBusy (bool b, int cmdId)
{
  static int refcount;

  if (b)
    {
      if (cancelButton && pwm->state () == Pwmd::Connecting)
        cancelButton->setHidden (false);

      if (!refcount)
	{
          setElementStatusTip (selectedElement);
	  setCursor (Qt::BusyCursor);
	}

      refcount++;
    }
  else
    {
      refcount--;
      if (refcount < 0)
	refcount = 0;

      if (!refcount)
        {
          if (pwm->state () != Pwmd::Connecting)
            {
              if (cancelButton)
                cancelButton->setHidden (true);

              progressBar->setHidden (true);
              progressBar->setValue (0);
              progressBar->setMaximum (100);
              progressBar->setFormat ("");
              unsetCursor ();
            }
        }
    }

  switch (cmdId)
    {
    case PwmdCmdIdInvalid:
    case PwmdCmdIdClientList:
    case PwmdCmdIdClientKill:
    case PwmdCmdIdClientState:
    case PwmdCmdIdClientNCache:
    case PwmdCmdIdClientNClients:
    case PwmdCmdIdFileGetConfig:
    case PwmdCmdIdFileIsCached:
    case PwmdCmdIdFileClearCache:
    case PwmdCmdIdFileCacheTimeout:
    case PwmdCmdIdFileKeyInfo:
      return;
    default:
      break;
    }

  if (pwm->queued () > 0 && isBusy)
    return;

  isBusy = b || refcount;
  if (!isBusy && pwm->state () == Pwmd::Init)
    return;

  fixupToolBar (fileToolBar, FileToolBarOpen, !isBusy && state != Pwmd::Init
                && !fileOptionsDialog);
  fixupToolBar (fileToolBar, FileToolBarSave, !isBusy && isWindowModified ());
  fixupMenu (ui.menuFile, FileMenuSaveExt, !isBusy && state == Pwmd::Opened);

  QEvent *ev = new QEvent (QEvent::Type (PwmdEventBusy));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdMainWindow::showError (gpg_error_t rc)
{
  Pwmd::showError (rc, pwm, hasMultiSelection ());
}

void
PwmdMainWindow::reloadFinalize ()
{
  state = Pwmd::Connected;
  setConnected ();
  QEvent *ev = new QEvent (QEvent::Type (PwmdEventOpenFileConfirm));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdMainWindow::saveExpandedItemList ()
{
  expandedItemsList.clear ();
  foreach (QTreeWidgetItem * item,
           ui.elementTree->findItems ("*", Qt:: MatchRecursive
                                      | Qt:: MatchWildcard))
    {
      if (item->isExpanded ())
        expandedItemsList.append (elementPath (item));
    }
}

void
PwmdMainWindow::slotReload ()
{
  bool b;

  if (!doFinished (b, true))
    {
      pwm->runQueue ();
      return;
    }

  fixupToolBar (passwordToolBar, PasswordToolBarOk, false);
  fixupToolBar (contentToolBar, ContentToolBarOk, false);
  saveExpandedItemList ();
  pwm->runQueue ();
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdReload, "RESET"));
}

void
PwmdMainWindow::resetSelectedItems ()
{
  QTreeWidgetItem *item = firstSelectedItem ();
  ui.elementTree->setCurrentItem (item, 0, QItemSelectionModel::ClearAndSelect);
}

void
PwmdMainWindow::slotToggleMultiMode (bool b)
{
#ifdef Q_OS_ANDROID
  if (b)
    ui.elementTree->setSelectionMode (QAbstractItemView::MultiSelection);
  else
    ui.elementTree->setSelectionMode (QAbstractItemView::SingleSelection);
#else
  (void) b;
#endif
}

void
PwmdMainWindow::slotRefreshElementTree ()
{
  pwm->flushQueue ();
  fixupToolBar (passwordToolBar, PasswordToolBarOk, false);
  fixupToolBar (contentToolBar, ContentToolBarOk, false);
  saveExpandedItemList ();
  refreshElementTree ();
}

bool
PwmdMainWindow::refreshElementTree (bool target)
{
  if (state != Pwmd::Opened)
    return false;

  nextElement = selectedElement = attributeElement = dropElement = nullptr;
  oldSelectedItem = nullptr;
  doSaveContent (true);
  setContent ("");
  clearAttributeTab ();
  PwmdInquireData *inq = new PwmdInquireData ("--recurse --sexp");
  inq->setUserBool (target);
  return pwm->command (new PwmdCommandQueueItem (PwmdCmdIdElementTree,
                                                 "LIST",
                                                 Pwmd::inquireCallback, inq));
}

void
PwmdMainWindow::refreshElementTreeFinalize (QByteArray &tmp)
{
  QString path = QString ();

  freeSearchResults ();

  if (!selectedElementPath.isEmpty ())
    path = selectedElementPath;

  clearElementTree (ui.elementTree);
  previousElement = targetAttributeElement = nullptr;
  QList < QTreeWidgetItem * >items = buildElementTree (tmp);

  ui.elementTree->insertTopLevelItems (0, items);
  ui.elementTree->setSortingEnabled (sortElementTree);
  if (sortElementTree)
    ui.elementTree->sortByColumn (0, Qt::AscendingOrder);

  selectedElement = findElement (path);
  if (!selectedElement)
    setContent ("");

  setCurrentElement (selectedElement);

  if (ui.actionExpandAll->isChecked ())
    slotExpandAll (ui.actionExpandAll->isChecked ());
  else if (!expandedItemsList.isEmpty ())
    {
      foreach (QTreeWidgetItem * item,
               ui.elementTree->findItems ("*", Qt:: MatchRecursive | Qt:: MatchWildcard))
        {
          if (expandedItemsList.contains (elementPath (item)))
              item->setExpanded (true);
        }
    }

#ifdef Q_OS_ANDROID
  QList <QAction *>actions = ui.menuElement->actions();
  actions[ElementMenuMulti]->setChecked (false);
#endif
  ui.elementTree->scrollToItem (selectedElement,
				QAbstractItemView::PositionAtCenter);
  setElementStatusTip (selectedElement);
  elementSelected (selectedElement);
}

void
PwmdMainWindow::slotStatusMessage (QString line, void *)
{
  QStringList l = QString (line).split (" ");

  if (l.at (0) == "NEWFILE")
    {
      newFile = true;
    }
  else if (l.at (0) == "XFER")
    {
      progressBar->setHidden (false);
      progressBar->setMaximum (l.at (2).toULong (nullptr));
      progressBar->setValue (l.at (1).toULong (nullptr));
      progressBar->setFormat (tr ("Transferring - %v of %m %p%"));
    }
  else if (l.at (0) == "LOCKED")
    {
      progressBar->setHidden (false);
      progressBar->setMaximum (0);
      progressBar->setValue (0);
      progressBar->setFormat (tr ("Waiting for lock"));
    }
  else if (l.at (0) == "EXPIRE")
    {
      if (l.at(2).toUInt() != 0) // STORE status message, not GET
        updateElementExpiry (l.at(2).toLong ());
    }
  else if (l.at (0) == "MODIFIED")
    {
      QString title = QString (tr ("The currently opened data file has been modified by another client."));
      QString desc = QString (tr ("You will be unable to commit any current and further changes and will need to find a way to record any changes made then reopen the data file and begin editing again. This can be avoided by locking the data file before editing (File->File Options)"));
      QMessageBox m;

      m.setText (title);
      m.setInformativeText (desc);
      m.setIcon (QMessageBox::Information);
      m.exec ();
    }
}

void
PwmdMainWindow::slotToggleBase64Content (bool)
{
  setFileModified ();
  doSaveContent (true, false, false, nullptr, true);
  pwm->runQueue ();
}

void
PwmdMainWindow::setCurrentElement (QTreeWidgetItem *item)
{
  disconnect (ui.elementTree,
              SIGNAL (currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)),
              this,
              SLOT (slotElementSelected (QTreeWidgetItem *, QTreeWidgetItem *)));
  ui.elementTree->setCurrentItem (item, 0, QItemSelectionModel::ClearAndSelect);
  connect (ui.elementTree,
           SIGNAL (currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)),
           this,
           SLOT (slotElementSelected (QTreeWidgetItem *, QTreeWidgetItem *)));
}

void
PwmdMainWindow::updateContentPointer (QTreeWidgetItem * i,
                                      PwmdElementContent * newData)
{
  QTreeWidgetItem *r = resolveTargets (i);
  if (!r)
    {
      delete newData;
      return;
    }
  PwmdTreeWidgetItemData *data =
    qvariant_cast < PwmdTreeWidgetItemData * >(r->data (0, Qt::UserRole));
  PwmdElementContent *old = data->content ();

  // For elements with a "_target" attribute pointing to this element.
  foreach (QTreeWidgetItem * item, ui.elementTree->findItems ("*",
							      Qt::
							      MatchRecursive |
							      Qt::
							      MatchWildcard))
  {
    if (item == r)
      continue;

    data = qvariant_cast <PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
    if ((old && data->content () == old) || (resolveTargets (item) == r))
      data->setContent (newData);
  }

  data = qvariant_cast < PwmdTreeWidgetItemData * >(r->data (0, Qt::UserRole));
  data->setContent (newData);
}

bool
PwmdMainWindow::createAttributeCache (QTreeWidgetItem *item, const QString &tmp)
{
  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  if (tmp.isEmpty () || !data)
    return false;

  QStringList attrs = tmp.split ("\n");
  PwmdAttributeList *newAttrs = new PwmdAttributeList ();

  for (int attr = 0, t = attrs.count (); attr < t; attr++)
    {
      int n = attrs.at (attr).indexOf (QChar (' '));
      QString s;

      if (n == -1)
        s.append (attrs.at (attr));
      else
        s.append (attrs.at (attr).left (n));

      data->addAttribute (newAttrs, s, attrs.at (attr).mid (n + 1));
    }

  data->setAttributes (newAttrs);
  data->setNeedsAttrRefresh (false);
  return true;
}

int
PwmdMainWindow::previousAttributeIndex ()
{
  if (advancedSearchDialog && advancedSearchDialog->isVisible ())
    {
      if (currentSearchResult
          && ((currentSearchResult->resultType () & SearchResult::Attr)
              || (currentSearchResult->resultType () & SearchResult::AttrValue)))
        return currentSearchResult->curAttr ();
    }

  for (int i = 0; i < ui.attributeList->count (); i++)
    {
      QListWidgetItem *item = ui.attributeList->item (i);

      if (item->text () == previousAttributeText)
        return i;
    }

  return 0;
}

void
PwmdMainWindow::elementSelectedFinalize (QTreeWidgetItem *item,
                                         const QString &result, bool attrs)
{
  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  previousElement = selectedElement;
  selectedElement = attributeElement = item;
  ui.elementTree->scrollToItem (selectedElement);
  setElementColors (item);
  setElementItemsEnabled (item, true);

  if (sortElementTree)
    item->sortChildren (0, Qt::AscendingOrder);

  if (ui.tabWidget->currentWidget () == ui.attributeTab)
    refreshAttributeListFinalize (item, QString (), false);

  if (ui.tabWidget->currentWidget () == ui.contentTab)
    {
      if (data->hasAttribute ("password"))
        setPasswordItemsEnabled ();
      else
        setContentItemsEnabled ();
    }
  else if (ui.tabWidget->currentWidget () == ui.attributeTab)
    {
      if (attrs)
        pwm->runQueue ();
      else
        slotEditAttributes ();
    }

  setContent (result, item);
  ui.tabWidget->setEnabled (true);
  if (cacheContent)
    updateContentPointer (item, new PwmdElementContent (result.toUtf8 ()));

  if (item->isDisabled ())
    ui.contentTab->setEnabled (false);
}

void
PwmdMainWindow::elementSelected (QTreeWidgetItem * item, bool refresh,
                                 QTreeWidgetItem *old)
{
  if (!item)
    {
      pwm->runQueue ();
      return;
    }

  bool attrs = refreshAttributeList (item, !cacheContent || refresh, true);
  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  QString s = QString ();

  if (cacheContent && data->contentCached () && !refresh)
    {
      s = QString::fromUtf8 (data->content ()->data(), -1);
      elementSelectedFinalize (item, s, attrs);
    }
  else if (!data->hasError () && (refresh || item != old))
    {
      if (hasMultiSelection ())
        return;

      PwmdInquireData *inq = new PwmdInquireData (elementPath (item));
      inq->setUserPtr (item);
      pwm->command (new PwmdCommandQueueItem (PwmdCmdIdGetContent, "GET",
                                              Pwmd::inquireCallback, inq));
      return;
    }
  else
    {
      setContent ("");
      if (ui.tabWidget->currentWidget () == ui.contentTab)
        {
          ui.contentTab->setEnabled (false);
          setContentItemsEnabled (true);
          ui.attributeTab->setEnabled (false);
        }
      else if (ui.tabWidget->currentWidget () == ui.attributeTab)
        {
          if (targetAttributeElement)
            {
              setElementColors (targetAttributeElement, true, true);
              targetAttributeElement = nullptr;
              selectedElement = attributeElement = item;
            }
          if (!attrs)
            slotEditAttributes ();
        }
    }

  pwm->runQueue ();
}

bool
PwmdMainWindow::hasOwnerOfItemSelected ()
{
  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      if (isElementOwner (item))
        return true;
    }

  return false;
}

void
PwmdMainWindow::setAttributeItemsEnabled (bool b, QTreeWidgetItem *multi)
{
  QTreeWidgetItem *element = multi ? multi : selectedElement;
  element = !multi && attributeElement ? attributeElement : element;

  b = multi || b;
  if (b && !multi)
    {
      setContentItemsEnabled (false);
      setCommandItemsEnabled (false);
      setPasswordItemsEnabled (false);
      ui.tabWidget->setCurrentWidget (ui.attributeTab);
    }

  setElementItemsEnabled (element, !isBusy, multi);

  if (!multi)
    attributeToolBar->setHidden (!b);

  b = b && element && state == Pwmd::Opened && !isBusy;
  attributeToolBar->setEnabled (b);
  ui.attributeTab->setEnabled (b);
  PwmdTreeWidgetItemData *data = nullptr;
  if (element)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(element->data (0, Qt::UserRole));

  b = b && data && !data->hasError ()
    && isElementOwner (resolveTargets (element));

  fixupMenu (ui.menuAttributes, AttributeMenuTarget, !isBusy && !multi
             && (targetAttributeElement
                 || (hasTarget (element) && !badTarget (element)))
             && ui.tabWidget->currentWidget () == ui.attributeTab);
  fixupToolBar (attributeToolBar, AttributeToolBarNew, b);
  fixupToolBar (attributeToolBar, AttributeToolBarDelete,
                b && ui.attributeList->count () > 1
                && ui.attributeList->selectedItems ().count ());
  fixupToolBar (attributeToolBar, AttributeToolBarOk, !isBusy && data
                && !data->badPermissions ()
                && fixupToolBar (attributeToolBar, AttributeToolBarOk, false, true));
  fixupToolBar (attributeToolBar, AttributeToolBarRefresh, !isBusy && !multi
                && element
                && ui.tabWidget->currentWidget () == ui.attributeTab
                && hasSingleSelection ());
  ui.cb_searchAndReplace->setEnabled (ui.attributeList->currentItem ());
}

void
PwmdMainWindow::setContentItemsEnabled (bool b, bool hide,
                                        QTreeWidgetItem *multi)
{
  QTreeWidgetItem *element = multi ? multi : selectedElement;
  PwmdTreeWidgetItemData *data = nullptr;

  if (!multi && ui.tabWidget->currentWidget () == ui.contentTab)
    setElementColors (selectedElement);

  if (element)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(element->data (0, Qt::UserRole));

  if (b && !multi)
    {
      setAttributeItemsEnabled (false);
      setCommandItemsEnabled (false);
      if (!data || !data->hasAttribute ("password"))
        setPasswordItemsEnabled (false);
      ui.tabWidget->setCurrentWidget (ui.contentTab);
    }

  setElementItemsEnabled (element, b && !isBusy, multi);

  if ((multi || element) && b)
    {
      if (data && data->hasAttribute ("password"))
        {
          setPasswordItemsEnabled (true, multi);
          if (!multi)
            return;
        }
    }

  if (!multi)
    {
      contentToolBar->setHidden (!b &&
                                 (hide || ui.tabWidget->currentWidget () != ui.contentTab));

      ui.textEdit->setHidden (data && data->hasAttribute ("hidden"));
      ui.passwordWidget->setHidden (true);
    }

  ui.textEdit->setEnabled (element && hasSingleSelection ());
  contentToolBar->setEnabled (element &&
                              data && !data->hasError () && !isBusy);
  b = (multi || b)
    && element && state == Pwmd::Opened
    && !element->isDisabled ()
    && !badTarget (element)
    && data && !data->badPermissions ()
    && isElementOwner (resolveTargets (element));

  if (!multi)
    {
      if (data && !ui.f_expire->isHidden ())
        setElementExpireLabel (data->attribute ("_expire").toUInt ());

      ui.f_expire->setHidden (!b || (data && data->attribute ("_expire").isEmpty ()));
      ui.f_expire->setEnabled (b && isElementOwner (element));
      ui.contentTab->setEnabled (b && !isBusy);
    }

  fixupMenu (ui.menuContent, ContentMenuBase64, b && !isBusy
             && hasSingleSelection ());
  fixupMenu (ui.menuContent, ContentMenuHidden, b && !isBusy
             && hasSingleSelection ());
  fixupMenu (ui.menuContent, ContentMenuClipboard, data && !data->hasError ()
             && hasSingleSelection ()
             && isElementOwner (element, true));
  fixupToolBar (contentToolBar, ContentToolBarSave, data && !data->hasError ()
                && hasSingleSelection ()
                && isElementOwner (element, true));
  fixupToolBar (contentToolBar, ContentToolBarInsert, b && !isBusy
                && hasSingleSelection ());
  fixupToolBar (contentToolBar, ContentToolBarRefresh, b && !isBusy
                && hasSingleSelection ());
  fixupToolBar (contentToolBar, ContentToolBarPassword, b && !isBusy
                && isElementOwner (resolveTargets (element)));
  fixupToolBar (contentToolBar, ContentToolBarExpiry, b && !isBusy
                && isElementOwner (resolveTargets (element)));

  if (!multi)
    setElementStatusTip (selectedElement);
}

void
PwmdMainWindow::setElementExpireLabel (long expire)
{
  QDateTime date;

  date.setSecsSinceEpoch (expire);
  ui.l_expires->setText (date.toString ());
  ui.l_expiresTitle->setText (tr (expire <= time (nullptr) ? "Expired:" : "Expires:"));
}

void
PwmdMainWindow::setPasswordItemsEnabled (bool b, QTreeWidgetItem *multi)
{
  QTreeWidgetItem *element = multi ? multi : selectedElement;

  if (b && !multi)
    {
      setContentItemsEnabled (false);
      setAttributeItemsEnabled (false);
      setCommandItemsEnabled (false);
      ui.contentTab->setEnabled (true);
    }

  PwmdTreeWidgetItemData *data = nullptr;
  if (element)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(element->data (0, Qt::UserRole));

  if (!multi)
    {
      ui.passwordFrame->setHidden (!b);
      ui.passwordFrame->setEnabled (element &&
                                    isElementOwner (element, true)
                                    && hasSingleSelection ());
      ui.textEdit->setHidden (true);
      ui.passwordWidget->setHidden (false);
      passwordToolBar->setHidden (!b);
      passwordToolBar->setEnabled (element);
    }

  b = (multi || b) && element && state == Pwmd::Opened
    && !element->isDisabled () && !badTarget (element);

  if (!multi)
    {
      ui.f_expire->setHidden (!b || (data && data->attribute ("_expire").isEmpty ()));
      if (data && !ui.f_expire->isHidden ())
        setElementExpireLabel (data->attribute ("_expire").toUInt ());
    }

  b = b && isElementOwner (resolveTargets (element));
  if (!multi)
    ui.f_expire->setEnabled (b);

  fixupToolBar (passwordToolBar, PasswordToolBarInsert, b && !isBusy
                && hasSingleSelection ());
  fixupToolBar (passwordToolBar, PasswordToolBarSave, b && !isBusy
                && hasSingleSelection ());
  fixupToolBar (passwordToolBar, PasswordToolBarExpiry, b && !isBusy);
  fixupToolBar (passwordToolBar, PasswordToolBarRefresh, b && !isBusy
                && hasSingleSelection ());

  fixupMenu (ui.menuContent, ContentMenuInsert, b && !isBusy
             && hasSingleSelection ());
  fixupMenu (ui.menuContent, ContentMenuSave, data && !data->hasError ()
             && hasSingleSelection ());
  fixupMenu (ui.menuContent, ContentMenuClipboard, data && !data->hasError ()
             && hasSingleSelection ());
  fixupMenu (ui.menuContent, ContentMenuExpiry, b && !isBusy);
  fixupMenu (ui.menuContent, ContentMenuPassword, false);
  fixupMenu (ui.menuContent, ContentMenuBase64, false);
  fixupMenu (ui.menuContent, ContentMenuHidden, false);
  fixupMenu (ui.menuContent, ContentMenuRefresh, b && !isBusy
             && hasSingleSelection ());
  passwordToolBar->setEnabled (b && !isBusy);
  ui.passwordWidget->setEditable (b && !isBusy
                                  && hasSingleSelection ()
                                  && isElementOwner (resolveTargets (element)));
}


void
PwmdMainWindow::setCommandItemsEnabled (bool b, QTreeWidgetItem *multi)
{
  if (b && !multi)
    {
      ui.tabWidget->setEnabled (true);
      setContentItemsEnabled (false);
      setAttributeItemsEnabled (false);
      setPasswordItemsEnabled (false);
    }

  setElementItemsEnabled (selectedElement, multi || !isBusy, multi);
  ui.commandTab->setEnabled (b && !isBusy);
  commandToolBar->setHidden (!b);
  fixupToolBar (commandToolBar, CommandToolBarOk, state == Pwmd::Opened
                && !isBusy && !ui.commandTextEdit->toPlainText ().isEmpty ());
  fixupToolBar (commandToolBar, CommandToolBarRefresh, state == Pwmd::Opened
                && !isBusy);
  fixupToolBar (commandToolBar, CommandToolBarHelp, !isBusy
                && (state == Pwmd::Opened || state == Pwmd::Connected));
}

void
PwmdMainWindow::setElementItemsEnabled (QTreeWidgetItem *item, bool b,
                                        QTreeWidgetItem *multi)
{
  QTreeWidgetItem *element = multi ? multi : item;
  PwmdTreeWidgetItemData *data = nullptr;

  b = multi || b;

  if (element)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(element->data (0, Qt::UserRole));

  // Toolbar icons
  elementToolBar->setEnabled (state == Pwmd::Opened && !isBusy);
  fixupToolBar (elementToolBar, ElementToolBarCreate, !newElement
                && state == Pwmd::Opened
                && !hasMultiSelection ());
  fixupToolBar (elementToolBar, ElementToolBarDelete, b &&
                ((data && !data->badPermissions() && isElementOwner (element))
                 || isElementOwner (element)));
  fixupToolBar (elementToolBar, ElementToolBarRename, b &&
                data && !data->badPermissions() && isElementOwner (element)
                && hasSingleSelection ());
  fixupToolBar (elementToolBar, ElementToolBarFind, state == Pwmd::Opened
                && !isBusy &&ui.elementTree->topLevelItemCount () > 1);
  if (!multi)
    contentToolBar->setEnabled (element && b);

  // Menu items
  QList <QAction *>actions = ui.menuElement->actions();
  actions[ElementMenuNewRoot]->setEnabled (state == Pwmd::Opened);
  actions[ElementMenuNew]->setEnabled (state == Pwmd::Opened && element
                                       && !data->badPermissions()
                                       && !data->badTarget () && b
                                       && hasSingleSelection ());
  actions[ElementMenuNewSibling]->setEnabled (state == Pwmd::Opened && element
                                              && element->parent ()
                                              && hasSingleSelection ());
  actions[ElementMenuResolveTargets]->setEnabled (element
                                                  && hasTarget (element, true)
                                                  && !badTarget (element)
                                                  && (!data->badPermissions() ||
                                                      resolveTargets (element))
                                                  && b
                                                  && hasSingleSelection ());
  actions[ElementMenuEditContent]->setEnabled (element && b &&
                                               ((!data->badPermissions()
                                                && ui.tabWidget->currentWidget() != ui.contentTab)
                                               ||
                                               (ui.tabWidget->currentWidget() != ui.attributeTab)));
  actions[ElementMenuRefreshTree]->setEnabled (state == Pwmd::Opened
                                               && !isBusy);
#ifdef Q_OS_ANDROID
  actions[ElementMenuMulti]->setEnabled (state == Pwmd::Opened
                                         && !isBusy
                                         && ui.elementTree->findItems ("*", Qt::MatchRecursive|Qt::MatchWildcard).count () > 1);
#else
  actions[ElementMenuMulti]->setVisible (false);
#endif
}

void
PwmdMainWindow::slotElementClicked (QTreeWidgetItem *, int)
{
  slotFindFinished ();
}

void
PwmdMainWindow::fixupMultiToolbar ()
{
  bool b = false;;
  unsigned mask = 0;
  int count = ui.elementTree->selectedItems ().count ();

  if (count <= 1)
    return;

  if (ui.tabWidget->currentWidget () == ui.contentTab)
    {
      foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
        {
          setContentItemsEnabled (true, false, item);
          b = fixupToolBar (contentToolBar, ContentToolBarPassword, false,
                            true);
          if (b)
            mask |= ContentPasswordMask;

          b = fixupToolBar (contentToolBar, ContentToolBarExpiry, false,
                            true);
          if (b)
            mask |= ContentExpiryMask;
        }

      fixupToolBar (contentToolBar, ContentToolBarPassword,
                    (mask & ContentPasswordMask));
      fixupToolBar (contentToolBar, ContentToolBarExpiry,
                    (mask & ContentExpiryMask));
      if (mask)
        contentToolBar->setEnabled (true);
    }
  else if (ui.tabWidget->currentWidget () == ui.attributeTab)
    {
      b = fixupToolBar (attributeToolBar, AttributeToolBarRefresh, false, true);
      fixupToolBar (attributeToolBar, AttributeToolBarRefresh, b && count == 1);

      foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
        {
          setContentItemsEnabled (true, false, item);
          setAttributeItemsEnabled (true, item);
          b = fixupToolBar (attributeToolBar, AttributeToolBarNew, false,
                            true);
          if (b)
            mask |= AttributeNewMask;

          b = fixupToolBar (attributeToolBar, AttributeToolBarDelete, false,
                            true);
          if (b)
            mask |= AttributeDeleteMask;
        }

      fixupToolBar (attributeToolBar, AttributeToolBarNew,
                    (mask & AttributeNewMask));
      fixupToolBar (attributeToolBar, AttributeToolBarDelete,
                    (mask & AttributeDeleteMask));
      if (mask)
        attributeToolBar->setEnabled (true);
    }
  else if (ui.tabWidget->currentWidget () == ui.commandTab)
    {
      foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
        setCommandItemsEnabled (true, item);
    }

  mask = 0;
  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      setElementItemsEnabled (nullptr, true, item);
      b = fixupToolBar (elementToolBar, ElementToolBarDelete, false, true);
      mask |= b ? 1 : 0;
    }
  fixupToolBar (elementToolBar, ElementToolBarDelete, mask != 0);
}

/* Test if the command queue contains a command that when completed will
 * refresh the element tree. */
bool
PwmdMainWindow::hasQueuedRefreshTree ()
{
  if (pwm->isQueued (PwmdCmdIdElementTree)
      || pwm->isQueued (PwmdCmdIdDeleteElement)
      || pwm->isQueued (PwmdCmdIdRenameElement)
      || pwm->isQueued (PwmdCmdIdNewElement)
      || pwm->isQueued (PwmdCmdIdDndMoveElement)
      || pwm->isQueued (PwmdCmdIdDndCopyElement)
      || pwm->isQueued (PwmdCmdIdDndCreateTarget))
    return true;

  return false;
}

/* This slot is called after slotElementSelected(). It seems to be consistant
 * in the firing order but may break in the future in which case element
 * selection will also break. */
void
PwmdMainWindow::slotItemSelectionChanged ()
{
  QList <QTreeWidgetItem *> items = ui.elementTree->selectedItems ();
  static unsigned lastCount;
  QTreeWidgetItem *item = firstSelectedItem ();

  fixupMultiToolbar ();

  /* Prevent fetching content for other selected items. */
  if (items.count () > 1)
    {
      lastCount = items.count ();

      searchResultStatusBarLabel->show ();
      searchResultStatusBarLabel->setText (QString (tr ("%1 selected")).arg (items.count ()));
      return;
    }

    searchResultStatusBarLabel->hide ();

  /* The nextElement is set when there are commands queued from
   * slotElementSelected() and is the item to be selected when the queue
   * becomes empty. The lastCount is the number of selected items during the
   * previous call to this function. Here, it means that the selection has
   * changed back to only one item and the command queue is to be run. It
   * handles the case that the content of an element has been edited but yet to
   * be committed while selecting other elements. */
  if (nextElement && lastCount > 1)
    {
      lastCount = items.count ();
      nextElement = nullptr;
    }
  /* A different element selected with content of the original element yet to
   * be commited . */
  else if (nextElement)
    {
      lastCount = items.count ();
      elementSelected (oldSelectedItem, false, oldSelectedItem);
    }

  lastCount = items.count ();

  /* Handle the case where an item is selected, then edited, then other items
   * are selected and the edited item deselected. Let the edit be queued for
   * commit. */
  if (!items.contains (oldSelectedItem))
    {
      oldSelectedItem = nullptr;
      pwm->runQueue ();
    }

  if (!hasQueuedRefreshTree ())
    elementSelected (item, false, oldSelectedItem);

  pwm->runQueue ();
}

void
PwmdMainWindow::slotElementSelected (QTreeWidgetItem * item,
                                     QTreeWidgetItem * old)
{
  /* If there is content yet to be commited, only test for it once rather than
   * messing up a multi-element selection. */
  if (!pwm->isQueued (PwmdCmdIdStoreContent)
      && !pwm->isQueued (PwmdCmdIdAttrContent)
      &&  doSaveContent (true))
    {
      selectedElement = previousElement = old;
      nextElement = item;
    }

  if (!hasMultiSelection ())
    oldSelectedItem = old;
}

void
PwmdMainWindow::setAttributeContent (const QString & str)
{
  disconnect (ui.attributeContent, SIGNAL (modificationChanged (bool)), this,
	      SLOT (slotContentModified (bool)));
  disconnect (ui.attributeContent, SIGNAL (textChanged ()), this, SLOT (slotContentChanged ()));
  ui.attributeContent->document ()->setPlainText (str);
  connect (ui.attributeContent, SIGNAL (textChanged ()), SLOT (slotContentChanged ()));
  connect (ui.attributeContent, SIGNAL (modificationChanged (bool)), this,
	   SLOT (slotContentModified (bool)));
  ui.attributeContent->document ()->setModified (false);

  if (attributeElement)
    {
      PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(attributeElement->data (0, Qt::UserRole));
      ui.attributeContent->setReadOnly (data->badPermissions ());
    }
}

void
PwmdMainWindow::setContent (const QString & str, QTreeWidgetItem *item)
{
  PwmdTreeWidgetItemData *data = nullptr;

  item = item ? item : selectedElement;

  if (item)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  else
    ui.hiddenContentFrame->setHidden (true);

  if (data)
    {
      bool base64 = data->attribute ("encoding") == "base64";

      ui.actionBase64->setChecked (base64);

      if (item == selectedElement)
        {
          ui.actionHidden->setChecked (data->hasAttribute ("hidden"));
          if (!data->hasAttribute ("password"))
            ui.hiddenContentFrame->setHidden (!ui.actionHidden->isChecked ());

          if (ui.actionHidden->isChecked ())
            ui.hiddenContentLabel->setText (QString ("<html><head/><body><p><span style=\" font-style:italic;\">%1 %2.</span></p></body></html>").arg (tr ("The content for this element is hidden. Uncheck the Hidden item in the Content menu to make it visible. The length of the hidden content is "), QString::number (str.length ())));
        }
    }

  disconnect (ui.passwordWidget, SIGNAL (modified ()), this,
              SLOT (slotPasswordContentChanged ()));
  disconnect (ui.textEdit, SIGNAL (modificationChanged (bool)), this,
              SLOT (slotContentModified (bool)));
  disconnect (ui.textEdit, SIGNAL (textChanged ()), this,
              SLOT (slotContentChanged ()));
  if (item == selectedElement)
    {
      if (data && data->hasAttribute ("password"))
        {
          ui.passwordWidget->setPassword (str, !isElementOwner (item));
          ui.passwordWidget->setType (data->attribute ("password"));
          ui.hiddenContentFrame->setHidden (true);
          fixupToolBar (passwordToolBar, PasswordToolBarOk, false);
          ui.textEdit->document ()->setPlainText ("");
        }
      else
        {
          ui.passwordWidget->setPassword ("");
          ui.textEdit->document ()->setPlainText (str);
          ui.textEdit->document ()->setModified (false);

          if (data)
            ui.contentTab->setEnabled (!data->hasError ());
        }
    }

  connect (ui.passwordWidget, SIGNAL (modified ()), this,
           SLOT (slotPasswordContentChanged ()));
  connect (ui.textEdit, SIGNAL (textChanged ()),  this,
           SLOT (slotContentChanged ()));
  connect (ui.textEdit, SIGNAL (modificationChanged (bool)), this,
           SLOT (slotContentModified (bool)));
}

static bool
hasFlag (const QString &str, const QString &flags)
{
  if (flags.isEmpty () || str.isEmpty ())
    return false;

  for (int i = 0; i < flags.length (); i++)
    {
      if (str.contains (flags.at (i)))
	return true;
    }

  return false;
}

bool
PwmdMainWindow::hasBadTarget (QTreeWidgetItem * item, const QString & path,
                              const QString & flags)
{
  bool b = false;
  bool notAllowed = hasFlag (flags, "P");
  PwmdTreeWidgetItemData *data =
    qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  if (notAllowed)
    {
      Qt::ItemFlags flags = item->flags ();

      flags &= ~Qt::ItemIsEditable;
      item->setFlags (flags);
      data->setBadPermissions ();
    }

  if (!data->hasTarget ())
    {
      setElementColors (item);
      return false;
    }

  if (hasFlag (flags, "EO"))
    {
      b = true;
      data->setBadTarget (b);
    }

  data->setTarget (path);
  bool loop = hasFlag (flags, "O");
  data->setTargetLoop (loop);
  setElementColors (item);

  if (data->badPermissions())
    return false;

  return b;
}

bool
PwmdMainWindow::hasTarget (const QTreeWidgetItem * item, bool parent)
{
  if (!item)
    return false;

  PwmdTreeWidgetItemData *data =
    qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  if (!parent || data->hasTarget ())
    return data->hasTarget ();

  item = item->parent ();
  while (item)
    {
      data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
      if (data->hasTarget ())
        return true;
      item = item->parent ();
    }

  return false;
}

bool
PwmdMainWindow::targetLoop (const QTreeWidgetItem * item)
{
  PwmdTreeWidgetItemData *data =
    qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  return data->targetLoop ();
}

bool
PwmdMainWindow::badTarget (const QTreeWidgetItem * item)
{
  PwmdTreeWidgetItemData *data =
    qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  return data ? data->badTarget () : false;
}

QString
PwmdMainWindow::elementName (const QTreeWidgetItem * item)
{
  if (!item)
    return QString ();

  return item->text (0);
}

void
PwmdMainWindow::setItemText (QTreeWidgetItem * item, const QString & str,
                             bool target)
{
  item->setText (0, str);
  PwmdTreeWidgetItemData *data = new PwmdTreeWidgetItemData (target, str);
  item->setData (0, Qt::UserRole, QVariant::fromValue (data));

  if (target)
    {
      QBrush br = item->foreground (0);

      br.setColor (targetColor);
      item->setForeground (0, br);
    }

  item->setFlags (item->flags () | Qt::ItemIsEditable);
}

class ElementPath
{
 public:
  ElementPath (QString path, QString flags = 0)
    {
      _path = path;
      _flags = flags;
    };
  ~ElementPath () { };
  QString path () { return _path; };
  QString flags () { return _flags; };

  class ElementAttr
    {
     public:
      ElementAttr (QString name, QString value = 0)
        {
          _name = name;
          _value = value;
        };
      ~ElementAttr () { };
      QString name () { return _name; };
      QString value () { return _value; };

     private:
      QString _name;
      QString _value;
    };

  QList <ElementAttr *> attrs () { return _attrs; };
  void setAttrs (QList <ElementAttr *>list) { _attrs = list; };
 private:
  QString _path;
  QString _flags;
  QList <ElementAttr *> _attrs;
};

#define EXTRACT_TOKEN(ba,offset,ns,n,var)		\
  offset = 0;						\
  ns.clear ();						\
  foreach (QChar c, ba)					\
    {							\
      if (c.isDigit ())					\
        ns += QString::number (c.digitValue ());	\
      else						\
        break;						\
      offset++;						\
    }							\
    n = ns.toUInt (nullptr);				\
    var = ba.mid (++offset, n);				\
    ba = ba.mid (offset+n);

QList <QTreeWidgetItem *>PwmdMainWindow::buildElementTree (QByteArray &sexp,
                                                           bool withAttrs)
{
  QList <QTreeWidgetItem *>elementTree;
  QList <ElementPath *>elementPaths;
  int n;

  n = sexp.indexOf ("(4:path");
  if (sexp.isEmpty () || !sexp.startsWith ("(11:list-result(") || n != 15)
    return elementTree;

  sexp = sexp.mid (n);
  while (sexp.startsWith ("(4:path"))
    {
      QString ns, path, flags;
      unsigned offset = 0;

      sexp = sexp.mid (7); // "(4:path"
      EXTRACT_TOKEN (sexp, offset, ns, n, path);
      sexp = sexp.mid (7); // 7 = "5:flags"
      EXTRACT_TOKEN (sexp, offset, ns, n, flags);
      sexp = sexp.mid (8); // 8 = "(5:attrs"
      ElementPath *p = new ElementPath (path, flags);
      QList <ElementPath::ElementAttr *> attrs;

      do {
          QString name, value;
          EXTRACT_TOKEN (sexp, offset, ns, n, name);
          EXTRACT_TOKEN (sexp, offset, ns, n, value);
          attrs.append (new ElementPath::ElementAttr (name, value));
      } while (!sexp.isEmpty () && sexp.at (0) != ')');

      p->setAttrs (attrs);
      elementPaths.append (p);
      sexp = sexp.mid (2); // 2 = "))"
    }

  ui.elementTree->setSortingEnabled (false);

  foreach (ElementPath *path, elementPaths)
    {
      QStringList elements = path->path ().split ("\t");
      QTreeWidgetItem *parent = nullptr;
      bool nomatch = true;
      QTreeWidgetItem *item;
      int element = 1;
      bool isBad = hasFlag (path->flags (), "EO");
      QString target;

      if (hasFlag (path->flags (), "T"))
        {
          foreach (ElementPath::ElementAttr *a, path->attrs ())
            {
              if (a->name () == "_target")
                {
                  target = a->value ();
                  break;
                }
            }
        }

      for (int n = 0; n < elementTree.count (); n++)
	{
	  QTreeWidgetItem *root = elementTree.at (n);

	  if (elementName (root) == elements.at (0))
	    {
	      parent = root;
	      break;
	    }
	}

      if (!parent)
	{
	  parent = new QTreeWidgetItem ((QTreeWidget *) 0);
	  setItemText (parent, elements.at (0), !target.isEmpty ());
	  elementTree.append (parent);
	  hasBadTarget (parent, target, path->flags ());
	  if (isBad)
            goto attrs;
	}

      while ((item = parent->child (0)))
	{
	  if (elements.at (element) == elementName (item))
	    {
	      element++;
	      parent = item;
	      continue;
	    }

	  for (int i = 0; i < parent->childCount (); i++)
	    {
	      QTreeWidgetItem *
		tmp = parent->child (i);

	      if (elementName (parent->child (i)) == elements.at (element))
		{
		  parent = tmp;
		  element++;
		  nomatch = false;
		  break;
		}
	    }

	  if (element == elements.count () || nomatch)
	    break;

	  nomatch = true;
	}

      if (nomatch)
	{
	  for (; element < elements.count (); element++)
	    {
	      parent = new QTreeWidgetItem (parent);
	      setItemText (parent, elements.at (element), !target.isEmpty ());
	      hasBadTarget (parent, target, path->flags ());
	    }
	}
      else if (parent)
	{
          qWarning("%s(%i): %s", __FILE__, __LINE__, __FUNCTION__);
	  /* A path has been parsed whose current element is the parent of
	   * previously parsed element. Update the target and flags for the
	   * previously created item, if any. */
	  hasBadTarget (parent, target, path->flags ());
	}

attrs:
      foreach (ElementPath::ElementAttr *a, path->attrs ())
        {
          if (withAttrs)
            setAttribute (parent, a->name (), a->value (), true);
          delete a;
        }

      delete path;
    }

  return elementTree;
}

QString
PwmdMainWindow::elementPath (const QTreeWidgetItem *item, const QString &sep)
{
  QString path = QString ();

  if (!item)
    return path;

  do
    {
      path.prepend (QString ("%1%2").arg (sep, elementName (item)));
      item = item->parent ();
    }
  while (item);

  path.remove (0, sep.length ());
  return path;
}

QString
PwmdMainWindow::elementPath (const QString &sep)
{
  return elementPath (selectedElement, sep);
}

void
PwmdMainWindow::slotNewRootElement ()
{
  createNewElement (true);
}

void
PwmdMainWindow::slotNewSiblingElement ()
{
  createNewElement (!selectedElement ? true : false, true);
}

void
PwmdMainWindow::slotNewElement ()
{
  bool b = false;

  if (selectedElement)
    {
      PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(selectedElement->data (0, Qt::UserRole));
      if (data->badTarget () || data->badPermissions ())
        b = true;
    }

  createNewElement (!selectedElement || b);
}

void
PwmdMainWindow::slotNewSiblingElementFromPopup ()
{
  QPoint pos = ui.elementTree->mousePosition ();
  QTreeWidgetItem *item = ui.elementTree->itemAt (pos.x (), pos.y ());

  createNewElement (!item ? true : false, true);
}

void
PwmdMainWindow::slotNewElementFromPopup ()
{
  QPoint pos = ui.elementTree->mousePosition ();
  QTreeWidgetItem *item = ui.elementTree->itemAt (pos.x (), pos.y ());

  createNewElement (item == nullptr);
}

void
PwmdMainWindow::createNewElement (bool root, bool sibling)
{
  QTreeWidgetItem *item = selectedElement;
  newElement = nullptr;

  doSaveContent (true);
  if (!root && !selectedElement)
    {
      pwm->runQueue ();
      return;
    }

  if (!root)
    {
      PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(selectedElement->data (0, Qt::UserRole));
      if (data->badPermissions ())
        {
          pwm->runQueue ();
          return;
        }

      if (sibling && item)
        item = item->parent ();

      if (!item)
        root = true;
    }

  newElement = new QTreeWidgetItem ((QTreeWidget *) 0);
  setItemText (newElement, tr ("New element"));

  if (root)
    {
      ui.elementTree->setHorizontalScrollMode (QAbstractItemView::ScrollPerItem);
      ui.elementTree->addTopLevelItem (newElement);
    }
  else if (dropElement)
    {
      dropElement->addChild (newElement);
      dropElement->setExpanded (true);
    }
  else
    {
      item->addChild (newElement);
      item->setExpanded (true);
    }

  setElementItemsEnabled (newElement, false);
  previousElement = item;
  attributeElement = selectedElement = newElement;
  ui.elementTree->scrollToItem (newElement,
				QAbstractItemView::PositionAtCenter);
  ui.elementTree->editItem (newElement);
  ui.elementTree->setHorizontalScrollMode (QAbstractItemView::ScrollPerPixel);
}

void
PwmdMainWindow::setModifiedContent (bool b)
{
  setWindowModified (true);
  setFileModified (b);
}

void
PwmdMainWindow::deleteElementFinalize (PwmdCommandQueueItem *cmd)
{
  if (!cmd->isFinalItem ())
    return;

  if (ui.tabWidget->currentWidget () != ui.commandTab)
    ui.tabWidget->setCurrentWidget (ui.contentTab);

  if (hasMultiSelection ())
    selectedElementPath = QString ();

  setModified ();
  clearAttributeTab ();
  refreshElementTree ();
}

void
PwmdMainWindow::slotDeleteElement ()
{
  if (!selectedElement)
    return;

  saveExpandedItemList ();
  selectedElementPath = elementPath (ui.elementTree->itemBelow (selectedElement));
  if (selectedElementPath.isEmpty ())
    selectedElementPath = elementPath (ui.elementTree->itemAbove (selectedElement));

  if (selectedElementPath.isEmpty ())
    selectedElement = nullptr;

  QList <QTreeWidgetItem *> itemsToRemove;
  QList <QTreeWidgetItem *> itemsToRemoveWithTarget;
  QList <QTreeWidgetItem *> items = ui.elementTree->selectedItems ();
  foreach (QTreeWidgetItem *item, items)
    {
      if (hasTarget (item))
        {
          if (!itemsToRemoveWithTarget.contains (item) && isElementOwner (item))
            itemsToRemoveWithTarget.append (item);
          continue;
        }

      QTreeWidgetItem *r = resolveElementPath (item);
      if (!itemsToRemove.contains (r) && isElementOwner (r))
        itemsToRemove.append (r);
    }

  items = itemsToRemove;
again:
  foreach (QTreeWidgetItem *item, items)
    {
      PwmdCommandQueueItem *cmd;
      PwmdInquireData *inq = new PwmdInquireData (elementPath (item));
      cmd = new PwmdCommandQueueItem (PwmdCmdIdDeleteElement, "DELETE",
                                      Pwmd::inquireCallback, inq);
      ignoreErrors (cmd);
      pwm->command (cmd, true);
    }

  if (items != itemsToRemoveWithTarget)
    {
      items = itemsToRemoveWithTarget;
      goto again;
    }

  pwm->runQueue ();
}

void
PwmdMainWindow::slotRenameElement ()
{
  doSaveContent (true);

  if (!selectedElement)
    {
      pwm->runQueue ();
      return;
    }

  setElementItemsEnabled (nullptr, false);
  selectedElementPath = elementPath (selectedElement);
  ui.elementTree->editItem (selectedElement);
}

bool
PwmdMainWindow::editElements ()
{
  return refreshElementTree ();
}

void
PwmdMainWindow::slotEditElements ()
{
  editElements ();
}

QTreeWidgetItem *
PwmdMainWindow::resolveElementPath (QTreeWidgetItem * item)
{
  QString path = QString ();
  QTreeWidgetItem *i;

  for (i = item; i; i = i->parent ())
    {
      PwmdTreeWidgetItemData *data =
        qvariant_cast < PwmdTreeWidgetItemData * >(i->data (0, Qt::UserRole));

      if (data->badTarget ())
        return nullptr;

      if (hasTarget (i))
	{
	  path.prepend (data->target ());
	  return resolveElementPath (findElement (path));
	}

      path.prepend (elementName (i));
      path.prepend ("\t");
    }

  if (path.isEmpty ())
    return nullptr;

  if (path.at (0) == '\t')
    path.remove (0, 1);

  return findElement (path);
}

QTreeWidgetItem *
PwmdMainWindow::resolveTargets (QTreeWidgetItem * item)
{
  return resolveElementPath (item);
}

void
PwmdMainWindow::slotResolveTargets ()
{
  QTreeWidgetItem *i = resolveTargets (selectedElement);

  if (i)
    {
      ui.elementTree->setHorizontalScrollMode (QAbstractItemView::ScrollPerItem);
      ui.elementTree->setCurrentItem (i);
      selectedElement = i;
      ui.elementTree->scrollToItem (selectedElement,
				    QAbstractItemView::PositionAtCenter);
      ui.elementTree->setHorizontalScrollMode (QAbstractItemView::ScrollPerPixel);
    }
}

QTreeWidgetItem *
PwmdMainWindow::findElement (const QString &path, QTreeWidgetItem *skip,
                             bool any, PwmdTreeWidget *tree)
{
  if (path.isEmpty ())
    return nullptr;

  QStringList elements = path.split ("\t");
  int element = 0;
  if (!tree)
    tree = ui.elementTree;
  QTreeWidgetItem *item = tree->invisibleRootItem ();
  QTreeWidgetItem *match = nullptr;

  while (element < elements.count ())
    {
      match = nullptr;

      for (int n = 0, t = item->childCount (); n < t; n++)
	{
	  QTreeWidgetItem *child = item->child (n);

	  if (skip && child == skip)
	    continue;

	  if ((any && child->text (0) == elements.at (element)) ||
	      elementName (child) == elements.at (element))
	    {
	      match = item = child;
	      break;
	    }
	}

      element++;
    }

  return match;
}

gpg_error_t
PwmdMainWindow::doDndCopyElement (const QString & src, const QString & dst,
                                  bool queue)
{
  PwmdInquireData *inq = new PwmdInquireData (QString ("%1 %2").arg (src, dst));
  inq->setUser (dst);
  PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdDndCopyElement, "COPY", Pwmd::inquireCallback, inq);
  ignoreErrors (cmd, true);
  pwm->command (cmd, queue);
  return 0;
}

void
PwmdMainWindow::dndCopyMoveElementFinalize (const QString &path)
{
  saveExpandedItemList ();
  selectedElementPath = path;
  QEvent *ev = new QEvent (QEvent::Type (PwmdEventRefreshElementTree));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdMainWindow::slotDndCopyElement ()
{
  dndOperation = DndCopy;
  if (doSaveContent (true))
    {
      nextElement = selectedElement;
      pwm->runQueue ();
      return;
    }

  bool b = false;

  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      QString srcPath = elementPath (item);
      QString dst = elementPath (dropElement);
      int n = srcPath.lastIndexOf ('\t') + 1;

      if (dst.isEmpty () || srcPath != dst
          || (srcPath == dst && !findElement (dst + '\t' + dst)))
        {
          QString e = srcPath.mid (n);

          if (dst.isEmpty ())
            dst = e;
          else
            dst.append (QString ("\t%1").arg (e));

          if (!findElement (dst))
            {
              b = true;
              doDndCopyElement (srcPath, dst, true);
            }
        }
    }

  copyElement = selectedElement;
  if (b)
    {
      pwm->runQueue ();
      return;
    }

  createNewElement (dropElement == nullptr);
}

void
PwmdMainWindow::slotDndMoveElement ()
{
  dndOperation = DndMove;
  if (doSaveContent (true))
    {
      nextElement = selectedElement;
      pwm->runQueue ();
      return;
    }

  bool match = false;

  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      if (dropElement == item || isParentOf (item, dropElement))
        continue;

      match = true;
      QString args = QString ("%1 ").arg (elementPath (item));
      QString s;
      if (dropElement)
        {
          s = elementPath (dropElement);
          args.append (QString ("%1").arg (s));
        }

      PwmdInquireData *inq = new PwmdInquireData (args);
      if (!dropElement)
        inq->setUser (item->text (0));
      else
        inq->setUser (s + "\t" + item->text (0));

      PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdDndMoveElement, "MOVE", Pwmd::inquireCallback, inq);
      ignoreErrors (cmd, true);
      pwm->command (cmd, true);
    }

  if (!match)
    {
      dndOperation = DndNone;
      return;
    }

  pwm->runQueue ();
}

void
PwmdMainWindow::dndCreateTargetFinalize ()
{
  selectedElementPath = elementPath (selectedElement);
  QEvent *ev = new QEvent (QEvent::Type (PwmdEventDnDTargetAttr));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdMainWindow::slotDndCreateTarget ()
{
  dndOperation = DndTarget;
  if (doSaveContent (true))
    {
      nextElement = selectedElement;
      pwm->runQueue ();
      return;
    }

  bool b = false;

  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      if (!dropElement || dropElement == item)
        continue;

      b = true;
      QString dropPath = elementPath (dropElement);
      PwmdInquireData *inq = new PwmdInquireData (QString ("SET _target %1 %2").arg (elementPath (item), dropPath));
      inq->setUser (dropPath);
      inq->setUserPtr (item);
      PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdDndCreateTarget, "ATTR", Pwmd::inquireCallback, inq);
      ignoreErrors (cmd, true);
      pwm->command (cmd, true);
    }

  if (!b)
    {
      dndOperation = DndNone;
      return;
    }

  pwm->runQueue ();
}

bool
PwmdMainWindow::isParentOf (QTreeWidgetItem * a, QTreeWidgetItem * b)
{
  if (!a || !b)
    return false;

  while (b)
    {
      b = b->parent ();

      if (b == a)
	return true;
    }

  return false;
}

static void
recurseExpand (QTreeWidgetItem * item, bool b)
{
  item->setExpanded (b);

  for (int i = 0; i < item->childCount (); i++)
    {
      QTreeWidgetItem *tmp = item->child (i);

      recurseExpand (tmp, b);
    }
}

void
PwmdMainWindow::slotExpandElement ()
{
  setBusy ();
  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    recurseExpand (item, true);
  setBusy (false);
}

void
PwmdMainWindow::slotCollapseElement ()
{
  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    recurseExpand (item, false);
}

void
PwmdMainWindow::slotMenuItemHovered (QAction * a)
{
  QStatusBar *sb = statusBar ();
  sb->showMessage (a->statusTip ());
}

void
PwmdMainWindow::popupContextMenu ()
{
  QTreeWidgetItem *item = selectedElement;

  selectedElement = ui.elementTree->currentItem ();
  selectedAttribute = ui.attributeList->currentItem ();

  PwmdTreeWidgetItemData *data = nullptr;
  if (item)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(selectedElement->data (0, Qt::UserRole));

  QMenu *m = new QMenu (this);
  connect (m, SIGNAL (hovered (QAction *)), this,
	   SLOT (slotMenuItemHovered (QAction *)));
  QAction *a = m->addAction (tr ("New &root element"), this,
                             SLOT (slotNewRootElement ()));
  a->setEnabled (fixupMenu (ui.menuElement, ElementMenuNewRoot, false, false, true));
  a = m->addAction (tr ("&New child element"), this,
                    SLOT (slotNewElementFromPopup ()));
  a->setEnabled (fixupMenu (ui.menuElement, ElementMenuNew, false, false, true));
  a = m->addAction (tr ("New &sibling element"), this,
                             SLOT (slotNewSiblingElementFromPopup ()));
  a->setEnabled (fixupMenu (ui.menuElement, ElementMenuNewSibling, false, false, true));
  a = m->addAction (tr ("Rena&me"), this, SLOT (slotRenameElement ()));
  a->setEnabled (fixupMenu (ui.menuElement, ElementMenuRename, false, false, true));
  a = m->addAction (tr ("&Delete"), this, SLOT (slotDeleteElement ()));
  a->setEnabled (fixupMenu (ui.menuElement, ElementMenuDelete, false, false, true));

  m->addSeparator ();
  if (ui.tabWidget->currentWidget () != ui.contentTab)
    a = m->addAction (tr ("&Edit content"), this,
                      SLOT (slotEditContent ()));
  else
    a = m->addAction (tr ("&Edit attributes"), this,
                      SLOT (slotEditAttributes ()));
  a->setEnabled (fixupMenu (ui.menuElement, ElementMenuEditContent, false, false, true));
  a = m->addAction (tr ("Resolve &targets"), this,
                    SLOT (slotResolveTargets ()));
  a->setEnabled (fixupMenu (ui.menuElement, ElementMenuResolveTargets, false, false, true));

  m->addSeparator ();
  a = m->addAction (tr ("E&xpand"), this, SLOT (slotExpandElement ()));
  a->setEnabled (fixupMenu (ui.menuView, ViewMenuExpandAll, false, false, true));
  a->setEnabled (item && !data->badPermissions () && !data->badTarget ());
  a = m->addAction (tr ("Colla&pse"), this, SLOT (slotCollapseElement ()));
  a->setEnabled (item && !data->badPermissions () && !data->badTarget ());
  a = m->addAction (tr ("Cop&y element path"), this,
                    SLOT (slotClipboardElementPath ()));
  a->setEnabled (item);

  m->exec (QCursor::pos ());
  delete m;
}

bool
PwmdMainWindow::isInvoker ()
{
  return !connectedUser.isEmpty() && !invokingUser.isEmpty ()
    && invokingUser.indexOf (connectedUser) != -1;
}

bool
PwmdMainWindow::isElementOwner (QTreeWidgetItem *item, bool secondary)
{
  if (!item)
    return false;

  if (isInvoker ())
    return true;

  PwmdTreeWidgetItemData *data = item ? qvariant_cast <PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole)) : nullptr;
  if (data)
    {
      QString acl = data->attribute("_acl");
      QStringList list = acl.split(",");

      if (!list.isEmpty ()
          && (list.at(0) == connectedUser || list.at(0).isEmpty ()))
        return true;

      if (secondary)
        {
          foreach (QString user, list)
            {
              if (user == connectedUser)
                return true;
            }
        }
    }

  return false;
}

void
PwmdMainWindow::setTimeLabels (QTreeWidgetItem * item, bool needsRefresh)
{
  if (!item)
    {
      ui.l_ctime->setText (tr ("-"));
      ui.l_mtime->setText (tr ("-"));
      return;
    }

  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  if (needsRefresh) {
      if (item == attributeElement)
        {
          ui.l_ctime->setText (tr("click refresh to update"));
          ui.l_mtime->setText (tr("click refresh to update"));
        }

      data->setNeedsAttrRefresh ();
      return;
  }

  QString s = data->attribute ("_ctime");
  unsigned t = s.toUInt ();
  QDateTime date;

  date.setSecsSinceEpoch (t);
  ui.l_ctime->setText (!s.isEmpty () ? date.toString () : tr ("-"));
  s = data->attribute ("_mtime");
  t = s.toUInt ();
  date.setSecsSinceEpoch (t);
  ui.l_mtime->setText (!s.isEmpty () ? date.toString () : tr ("-"));
}

void
PwmdMainWindow::slotEditTargetAttributes (bool b)
{
  if (doSaveContent (true))
    {
      editTargetAttributes = b;
      disconnect (ui.actionToggleTargetAttribute, SIGNAL (triggered (bool)), this, SLOT (slotEditTargetAttributes(bool)));
      ui.actionToggleTargetAttribute->setChecked (!b);
      connect (ui.actionToggleTargetAttribute, SIGNAL (triggered (bool)), this, SLOT (slotEditTargetAttributes(bool)));
      pwm->runQueue ();
      return;
    }

  editAttributes (b, selectedElement);
}

void
PwmdMainWindow::fixupEditContentOrAttributes (bool attrs)
{
  if (attrs)
    {
      disconnect (ui.actionEditContentOrAttributes, SIGNAL (triggered()), this, SLOT (slotEditContent ()));
      ui.actionEditContentOrAttributes->setText (tr ("&Edit attributes"));
      connect (ui.actionEditContentOrAttributes, SIGNAL (triggered()), this, SLOT (slotEditAttributes ()));
      return;
    }

  disconnect (ui.actionEditContentOrAttributes, SIGNAL (triggered()), this, SLOT (slotEditAttributes ()));
  ui.actionEditContentOrAttributes->setText (tr ("&Edit content"));
  connect (ui.actionEditContentOrAttributes, SIGNAL (triggered()), this, SLOT (slotEditContent ()));
}

void
PwmdMainWindow::slotEditAttributes ()
{
  fixupEditContentOrAttributes ();
  targetAttributeElement = nullptr;
  ui.actionToggleTargetAttribute->setChecked (false);
  editAttributes (false, selectedElement);
}

void
PwmdMainWindow::setElementColors (QTreeWidgetItem *item, bool reset,
                                  bool target)
{
  PwmdTreeWidgetItemData *data = nullptr;

  if (targetAttributeElement && target)
    {
      if (!reset)
        {
          QBrush b = targetAttributeElement->foreground (0);
          QBrush bg(QPalette::Window);

          b.setColor (hilightColor);
          targetAttributeElement->setForeground (0, b);
          bg.setColor(hilightColorBg);
          targetAttributeElement->setBackground(0, bg);
          return;
        }
    }

again:
  if (item)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  else
    return;

  if (!data)
    {
      if (targetAttributeElement)
        {
          item = targetAttributeElement;
          goto again;
        }
      return;
    }

  if (data->badPermissions ())
    {
      QBrush br = item->foreground (0);
      QBrush bg(QPalette::Window);

      br.setColor (permissionsColor);
      item->setForeground (0, br);
      bg.setColor(permissionsColorBg);
      item->setBackground(0, bg);
    }
  else if (data->targetLoop ())
    {
      QBrush br = item->foreground (0);
      QBrush bg(QPalette::Window);

      bg.setColor(targetLoopColorBg);
      item->setBackground(0, bg);

      br.setColor (targetLoopColor);
      item->setForeground (0, br);
    }
  else if (data->badTarget ())
    {
      QBrush br = item->foreground (0);
      QBrush bg(QPalette::Window);

      bg.setColor(invalidTargetColorBg);
      item->setBackground(0, bg);

      br.setColor (invalidTargetColor);
      item->setForeground (0, br);
    }
  else if (data->hasTarget ())
    {
      QBrush br = item->foreground (0);
      QBrush bg(QPalette::Window);

      bg.setColor(targetColorBg);
      item->setBackground(0, bg);

      br.setColor (targetColor);
      item->setForeground (0, br);
    }
  else
    {
      QBrush b (QPalette::Text);
      b.setColor ("Black");
      item->setForeground (0, b);
      b.setColor ("White");
      item->setBackground (0, b);
    }

  if (item != targetAttributeElement)
    {
      item = targetAttributeElement;
      targetAttributeElement = nullptr;
      ui.actionToggleTargetAttribute->setChecked (false);
      editTargetAttributes = false;
      goto again;
    }
}

void
PwmdMainWindow::editAttributes (bool resolve, QTreeWidgetItem *item)
{
  if (doSaveContent (true))
    {
      editTargetAttributes = resolve;
      pwm->runQueue ();
      return;
    }

  if (!item)
    {
      fixupMenu (ui.menuAttributes, AttributeMenuTarget,
                 hasTarget (item) && !badTarget (item),
                 hasTarget (item) && !badTarget (item)
                 && ui.actionToggleTargetAttribute->isChecked ());
      pwm->runQueue ();
      return;
    }

  editTargetAttributes = resolve;
  setElementItemsEnabled (item, item);
  QTreeWidgetItem *titem = !resolve ? item : resolveTargets (item);
  targetAttributeElement = resolve ? titem : targetAttributeElement;
  attributeElement = titem ? titem : item;

  PwmdTreeWidgetItemData *data = attributeElement ? qvariant_cast < PwmdTreeWidgetItemData * >(attributeElement->data (0, Qt::UserRole)) : nullptr;
  if (!resolve && data)
    {
      if (data->badPermissions () && data->hasTarget ())
        attributeElement = findElement (elementPath (attributeElement));
    }

  refreshAttributeList (attributeElement);
  setAttributeItemsEnabled ();
  ui.attributeList->setCurrentRow (previousAttributeIndex ());
  setElementColors (targetAttributeElement, !resolve, true);
  ui.elementTree->scrollToItem (attributeElement);
}

void
PwmdMainWindow::slotAttrSearchAndReplaceChanged (const QString &)
{
  ui.pb_searchAndReplace->setEnabled (ui.cb_searchAndReplace->lineEdit ()->hasAcceptableInput ());
}

void
PwmdMainWindow::slotAttrSearchAndReplace ()
{
  QString str = ui.cb_searchAndReplace->lineEdit ()->text ();
  bool escaped = false;
  bool doingReplace = false;
  QString pattern, replace;

  foreach (QChar c, str.mid (1))
    {
      if (doingReplace || (c == '/' && !escaped))
        {
          if (!doingReplace)
            doingReplace = true;
          else
            replace += c;
          continue;
        }

      if (escaped)
        {
          pattern += c;
          escaped = false;
          continue;
        }

      escaped = c == '\\';
      if (!escaped)
        pattern += c;
    }

  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      searchAndReplace = true;
      PwmdTreeWidgetItemData *data = qvariant_cast <PwmdTreeWidgetItemData *>(item->data (0, Qt::UserRole));
      QString value = data->attribute (selectedAttribute->text ());
      QRegularExpression rx (pattern);
      QRegularExpressionMatch match = rx.match (value);
      if (match.hasMatch ()) {
          value.replace (rx, replace);
          while (value.endsWith (','))
            value.chop (1);

          setAttributeContentOnce (item, elementPath (item), value, false,
                                   false, true);
          fixupToolBar (attributeToolBar, AttributeToolBarOk, true);
      }
    }

  ui.cb_searchAndReplace->lineEdit ()->setText ("");
  pwm->runQueue ();
}

void
PwmdMainWindow::slotRefreshAttributes ()
{
  refreshAttributeList (attributeElement, true);
}

void
PwmdMainWindow::clearAttributeTab ()
{
  setTimeLabels (nullptr);
  disconnect (ui.attributeContent, SIGNAL (modificationChanged (bool)), this,
	      SLOT (slotContentModified (bool)));
  disconnect (ui.attributeContent, SIGNAL (textChanged ()), this, SLOT (slotContentChanged ()));
  ui.attributeContent->document()->setPlainText ("");
  ui.attributeList->clear ();
  connect (ui.attributeContent, SIGNAL (textChanged ()), SLOT (slotContentChanged ()));
  connect (ui.attributeContent, SIGNAL (modificationChanged (bool)), this,
	   SLOT (slotContentModified (bool)));
}

void
PwmdMainWindow::refreshAttributeListFinalize (QTreeWidgetItem *element,
                                              const QString &result,
                                              bool create, bool fromTree)
{
  if (create && !createAttributeCache (element, result))
    return;

  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(element->data (0, Qt::UserRole));
  PwmdAttributeList *attrs = data->attributes ();
  QList<QListWidgetItem *> items;

  for (int i = 0, t = attrs->count (); i < t; i++)
    {
      PwmdElementAttr *attr = attrs->at (i);
      QListWidgetItem *item = new QListWidgetItem (attr->name ());

      if (PwmdElementAttr::isProtectedAttribute (attr->name ()))
        item->setFlags (~(item->flags () & Qt::ItemIsEnabled));

      items.append (item);
    }

  if (!editTargetAttributes && !fromTree)
    selectedElement = attributeElement = element;

  if (ui.tabWidget->currentWidget () == ui.attributeTab
      && (element == attributeElement || element == targetAttributeElement
          || element == ui.elementTree->currentItem ()))
    {
      setAttributeContent ("");
      selectedAttribute = nullptr;
      clearAttributeTab ();
      data = qvariant_cast <PwmdTreeWidgetItemData *>(element->data (0, Qt::UserRole));

      foreach (QListWidgetItem *item, items)
        {
          if (!PwmdElementAttr::isReservedAttribute (item->text ())
              && data->hasTarget ())
            {
              QBrush b = item->foreground ();
              QBrush bg(QPalette::Window);

              b.setColor (hilightColor);
              item->setForeground (b);
              bg.setColor(hilightColorBg);
              item->setBackground(bg);
            }

          ui.attributeList->addItem (item);
        }

      setTimeLabels (element, data->needsAttrRefresh ());
    }
  else if (ui.tabWidget->currentWidget () == ui.contentTab
           && element == selectedElement)
    {
      foreach (QListWidgetItem *item, items)
        delete item;
    }
  else
    {
      foreach (QListWidgetItem *item, items)
        delete item;
    }

  if (isElementOwner (element))
    element->setFlags (element->flags () | Qt::ItemIsEditable);

  fixupToolBar (attributeToolBar, AttributeToolBarOk, false);
  ui.attributeList->setCurrentRow (previousAttributeIndex ());

  /* The item attributes are wanted during a DnD operation. Emit a signal
     to tell the element tree that certain popup context menu items may be
     available. */
  if (fromTree)
    {
      AttributesRetrievedEvent *ev = new AttributesRetrievedEvent
        (QEvent::Type (PwmdEventAttrsRetrieved));
      ev->setItem (element);
      QCoreApplication::postEvent (this, ev);
    }
}

bool
PwmdMainWindow::refreshAttributeList (QTreeWidgetItem *item, bool force,
                                      bool queue, bool fromTree)
{
  if (!item)
    {
      clearAttributeTab ();
      return false;
    }

  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  if (!data->attributes ()->isEmpty () && !force)
    {
      refreshAttributeListFinalize (item, QString (), false, fromTree);
      if (!queue)
        pwm->runQueue ();
      return false;
    }

  if (!fromTree && item != selectedElement)
    clearAttributeTab ();

  PwmdInquireData *inq = new PwmdInquireData (QString ("LIST %1").arg (elementPath (item)));
  inq->setUserPtr (item);
  inq->setUserBool (fromTree);
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdAttrList, "ATTR",
                                          Pwmd::inquireCallback, inq), queue);
  return true;
}

void
PwmdMainWindow::slotAttributeEditorClosed (QWidget * w,
                                           QAbstractItemDelegate::EndEditHint h)
{
  QLineEdit *le = qobject_cast < QLineEdit * >(w);
  QString s = le->text ();
  int pos = 0;

  if (le->text () == tr ("New attribute") || le->text ().isEmpty () ||
      h == QAbstractItemDelegate::RevertModelCache
      || (le->validator() && le->validator()->validate (s, pos) == QValidator::Invalid))
    {
      QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewAttributeCancel));
      QCoreApplication::postEvent (this, ev);
      return;
    }
}

/* Update element attributes for all items that have a target to "item", or
 * just the "item" itself. */
void
PwmdMainWindow::updateTargetAttributes (QTreeWidgetItem *item,
                                        const QString &name,
                                        const QString &value, bool remove)
{
  /* Cannot set time attributes and other reserved attributes require a tree
   * refresh. */
  if (PwmdElementAttr::isReservedAttribute (name))
    return;

  QTreeWidgetItem *t = resolveTargets (item);
  if (!t)
    return;

  foreach (QTreeWidgetItem *e, ui.elementTree->findItems ("*",
                                                          Qt::MatchRecursive
                                                          |Qt::MatchWildcard))
    {
      if (resolveTargets (e) != t)
        continue;

      if (remove)
        removeAttribute (e, name);
      else
        setAttribute (e, name, value);
    }

  setTimeLabels (t, true);
}

void
PwmdMainWindow::setNewAttributeFinalize (AttributeFinalizeT *attr,
                                         bool fromSlot)
{
  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(attr->item->data (0, Qt::UserRole));
  setAttribute (attr->item, attr->name, attr->content, true);
  setModified ();

  if (attr->item != selectedElement && attr->item != attributeElement)
    return;

  if (attr->name == "password"
      && ui.tabWidget->currentWidget() == ui.contentTab)
    {
      elementSelected (selectedElement);
      return;
    }
  else if (attr->name == "_expire"
           && ui.tabWidget->currentWidget () == ui.contentTab)
    {
      setElementExpireLabel (attr->content.toUInt ());
      setContentItemsEnabled ();
    }
  else if (attr->name == "hidden")
    {
      ui.textEdit->setHidden (true);
      ui.hiddenContentFrame->setHidden (data->hasAttribute ("password"));
      disconnect (ui.actionHidden, SIGNAL (toggled(bool)), this, SLOT (slotToggleHiddenContent(bool)));
      ui.actionHidden->setChecked (true);
      connect (ui.actionHidden, SIGNAL (toggled(bool)), this, SLOT (slotToggleHiddenContent(bool)));
    }
  else if (attr->name == "encoding" && attr->content == "base64")
    {
      disconnect (ui.actionBase64, SIGNAL (triggered(bool)), this, SLOT (slotToggleBase64Content(bool)));
      ui.actionBase64->setChecked (true);
      connect (ui.actionBase64, SIGNAL (triggered(bool)), this, SLOT (slotToggleBase64Content(bool)));
    }

  if (!attr->listItem)
    return;

  Qt::ItemFlags flags = attr->listItem->flags();
  flags &= ~Qt::ItemIsEditable;
  attr->listItem->setFlags(flags);

  if (ui.tabWidget->currentWidget () == ui.attributeTab)
    {
      data = qvariant_cast < PwmdTreeWidgetItemData * >(attr->item->data (0, Qt::UserRole));
      if (!PwmdElementAttr::isReservedAttribute (attr->listItem->text ())
          && data->hasTarget ())
        {
          QBrush b = attr->listItem->foreground ();
          QBrush bg(QPalette::Window);

          b.setColor (hilightColor);
          attr->listItem->setForeground (b);
          bg.setColor(hilightColorBg);
          attr->listItem->setBackground(bg);
        }
    }

  if (fromSlot && (attr->item == selectedElement
                   || attr->item == attributeElement))
    {
      slotAttributeSelected (attr->listItem, nullptr);
      ui.attributeList->setCurrentItem (attr->listItem);

      if (attr->name == "password")
        elementSelected (selectedElement);
    }
}

bool
PwmdMainWindow::setNewAttribute (QTreeWidgetItem *item, QString name,
                                 QString value, bool fromSlot, bool queue)
{
  PwmdInquireData *inq = new PwmdInquireData (QString ("SET %1 %2 %3").arg (name, elementPath (item), value));

  AttributeFinalizeT *attr = new AttributeFinalizeT;
  attr->item = item;
  attr->listItem = newAttribute ? newAttribute->clone () : nullptr;
  attr->name = name;
  attr->content = value;
  inq->setUserPtr (attr);
  inq->setUserBool (fromSlot);
  PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdAttrNew,
                                                        "ATTR",
                                                        Pwmd::inquireCallback,
                                                        inq);
  ignoreErrors (cmd);
  return pwm->command (cmd, queue);
}

bool
PwmdMainWindow::attributeIsEditable (QTreeWidgetItem *item,
                                     QListWidgetItem *attr)
{
  if (!item || !attr)
    return false;

  PwmdTreeWidgetItemData *data = nullptr;
  data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
  if (!data)
    return false;

  if (attr->text() == "password")
    return false;

  if (PwmdElementAttr::isReservedAttribute (attr->text ()))
    return isElementOwner (item);

  return !data->hasError () && isElementOwner (item);
}

void
PwmdMainWindow::slotAttributeSelected (QListWidgetItem *item, QListWidgetItem *)
{
  if (ui.tabWidget->currentWidget () != ui.attributeTab)
    return;

  PwmdTreeWidgetItemData *data = nullptr;
  if (attributeElement)
    data = qvariant_cast < PwmdTreeWidgetItemData * >(attributeElement->data (0, Qt::UserRole));

  ui.attributeContent->setEnabled (item && (item->flags() & Qt::ItemIsEnabled));
  setAttributeItemsEnabled ();

  doSaveContent (true);
  if (!item)
    {
      pwm->runQueue ();
      return;
    }

  if (!(item->flags () & Qt::ItemIsEnabled))
    {
      ui.attributeList->setCurrentItem (selectedAttribute);
      pwm->runQueue ();
      return;
    }

  selectedAttribute = item;
  previousAttributeText = item->text ();
  QTreeWidgetItem *i = attributeElement;
  data = qvariant_cast < PwmdTreeWidgetItemData * >(i->data (0, Qt::UserRole));
  QString s = data->attribute (item->text ());
  setAttributeContent (s.isEmpty ()? "" : s.replace ("\t", "<TAB>", Qt::CaseInsensitive));

  if (item && item->text () == "_target")
    fixupToolBar (attributeToolBar, AttributeToolBarDelete, false);
  else if (item && item->text () == "_acl" && isElementOwner (selectedElement))
    fixupToolBar (attributeToolBar, AttributeToolBarDelete, true);

  ui.attributeContent->setReadOnly (!attributeIsEditable (attributeElement, item));
}

void
PwmdMainWindow::slotConfirmNewAttribute (QWidget * w)
{
  QLineEdit *le = qobject_cast < QLineEdit * >(w);

  if (!newAttribute)
    return;

  PwmdTreeWidgetItemData *data = qvariant_cast <PwmdTreeWidgetItemData *>(attributeElement->data (0, Qt::UserRole));

  if (le->text () == tr ("New attribute")
      || le->text ().isEmpty ()
      || PwmdElementAttr::isProtectedAttribute (le->text ()))
    {
      QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewAttributeCancel));
      QCoreApplication::postEvent (this, ev);
      return;
    }

  bool match = false;
  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      QTreeWidgetItem *ritem = resolveTargets (item);
      data = qvariant_cast <PwmdTreeWidgetItemData *>(item->data (0, Qt::UserRole));
      if (!data->hasAttribute (le->text ())
          && attributeIsEditable (item, newAttribute))
        {
          data = qvariant_cast <PwmdTreeWidgetItemData *>(ritem->data (0, Qt::UserRole));
          if (ritem == item
              || (attributeIsEditable (ritem, newAttribute)
                  && !data->hasAttribute (le->text ())))
            {
              match = true;
              setNewAttribute (item, le->text (), QString (), true, true);
            }
        }
    }

  delete newAttribute;
  newAttribute = nullptr;
  if (!match)
    {
      QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewAttributeCancel));
      QCoreApplication::postEvent (this, ev);
    }

  pwm->runQueue ();
}

void
PwmdMainWindow::slotNewAttribute ()
{
  if (doSaveContent (true))
    {
      newAttribute = new QListWidgetItem (tr ("New attribute"));
      pwm->runQueue ();
      return;
    }

  setAttributeContent ("");
  ui.attributeContent->setEnabled (false);
  fixupToolBar (attributeToolBar, AttributeToolBarNew, false);
  fixupToolBar (attributeToolBar, AttributeToolBarDelete, false);
  delete newAttribute;
  newAttribute = new QListWidgetItem (tr ("New attribute"));
  newAttribute->setFlags (newAttribute->flags () | Qt::ItemIsEditable);
  ui.attributeList->addItem (newAttribute);
  ui.attributeList->scrollToItem (newAttribute);
  ui.attributeList->editItem (newAttribute);
}

void
PwmdMainWindow::slotDeleteAttribute ()
{
  if (!selectedAttribute
      || !(selectedAttribute->flags () & Qt::ItemIsEnabled))
    return;


  doSaveContent (true);
  foreach (QTreeWidgetItem *item, ui.elementTree->selectedItems ())
    {
      PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));
      QString attr = selectedAttribute->text ();
      QTreeWidgetItem *ritem = resolveTargets (item);

      if (data->hasAttribute (attr) &&
          ((!PwmdElementAttr::isReservedAttribute (attr) || attr == "_acl")
           && isElementOwner (item)))
        {
          if (ritem == item || isElementOwner (ritem))
            deleteAttribute (item, attr, true);
        }
    }

  pwm->runQueue ();
}

void
PwmdMainWindow::setAttribute (QTreeWidgetItem *item, const QString &name,
                              const QString &value, bool update)
{
  if (!item)
    return;

  PwmdTreeWidgetItemData *data =
    qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  if (!data)
    return;

  data->addAttribute (name, value);
  if (update)
    updateTargetAttributes (item, name, value);

  refreshAttributeListFinalize (item, QString (), false);
}

void
PwmdMainWindow::removeAttribute (QTreeWidgetItem * item, const QString & name,
                                 bool update)
{
  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  data->removeAttribute (name);
  data->setNeedsAttrRefresh ();
  if (update)
    updateTargetAttributes (item, name, QString (), true);
}

void
PwmdMainWindow::deleteAttributeFinalize (QTreeWidgetItem *item,
                                         const QString &name)
{
  removeAttribute (item, name, true);
  setModified ();
  if (item != selectedElement)
    return;

  PwmdTreeWidgetItemData *data = qvariant_cast < PwmdTreeWidgetItemData * >(item->data (0, Qt::UserRole));

  if (name == "_expire" && ui.tabWidget->currentWidget () == ui.contentTab)
    setContentItemsEnabled ();
  else if (name == "hidden")
    {
      ui.textEdit->setHidden (data->hasAttribute ("password"));
      ui.hiddenContentFrame->setHidden (true);
      disconnect (ui.actionHidden, SIGNAL (toggled(bool)), this, SLOT (slotToggleHiddenContent(bool)));
      ui.actionHidden->setChecked (false);
      connect (ui.actionHidden, SIGNAL (toggled(bool)), this, SLOT (slotToggleHiddenContent(bool)));
    }
  else if (name == "encoding" && data->attribute ("encoding") != "base64")
    {
      disconnect (ui.actionBase64, SIGNAL (triggered(bool)), this, SLOT (slotToggleBase64Content(bool)));
      ui.actionBase64->setChecked (false);
      connect (ui.actionBase64, SIGNAL (triggered(bool)), this, SLOT (slotToggleBase64Content(bool)));
    }

  if (item == selectedElement || item == attributeElement)
    {
      if (selectedAttribute)
        delete selectedAttribute;

      selectedAttribute = ui.attributeList->currentItem ();
      setAttributeContent ("");

      if (name == "_target")
	{
	  selectedElementPath = elementPath (attributeElement);
	  QEvent *ev = new QEvent (QEvent::Type (PwmdEventTargetAttr));
	  QCoreApplication::postEvent (this, ev);
	}
      else
        elementSelected (selectedElement);
    }
}

bool
PwmdMainWindow::deleteAttribute (QTreeWidgetItem *item, const QString &name,
                                 bool queue)
{
  PwmdInquireData *inq = new PwmdInquireData (QString ("DELETE %1 %2").arg (name, elementPath (item)));

  inq->setUserPtr (item);
  inq->setUser (name);
  PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdAttrDelete,
                                                        "ATTR",
                                                        Pwmd::inquireCallback,
                                                        inq);
  ignoreErrors (cmd);
  return pwm->command (cmd, queue);
}

bool
PwmdMainWindow::hasMultiSelection ()
{
  return ui.elementTree->selectedItems ().count () > 1;
}

bool
PwmdMainWindow::hasSingleSelection ()
{
  return !hasMultiSelection ();
}

QTreeWidgetItem *
PwmdMainWindow::firstSelectedItem ()
{
  return ui.elementTree->selectedItems ().count ()
    ? ui.elementTree->selectedItems ().at (0) : nullptr;
}
