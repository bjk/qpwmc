/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDKEYGENDIALOG
#define PWMDKEYGENDIALOG

#include <QDialog>
#include "pwmd.h"
#include "pwmdKey.h"
#include "ui_pwmdKeyGenDialog.h"

class PwmdKeyGenDialog : public QDialog
{
 Q_OBJECT
 public:
  PwmdKeyGenDialog (QWidget *p, Pwmd *);
  ~PwmdKeyGenDialog ();
  QString name ();
  void setName (const QString &);
  QString email ();
  void setEmail (const QString &);
  QString algorithm ();
  unsigned long expires ();
  bool protection ();
  QString usage ();
  void setSubKey (PwmdKey *);
  QString subKey ();

 private slots:
  void slotNeverExpire (int);
  void slotProtected ();
  void slotNameChanged (const QString &);
  void slotEmailChanged (const QString &);
  void slotSubKeyChanged (const QString &);
  void slotGenerate ();
  void slotSelectKeyFile ();
  void slotCommandResult (PwmdCommandQueueItem *item, QString result,
                          unsigned err, bool queued);

 private:
  Ui::PwmdKeyGenDialog ui;
  Pwmd *pwm;
  gpg_error_t genKeyError;
};

#endif
