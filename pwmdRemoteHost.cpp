/*
    Copyright (C) 2012-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QStringList>
#include <QSettings>
#include "pwmdRemoteHost.h"
#include <libpwmd.h>

PwmdRemoteHost::PwmdRemoteHost (QString name)
{
  _name = name;
  _type = PWMD_SOCKET_LOCAL;
  _hostname = _sshUsername = QString ();
  _port = 0;
  _ipProtocol = 0;
  _tlsVerify = false;
  _tlsPriority = QString ();
  _socketTimeout = 0;
  _connectTimeout = 0;
  _sshAgent = false;
#ifdef Q_OS_ANDROID
  _clientCertificateAlias = QString ();
#endif
}

bool
PwmdRemoteHost::operator == (const PwmdRemoteHost &other) const
{
  if (this->_name == other._name
#ifdef Q_OS_ANDROID
      && this->_clientCertificateAlias == other._clientCertificateAlias
#endif
      && this->_type == other._type
      && this->_hostname == other._hostname
      && this->_port == other._port
      && this->_ipProtocol == other._ipProtocol
      && this->_tlsVerify == other._tlsVerify
      && this->_tlsPriority == other._tlsPriority
      && this->_socketTimeout == other._socketTimeout
      && this->_connectTimeout == other._connectTimeout
      && this->_sshAgent == other._sshAgent
      && this->_socketArgs == other._socketArgs)
    return true;

  return false;
}

bool
PwmdRemoteHost::operator != (const PwmdRemoteHost &other) const
{
  return !(*this == other);
}

PwmdRemoteHost::~PwmdRemoteHost ()
{
}

QString
PwmdRemoteHost::name ()
{
  return _name;
}

void
PwmdRemoteHost::setName (QString str)
{
  _name = str;
}

QString
PwmdRemoteHost::hostname ()
{
  return _hostname;
}

void
PwmdRemoteHost::setHostname (QString h)
{
  _hostname = h;
}

int
PwmdRemoteHost::type ()
{
  return _type;
}

void
PwmdRemoteHost::setType (int n)
{
  _type = n;
}

int
PwmdRemoteHost::port ()
{
  return _port;
}

void
PwmdRemoteHost::setPort (int n)
{
  _port = n;
}

int
PwmdRemoteHost::ipProtocol ()
{
  return _ipProtocol;
}

void
PwmdRemoteHost::setIpProtocol (int n)
{
  _ipProtocol = n;
}

QStringList
PwmdRemoteHost::socketArgs ()
{
  return _socketArgs;
}

void
PwmdRemoteHost::setSocketArgs (QStringList list)
{
  _socketArgs = list;
}

QString
PwmdRemoteHost::tlsPriority ()
{
  return _tlsPriority;
}

void
PwmdRemoteHost::setTlsPriority (QString s)
{
  _tlsPriority = s;
}

bool
PwmdRemoteHost::tlsVerify ()
{
  return _tlsVerify;
}

void
PwmdRemoteHost::setTlsVerify (bool b)
{
  _tlsVerify = b;
}

int
PwmdRemoteHost::socketTimeout ()
{
  return _socketTimeout;
}

void
PwmdRemoteHost::setSocketTimeout (int n)
{
  _socketTimeout = n;
}

int
PwmdRemoteHost::connectTimeout ()
{
  return _connectTimeout;
}

void
PwmdRemoteHost::setConnectTimeout (int n)
{
  _connectTimeout = n;
}

QString
PwmdRemoteHost::sshUsername ()
{
  return _sshUsername;
}

void
PwmdRemoteHost::setSshUsername (QString s)
{
  _sshUsername = s;
}

bool
PwmdRemoteHost::sshAgent ()
{
  return _sshAgent;
}

void
PwmdRemoteHost::setSshAgent (bool b)
{
  _sshAgent = b;
}

#ifdef Q_OS_ANDROID
QString
PwmdRemoteHost::clientCertificateAlias ()
{
  return _clientCertificateAlias;
}

void
PwmdRemoteHost::setClientCertificateAlias (QString s)
{
  _clientCertificateAlias = s;
}
#endif

bool
PwmdRemoteHost::fillRemoteHost (const QString &name, PwmdRemoteHost &data)
{
  QSettings cfg ("qpwmc");
  int size = cfg.beginReadArray ("remoteHosts");

  for (int i = 0; i < size; ++i)
    {
      cfg.setArrayIndex (i);

      if (cfg.value ("name").toString () != name)
	continue;

      data = PwmdRemoteHost (cfg.value ("name").toString ());
      data.setType (cfg.value ("type").toInt ());
      data.setHostname (cfg.value ("hostname").toString ());
      data.setPort (cfg.value ("port").toInt ());
      data.setIpProtocol (cfg.value ("ipProtocol").toInt ());
      data.setSocketArgs (cfg.value ("socketArgs").toStringList ());
      data.setTlsVerify (cfg.value ("tlsVerify").toBool ());
      data.setTlsPriority (cfg.value ("tlsPriority").toString ());
      data.setConnectTimeout (cfg.value ("connectTimeout").toInt ());
      data.setSocketTimeout (cfg.value ("socketTimeout").toInt ());
      data.setSshAgent (cfg.value ("sshAgent").toBool ());
      data.setSshUsername (cfg.value ("sshUsername").toString ());
#ifdef Q_OS_ANDROID
      data.setClientCertificateAlias (cfg.value ("clientCertificateAlias").toString ());
#endif
      return true;
    }

  cfg.endArray ();
  return false;
}

QString
PwmdRemoteHost::socketUrl (PwmdRemoteHost &host)
{
  QString s = QString ();

  if (host.type () == PWMD_SOCKET_SSH)
    s.append ("ssh");
  else
    s.append ("tls");

  if (host.ipProtocol () != 0 && host.ipProtocol () == 1)
    s.append ("4");
  else if (host.ipProtocol () != 0)
    s.append ("6");

  s.append ("://");

  if (host.type () == PWMD_SOCKET_SSH)
    {
      s.append (host.sshUsername ().isEmpty ()? "" : host.sshUsername ());
      s.append ("@");
    }

  if (host.ipProtocol () != 0)
    s.append ('[');

  s.append (host.hostname ());

  if (host.ipProtocol () != 0)
    s.append (']');

  s.append (QString (":%1").arg (host.port ()));
  return s;
}
