/*
    Copyright (C) 2013-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef APPLICATIONFORMWIDGET
#define APPLICATIONFORMWIDGET

class ApplicationFormAttr
{
 public:
   ApplicationFormAttr (const QString &n, const QString &v)
     {
       _name = n;
       _value = v;
     };

   ~ApplicationFormAttr () { };

   QString name ()
     {
       return _name;
     };

   QString value ()
     {
       return _value;
     };

 private:
   QString _name;
   QString _value;
};

class ApplicationFormWidget
{
 public:
  enum {
      AppWidgetNone, AppWidgetRadioButton, AppWidgetSpinBox, AppWidgetLineEdit,
      AppWidgetCheckBox, AppWidgetDateSelector,
      AppWidgetInteralOverwriteCheckBox
  };

  ApplicationFormWidget ();
  ~ApplicationFormWidget ();
  void setType (int);
  int type ();
  QWidget *widget ();
  void setValue (QString);
  QString value ();
  void setElementSelector (bool = true);
  bool elementSelector ();
  void setFileSelector (bool = true);
  bool fileSelector ();
  void setDateSelector (bool = true);
  bool dateSelector ();
  void setPasswordGenerator (bool = true);
  bool passwordGenerator ();
  void setId (QString);
  QString id ();
  void setChildOf (QString);
  QString childOf ();
  void setPwmdName (QString);
  QString pwmdName ();
  void setLabel (QString);
  QString label ();
  void addRadioButton (QString);
  QStringList radioButtons ();
  void addRadioButtonId (int);
  bool hasRadioButtonId (int);
  void setHidden ();
  bool isHidden ();
  void setRequired ();
  bool isRequired ();
  void setWhatsThis (QString);
  QString whatsThis ();
  void setAttr (QString name, QString value = 0);
  QString attr (const QString &);
  QList<ApplicationFormAttr *> attrs ();
  bool expiry ();
  void setExpiry (time_t = 0, time_t = 0);
  time_t expiryDate ();
  time_t expiryAge ();
  void setPlaceholderText (const QString &);
  QString placeholderText ();

 private:
  int _type;
  QWidget *_widget;
  QString _value;
  bool _elementSelector;
  bool _fileSelector;
  bool _passwordGenerator;
  bool _dateSelector;
  QString _id;
  QString _childOf;
  QString _pwmdName;
  QString _label;
  QStringList _radioButtons;
  QList<int> _radioButtonIds;
  bool _isHidden;
  bool _isRequired;
  QString _whatsThis;
  bool _expiry;
  time_t _expiryDate;
  time_t _expiryAge;
  QList<ApplicationFormAttr *> _attrs;
  QString _placeholderText;
};

#endif
