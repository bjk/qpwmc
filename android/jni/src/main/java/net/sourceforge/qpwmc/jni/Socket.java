/*
    Copyright (C) 2019-2022 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
package net.sourceforge.qpwmc.jni;

import android.content.Context;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public class Socket {
    private int sockFd = -1;
    private SSLSocket sock = null;
    private static ParcelFileDescriptor parcelFd = null;
    private int mTimeout = 0;
    private static final int ErrorUnknownError = -1;
    private static final int ErrorNoError = 0;
    private static final int ErrorTimedOut = 1;
    private static final int ErrorRefused = 2;
    private static final int ErrorUnknownHost = 3;
    private static final int ErrorIO = 4;
    private static int errorCode = ErrorNoError;

    public int connect(Context context, String certAlias, final String host, final int port) {
        SSLContext ctx;
        SSLSocketFactory sf;

        try {
            TrustManager[] trustManagers = new TrustManager[] {
                    net.sourceforge.qpwmc.jni.TrustManagerFactory.get(host, port)
            };
            KeyManager[] keyManagers = new KeyManager[] {
                    new net.sourceforge.qpwmc.jni.KeyChainKeyManager(context, certAlias)
            };
            ctx = SSLContext.getInstance("TLS");
            ctx.init(keyManagers, trustManagers, null);
            sf = ctx.getSocketFactory();
            sock = (SSLSocket)sf.createSocket();
            sock.connect(new InetSocketAddress(host, port), mTimeout);
            parcelFd = ParcelFileDescriptor.fromSocket(sock);
            sockFd = parcelFd.getFd();
            sock.startHandshake();
        } catch (Exception e) {
            if (e instanceof UnknownHostException) {
                errorCode = ErrorUnknownHost;
            } else if (e instanceof UnknownServiceException) {
                errorCode = ErrorRefused;
            } else if (e instanceof SocketTimeoutException) {
                errorCode = ErrorTimedOut;
            } else if (e instanceof IOException) {
                errorCode = ErrorIO;
            } else {
                errorCode = ErrorUnknownError;
            }

            System.out.println(e);
            try {
                if (sock != null) {
                    sock.close();
		    sock = null;
                }
                if (parcelFd != null) {
                    parcelFd.close();
		    parcelFd = null;
                }
            } catch (Exception sockExeption) {
                Log.d("QPWMC", sockExeption.getMessage());
            }
            sockFd = -1;
        }

        if (sockFd != -1)
            errorCode = ErrorNoError;

        return sockFd;
    }

    public void setTimeout (int t) {
        mTimeout = t*1000;
        if (sock != null) {
            try {
                sock.setSoTimeout(mTimeout);
            } catch (Exception e) {
                Log.d("QPWMC", e.getMessage());
            }
        }
    }

    public void close() {
        try {
            if (sock != null && !sock.isClosed()) {
                sock.close();
		sock = null;
            }
            if (parcelFd != null) {
                parcelFd.close();
		parcelFd = null;
            }
        } catch (Exception e) {
            Log.d("QPWMC", e.getMessage());
        }

        errorCode = ErrorNoError;
        sockFd = -1;
    }

    public SocketReadResult read(final int len) {
        DataInputStream in = null;
        byte[] dst = new byte[len];
        try {
            in = new DataInputStream(sock.getInputStream());
        } catch (Exception e) {
            Log.d("QPWMC", e.getMessage());
            dst = null;
            return null;
        }

        int n = -1;
        try {
            n = in.read(dst, 0, len);
        } catch (Exception e) {
            if (e instanceof IOException) {
                errorCode = ErrorIO;
            } else {
                errorCode = ErrorUnknownError;
            }

            Log.d("QPWMC", e.getMessage());
            in = null;
            dst = null;
            return null;
        }

        errorCode = ErrorNoError;
        return new SocketReadResult(dst, n);
    }

    public int write(final byte[] data, final int len) {
        DataOutputStream out = null;

        try {
            out = new DataOutputStream(sock.getOutputStream());
        } catch (Exception e) {
            Log.d("QPWMC", e.getMessage());
            return -1;
        }

        try {
            out.write(data, 0, len);
            out.flush();
        } catch (Exception e) {
            if (e instanceof IOException) {
                errorCode = ErrorIO;
            } else {
                errorCode = ErrorUnknownError;
            }

            Log.d("QPWMC", "write: " + e.getMessage());
            out = null;
            return -1;
        }

        errorCode = ErrorNoError;
        return len;
    }

    public int error() {
        return errorCode;
    }
}
