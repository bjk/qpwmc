/*
    Copyright (C) 2019-2022 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
package net.sourceforge.qpwmc.jni;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;
import android.content.ClipData;
import android.content.ClipboardManager;

public class Util {
    static public void toast(final Context ctx, final String s, final int duration) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ctx, s, duration).show();
            }
        });
    }

    static public void clearClipboard (final Context ctx) {
	ClipboardManager cb = (ClipboardManager)ctx.getSystemService (ctx.CLIPBOARD_SERVICE);
	ClipData data = ClipData.newPlainText ("text", " ");
	cb.setPrimaryClip (data);
	data = ClipData.newPlainText ("text", "");
	cb.setPrimaryClip (data);
	cb.clearPrimaryClip ();
    }
}
