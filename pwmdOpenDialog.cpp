/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QTimer>
#include <QCommonStyle>
#include <QScroller>
#include <QSettings>
#include "pwmdOpenDialog.h"
#include "cmdIds.h"
#include "pwmdFileDialog.h"

#define PwmdEventNewFileCancel QEvent::User+100
#define PwmdEventNewFile QEvent::User+101

PwmdOpenDialog::PwmdOpenDialog (Pwmd *h, QWidget * p) : QDialog (p)
{
  ui.setupUi (this);
  QSettings cfg ("qpwmc");
  restoreGeometry (cfg.value ("openDialogGeometry").toByteArray ());
  QCommonStyle style;
  newFilename = nullptr;

  pwm = h;
  ui.fileOptions->setHandle (h);
  ui.fileOptions->setFilename (QString (), false);
  connect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));
  connect (ui.tb_openNew, SIGNAL (clicked ()), SLOT (slotNewFilename ()));
  ui.tb_openNew->setIcon (QIcon (":icons/matt-icons_document-new.svg"));
  connect (ui.openList, SIGNAL (currentItemChanged (QListWidgetItem *, QListWidgetItem *)), this, SLOT (slotFilenameSelected (QListWidgetItem *, QListWidgetItem *)));
  connect (ui.openList, SIGNAL (itemDoubleClicked (QListWidgetItem *)), this,
	   SLOT (slotFilenameDoubleClicked (QListWidgetItem *)));
  ui.openList->setItemDelegate (new PwmdListWidgetItemDelegate (ui.openList));
  connect (ui.openList->itemDelegate (), SIGNAL (commitData (QWidget *)),
	   this, SLOT (slotConfirmNewFile (QWidget *)));
  connect (ui.openList->itemDelegate (), SIGNAL (closeEditor (QWidget *, QAbstractItemDelegate::EndEditHint)), this, SLOT (slotOpenEditorClosed (QWidget *, QAbstractItemDelegate::EndEditHint)));
  ui.openList->setFocus ();
  ui.fileDetailsGroupBox->setEnabled (false);
  ui.lAccessTime->setText ("-");
  ui.lModTime->setText ("-");
  ui.lChangeTime->setText ("-");

  QScroller::grabGesture (ui.scrollArea, QScroller::TouchGesture);
#ifdef Q_OS_ANDROID
  showMaximized();
#endif
  QTimer::singleShot (1, this, SLOT (slotRefreshFilenameList ()));
}

PwmdOpenDialog::~PwmdOpenDialog()
{
  QSettings cfg ("qpwmc");
  cfg.setValue ("openDialogGeometry", saveGeometry ());
  foreach (QListWidgetItem *item, ui.openList->findItems ("*", Qt::MatchWildcard))
    {
      FilenameData *data = qvariant_cast <FilenameData *> (item->data (Qt::UserRole));
      delete data;
    }
}

void
PwmdOpenDialog::slotCommandResult (PwmdCommandQueueItem *item,
                                   QString result, gpg_error_t rc, bool queued)
{
  if (item->id () <= PwmdCmdIdMainMax || item->id () >= PwmdCmdIdOpenMax)
    {
      item->setSeen ();
      return;
    }

  switch (item->id ())
    {
    case PwmdCmdIdOpenLs:
      if (!rc)
        {
          refreshFilenameListFinalize (result);
          break;
        }
      break;
    default:
      break;
    }

  if (rc && !item->checkError (rc) && !queued)
    Pwmd::showError (rc, pwm);

  item->setSeen ();
}

bool
PwmdOpenDialog::event (QEvent *ev)
{
  int type = ev->type ();

  switch (type)
    {
    case PwmdEventNewFileCancel:
      delete newFilename;
      newFilename = nullptr;
      slotFilenameSelected (ui.openList->currentItem (), nullptr);
      ui.tb_openNew->setEnabled (true);
      ev->accept ();
      return true;
    case PwmdEventNewFile:
      accept ();
      return true;
    default:
      break;
    }

  return QDialog::event (ev);
}

QString
PwmdOpenDialog::filename ()
{
  return ui.openList->currentItem () ? ui.openList->currentItem()->text() : QString ();
}

QString
PwmdOpenDialog::keyFile ()
{
  return ui.fileOptions->decryptKeyFile ();
}

bool
PwmdOpenDialog::lock ()
{
  return ui.fileOptions->lock ();
}

void
PwmdOpenDialog::slotFilenameDoubleClicked (QListWidgetItem * item)
{
  slotFilenameSelected (item, nullptr);
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Ok);
  pb->animateClick ();
}

void
PwmdOpenDialog::slotFilenameSelected (QListWidgetItem * item,
				  QListWidgetItem * old)
{
  (void) old;

  if (newFilename)
    return;

  ui.openList->scrollToItem (item);
  FilenameData *data = qvariant_cast <FilenameData *> (item->data (Qt::UserRole));
  QDateTime dt;
  int n = data->access ().indexOf ('.');

  dt.setSecsSinceEpoch (data->access ().left (n).toULong ());
  QString s = QString ("%1%2 %3").arg (dt.toString ("ddd MMM d hh:mm:ss"),
                                       data->access ().mid (n,6),
                                       dt.toString ("yyyy"));
  ui.lAccessTime->setText (s);
  ui.lAccessTime->setEnabled (true);

  n = data->mod ().indexOf ('.');
  dt.setSecsSinceEpoch (data->mod ().left (n).toULong ());
  s = QString ("%1%2 %3").arg (dt.toString ("ddd MMM d hh:mm:ss"),
                               data->mod ().mid (n, 6),
                               dt.toString ("yyyy"));
  ui.lModTime->setText (s);
  ui.lModTime->setEnabled (true);

  n = data->change ().indexOf ('.');
  dt.setSecsSinceEpoch (data->change ().left (n).toULong ());
  s = QString ("%1%2 %3").arg (dt.toString ("ddd MMM d hh:mm:ss"),
                               data->change ().mid (n, 6),
                               dt.toString ("yyyy"));
  ui.lChangeTime->setText (s);
  ui.lChangeTime->setEnabled (true);

  ui.fileDetailsGroupBox->setEnabled (item != nullptr);
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Ok);
  pb->setEnabled (true);
  ui.fileOptions->setFilename (item->text (), false);
}

void
PwmdOpenDialog::slotConfirmNewFile (QWidget * w)
{
  QLineEdit *le = qobject_cast < QLineEdit * >(w);

  if (!newFilename)
    return;

  if (le->text () == tr ("New file") || le->text ().isEmpty ())
    {
      QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewFileCancel));
      QCoreApplication::postEvent (this, ev);
      return;
    }

  for (int i = 0, t = ui.openList->count (); i < t; i++)
    {
      QListWidgetItem *item = ui.openList->item (i);

      if (ui.openList->itemWidget (item) != w && item->text () == le->text ())
	{
	  QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewFileCancel));
	  QCoreApplication::postEvent (this, ev);
	  return;
	}
    }

  ui.openList->setCurrentItem (newFilename);
  QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewFile));
  QCoreApplication::postEvent (this, ev);
}

void
PwmdOpenDialog::slotOpenEditorClosed (QWidget * w,
				  QAbstractItemDelegate::EndEditHint h)
{
  QLineEdit *le = qobject_cast < QLineEdit * >(w);
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Ok);

  pb->setEnabled (true);
  ui.fileDetailsGroupBox->setEnabled (ui.openList->currentItem() != nullptr);

  if (le->text () == tr ("New file") || le->text ().isEmpty ()
      || h == QAbstractItemDelegate::RevertModelCache)
    {
      ui.tb_openNew->setEnabled (true);
      QEvent *ev = new QEvent (QEvent::Type (PwmdEventNewFileCancel));
      QCoreApplication::postEvent (this, ev);
      return;
    }
}

void
PwmdOpenDialog::slotNewFilename ()
{
  QPushButton *pb = ui.buttonBox->button (QDialogButtonBox::Ok);

  pb->setEnabled (false);
  ui.fileDetailsGroupBox->setEnabled (false);
  ui.tb_openNew->setEnabled (false);
  newFilename = new QListWidgetItem (tr ("New file"));
  newFilename->setFlags (newFilename->flags () | Qt::ItemIsEditable);
  ui.openList->addItem (newFilename);
  ui.openList->scrollToItem (newFilename);
  ui.openList->editItem (newFilename);
}

void
PwmdOpenDialog::refreshFilenameListFinalize (const QString &result)
{
  QList < QListWidgetItem * >items;

  QStringList files = result.split ("\n");

  for (int file = 0; file < files.count (); file++)
    {
      if (files.at (file).isEmpty ())
        continue;

      QStringList fields = files.at (file).split (' ');

      QListWidgetItem *item;
      item = new QListWidgetItem (fields.at (0));
      items.append (item);

      FilenameData *data = new FilenameData (fields.at (0), fields.at (1),
                                             fields.at (2), fields.at (3));
      item->setData (Qt::UserRole, QVariant::fromValue (data));
    }

  while (ui.openList->count ())
    {
      QListWidgetItem *item = ui.openList->takeItem (0);
      FilenameData *data = qvariant_cast <FilenameData *> (item->data (Qt::UserRole));
      delete data;
    }

  ui.openList->clear ();
  foreach (QListWidgetItem * item, items)
    ui.openList->addItem (item);

  if (!ui.openList->findItems (pwm->filename (), Qt::MatchExactly).isEmpty ())
    ui.openList->setCurrentItem (ui.openList->findItems (pwm->filename (), Qt::MatchExactly).at (0));
  ui.openList->scrollToItem (ui.openList->currentItem (), QAbstractItemView::EnsureVisible);
}

void
PwmdOpenDialog::slotRefreshFilenameList ()
{
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdOpenLs, "LS --verbose"));
}
