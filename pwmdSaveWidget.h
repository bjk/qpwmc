/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDSAVEWIDGET_H
#define PWMDSAVEWIDGET_H

#include <QFrame>
#include <QDialogButtonBox>
#include "pwmd.h"
#include "pwmdKey.h"
#include "pwmdKeyTreeWidget.h"
#include "ui_pwmdSaveWidget.h"

class PwmdSaveDialog;
class PwmdSaveWidget : public QFrame
{
  Q_OBJECT
 public:
  PwmdSaveWidget (QWidget *);
  ~PwmdSaveWidget ();
  void setHandle (Pwmd *, bool newFile = false);
  gpg_error_t save (const QString &args = 0, bool fromWidget = false);
  void setIsNew (bool = true);
  QString recipients();
  QString signers();
  // Reset the widget to the key list display.
  void reset ();
  void resetRecipients ();
  bool isSearching ();

  // Set if the filename changed.
  void setNeedsRefresh (bool = true);
  bool needsRefresh ();

 public slots:
  void slotResetRecipients ();

 private slots:
  void slotKeyIdChanged (QTreeWidgetItem *, QTreeWidgetItem *);
  void slotConfirmKeySelection ();
  void slotRefreshKeyList ();
  void slotSearchKeyList ();
  void slotSearchKeyListChanged (const QString &);
  void slotFindKeyActivated ();
  void slotAddRecipients ();
  void slotRemoveRecipients ();
  void slotAddSubKey ();
  void slotHeaderSectionResized (int, int, int);
  void slotSymmetricChanged (int);
  void slotSecretOnlyChanged (int);
  void slotJumpToKey (const QString & = 0);
  void slotSelectKeyFile ();
  void slotCommandResult (PwmdCommandQueueItem *item, QString result,
                          unsigned err, bool queued);
  void slotCopyKeyIdToClipboard ();
  void slotCopyRecipientKeyIdToClipboard ();
  void slotLearn ();
  void slotGenerate ();
  void slotStatusMessage (QString line, void *user);
  void slotDeleteKey ();
  void slotKeySelectionChanged ();

 signals:
  void saveReady (bool);

 private:
  friend class PwmdKeyTreeWidget;

  void getKeyList (const QString &, bool = false, bool = false);
  void setKeyId (bool = false);
  void keyPressEvent (QKeyEvent *);
  void selectKeyId (bool = false);
  void createSubKeyItem (PwmdKey *);
  void createSubKeyItemOnce (QTreeWidgetItem *, QString , PwmdKey *, QString = 0);
  void showEvent (QShowEvent *);
  void updateCurrentKeyInfo ();
  void updateKeyList (bool);
  void setKeyListRecipients ();
  void addRecipient (PwmdKeyItemData *);
  void changeKeyListFocus (PwmdKeyTreeWidget *, bool = false);
  void doSetKeyListRecipients (QString);
  void setDefaultKeyParam ();
  void refreshKeyLists (bool);
  QTreeWidgetItem *searchKeyList (const QString &, int &, bool = false,
                                  bool = false);
  void updateCurrentKeyInfoFinalize (const QString &);
  void getKeyListFinalize (const QString &, bool);
  void saveFinalize ();
  void clearKeyList ();
  void clearRecipientList ();
  void setSymmetric ();
  bool testRecipients ();

  Ui::PwmdSaveWidget ui;
  Pwmd *pwm;
  bool newFile;
  bool selectSignKeyId;
  int currentKeyListIndex;
  QList <PwmdKey *> keyList;
  QList <PwmdKey *> selectedKeys;
  QString _recipients;
  QString _signers;
  bool _needsRefresh;
  QString newSigners;
  QString newRecipients;
  QString keyFile;
  QString encryptKeyFile;
  QString signKeyFile;
  bool init;
  QString generatedKeyId;
};

#endif
