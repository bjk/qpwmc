/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QCoreApplication>
#include <QJniObject>
#include <QJniEnvironment>
#include <errno.h>
#include "pwmdAndroidJNI.h"

enum
{
  ErrorUnknownError = -1,
  ErrorNoError = 0,
  ErrorTimedOut = 1,
  ErrorRefused = 2,
  ErrorUnknownHost = 3,
  ErrorIO = 4
};

static QLineEdit *clientCertificateAliasLineEdit;

PwmdAndroidJNI::PwmdAndroidJNI (QObject *p) : QObject (p), jniThread (this)
{
  androidSocket = QJniObject ("net/sourceforge/qpwmc/jni/Socket");
  cancelling = false;
}

PwmdAndroidJNI::~PwmdAndroidJNI ()
{
}

void
PwmdAndroidJNI::init ()
{
  QJniObject act = QNativeInterface::QAndroidApplication::context ();
  QJniObject w = act.callObjectMethod ("getWindow",
                                       "()Landroid/view/Window;");
  if (!w.isValid ())
    return;

  const int f = 8192; // WindowManager.LayoutParams.FLAG_SECURE
  w.callObjectMethod ("addFlags", "(I)V", f);
}

void
PwmdAndroidJNI::handleActivityResult (int code, int result,
                                      const QJniObject &obj)
{
  (void) obj;

  if (code != PwmdAndroidJNI::AuthActivity)
    return;

  jint b = QJniObject::getStaticField<jint> ("android/app/Activity",
                                             "RESULT_OK");
  if (result == b)
    {
      emit jniAuthenticated ();
      return;
    }

  showToast (tr ("Key guard not verified."));
  qApp->quit ();
}

bool
PwmdAndroidJNI::authenticate ()
{
  QJniObject ctx = QNativeInterface::QAndroidApplication::context ();
  if (!ctx.isValid ())
    return false;

  QJniObject s = QJniObject::getStaticObjectField <jstring>("android/content/Context", "KEYGUARD_SERVICE");
  if (!s.isValid ())
    return false;

  QJniObject kg = ctx.callObjectMethod ("getSystemService",
                                        "(Ljava/lang/String;)"
                                        "Ljava/lang/Object;",
                                        s.object<jstring> ());
  if (!kg.isValid ())
    return false;

  jboolean enabled = kg.callMethod<jboolean> ("isKeyguardSecure", "()Z");
  if (!enabled)
    return false;

  QJniObject intent = kg.callObjectMethod ("createConfirmDeviceCredentialIntent",
                                           "(Ljava/lang/CharSequence;"
                                           "Ljava/lang/CharSequence;)"
                                           "Landroid/content/Intent;",
                                           QJniObject::fromString (tr ("Authenticate")).object (),
                                           QJniObject::fromString (tr ("Please enter your device credentials to use QPwmc.")).object ());
  if (!intent.isValid ())
    return false;

  QtAndroidPrivate::startActivity (intent.object<jobject> (),
                                   PwmdAndroidJNI::AuthActivity, this);
  return true;
}

void
PwmdAndroidJNI::showToast (const QString &s, int duration)
{
  QJniObject toast = QJniObject::fromString(s);
  QJniObject context = QNativeInterface::QAndroidApplication::context ();

  QJniObject::callStaticMethod<void>("net/sourceforge/qpwmc/jni/Util",
                                     "toast",
                                     "(Landroid/content/Context;"
                                     "Ljava/lang/String;I)V",
                                     context.object<jobject>(),
                                     toast.object<jstring> (),
                                     duration);
}

void
PwmdAndroidJNI::clearClipboard (int d)
{
  QJniObject context = QNativeInterface::QAndroidApplication::context ();
  QJniObject::callStaticMethod<void>("net/sourceforge/qpwmc/jni/Util",
                                     "clearClipboard",
                                     "(Landroid/content/Context;)V",
                                     context.object<jobject>());
  showToast (tr ("Clipboard cleared."), d);
}

void
PwmdAndroidJNI::setTimeout (int t)
{
  androidSocket.callMethod<void>("setTimeout", "(I)V", t);
}

bool
PwmdAndroidJNI::connectSocket (PwmdRemoteHost &hostData)
{
  cancelling = false;
  setTimeout (hostData.connectTimeout());
  jniThread.connectSocket (hostData);
  return true;
}

void
PwmdAndroidJNI::cancel ()
{
  cancelling = true;
  androidSocket.callMethod<void>("close");
  jniThread.wait ();
}

int
PwmdAndroidJNI::exceptionResult (int n, bool doEmit)
{
  int e = 0;

  switch (n)
    {
    case ErrorNoError:
      e = 0;
      break;
    case ErrorTimedOut:
      e = ETIMEDOUT;
      break;
    case ErrorRefused:
      e = ECONNREFUSED;
      break;
    case ErrorUnknownHost:
      e = EHOSTUNREACH;
      break;
    case ErrorIO:
      e = EREMOTEIO;
      break;
    case ErrorUnknownError:
      e = -1;
      break;
    }

  if (doEmit)
    emit (jniConnectionError (gpg_error_from_errno (e)));

  errno = e;
  return -1;
}

ssize_t
PwmdAndroidJNI::read (void *user, int fd, void *data, size_t len)
{
  Q_UNUSED(fd);
  PwmdAndroidJNI *jni = static_cast<PwmdAndroidJNI *>(user);
  QJniObject result = jni->androidSocket.callObjectMethod("read",
                                                          "(I)Lnet/sourceforge/qpwmc/jni/SocketReadResult;",
                                                          (jint)len);
  if (!result.isValid())
    {
      jint rc = jni->androidSocket.callMethod<jint>("error", "()I");
      return jni->exceptionResult (rc);
    }

  QJniObject dst = result.callObjectMethod("data", "()[B");
  QJniEnvironment env;
  jbyte *a = env->GetByteArrayElements(dst.object<jbyteArray>(), nullptr);
  jint nRead = result.callMethod<jint>("length", "()I");
  if (nRead > 0)
      memcpy (data, a, nRead);
  return nRead;
}

ssize_t
PwmdAndroidJNI::write (void *user, int fd, const void *data, size_t len)
{
  Q_UNUSED(fd);
  PwmdAndroidJNI *jni = static_cast<PwmdAndroidJNI *>(user);
  QJniEnvironment env;
  jbyteArray buf = env->NewByteArray(len);
  jbyte *dst = env->GetByteArrayElements(buf, nullptr);

  memcpy (dst, data, len*sizeof(jbyte));
  env->SetByteArrayRegion(buf, 0, len, dst);
  jint n = jni->androidSocket.callMethod<jint>("write",
                                               "([BI)I",
                                               buf, len);
  env->ReleaseByteArrayElements(buf, dst, 0);

  if (n == -1)
    {
      jint rc = jni->androidSocket.callMethod<jint>("error", "()I");
      (void)jni->exceptionResult (rc);
    }

  return n;
}

PwmdAndroidJNIThread::PwmdAndroidJNIThread (PwmdAndroidJNI *p)
{
  jni = p;
}

PwmdAndroidJNIThread::~PwmdAndroidJNIThread ()
{
  PwmdAndroidJNI::clearClipboard (0);
}

void
PwmdAndroidJNIThread::run ()
{
  QJniObject host = QJniObject::fromString(hostData.hostname());
  QJniObject alias = QJniObject::fromString(hostData.clientCertificateAlias());
  QJniObject context = QNativeInterface::QAndroidApplication::context ();
  jint fd = jni->androidSocket.callMethod<jint>("connect",
                                                "(Landroid/content/Context;"
                                                "Ljava/lang/String;"
                                                "Ljava/lang/String;I)I",
                                                context.object<jobject>(),
                                                alias.object<jstring>(),
                                                host.object<jstring>(),
                                                hostData.port());
  if (fd == -1)
    {
      jint rc = jni->androidSocket.callMethod<jint>("error", "()I");
      if (rc && !jni->cancelling)
        (void) jni->exceptionResult (rc, true);
      return;
    }

  emit jni->jniConnectionResult (hostData, fd);
}

void
PwmdAndroidJNIThread::connectSocket (PwmdRemoteHost &host)
{
  hostData = host;
  start ();
}

bool
PwmdAndroidJNI::checkPermissions (QFileDialog::AcceptMode mode)
{
  QString p = "android.permission.READ_EXTERNAL_STORAGE";

  if (mode & QFileDialog::AcceptSave)
    p = "android.permission.WRITE_EXTERNAL_STORAGE";

  QtAndroidPrivate::PermissionResult r;
  QFuture <QtAndroidPrivate::PermissionResult> f;
  f = QtAndroidPrivate::checkPermission (p);
  r = f.result ();
  if (r != QtAndroidPrivate::PermissionResult::Authorized)
    {
      f = QtAndroidPrivate::requestPermission (p);
      r = f.result ();
      if (r != QtAndroidPrivate::PermissionResult::Authorized)
        return false;
    }

  return true;
}

static void setClientCertificateAlias (JNIEnv *env, jobject thiz, jstring s)
{
  Q_UNUSED (env);
  Q_UNUSED (thiz);
  QJniObject obj(s);
  clientCertificateAliasLineEdit->setText (obj.toString());
}

static void registerNativeMethods ()
{
  static const JNINativeMethod m[] = {
        { "callNativeSetAlias", "(Ljava/lang/String;)V", reinterpret_cast<void *>(setClientCertificateAlias) }
  };

  QJniObject clazz("net/sourceforge/qpwmc/jni/ClientCertificateAliasResult");
  QJniEnvironment env;
  jclass obj = env->GetObjectClass(clazz.object<jobject>());
  if (!env->RegisterNatives(obj, m, sizeof (m)/sizeof (m[0])))
    {
      qWarning ("%s %i", __FILE__, __LINE__);
      return;
    }

  env->DeleteLocalRef(obj);
}

void
PwmdAndroidJNI::selectCertificateAlias (QLineEdit *w)
{
  registerNativeMethods();
  if (!QJniObject::isClassAvailable("net/sourceforge/qpwmc/jni/ClientCertificate"))
    return;

  clientCertificateAliasLineEdit = w;
  QJniObject keyChain;
  QNativeInterface::QAndroidApplication::runOnAndroidMainThread([&keyChain]() {
                                                            QJniObject context = QNativeInterface::QAndroidApplication::context ();
                                QJniObject alias = QJniObject::fromString(clientCertificateAliasLineEdit->text().toUtf8().data());
                                keyChain.callStaticMethod<void>("net/sourceforge/qpwmc/jni/ClientCertificate", "selectCertificate",
                                                                "(Landroid/content/Context;"
                                                                "Ljava/lang/String;"
                                                                ")V",
                                                                context.object<jobject>(),
                                                                alias.object<jstring>());
                                });
}
