/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDPASSWORDDIALOG_H
#define PWMDPASSWORDDIALOG_H

#include <QDialog>
#include "ui_pwmdPasswordDialog.h"

class PwmdPasswordDialog : public QDialog
{
 Q_OBJECT
 public:
   PwmdPasswordDialog (QWidget * = 0);
   ~PwmdPasswordDialog ();
   QString decryptKeyFile ();
   void setDecryptKeyFile (QString);
   void setEncryptKeyFile (QString);
   void setSignKeyFile (QString);
   QString encryptKeyFile ();
   QString signKeyFile ();
   bool useKeyFile ();
   void setSymmetric (bool = true);
   void setNewFile (bool = true);
   void setHasSigners (bool = true);

 private slots:
   void slotSelectDecryptKeyFile ();
   void slotSelectEncryptKeyFile ();
   void slotSelectSignKeyFile ();

 private: 
   Ui::PwmdPasswordDialog ui;
   bool _symmetric;
   bool _newFile;
   bool _signers;
};

#endif
