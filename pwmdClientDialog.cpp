/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QSettings>
#include <QCommonStyle>
#include <QTimer>
#include <QPushButton>
#include <QColorDialog>
#include <QDateTime>
#include <QSettings>
#include "pwmdClientDialog.h"
#include "pwmdMainWindow.h"
#include "cmdIds.h"

static QMutex listMutex;

PwmdClientDialog::PwmdClientDialog (Pwmd *h, const QStringList &invokers,
                                    const QString &current, PwmdMainWindow *p) :
  QDialog (p)
{
  ui.setupUi (this);
  QSettings cfg ("qpwmc");
  restoreGeometry (cfg.value ("clientListGeometry").toByteArray ());
  ui.clientList->header()->restoreState (cfg.value ("clientListHeaderState").toByteArray ());
  ui.sp_cleanup->setValue (cfg.value ("clientListCleanup").toInt());
  staleColor = cfg.value ("permissionsColor", "Green").value < QColor > ();
  bool b = cfg.value ("buttonLabels", false).toBool ();
  ui.tb_killClient->setToolButtonStyle (b ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly);
  ui.tb_refreshClientList->setToolButtonStyle (b ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly);
  QCommonStyle style;
  pwm = h;
  connect (pwm, SIGNAL (busy (int, bool)), this, SLOT (slotBusy (int, bool)));
  connect (pwm, SIGNAL (statusMessage (QString, void *)), this,
	   SLOT (slotStatusMessage (QString, void *)));
  connect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));
  connect (pwm, SIGNAL (stateChanged (Pwmd::ConnectionState)), this,
	   SLOT (slotConnectionStateChanged (Pwmd::ConnectionState)));
  connect (ui.tb_killClient, SIGNAL (clicked ()), SLOT (slotKillClient ()));
  ui.tb_killClient->setIcon (QIcon (":icons/liftarn-Fly-swatter.svg"));
  connect (ui.tb_refreshClientList, SIGNAL (clicked ()),
           SLOT (slotRefreshClientList ()));
  connect (ui.clientList, SIGNAL (currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)), this, SLOT (slotClientItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)));
  ui.tb_refreshClientList->setIcon (QIcon (":icons/matt-icons_view-refresh.svg"));
  connect (p, SIGNAL (mainClientLockStateChanged (bool)), this,
                      SLOT (slotMainClientLockStateChanged (bool)));

  uint32_t v;
  pwmd_getopt (pwm->handle (), PWMD_OPTION_SERVER_VERSION, &v);
  ui.l_version->setText (QString::asprintf ("%u.%u.%u",
                                            (v >> 16) & 0xff,
                                            (v >> 8) & 0xff,
                                            v & 0xff));

  invokingUser = invokers;
  connectedUser = current;

  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdClientState, "OPTION",
                                          Pwmd::inquireCallback,
                                          new PwmdInquireData ("CLIENT-STATE=1")), true);

  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdClientNCache, "GETINFO",
                                          Pwmd::inquireCallback,
                                          new PwmdInquireData ("--data CACHE")),  true);

  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdClientNClients, "GETINFO",
                                          Pwmd::inquireCallback,
                                          new PwmdInquireData ("--data CLIENTS")), true);

  // Run the command queue
  slotRefreshClientList ();

  staleTimer = new QTimer (this);
  connect (staleTimer, SIGNAL (timeout ()), SLOT (slotRemoveStaleClients ()));
#ifdef Q_OS_ANDROID
  showMaximized();
#endif
}

PwmdClientDialog::~PwmdClientDialog ()
{
  QSettings cfg ("qpwmc");

  listMutex.lock ();
  cfg.setValue ("clientListGeometry", saveGeometry ());
  cfg.setValue ("clientListHeaderState", ui.clientList->header()->saveState());
  cfg.setValue ("clientListCleanup", ui.sp_cleanup->value());
  clearClientList (true);
  listMutex.unlock ();
}

void
PwmdClientDialog::slotMainClientLockStateChanged (bool)
{
  refreshClientList ();
}

void
PwmdClientDialog::slotRemoveStaleClients ()
{
  time_t now = time (nullptr);
  int interval = ui.sp_cleanup->value ();

  listMutex.lock ();
  foreach (QTreeWidgetItem * item, ui.clientList->findItems ("*",
                                                             Qt::MatchRecursive |
                                                             Qt::MatchWildcard))
    {
      ClientInfo *data = qvariant_cast<ClientInfo *>(item->data (0, Qt::UserRole));

      if (data->stale () && now - data->stale () >= interval)
        {
          int n = ui.clientList->indexOfTopLevelItem (item);
          ui.clientList->takeTopLevelItem (n);
          n = staleList.indexOf (item);
          staleList.removeAt (n);
          delete data;
          delete item;
        }
    }
  listMutex.unlock ();
}

void
PwmdClientDialog::slotStatusMessage (QString line, void *data)
{
  (void)data;
  QStringList l = QString (line).split (" ");

  if (l.at (0) == "CLIENTS")
    {
      ui.l_clients->setText (line.mid (8));

      // Only update if a client has disconnected
      listMutex.lock ();
      unsigned n = (unsigned)ui.clientList->topLevelItemCount ();
      listMutex.unlock ();
      if (l.at (1).toUInt() < n)
        refreshClientList ();
    }
  else if (l.at (0) == "STATE")
    {
      updateStateForClient (line.mid(6));
    }
  else if (line.contains ("CACHE"))
    ui.l_cached->setText (line.mid (6));
}

void
PwmdClientDialog::slotConnectionStateChanged (Pwmd::ConnectionState s)
{
  if (s == Pwmd::Init)
    disable ();
  else
    {
      disable (false);
      refreshClientList ();
    }
}

void
PwmdClientDialog::disable (bool b)
{
  listMutex.lock ();
  if (b)
    clearClientList (true);

  ui.tb_killClient->setEnabled (!b);
  ui.tb_refreshClientList->setEnabled (!b);
  ui.serverStatus->setEnabled (!b);
  listMutex.unlock ();
}

void
PwmdClientDialog::slotBusy (int, bool b)
{
  static int refcount;

  refcount += b ? 1 : -1;
  if (refcount < 0)
    refcount = 0;

  setClientState (!refcount ? ClientInfo::Idle: ClientInfo::Command);
}

void
PwmdClientDialog::slotCommandResult (PwmdCommandQueueItem *item,
                                     QString result, gpg_error_t rc,
                                     bool queued)
{
  if (item->id () < PwmdCmdIdSaveMax || item->id () >= PwmdCmdIdClientMax)
    {
      if (item->id () != PwmdCmdIdFileLock)
        {
          item->setSeen ();
          return;
        }
    }

  switch (item->id ())
    {
    case PwmdCmdIdFileLock:
      setLocked (rc == 0);
      break;
    case PwmdCmdIdClientList:
      if (!rc)
        refreshClientListFinalize (result);
      break;
    case PwmdCmdIdClientKill:
      break;
    case PwmdCmdIdClientState:
      break;
    case PwmdCmdIdClientNClients:
      if (!rc)
        ui.l_clients->setText (result);
      break;
    case PwmdCmdIdClientNCache:
      if (!rc)
        ui.l_cached->setText (result);
      break;
    default:
      break;
    }

  if (rc && !item->checkError (rc) && !queued)
    Pwmd::showError (rc, pwm);

  item->setSeen ();
}

void
PwmdClientDialog::slotClientItemChanged (QTreeWidgetItem *item, QTreeWidgetItem *old)
{
  (void)old;

  if (!item)
    {
      ui.tb_killClient->setEnabled (false);
      return;
    }

  ClientInfo *ci = qvariant_cast <ClientInfo *>(item->data (0, Qt::UserRole));
  bool b = ci && (invokingUser.indexOf (connectedUser) != -1
                  || connectedUser == ci->userId ()
                  || connectedUser == ci->username ());

  ui.tb_killClient->setEnabled (b && ci && !ci->stale ());
}

void
PwmdClientDialog::slotKillClient ()
{
  listMutex.lock ();
  QTreeWidgetItem *item = ui.clientList->currentItem ();
  if (!item)
    {
      listMutex.unlock ();
      return;
    }

  ClientInfo *ci = qvariant_cast <ClientInfo *>(item->data (0, Qt::UserRole));
  QString id = ci->id ();
  listMutex.unlock ();
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdClientKill, QString ("KILL %1").arg (id)));
}

void
PwmdClientDialog::setLocked (bool b)
{
  listMutex.lock ();
  foreach (QTreeWidgetItem *item, ui.clientList->findItems ("*",
                                                            Qt::MatchRecursive |
                                                            Qt::MatchWildcard))
    {
      ClientInfo *data = qvariant_cast<ClientInfo *>(item->data (0, Qt::UserRole));
      if (data->self ())
        {
          data->setLocked (b);
          item->setText (PwmdClientDialog::Lock,
                         data->locked () ? tr ("true") : tr ("false"));
          break;
        }
    }
  listMutex.unlock ();
}

void
PwmdClientDialog::setClientState (unsigned s)
{
  listMutex.lock ();
  foreach (QTreeWidgetItem *item, ui.clientList->findItems ("*",
                                                            Qt::MatchRecursive |
                                                            Qt::MatchWildcard))
    {
      ClientInfo *data = qvariant_cast<ClientInfo *>(item->data (0, Qt::UserRole));
      if (data->self ())
        {
          data->setState (s);
          item->setText (PwmdClientDialog::State, clientState (data->state ()));
          break;
        }
    }
  listMutex.unlock ();
}

QString
PwmdClientDialog::clientState (unsigned n)
{
  switch (n)
    {
    case ClientInfo::Init:
      return tr ("Init");
    case ClientInfo::Idle:
      return tr ("Idle");
    case ClientInfo::Command:
      return tr ("In command");
    case ClientInfo::Disconnect:
      return tr ("Disconnecting");
    default:
      break;
    }

  return tr ("Unknown");
}

QTreeWidgetItem *
PwmdClientDialog::buildClientStateItem (const QString &line,
                                        QTreeWidgetItem *item)
{
  if (!item)
    item = new QTreeWidgetItem ();

  ClientInfo *ci = ClientInfo::parseLine (line);

  item->setText (PwmdClientDialog::Name, ci->name());
  item->setText (PwmdClientDialog::Time, QDateTime::fromSecsSinceEpoch (ci->connectedAt()).toString ("hh:mm:ss MM/dd/yy"));
  item->setText (PwmdClientDialog::File, ci->filename());
  item->setText (PwmdClientDialog::Lock,
                 ci->locked () ? tr ("true") : tr ("false"));

  item->setText (PwmdClientDialog::State, clientState (ci->state ()));
  if (ci->state() == ClientInfo::Disconnect && ui.sp_cleanup->value ())
    {
      ci->setStale (time(nullptr));
      QBrush b = item->foreground (0);
      b.setColor (staleColor);
      for (int i = 0; i <= PwmdClientDialog::MaxFields; i++)
        item->setForeground (i, b);
    }

  item->setText (PwmdClientDialog::Host, ci->host());

  if (ci->userId ().at(0) != '#')
    item->setText (PwmdClientDialog::Uid, ci->username() + "(" + ci->userId() + ")");
  else
    item->setText (PwmdClientDialog::Uid, ci->userId ());

  item->setData (0, Qt::UserRole, QVariant::fromValue (ci));
  item->setDisabled (ci->self());

  if (ci->stale ())
    staleList.append (item);
  return item;
}

void
PwmdClientDialog::updateStateForClient (const QString &line)
{
  listMutex.lock ();
  QList <QTreeWidgetItem *> list = ui.clientList->findItems ("*", Qt::MatchWildcard);
  int i;
  QTreeWidgetItem *item = nullptr, *match = nullptr;
  QStringList fields = line.split (" ");

  for (i = 0; i < list.count(); i++)
    {
      item = list.at (i);
      ClientInfo *ci = qvariant_cast <ClientInfo *>(item->data (0, Qt::UserRole));

      if (ci->id() == fields.at (0) && !ci->stale ())
        {
          match = item;
          break;
        }
    }

  if (match)
    {
      ClientInfo *data = qvariant_cast<ClientInfo *>(match->data (0, Qt::UserRole));
      if (!data->stale ())
        delete data;
    }

  if (!line.isEmpty())
    {
      item = buildClientStateItem (line, match);
      if (item != match)
        ui.clientList->addTopLevelItem (item);
    }

  listMutex.unlock ();
}

void
PwmdClientDialog::clearClientList (bool stale)
{
  foreach (QTreeWidgetItem * item, ui.clientList->findItems ("*",
                                                             Qt::MatchRecursive |
                                                             Qt::MatchWildcard))
    {
      ClientInfo *data = qvariant_cast<ClientInfo *>(item->data (0, Qt::UserRole));
      if (stale || !data->stale ())
        {
          item->setData (0, Qt::UserRole, QVariant::fromValue (nullptr));
          delete data;
        }
      else
        {
          int n = ui.clientList->indexOfTopLevelItem (item);
          ui.clientList->takeTopLevelItem (n);
        }
    }

  if (stale)
    staleList.clear ();

  ui.clientList->clear();
}

void
PwmdClientDialog::refreshClientList ()
{
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdClientList, "GETINFO",
                                          nullptr,
                                          new PwmdInquireData ("--verbose CLIENTS")));
}

void
PwmdClientDialog::refreshClientListFinalize (const QString &result)
{
  listMutex.lock ();
  QList <QTreeWidgetItem *>items;
  QStringList list = result.split ("\n");
  unsigned t = list.count();
  QTreeWidgetItem *match = nullptr;
  QTreeWidgetItem *cur = ui.clientList->currentItem ();
  ClientInfo *cdata = cur ? qvariant_cast<ClientInfo *> (cur->data (0, Qt::UserRole)) : nullptr;

  for (unsigned i = 0; i < t; i++)
    {
      if (list.at (i).isEmpty())
        continue;

      QTreeWidgetItem *item = buildClientStateItem (list.at (i));
      if (item)
        {
          ClientInfo *data = qvariant_cast<ClientInfo *> (item->data (0, Qt::UserRole));
          if (data && cdata && data->id () == cdata->id ())
            match = item;

          items.append (item);
        }
    }

  t = staleList.count();
  for (unsigned i = 0; i < t; i++)
    items.append (staleList.at (i));

  clearClientList ();
  ui.clientList->addTopLevelItems (items);
  ui.tb_killClient->setEnabled (false);
  staleTimer->start (1000);

  if (match)
    ui.clientList->setCurrentItem (match);

  listMutex.unlock ();
}

void
PwmdClientDialog::slotRefreshClientList ()
{
  refreshClientList ();
}
