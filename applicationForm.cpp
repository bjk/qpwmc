/*
    Copyright (C) 2013-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QButtonGroup>
#include <QPushButton>
#include <QLineEdit>
#include <QSpinBox>
#include <QMessageBox>
#include <QCalendarWidget>

#include "pwmdMainWindow.h"
#include "pwmdSocketDialog.h"
#include "applicationForm.h"
#include "applicationFormXmlStream.h"
#include "applicationFormRadioButton.h"
#include "applicationFormPage.h"
#include "applicationFormFileSelector.h"
#include "applicationFormFinalizePage.h"
#include "applicationFormGeneratePassword.h"
#include "applicationFormExpiryButton.h"
#include "applicationFormDateSelector.h"
#include "pwmdExpireDialog.h"

ApplicationForm::ApplicationForm (Pwmd *pwmHandle, const QString &filename,
                                  PwmdTreeWidget *w, PwmdMainWindow *p)
     : QWizard ()
{
  parent = p;
  elementTree = w;
  _modified = false;
  QAbstractButton *pb = button (QWizard::CancelButton);
  connect (pb, SIGNAL (clicked (bool)), this, SLOT (slotCancel (bool)));
  if (elementTree)
    {
      connect (elementTree,
               SIGNAL (currentItemChanged (QTreeWidgetItem *, QTreeWidgetItem *)),
               this,
               SLOT (slotFormElementSelected (QTreeWidgetItem *, QTreeWidgetItem *)));
    }

  pwm = pwmHandle;
  doForm (filename);
}

void
ApplicationForm::setModified ()
{
  _modified = true;
}

bool
ApplicationForm::modified ()
{
  return _modified;
}

void
ApplicationForm::slotCancel (bool)
{
  setHasError ();
}

ApplicationForm::ApplicationForm (const QString &filename,
                                  QString socket, QString dataFilename,
                                  QString path) : QWizard ()
{
  QAbstractButton *pb = button (QWizard::CancelButton);
  connect (pb, SIGNAL (clicked (bool)), this, SLOT (slotCancel (bool)));
  elementTree = nullptr;
  parent = nullptr;
  pwm = nullptr;
  _socket = socket;
  _dataFilename = dataFilename;
  _elementPath = path;
  doForm (filename);
}

ApplicationForm::ApplicationForm (const QString &filename) : QWizard ()
{
  QAbstractButton *pb = button (QWizard::CancelButton);
  connect (pb, SIGNAL (clicked (bool)), this, SLOT (slotCancel (bool)));
  parent = nullptr;
  elementTree = nullptr;
  pwm = nullptr;
  doForm (filename);
}

void
ApplicationForm::slotAccepted ()
{
}

void
ApplicationForm::slotFormElementSelected (QTreeWidgetItem * item,
                                          QTreeWidgetItem * old)
{
  (void)old;
  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->id () == "pwmdRootElement")
        {
          w->setValue (parent->elementPath (item, "<TAB>"));
          break;
        }
    }
}

void
ApplicationForm::addFormRow (QFormLayout *fl, QHBoxLayout *hb,
                             ApplicationFormWidget *w, QWizardPage *wp,
                             bool withLabel)
{
  if (w->isRequired () && w->widget ())
    {
      connect (w->widget (), SIGNAL (textChanged (const QString &)), wp,
               SIGNAL (completeChanged ()));
    }

  QLabel *l = new QLabel (w->label ());
  l->setWhatsThis (w->whatsThis ());
  widgetsToDelete.append (l);

  if (hb)
    fl->addRow (withLabel ? l : 0, hb);
  else
    fl->addRow (withLabel ? l : 0, w->widget ());
}

void
ApplicationForm::setHasError (bool b)
{
  _error = b;
}

bool
ApplicationForm::hasError ()
{
  return _error;
}

void
ApplicationForm::slotPageChanged (int n)
{
  if (n == 2)
    finalPage->commit ();
}

void
ApplicationForm::slotChangeExpiry ()
{
  QWidget *fw = QApplication::focusWidget ();
  ApplicationFormExpiryButton *pb = static_cast<ApplicationFormExpiryButton*>(fw);
  ApplicationFormWidget *w = pb->formWidget ();
  PwmdExpireDialog *d = new PwmdExpireDialog (w->expiryDate (),
                                              w->expiryAge (), this);

  if (d->exec ())
    w->setExpiry (d->expire (), d->increment ());

  delete d;
}

void
ApplicationForm::addExpiry (ApplicationFormWidget *w, QHBoxLayout *hb)
{
  if (hb && w && w->expiry ())
    {
      ApplicationFormExpiryButton *pb = new ApplicationFormExpiryButton (w);

      pb->setIcon (QIcon (":icons/expiry.svg"));
      hb->addWidget (pb);
      connect (pb, SIGNAL (clicked ()), this, SLOT (slotChangeExpiry ()));
    }
}

void
ApplicationForm::slotDateSelector (const QDate &d)
{
  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->dateSelector ())
        {
          ApplicationFormDateSelector *s = qobject_cast<ApplicationFormDateSelector *> (w->widget ());
          s->setDate (d);
        }
    }
}

void
ApplicationForm::doForm (const QString &filename)
{
  setWindowFlags (Qt::WindowTitleHint);
  setWindowTitle (tr ("Pwmd Application Form"));
  ui.setupUi (this);
  stream = nullptr;
  _error = false;
  finalPage = nullptr;
  QRegularExpression rx ("^[!]?[^!\\s][^\\s]*");
  lineEditValidator = new QRegularExpressionValidator (rx, this);

  connect (this, SIGNAL (currentIdChanged (int)), this, SLOT (slotPageChanged (int)));
  connect (this, SIGNAL (accepted ()), this, SLOT (slotAccepted ()));

  stream = new ApplicationFormXmlStream (filename, this);
  if (stream->error () != QXmlStreamReader::NoError)
    {
      _error = true;
      return;
    }

  QList<ApplicationFormWidget *> widgets = stream->widgets ();
  int radioButtonId = 0;
  ApplicationFormPage *wp = new ApplicationFormPage ();
  QFormLayout *fl = new QFormLayout (wp);

  wp->setWidgets (widgets);

  foreach (ApplicationFormWidget *w, widgets)
    {
      QHBoxLayout *hb = new QHBoxLayout ();
      bool withLabel = true;

      if (w->type () == ApplicationFormWidget::AppWidgetRadioButton)
        {
          QButtonGroup *bg = new QButtonGroup ();
          QStringList rbs = w->radioButtons ();

          widgetsToDelete.append (bg);

          for (int n = 0; n < rbs.count (); n++)
            {
              ApplicationFormRadioButton *rb = new ApplicationFormRadioButton (rbs.at (n));

              bg->addButton (rb);
              hb->addWidget (rb);
              rb->setId (radioButtonId);
              w->addRadioButtonId (radioButtonId++);
              connect (rb, SIGNAL (clicked (bool)), this,
                       SLOT (slotRadioButtonClicked (bool)));

              if (w->value () == rbs.at (n))
                rb->setChecked (true);
            }
        }

      if (w->elementSelector ())
        {
          if (elementTree)
            {
              hb->addWidget (elementTree);
              addFormRow (fl, hb, w, wp, true);
              hb = new QHBoxLayout ();
              hb->addWidget (w->widget ());
              QLineEdit *le = static_cast <QLineEdit *> (w->widget ());
              le->setPlaceholderText (tr ("Enter or select from above..."));
              withLabel = false;
            }
          else
            {
              QPushButton *pb = new QPushButton ();
              pb->setText (tr ("Select"));
              hb->addWidget (w->widget ());
              hb->addWidget (pb);
              if (w->id () == "pwmdSocket")
                connect (pb, SIGNAL (clicked ()), this,
                         SLOT (slotSocketSelector ()));
              else
                connect (pb, SIGNAL (clicked ()), this,
                         SLOT (slotElementSelector ()));
            }
        }
      else if (w->dateSelector ())
        {
          QCalendarWidget *cal = new QCalendarWidget ();
          connect (cal, SIGNAL (clicked (const QDate &)), this,
                   SLOT (slotDateSelector (const QDate &)));
          hb->addWidget (cal);
          addFormRow (fl, hb, w, wp, true);
          hb = new QHBoxLayout ();
          hb->addWidget (w->widget ());
          ApplicationFormDateSelector *le = static_cast <ApplicationFormDateSelector*> (w->widget ());
          le->setText (w->value ());
          withLabel = false;
        }
      else if (w->fileSelector ())
        {
          ApplicationFormFileSelector *pb = new ApplicationFormFileSelector (w->widget (), this);
          pb->setText (tr ("Select"));
          hb->addWidget (w->widget ());
          hb->addWidget (pb);
          connect (pb, SIGNAL (clicked ()), pb, SLOT (slotFileSelector ()));
        }
      else if (w->passwordGenerator ())
        {
          ApplicationFormGeneratePassword *pb = new ApplicationFormGeneratePassword (w, this);
          pb->setText (tr ("Generate"));
          hb->addWidget (w->widget ());
          hb->addWidget (pb);
          connect (pb, SIGNAL (clicked ()), pb, SLOT (slotGeneratePassword ()));
        }
      else if (w->widget ())
        hb->addWidget (w->widget ());

      if (w->type () == ApplicationFormWidget::AppWidgetLineEdit)
        {
          /* This QLineEdit creates an element from its value. Not a perfect
           *  RE, though. */
          if (w->pwmdName ().isEmpty ())
            {
              QLineEdit *le = static_cast<QLineEdit *>(w->widget ());
              le->setValidator (lineEditValidator);
            }
        }

      addExpiry (w, hb);
      addFormRow (fl, hb, w, wp, withLabel);
    }

  wp->setTitle (ui.wz_start->title ());
  wp->setSubTitle (ui.wz_start->subTitle ());

  if (elementTree)
    slotFormElementSelected (elementTree->currentItem (), nullptr);

  slotDateSelector (QDate::currentDate ());
  addPage (wp);
  finalPage = new ApplicationFormFinalizePage ();
  finalPage->setTitle (ui.wz_start->title ());
  finalPage->setSubTitle (ui.wz_start->subTitle ());
  finalPage->setHandle (pwm);
  updateForm (_dataFilename, _elementPath, _socket);
  addPage (finalPage);
}

QString
ApplicationForm::socket ()
{
  QString s = QString ();

  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->id () == "pwmdSocket")
        {
          QLineEdit *le = static_cast<QLineEdit *>(w->widget ());

          w->setValue (le->text ());
          s = w->value ();
        }
    }

  return s;
}

QString
ApplicationForm::filename ()
{
  QString s = QString ();

  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->id () == "pwmdFilename")
        {
          QLineEdit *le = static_cast<QLineEdit *>(w->widget ());

          w->setValue (le->text ());
          s = w->value ();
        }
    }

  return s;
}

QString
ApplicationForm::elementPath ()
{
  QString s = QString ();

  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->id () == "pwmdRootElement")
        s = w->value ();
    }

  return s;
}

void
ApplicationForm::updateForm (QString filename, QString element, QString sock)
{
  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->id () == "pwmdFilename" && !filename.isEmpty ())
        w->setValue (filename);
      else if (w->id () == "pwmdRootElement" && !element.isEmpty ())
        w->setValue (element.replace ("\t", "<TAB>"));
      else if (w->id () == "pwmdSocket" && !sock.isEmpty ())
        w->setValue (sock);
    }
}

void
ApplicationForm::keyPressEvent (QKeyEvent *ev)
{
  if (ev->key () == Qt::Key_Escape)
    setHasError ();

  QDialog::keyPressEvent (ev);
}

void
ApplicationForm::slotKnownHostCallback (void *data, const char *host,
                                        const char *key, size_t len)
{
  gpg_error_t rc = Pwmd::knownHostPrompt (data, host, key, len);
  setHasError (rc != 0);
  emit knownHostRc (rc);
}

void
ApplicationForm::slotElementSelector ()
{
  QString result;
  bool notFound = false;
  QString filename = buildElementPath (QString (), "pwmdFilename", notFound);
  QString rootElement = buildElementPath (QString (), "pwmdRootElement",
                                          notFound);
  QString sock = buildElementPath (QString (), "pwmdSocket", notFound);
  int n;
  bool ownHandle = false;

  if (!pwm)
    {
      ownHandle =  true;
      pwm = new Pwmd (0, 0, 0, this);
      connect (pwm, SIGNAL (knownHost (void *, const char *, const char *, size_t)), this, SLOT (slotKnownHostCallback (void *, const char *, const char *, size_t)));
    }

  if (pwm->spawnEditor (result, sock, filename, rootElement, false))
    {
      sock = Pwmd::extractToken (result, "<SOCKET>", n);
      filename = Pwmd::extractToken (result, "<FILE>", n);
      rootElement = Pwmd::extractToken (result, "<PATH>", n);
      updateForm (filename, rootElement, sock);
    }

  if (ownHandle)
    {
      delete pwm;
      pwm = nullptr;
    }
}

void
ApplicationForm::slotSocketSelector ()
{
  QString sock = QString ();

  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->id () == "pwmdSocket")
        {
          sock = w->value ();
          break;
        }
    }

  PwmdSocketDialog *d = new PwmdSocketDialog (sock, this);
  d->setParent (this, Qt::Dialog);
  if (d->exec ())
    updateForm (QString (), QString (), d->socket ());

  delete d;
}

void
ApplicationForm::slotRadioButtonClicked (bool b)
{
  (void)b;
  QWidget *fw = QApplication::focusWidget ();
  ApplicationFormRadioButton *rb = static_cast<ApplicationFormRadioButton *>(fw);

  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->type () == ApplicationFormWidget::AppWidgetRadioButton)
        {
          if (w->hasRadioButtonId (rb->id ()))
            {
              w->setValue (rb->text ());
              return;
            }
        }
    }
}

ApplicationForm::~ApplicationForm ()
{
  if (stream)
    delete stream;

  if (finalPage)
    delete finalPage;

  foreach (QObject *w, widgetsToDelete)
    {
      delete w;
    }

  if (lineEditValidator)
    delete lineEditValidator;
}

QString
ApplicationForm::buildElementPath (QString path, QString element,
                                   bool &notFound)
{
  foreach (ApplicationFormWidget *w, stream->widgets ())
    {
      if (w->id () == element)
        {
          if (!w->childOf ().isEmpty ())
            {
              path = buildElementPath (path, w->childOf (), notFound);
              if (path.isEmpty () && w->childOf () != "pwmdRootElement")
                {
                  qWarning ("%s: could not find parent '%s'\n",
                            element.toUtf8 ().data (),
                            w->childOf ().toUtf8 ().data ());
                  notFound = true;
                  return QString ();
                }

              if (!w->pwmdName ().isEmpty ())
                {
                  if (!path.isEmpty ())
                    path.append ("\t");
                  path.append (w->pwmdName ());
                }
              else if (!w->value ().isEmpty ())
                {
                  if (!path.isEmpty ())
                    path.append ("\t");
                  path.append (w->value ());
                }
            }
          else
            {
              if (!w->pwmdName ().isEmpty ())
                path.append (w->pwmdName ());

              if (!w->value ().isEmpty ())
                {
                  if (!w->pwmdName ().isEmpty ())
                    path.append ("\t");

                  path.append (w->value ());
                }
            }

          return path;
        }
    }

  return path;
}
