/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDMAINWINDOW_H
#define PWMDMAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QListWidget>
#include <QValidator>
#include <QStringList>
#include <QClipboard>
#include <QToolBar>
#include <QProgressBar>
#include <QPushButton>
#include <QUrl>

#include "pwmd.h"
#include "pwmdRemoteHost.h"
#include "pwmdClientDialog.h"
#include "pwmdOptionsDialog.h"
#include "pwmdFileOptionsDialog.h"
#include "pwmdOpenDialog.h"
#include "pwmdSearchDialog.h"
#ifdef Q_OS_ANDROID
#include "pwmdAndroidJNI.h"
#endif
#include "ui_pwmdMainWindow.h"

class ClientInfo;
class PwmdFileOptionsWidget;

class PwmdMainWindow : public QMainWindow
{
 Q_OBJECT
 public:
  // The 'socket' parameter may be a PwmdRemoteHost that has been
  // previously created in the Options and is the label of the
  // configured host. If not found then it is treated as a local socket.
  PwmdMainWindow(const QString & filename = 0,
	      const QString & name = "qpwmc", bool lock = true,
	      const QString & path = 0, const QString & socket = 0,
	      QWidget * parent = 0);
  ~PwmdMainWindow();

  // Returns the element path of the selected item in the element tree
  // with each element separated by the specified character.
  QString elementPath (const QString & sep = "\t");

  // Returns the opened data filename.
  QString filename ();

  // Returns the URL of the connected socket.
  QString socket ();

 private slots:
  void slotClientLockStateChanged (bool);
  void slotElementSelected (QTreeWidgetItem *, QTreeWidgetItem *);
  void slotElementClicked (QTreeWidgetItem *, int);
  void slotToggleHiddenContent (bool);
  void slotRefreshElementTree ();
  void slotContentChanged ();
  void slotContentModified (bool);
  void slotNewElement ();
  void slotNewRootElement ();
  void slotNewElementFromPopup ();
  void slotNewSiblingElement ();
  void slotNewSiblingElementFromPopup ();
  void slotDeleteElement ();
  void slotRenameElement ();
  void slotSearchShiftEnterPressed ();
  void slotAdvancedSearchAction ();
  void slotAdvancedSearch (const QString &);
  void slotAdvancedSearchNext (const QString &, bool, bool, bool, bool, bool);
  void slotAdvancedSearchFinished (int);
  void slotFindElement ();
  void slotFindTextChanged (const QString &);
  void slotFindFinished ();
  void slotEditElements ();
  void slotResolveTargets ();
  void slotExpandAll (bool);
  void slotReload ();
  void slotDndCopyElement ();
  void slotDndMoveElement ();
  void slotDndCreateTarget ();
  void slotExpandElement ();
  void slotCollapseElement ();
  void slotRenameElementUpdate (QWidget *);
  void slotElementEntered (QTreeWidgetItem *, int);
  void slotElementEditorClosed (QWidget *,
      			  QAbstractItemDelegate::EndEditHint);
  void slotConnectionStateChanged (Pwmd::ConnectionState);
  void slotInsertContent ();
  void slotRefreshContent ();
  void slotSaveToFile ();
  void slotCopyContentToClipboard ();
  void slotSaveContent ();
  void slotToggleBase64Content (bool);
  void slotMenuItemHovered (QAction *);
  void slotStatusMessage (QString, void *);
  void slotBusy (int, bool);
  void slotSocketOptions ();
  void slotGeneralOptions ();
  void slotClientList ();
  void slotConnectSocket ();
  void slotDisconnectSocket ();
  void slotOpenFile ();
  void slotOpenFileDone (int);
  void slotOpenForm ();
  void slotCloseFile ();
  void slotDoOpenFile ();
  void slotEditAttributes ();
  void slotEditTargetAttributes (bool);
  void slotRefreshAttributes ();
  void slotAttrSearchAndReplace ();
  void slotAttrSearchAndReplaceChanged (const QString &);
  void slotAttributeSelected (QListWidgetItem * item, QListWidgetItem * old);
  void slotAttributeEditorClosed (QWidget *,
      			    QAbstractItemDelegate::EndEditHint);
  void slotConfirmNewAttribute (QWidget *);
  void slotNewAttribute ();
  void slotDeleteAttribute ();
  void setElementColors (QTreeWidgetItem *, bool reset = true,
                         bool target = false);
  void slotTabChanged (int);
  void slotClientListDone (int);
  void slotGeneralOptionsDone (int);
  void slotButtonLabels (bool);
  void slotSaveDocument ();
  void slotSaveExtended ();
  void slotPasswordContentChanged ();
  void slotClipboardAttribute ();
  void slotClipboardElementPath ();
  void slotClearClipboard ();
  void slotClipboardTimer ();
  void slotReleaseTimer();
  void slotExecuteCommand ();
  void slotCommandContentChanged ();
  void slotCommandResultContentChanged ();
  void slotCommandAnchorClicked (const QUrl &);
  void slotCommandHelp ();
  void slotClearCommand ();
  void slotCommandInquireStateChanged (int);
  void slotCommandInquireFilename ();
  void slotSearchCommandResults ();
  void slotCommandHistoryChanged (int);
  void slotSaveCommandResult ();
  void slotEditContent ();
  void slotCreatePasswordAttribute ();
  void slotFileOptions ();
  void slotFileOptionsDone (int);
  void slotVersion ();
  void slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool);
  void slotKnownHostCallback (void *data, const char *host, const char *key,
                              size_t len);
  void slotUpdateConnectTimer ();
  void slotCancel ();
  void slotChangeExpire ();
  void slotItemSelectionChanged ();
#ifdef Q_OS_ANDROID
  void slotAndroidConnectionError (gpg_error_t);
  void slotAndroidConnectResult (PwmdRemoteHost, int);
  void slotAndroidConnectReady (PwmdRemoteHost);
  void slotAndroidAuthenticated ();
  void slotApplicationStateChanged (Qt::ApplicationState);
#endif
  void slotToggleMultiMode (bool);
  void showError (gpg_error_t);

 signals:
  void knownHostRc (gpg_error_t);
  void attributesRetrieved (QTreeWidgetItem *);
  void mainClientLockStateChanged (bool);

 private:
  friend class PwmdFileOptionsDialog;

  typedef struct {
      QString content;
      QString name;
      QString password;
      QTreeWidgetItem *item;
      QListWidgetItem *listItem;
  } AttributeFinalizeT;

  typedef enum
  {
    ButtonStateOptions,
    ButtonStateOpenFile,
    ButtonStateSave,
    ButtonStateMain
  } MainButtonState;

  friend class PwmdTreeWidget;
  friend class PwmdListWidget;
  friend class ApplicationForm;
  friend class PwmdPlainTextEdit;

/* Must match the order of buttons in a QToolBar. See setupToolbars(). */
  enum {
      FileToolBarOpen = 0,
      FileToolBarSave = 1,
      ElementToolBarCreate = 0,
      ElementToolBarDelete = 1,
      ElementToolBarRename = 2,
      ElementToolBarFind = 3,
      ContentToolBarInsert = 0,
      ContentToolBarSave = 1,
      ContentToolBarPassword = 2,
      ContentToolBarExpiry = 3,
      ContentToolBarOk = 5,
      ContentToolBarRefresh = 6,
      AttributeToolBarNew = 0,
      AttributeToolBarDelete = 1,
      AttributeToolBarOk = 3,
      AttributeToolBarRefresh = 4,
      PasswordToolBarInsert = 0,
      PasswordToolBarSave = 1,
      PasswordToolBarExpiry = 2,
      PasswordToolBarOk = 4,
      PasswordToolBarRefresh = 5,
      CommandToolBarSave = 0,
      CommandToolBarClear = 1,
      CommandToolBarOk = 3,
      CommandToolBarRefresh = 4,
      CommandToolBarHelp = 6
  };

  void fixupEditContentOrAttributes (bool attrs = false);
  void fixupNextTabOrElement ();
  void setContent (const QString &, QTreeWidgetItem * = nullptr);
  void setAttributeContent (const QString &);
  QString elementPath (const QTreeWidgetItem *item, const QString & sep = "\t");
  QList < QTreeWidgetItem * >buildElementTree (QByteArray &sexp, bool = true);
  QList < QListWidgetItem * >buildFilenameTree (gpg_error_t &);
  bool createAttributeCache (QTreeWidgetItem *, const QString &);
  bool doSaveContent (bool queue = false, bool save = false,
                      bool exteneded = false, int *which = nullptr,
                      bool force = false);
  void setAttributeContentOnce (QTreeWidgetItem *item, const QString &path,
                                const QString &value, bool save, bool extended,
                                bool queue);
  void setBusy (bool b = true, int cmdId = PwmdCmdIdInvalid);
  void setConnected (bool = true, Pwmd::ConnectionState = Pwmd::Connected);
  void saveDocument (bool exteneded = false);
  bool doFinished (bool &no, bool ignore = false);
  bool refreshElementTree (bool target = false);
  void refreshElementTreeFinalize (QByteArray &);
  bool editElements ();
  bool hasBadTarget (QTreeWidgetItem *, const QString &, const QString &);
  bool badTarget (const QTreeWidgetItem *);
  bool targetLoop (const QTreeWidgetItem *);
  QTreeWidgetItem *findElement (const QString &, QTreeWidgetItem * = 0,
                                bool = false, PwmdTreeWidget * = nullptr);
  void elementSelected (QTreeWidgetItem *, bool refresh = false,
                        QTreeWidgetItem * = 0);
  void elementSelectedFinalize (QTreeWidgetItem *, const QString &,
                                bool attrs = false);
  void sortAttributes ();
  bool refreshAttributeList (QTreeWidgetItem *, bool force = false,
                             bool queue = false, bool fromTree = false);
  void refreshAttributeListFinalize (QTreeWidgetItem *, const QString &,
                                     bool create = true, bool fromTree = false);
  bool hasTarget (const QTreeWidgetItem *, bool parent = false);
  QString elementName (const QTreeWidgetItem *);
  void setItemText (QTreeWidgetItem *, const QString &, bool hasTarget = false);
  void tabify (QPlainTextEdit *);
  void setOpen (bool = true);
  void popupContextMenu ();
  void resetSelectedItems ();
  bool isParentOf (QTreeWidgetItem *, QTreeWidgetItem *);
  void setModified (bool = true);
  void setModifiedContent (bool b = true);
  void setFileModified (bool = true);
  QString updateSocket ();
  void createNewElement (bool root = false, bool sibling = false);
  bool event (QEvent *);
  QTreeWidgetItem *resolveTargets (QTreeWidgetItem *);
  QTreeWidgetItem *resolveElementPath (QTreeWidgetItem *);
  void setAttribute (QTreeWidgetItem *, const QString &, const QString & = 0,
                     bool update = false);
  void removeAttribute (QTreeWidgetItem *, const QString &,
                        bool update = false);
  bool deleteAttribute (QTreeWidgetItem *, const QString &name, bool queuequeue  = false);
  void deleteAttributeFinalize (QTreeWidgetItem *, const QString &);
  void setMainButtons (MainButtonState);
  void setTimeLabels (QTreeWidgetItem *, bool needsRefresh = false);
  void clearElementTree (QTreeWidget *);
  gpg_error_t doDndCopyElement (const QString & src, const QString & dst,
                                bool queue = false);
  void dndCopyMoveElementFinalize (const QString & = 0);
  void dndCreateTargetFinalize ();
  void updateContentPointer (QTreeWidgetItem *, PwmdElementContent *);
  void editAttributes (bool resolve = false, QTreeWidgetItem * = 0);
  QString toBase64 (QTreeWidgetItem *, QByteArray &);
  bool setNewAttribute (QTreeWidgetItem *, QString, QString = 0, bool fromSlot = false, bool queue = false);
  void setNewAttributeFinalize (AttributeFinalizeT *, bool fromSlot = false);
  void updateSocketOptions ();
  void updateCacheTimeout (const QString &);
  void toggleOtherTabs (bool);
  bool isElementOwner (QTreeWidgetItem *, bool secondary = false);
  bool hasOwnerOfItemSelected ();
  static gpg_error_t badPassphraseCallback (void *, bool);
  void setElementStatusTip (QTreeWidgetItem *);
  QString clientState (unsigned);
  void updateStateForClient (QString);
  void clearClientList (QTreeWidget *);
  void readConfigSettings (QString &filename, QString &sock, bool init = true);
  void closeEvent (QCloseEvent *);
  bool confirmQuit ();
  void setupToolbars ();
  void setupToolBarLabels (bool b);
  void setElementItemsEnabled (QTreeWidgetItem *, bool, QTreeWidgetItem * = 0);
  void setContentItemsEnabled (bool = true, bool = true, QTreeWidgetItem * = 0);
  void setPasswordItemsEnabled (bool = true, QTreeWidgetItem * = 0);
  void setAttributeItemsEnabled (bool = true, QTreeWidgetItem * = 0);
  void setCommandItemsEnabled (bool = true, QTreeWidgetItem * = 0);
  void clearAttributeTab ();
  void setCommandInquireFilename (const QString &);
  bool fixupToolBar (QToolBar *, int, bool enable, bool test = false,
                     QWidget ** = 0);
  bool fixupMenu (QMenu *, int which, bool enable, int checked = -1,
                  bool test = false);
  void fixupMultiToolbar ();
  void ignoreErrors (PwmdCommandQueueItem *, bool = false);
  bool needsUpdate ();
  void cacheContentToggled ();
  void saveAttrContentFinalize (const QString &path, AttributeFinalizeT *);
  void saveContentFinalize (QTreeWidgetItem *, const QString &content,
                            bool password);
  void savePasswordContentFinalize (QTreeWidgetItem *, const QString &type);
  void renameElementFinalize ();
  void newElementFinalize (QString path);
  void deleteElementFinalize (PwmdCommandQueueItem *);
  void invokingUserFinalize (const QString &);
  void currentUserFinalize (const QString &);
  void reloadFinalize ();
  void openFileFinalize (gpg_error_t);
  void openFormFinalize (QByteArray &, const QString &, const QString &);
  void setWindowTitle (bool);
  void updateConnectTimer (bool reset = false, bool cancel = false);
  void updatePasswordAttribute (QTreeWidgetItem *, bool queue = false);
  bool isInvoker ();
  void updateElementExpiry (long);
  void setElementExpireLabel (long);
  void updateTargetAttributes (QTreeWidgetItem *item, const QString &name,
                               const QString &value, bool remove = false);
  bool attributeIsEditable (QTreeWidgetItem *, QListWidgetItem *);
  bool isReservedAttribute (const QString &);
  bool isProtectedAttribute (const QString &);
  int previousAttributeIndex ();
  void updateBusyStatusBar (Pwmd::ConnectionState);
  QTreeWidgetItem *findElementText (const QString &str,
                                    bool elements = true,
                                    bool attrs = false,
                                    bool attrValues = false);
  void findElementCommon (bool previous);
  void setCurrentElement (QTreeWidgetItem *);
  void saveExpandedItemList ();
  void setClipboard (const QString &);
  bool hasQueuedRefreshTree ();
  bool hasMultiSelection ();
  bool hasSingleSelection ();
  QTreeWidgetItem *firstSelectedItem ();

  Pwmd *pwm;
  Pwmd::ConnectionState state;
  QString _filename;
  QString _socket;
  Ui::PwmdMainWindow ui;
  QTreeWidgetItem *attributeElement;
  QTreeWidgetItem *selectedElement;
  QTreeWidgetItem *previousElement;
  QTreeWidgetItem *targetAttributeElement;
  QTreeWidgetItem *copyElement;
  QTreeWidgetItem *oldSelectedItem;
  QColor previousElementColor;
  QColor previousElementColorBg;
  QString selectedElementPath;
  QTreeWidgetItem *dropElement;
  QTreeWidgetItem *newElement;
  QListWidgetItem *selectedAttribute;
  QListWidgetItem *newAttribute;
  QListWidgetItem *newFilename;
  QString selectedFilename;
  unsigned iterationsOrig;
  PwmdRemoteHost currentHostData;
  bool importRoot;
  QWidget *previousRightPage;
  QColor targetColor;
  QColor invalidTargetColor;
  QColor hilightColor;
  QColor targetLoopColor;
  QColor permissionsColor;
  QColor targetColorBg;
  QColor invalidTargetColorBg;
  QColor hilightColorBg;
  QColor targetLoopColorBg;
  QColor permissionsColorBg;
  bool sortElementTree;
  QString clientName;
  int dndOperation;
  int ipVersion;
  QTimer *clipboardTimer;
  QTimer *releaseTimer;
  QClipboard *clipboard;
  unsigned clipboardTimeout;
  bool newFile;
  QStringList connectParameters;
  bool editTargetAttributes;
  unsigned features;		// pwmd_features()
  int pinentry_try;
  MainButtonState mainButtonState;
  QStringList invokingUser;
  QString connectedUser;
  PwmdClientDialog *clientDialog;
  PwmdOptionsDialog *optionsDialog;
  QToolBar *fileToolBar;
  QToolBar *editToolBar;
  QToolBar *elementToolBar;
  QToolBar *contentToolBar;
  QToolBar *passwordToolBar;
  QToolBar *attributeToolBar;
  QToolBar *commandToolBar;
  bool cacheContent;
  int lockTimeout;
  QProgressBar *progressBar;
  PwmdFileOptionsDialog *fileOptionsDialog;
  PwmdOpenDialog *openDialog;
  QPushButton *cancelButton;
  QString previousAttributeText;
  bool isBusy;
  QString commandInquireFilename;
  QAction *findElementAction;
  QWidget *findElementWidget;
  unsigned nSelectedElements;
  bool searchAndReplace;
  PwmdSearchDialog *advancedSearchDialog;
  QLabel *searchResultStatusBarLabel;
#ifdef Q_OS_ANDROID
  bool androidAuthenticating;
#endif

  typedef enum {
      DndNone, DndCopy, DndMove, DndTarget, DndAny
  } DndOperation;
};

class AttributesRetrievedEvent : public QEvent
{
 public:
  AttributesRetrievedEvent (QEvent::Type t) : QEvent (t)
  {
    _item = nullptr;
  };

  void setItem (QTreeWidgetItem *item)
    {
      _item = item;
    };

  QTreeWidgetItem *item ()
    {
      return _item;
    };

 private:
  QTreeWidgetItem *_item;
};

class PwmdPasswordOnlyValidate:public QValidator
{
 Q_OBJECT
 public:
  State validate (QString & in, int &pos) const
  {
    (void) pos;

    for (int i = 0; i < in.length (); i++)
      {
	if (in.count (in.at (i)) > 1)
	  return Invalid;
      }

    return Acceptable;
  };
};

class PwmdCommandHistoryData
{
 public:
  PwmdCommandHistoryData (QString args, bool inquire = false,
                          QString inquireCmd = 0, QString inquireFilename = 0)
    {
      _args = args;
      _inquire = inquire;
      _inquireCmd = inquireCmd;
      _inquireFilename = inquireFilename;
    };
  ~PwmdCommandHistoryData () {};

  bool isInquire ()
    {
      return _inquire;
    };

  const QString &inquireCmd ()
    {
      return _inquireCmd;
    };

  const QString &args ()
    {
      return _args;
    };

  const QString &inquireFilename ()
    {
      return _inquireFilename;
    };

 private:
  bool _inquire;
  QString _args;
  QString _inquireCmd;
  QString _inquireFilename;
};

Q_DECLARE_METATYPE (PwmdCommandHistoryData *);

class SearchResult
{
  public:
    enum {
        None = 0x0,
        Element = 0x1,
        Attr = 0x2,
        AttrValue = 0x4
    } SearchResultType;

    SearchResult (QTreeWidgetItem *e, int type, QList <int>attrs)
      {
        _element = e;
        _attrs = attrs;
        _curAttr = reset ();
        _type = type;
      };
    ~SearchResult () {};

    const QList <int> attrs ()
      {
        return _attrs;
      };

    int resultType ()
      {
        return _type;
      };

    QTreeWidgetItem *element ()
      {
        return _element;
      };

    int nextAttr ()
      {
        int n = _attrs.indexOf (curAttr ());
        if (n < 0 || n+1 >= _attrs.count ())
          {
            _curAttr = -1;
            return curAttr ();
          }

        _curAttr = _attrs.at (n+1);
        return curAttr ();
      };

    int curAttr ()
      {
        return _curAttr;
      };

    int lastAttr ()
      {
        if (_attrs.count () == 0)
          return -1;

        _curAttr = _attrs.last ();
        return curAttr ();
      };

    int prevAttr ()
      {
        int n = _attrs.indexOf (curAttr ());
        if (n < 0 || n-1 < 0)
          {
            _curAttr = -1;
            return curAttr ();
          }

        _curAttr = _attrs.at (n-1);
        return curAttr ();
      };

    int reset ()
      {
        _curAttr = _attrs.count () ? _attrs.at (0) : -1;
        return curAttr ();
      };

  private:
    QTreeWidgetItem *_element;
    QList <int>_attrs;
    int _curAttr;
    int _type;
};

#endif
