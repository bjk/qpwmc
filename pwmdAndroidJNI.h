/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDANDROIDJNI_H
#define PWMDANDROIDJNI_H

#include <QThread>
#include <QJniObject>
#include <QtCore/private/qandroidextras_p.h>
#include <QFileDialog>
#include <QLineEdit>
#include "pwmd.h"
#include "pwmdRemoteHost.h"

class PwmdAndroidJNI;
class PwmdAndroidJNIThread : public QThread
{
 public:
  PwmdAndroidJNIThread (PwmdAndroidJNI *);
  ~PwmdAndroidJNIThread ();
  void run();
  void connectSocket (PwmdRemoteHost &);

 private:
   PwmdRemoteHost hostData;
   PwmdAndroidJNI *jni;
};

class PwmdAndroidJNI : public QObject, QAndroidActivityResultReceiver
{
 Q_OBJECT
 public:
   PwmdAndroidJNI (QObject * = 0);
   ~PwmdAndroidJNI ();
   void setTimeout (int);
   bool connectSocket (PwmdRemoteHost &);
   void cancel ();
   bool authenticate ();
   void init ();
   void selectCertificateAlias (QLineEdit *);
   static bool checkPermissions (QFileDialog::AcceptMode = QFileDialog::AcceptOpen);
   static void showToast (const QString &, int duration = 1);
   static void clearClipboard (int duration = 1);
   static ssize_t read (void *, int, void *, size_t);
   static ssize_t write (void *, int, const void *, size_t);

   enum {
       AuthActivity
   } AndroidActivity;


 signals:
   void jniConnectionResult (PwmdRemoteHost, int fd);
   void jniConnectionError (gpg_error_t);
   void jniAuthenticated ();

 private:
   friend class PwmdAndroidJNIThread;
   void handleActivityResult (int code, int result, const QJniObject &);
   int exceptionResult (int, bool = false);

   PwmdAndroidJNIThread jniThread;
   QJniObject androidSocket;
   bool cancelling;
};

#endif
