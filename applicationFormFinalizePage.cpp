/*
    Copyright (C) 2013-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QTimer>
#include <QCalendarWidget>
#include <QDateTime>
#include "pwmdMainWindow.h"
#include "applicationFormFinalizePage.h"
#include "applicationForm.h"
#include "applicationFormXmlStream.h"
#include "applicationFormDateSelector.h"
#include "applicationFormWidget.h"
#include "cmdIds.h"

static bool overwriteElementContent;

ApplicationFormFinalizePage::ApplicationFormFinalizePage (QWidget *p)
  : QWizardPage (p)
{
  ui.setupUi (this);
  pwm = nullptr;
  newFile = false;
  filename = QString ();
  ownHandle = true;
  saving = false;
}

void
ApplicationFormFinalizePage::slotBusy (int cmdId, bool b)
{
  (void)cmdId;

  if (b)
    setCursor (Qt::WaitCursor);
  else
    unsetCursor ();
}

void
ApplicationFormFinalizePage::slotKnownHostCallback (void *data,
                                                    const char *host,
                                                    const char *key, size_t len)
{
  gpg_error_t rc = Pwmd::knownHostPrompt (data, host, key, len);
  ApplicationForm *w = qobject_cast<ApplicationForm *> (wizard ());
  w->setHasError (rc != 0);
  emit knownHostRc (rc);
}

void
ApplicationFormFinalizePage::commit ()
{
  ApplicationForm *form = static_cast<ApplicationForm *>(wizard ());
  bool notFound = false;

  cleanup ();
  form->setHasError (false);
  saving = false;
  QAbstractButton *pb = wizard ()->button (QWizard::FinishButton);
  pb->setEnabled (false);

  foreach (ApplicationFormWidget *w, form->stream->widgets ())
    {
      QString path = QString ();

      if (w->id () == "pwmdRootElement")
        continue;

      if (w->type () == ApplicationFormWidget::AppWidgetInteralOverwriteCheckBox)
        {
          QCheckBox *cb = qobject_cast<QCheckBox *> (w->widget ());
          overwriteElementContent = cb->isChecked ();
          continue;
        }

      if (!w->childOf ().isEmpty ())
        {
          path = form->buildElementPath (path, w->childOf (), notFound);
          if (notFound)
            break;
        }

       if (!w->pwmdName ().isEmpty ())
         {
           if (!path.isEmpty ())
             path += "\t";

           path += w->pwmdName ();
           path.append ("\t");
           if (!w->dateSelector () && !w->value ().isEmpty ())
             path.append (w->value ());
         }
       else if (!path.isEmpty ())
         path.append ("\t");

       if (w->type () == ApplicationFormWidget::AppWidgetRadioButton
           && w->pwmdName ().isEmpty ())
         path += w->value() + '\t';

       if (w->dateSelector () && !w->value ().isEmpty ())
         {
           ApplicationFormDateSelector *d = qobject_cast<ApplicationFormDateSelector *> (w->widget ());
           path += d->date ().toString (w->value ());
           if (!w->value ().endsWith ("<TAB>"))
             path += "<TAB>";
         }

       path = path.replace ("<TAB>", "\t");
       if (path.isEmpty ())
         continue;

       ApplicationElement *ae = new ApplicationElement (path, w->isHidden ());
       ae->setAttrs (w->attrs ());
       elements.append (ae);
    }

  /* Remove paths that begin with elements from other paths to prevent
     redundant element creation. */
  foreach (ApplicationElement *e, elements)
    {
      foreach (ApplicationElement *x, elements)
        {
          if (e->path ().startsWith (x->path ()) && x != e)
            {
              elements.removeAt (elements.indexOf (x));
              delete x;
              break;
            }
        }
    }

  ui.progressBar->setMaximum (elements.count());

  QString filename = form->buildElementPath (QString (), "pwmdFilename", notFound);
  if (!notFound)
    filename = form->filename ();

  QString sock = form->buildElementPath (QString (), "pwmdSocket", notFound);
  if (!notFound)
    sock = form->socket ();

  qRegisterMetaType <Pwmd::ConnectionState>("Pwmd::ConnectionState");
  qRegisterMetaType <gpg_error_t>("gpg_error_t");

  if (!pwm)
    {
      ownHandle = true;
      pwm = new Pwmd (filename, "qpwmc", sock, this);
      pwm->setLockOnOpen (true);
      connect (pwm, SIGNAL (stateChanged (Pwmd::ConnectionState)),
	       this, SLOT (slotConnectionStateChanged (Pwmd::ConnectionState)));
      connect (pwm, SIGNAL (statusMessage (QString, void *)), this,
	       SLOT (slotStatusMessage (QString, void *)));
      connect (pwm, SIGNAL (knownHost (void *, const char *, const char *, size_t)), this, SLOT (slotKnownHostCallback (void *, const char *, const char *, size_t)));
    }

  if (ownHandle)
    {
      QString oldSocket = pwm->socket ();
      PwmdRemoteHost oldHostData;

      PwmdRemoteHost::fillRemoteHost (oldSocket, oldHostData);

      pwm->setData (sock.toUtf8 ().data ());
      if (PwmdRemoteHost::fillRemoteHost (sock, hostData))
        {
          pwm->setSocket (PwmdRemoteHost::socketUrl (hostData));
          pwm->setConnectParameters (hostData.socketArgs ());
          pwmd_setopt (pwm->handle (), PWMD_OPTION_SSH_AGENT,
                       hostData.sshAgent ());
          pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
                       hostData.socketTimeout ());
          pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_VERIFY,
                       hostData.tlsVerify ());
          if (!hostData.tlsPriority().isEmpty ())
            pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_PRIORITY,
                         hostData.tlsPriority ().toUtf8 ().data ());
        }
      else
        pwm->setSocket (sock);

      if (oldSocket != pwm->socket () || hostData != oldHostData)
        pwm->reset ();
    }

  disconnect (pwm, SIGNAL (busy (int, bool)), this, SLOT (slotBusy (int, bool)));
  connect (pwm, SIGNAL (busy (int, bool)), this, SLOT (slotBusy (int, bool)));
  disconnect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));
  connect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));

  if (notFound)
    return;

  if (!ownHandle)
    {
      openFileFinalize ();
      return;
    }

  pwm->setFilename (filename);
  if (pwm->state () == Pwmd::Init)
    pwm->connect (0, 0);
  else
    {
      PwmdInquireData *inq = new PwmdInquireData (pwm->handle (), pwm->filename ());
      pwm->open (Pwmd::inquireCallback, inq);
    }
}

void
ApplicationFormFinalizePage::attrStoreFinalize (ApplicationElement *e,
                                                gpg_error_t &rc, bool isFinal)
{
  (void)rc;
  if (isFinal)
    connectionProgress (elements.indexOf (e));
}

bool
ApplicationFormFinalizePage::getContentFinalize (ApplicationElement *e,
                                                 gpg_error_t &rc, bool queued,
                                                 bool &ntd)
{
  bool tab = e->path ().endsWith ("\t");
  QStringList l = e->path ().split ("\t");

  if (tab || e->isHidden ())
    l.removeLast ();

  if (rc && queued)
    return false;

  QString s = QString (tr ("Creating path: %1")).arg (l.join ("->"));
  message (s);

  switch (gpg_err_code (rc))
    {
    case 0:
    case GPG_ERR_NO_DATA:
      if (!overwriteElementContent)
        {
          message (tr ("Not overwriting existing content!"));
          rc = 0;
          ntd = true;
          return true;
        }

      message (tr ("Overwriting existing content!"));
      rc = 0;
      break;
    case GPG_ERR_ELEMENT_NOT_FOUND:
      rc = 0;
      break;
    default:
      break;
    }

  if (rc)
    {
      if (!queued)
        message (QString ("ERR %1: %2").arg (rc).arg (gpg_strerror (rc)));

      ApplicationForm *w = qobject_cast<ApplicationForm *> (wizard ());
      w->setHasError ();
      connectionProgress (elements.indexOf (e));
      return false;
    }

  PwmdInquireData *inq = new PwmdInquireData (e->path ());
  inq->setUserPtr (e);
  inq->setUserBool (!e->attrs ().count ()); // final command for element
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdFormStoreContent, "STORE",
                                          Pwmd::inquireCallback, inq), true);

  for (int i = 0; i < e->attrs ().count (); i++)
    {
      ApplicationFormAttr *a = e->attrs ().at (i);
      QString args = QString ("SET %1 %2").arg (a->name (), e->pathNoContent ());

      if (!a->value ().isEmpty ())
        args.append (QString (" %1").arg (a->value ()));

      inq = new PwmdInquireData (args);
      inq->setUserPtr (e);
      pwm->command (new PwmdCommandQueueItem (PwmdCmdIdFormAttrNew, "ATTR",
                                              Pwmd::inquireCallback, inq), true);
      if (i+1 == e->attrs ().count ())
        inq->setUserBool (true); // final command for element
    }

  return true;
}

void
ApplicationFormFinalizePage::openFileFinalize ()
{
  for (int i = 0; i < elements.count (); i++)
    {
      ApplicationElement *e = elements.at (i);

      PwmdInquireData *inq = new PwmdInquireData (e->pathNoContent ());
      inq->setUserPtr (e);
      PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdFormGetContent,
                                                             "GET",
                                                             Pwmd::inquireCallback,
                                                             inq);
      item->addError (GPG_ERR_SOURCE_USER_1, GPG_ERR_ELEMENT_NOT_FOUND);
      pwm->command (item, true);
    }

  pwm->runQueue ();
}

void
ApplicationFormFinalizePage::cleanup ()
{
  while (elements.count ())
    {
      ApplicationElement *e = elements.takeFirst ();
      delete e;
    }

  elements.clear ();
}

ApplicationFormFinalizePage::~ApplicationFormFinalizePage ()
{
  cleanup ();
}

void
ApplicationFormFinalizePage::slotStatusMessage (QString s, void *data)
{
  (void)data;
  if (s == "NEWFILE")
    newFile = true;
}

void
ApplicationFormFinalizePage::slotConnectionStateChanged (Pwmd::ConnectionState s)
{
  switch (s)
    {
    case Pwmd::Init:
      pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
		   hostData.connectTimeout ());
      pwmd_setopt (pwm->handle (), PWMD_OPTION_SSH_AGENT,
		   hostData.sshAgent ());
      pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_VERIFY,
		   hostData.tlsVerify ());
      if (!hostData.tlsPriority().isEmpty ())
        pwmd_setopt (pwm->handle (), PWMD_OPTION_TLS_PRIORITY,
                     hostData.tlsPriority ().toUtf8 ().data ());
      pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
		   hostData.connectTimeout ());
      break;
    case Pwmd::Connecting:
      message (QString (tr ("Connecting to %1...")).arg (pwm->socket ()));
      break;
    case Pwmd::Connected:
      message (QString (tr ("Opening data file %1...")).arg (filename));
      pwmd_setopt (pwm->handle (), PWMD_OPTION_SOCKET_TIMEOUT,
		     hostData.socketTimeout ());
        {
          PwmdInquireData *inq = new PwmdInquireData (pwm->handle (), pwm->filename ());
          pwm->open (Pwmd::inquireCallback, inq);
        }
      break;
    default:
      break;
    }
}

void
ApplicationFormFinalizePage::connectionProgress (int i)
{
  ui.progressBar->setValue (i+1);
  
  if (!saving && ui.progressBar->value () == ui.progressBar->maximum())
    {
      saving = true;
      save ();
    }
}

void
ApplicationFormFinalizePage::message (const QString &s)
{
  QString str = ui.textEdit->document ()->toPlainText ();

  ui.textEdit->document ()->setPlainText (QString (str + s + "\n").replace ("\t", "<TAB>"));
  ui.textEdit->moveCursor (QTextCursor::Down);
}

void
ApplicationFormFinalizePage::slotCommandResult (PwmdCommandQueueItem *item,
                                                QString result, gpg_error_t rc,
                                                bool queued)
{
  (void)result;
  PwmdInquireData *inq = item->data ();
  ApplicationForm *w = qobject_cast<ApplicationForm *> (wizard ());

  switch (item->id ())
    {
    case PwmdCmdIdInternalPassword:
      if (rc)
        w->setHasError ();
      item->setSeen ();
      return;
    case PwmdCmdIdInternalStatusError:
      if (ownHandle) // our own pwm handle (command-line form)
        message (QString (tr ("Connection error: %1")).arg (gpg_strerror (rc)));
      break;
    case PwmdCmdIdInternalConnect:
      if (rc)
        {
          if (gpg_err_code (rc) == GPG_ERR_ETIMEDOUT)
            pwm->reset ();
          message (QString ("%1: %2").arg (tr ("Failed to connect"), gpg_strerror (rc)));
        }
      break;
    case PwmdCmdIdInternalOpen:
      if (!rc)
        openFileFinalize ();
      else if (!queued)
        message (gpg_strerror (rc));
      break;
    case PwmdCmdIdInternalSave:
      if (rc && !queued)
        message (QString ("%1: %2").arg (tr ("Failed to save:"), gpg_strerror (rc)));
      else if (!rc)
        {
          message (tr ("Save succeeded."));
          QAbstractButton *pb = wizard ()->button (QWizard::FinishButton);
          pb->setEnabled (true);
        }
      break;
    case PwmdCmdIdFormGetContent:
        {
          bool ntd = false;

          if (getContentFinalize (static_cast <ApplicationElement *>
                                  (inq->userPtr()), rc, queued, ntd)
              && item->isFinalItem () && ntd)
            {
              message (tr ("Nothing to do."));
              QAbstractButton *pb = wizard ()->button (QWizard::FinishButton);
              pb->setEnabled (true);
            }
        }
      pwm->runQueue ();
      break;
    case PwmdCmdIdFormStoreContent:
      if (!rc)
        w->setModified ();

      attrStoreFinalize (static_cast <ApplicationElement *> (inq->userPtr()), rc, inq->userBool ());
      break;
    case PwmdCmdIdFormAttrNew:
      if (!rc)
        w->setModified ();

      attrStoreFinalize (static_cast <ApplicationElement *> (inq->userPtr()), rc, inq->userBool ());
    default:
      break;
    }

  if (rc && !item->checkError (rc) && !queued)
    w->setHasError ();

  item->setSeen ();
}

void
ApplicationFormFinalizePage::save ()
{
  if (ownHandle)
    {
      message (tr ("Saving ..."));
      PwmdSaveDialog *d = new PwmdSaveDialog (pwm, newFile);
      PwmdSaveWidget *w = d->widget ();

      if (newFile)
        {
          if (!d->exec ())
            message (tr ("Cancelled."));
        }
      else
        w->save (0, true);

      delete d;
    }
  else
    {
      ApplicationForm *w = qobject_cast<ApplicationForm *> (wizard ());
      message (w->hasError () ? tr ("Failed.") : tr ("Done!"));
      QAbstractButton *pb = wizard ()->button (QWizard::FinishButton);
      pb->setEnabled (true);
    }
}

void
ApplicationFormFinalizePage::setHandle (Pwmd *h)
{
  pwm = h;
  ownHandle = false;
}
